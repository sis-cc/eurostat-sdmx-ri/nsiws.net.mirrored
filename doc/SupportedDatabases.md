# Supported Databases

## Overview

While the application tries to be compatible with any recent version of the supported databases, there is no guarantee that versions that have not been tested will work.
Sometimes newer versions of database servers require new versions of the database driver, while newer versions of database drivers could drop support for older versions of databases.

## Oracle

The application is known to work with Oracle versions 19c and 21c express.

## Microsoft SQL Server

The application is known to work with Microsoft SQL Server 2016 and 2017 (Linux).
The database must have compatibility level of at least `130`.
More information can be found at [Microsoft](https://learn.microsoft.com/en-us/sql/relational-databases/databases/view-or-change-the-compatibility-level-of-a-database?view=sql-server-2016)

For example if the mapping store database is named `MASTORE` then as SQL Server administrator run:

```sql
ALTER DATABASE MASTORE SET COMPATIBILITY_LEVEL = 130
```

## MariaDB/MySQL

The application is known to work with MariaDB 10.11. Note on Linux installations the `lower_case_table_names` must be set to `1`.
See [MariaDB documentation](https://mariadb.com/docs/reference/mdb/system-variables/lower_case_table_names/).

