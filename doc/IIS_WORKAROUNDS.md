# IIS related workarounds

This document contains a list of issues and possible workarounds when running the Web Service under IIS.

## Issue 1: Getting 405 Method not allowed

This is more likely caused by the IIS module `WebDAVModule` that is intercepting the calls to the Web Service.

### Solution: Removing WebDAVModule

`WebDAVModule` can be removed either IIS wide using the `Turn Windows features on or off` screen or at Web Service level by modifying the `Web.Config` file.

![WindowsIISFeature](WebDAVModule.png)

If the WebDAVModule is installed the following lines in the web.config file need to be uncommented.

```xml
<!--
<modules runAllManagedModulesForAllRequests="false">
  <remove name="WebDAVModule" />
</modules>
-->
```

If the WebDAVModule is not installed the above lines in the web.config file need to remain commented.

## Issue 2: HTTP Error 400. The request URL is invalid

This could be occur in REST data & available constraint queries with a big number of criteria in key part.

### Solution: Modify the Windows Registry

The following steps need to be executed

1. Add a property called “UrlSegmentMaxLength” into “HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\services\HTTP\Parameters” with value 1024
   e.g.

```.reg
[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\services\HTTP\Parameters]

"UrlSegmentMaxLength"=dword:00000400
```

1. Reboot (restarting IIS wont help)
