# Common Web Service instructions

## Installation procedure (IIS)

The installation procedure consists of:

1. Copying the files to a specific folder
1. Creating the application in IIS that points to the above folder
1. Modifying Mapping Store database Connection settings, see [CONNECTION_STRING](CONNECTION_STRING.md) for details
1. Optionally modifying other configuration, see [CONFIGURATION](CONFIGURATION.md) for details
1. Optionally modifying the Logging configuration, see [CONFIGURATION](CONFIGURATION.md#logging-configuration) for details

*Note* Unlike previous versions there is no need for installing any drivers for the 3 supported databases. The Web Service already included drivers for Oracle, MariaDB/MySQL and SqlServer.

### Short instructions deploy to IIS

Copy the contents of the app folder into a folder configured as a IIS application and uses a No Managed application pool.
For more information creating a Web application on IIS please visit IIS documentation. For [IIS7](https://technet.microsoft.com/en-us/library/cc772042(v=ws.10).aspx)

*Note:* In order to allow access to the Web Service, please make sure the ports IIS is running are open in your firewall allowing both inbound and outbound traffic. The default ports are 80 for HTTP and 443 for HTTPS.

### IIS installation (if required)

A guide for setting up IIS for ASP.NET Core application can be found at [Microsoft documentation](https://docs.microsoft.com/en-us/aspnet/core/host-and-deploy/iis/?view=aspnetcore-6.0#iis-configuration)

### NET Core Hosting Bundle 6.0 installation

The installation and registration of .NET 6.0 with IIS must be verified.

Download and Install the .NET Core Hosting Bundle (required only once), [Microsoft documentation](https://docs.microsoft.com/en-us/aspnet/core/host-and-deploy/iis/?view=aspnetcore-6.0#install-the-net-core-hosting-bundle)

After the success message is displayed the Internet Information Service (IIS) requires a reset to finalise the registration of the framework: type “iisreset” in command prompt.

### Copying the files

Copy the contents of the app directory from the release package to the folder that you will use to hold the content (such as `C:\Inetpub\wwwroot\nsiws`).
_Please note that the contents should not be copied to the root IIS folder, usually `wwwroot`, as it is a special folder for ASP.NET._

### Creating the Application on IIS 7.x

In order to install the NSI Web Service, an Application should be created on the IIS:

1. Open IIS Manager and navigate to the level you want to manage. For information about opening IIS Manager, see [Open IIS Manager (IIS 7)](http://technet.microsoft.com/en-us/library/cc770472%28v=ws.10%29.aspx)
1. Expand the server name.
1. In the left pane, right-click Default Web Site, point to Add Application.
1. In the first text box, type an alias, or name, for the virtual directory (such as NSIWS)
1. In the second text box, click Browse. Locate the content folder that will be used to hold the content (such as C:\Inetpub\wwwroot\nsiws)
1. Make sure that the Application Pool .NET CLR is `No Managed Code` - integrated mode.
1. Click ok to finish.

To perform the same task in Power shell command line please refer to the [`ConvertTo-WebApplication` powershell command](https://docs.microsoft.com/en-us/powershell/module/webadministration/convertto-webapplication?view=win10-ps)

### IIS issues and workarounds

Please refer to [IIS issues and workarounds](IIS_WORKAROUNDS.md)

## HTTP Compression

How to enable for IIS 7.x

1. Open IIS Manager and navigate to the level you want to manage. For information about opening IIS Manager, see [Open IIS Manager (IIS 7)](http://technet.microsoft.com/en-us/library/cc770472%28v=ws.10%29.aspx)
1. In Features View, double-click Compression
1. On the Compression page, select the box next to Enable dynamic content compression.
1. On the same tab please
1. Click Apply in the Actions pane

For more details please check [Microsoft Website](http://technet.microsoft.com/en-us/library/cc753681%28v=ws.10%29.aspx)

In some cases the SDMX mime-types (application/vnd.sdmx. etc) are not compressed by IIS by default.
To enable them the following steps are needed:

1. Open IIS Manager and navigate to the level you want to manage. For information about opening IIS Manager, see [Open IIS Manager (IIS 7)](http://technet.microsoft.com/en-us/library/cc770472%28v=ws.10%29.aspx)
1. In Features View, double-click Configuration.
1. At Section select system.webServer/httpCompression.
1. At dynamicTypes click at the Edit items.
1. To add a new mime type click at Add and then set Enabled to True and enter at the mimeType a mime type. This step needs to be repeated for each of the supported types. The following mime types are supported by the distribute formats:

| mime type                                      |
|------------------------------------------------|
| application/xml                                |
| application/json                               |
| application/vnd.sdmx.structure+xml             |
| application/vnd.sdmx.genericdata+xml           |
| application/vnd.sdmx.structurespecificdata+xml |
| application/vnd.sdmx.compactdata+xml           |
| application/vnd.sdmx.crosssectionaldata+xml    |
| application/vnd.sdmx.structure+json            |
| application/vnd.sdmx.data+json            |
| application/vnd.sdmx.data+csv            |
| application/vnd.sdmx.edidata                   |

And any other output plugins may provide different mime-types

## Installation procedure (Kestrel)

[Kestrel](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/servers/kestrel?view=aspnetcore-2.2) is an alternative to IIS. It doesn't require a service like IIS to be running and it runs both on Linux and Windows.It is included by default.

The installation procedure consists of:

1. Install [.NET Core 2.2](https://dotnet.microsoft.com/download/dotnet-core/2.2)
1. Modifying Mapping Store database Connection settings, see [CONNECTION_STRING](CONNECTION_STRING.md) for details
1. Optionally modifying other configuration, see [CONFIGURATION](CONFIGURATION.md) for details
1. Optionally modifying the Logging configuration, see [CONFIGURATION](CONFIGURATION.md#logging-configuration) for details

## Running under Kestrel

*Note* this requires ASP.NET Core 2.2

1. Go in the `app` folder
1. Run the following command:

   ```shell
   dotnet NSIWebServiceCore.dll
   ```
