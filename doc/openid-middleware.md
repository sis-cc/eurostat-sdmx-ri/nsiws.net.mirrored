# OpenID connect Middleware for NSIWS

> Please note that currently middleware implements only authentication, user authorization logic will be supported later.

## Intro

Middleware can be used in scenarios where user is authenticated against 3-d party identity service, responsible for issue a [JWT](https://jwt.io/introduction) token which later is used with every request to NSIWS for user impersonation and enforcing local authorization rules.

Middleware requires HTTPS connection to keep token secure.

OpenId-connect middleware expects a [JWT](https://jwt.io/introduction) token sent as a [Bearer header](https://en.wikipedia.org/wiki/JSON_Web_Token#Use). Middleware validates that token is issued by expected Authority for expected Client and not expired.

In use-case where HTTP request has no token or it is not valid **401 Unauthorized** http code is returned

## Setup

To enable OpenID middleware set **OpenIdMiddlewareBuilder** under `<appSettings>` in `middlewareImplementation` of the  [main configuration file](CONFIGURATION.md#main-configuration-file)

Example:

```xml
<add key="middlewareImplementation" value="OpenIdMiddlewareBuilder"/>
```

Please see the [PLUGINS](PLUGINS.md) for more information regarding middleware configuration in NSIWS

## Configuration

add auth.json file to a config directory with following contents:

```json
{
    "auth": {
        "enabled":true,
        "allowAnonymous": false,
        "authority": "AUTHORITY URL",
        "clientId": "VALUE",
        "requireHttps": true,
        "validateIssuer": true,
        "showPii": false
    }
}
```

## Configuration settings

| Setting    | Description |
|------------|-------------|
| enabled | Is openid authentication enabled|
| allowAnonymous | Is anonymous access allowed (request without JWT token)|
| authority | Authority url of token issuer |
| clientId | Client/application Id |
| requireHttps | Is HTTPS connection to OpenId authority server required |
| validateIssuer | Is iss (issuer) claim in JTW token should match configured authority |
| showPii | If set to TRUE outputs addition debug information in case of invalid token |
| audience | Expected `aud` claim during token validation, if not set equals to `clientId` |