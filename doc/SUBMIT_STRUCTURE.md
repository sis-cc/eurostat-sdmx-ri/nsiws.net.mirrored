# Submit Structural metadata

The SDMX RI Web Service supports submitting structural metadata via SOAP with SDMX v2.1 Registry Interface SubmitStructure messages and via REST with SDMX v2.0/v2.1 Structure messages.

## Action Behavior

The following table contains the behavior of the NSI WS. There are some exceptions which are listed in the next section.
SDMX RR stands for SDMX Registry Request SOAP message.

| SDMX RR Action   | REST Action | Exists | Is Final | Is used | Dependencies exist or provided | Result                                              |
|------------------|-------------|--------|----------|---------|--------------------------------|-----------------------------------------------------|
| Append/Replace   | POST/PUT    | No     | -        | -       | Yes                            | Success - insert artefact                           |
| Append/Replace   | POST/PUT    | No     | -        | -       | No                             | No Results - include missing references in response |
| Append/Replace   | POST/PUT    | Yes    | No       | No      | Yes                            | Success - Delete and Insert artefact                |
| Append/Replace   | POST/PUT    | Yes    | No       | Yes     | *                              | Conflict                                            |
| Append/Replace   | POST/PUT    | Yes    | No       | No      | No (Corner case)               | No Results - include missing references in response |
| Append/Replace   | POST/PUT    | Yes    | Yes      | *       | *                              | Success - Update non final attributes; else Forbidden |
| Delete           | DELETE      | No     | *        | *       | *                              | No Results                                          |
| Delete           | DELETE      | Yes    | No       | No      | *                              | Success                                             |
| Delete           | DELETE      | Yes    | Yes      | No      | *                              | Success                                             |
| Delete           | DELETE      | Yes    | *        | Yes     | *                              | Conflict                                            |

## Error messages

Currently the SDMX guidelines provide SDMX codes and the corresponding HTTP Status Codes, see the corresponding [GitHub page](https://github.com/sdmx-twg/sdmx-rest/blob/v1.2.0/v2_1/ws/rest/docs/4_7_errors.md)

None of the codes can describe the following:

    The artefact cannot be deleted because it used by other artefacts or non SDMX application specific entities.
    The artefact cannot be append/replaced because it already exists.

One proposed solution would be to add a new client side SDMX code for resource conflicts, e.g. 160.The corresponding HTTP status code would be the existing [HTTP Status code 409 Conflict](https://httpstatuses.com/409).

| Error                                              | SDMX Error                | HTTP Status Code          | Notes                     |
|----------------------------------------------------|---------------------------|---------------------------|---------------------------|
| Delete artefact that doesn't exist       | 100 No results found      | 404 Not found             |                           |
| Delete artefact that is referenced                 | 160 Conflict              | 409 Conflict              | Code not in SDMX standard |
| Replacing non final artefact that is referenced    | 160 Conflict              | 409 Conflict              | Code not in SDMX standard |
| Authorization errors                               | 110 Unauthorized          | 401 Unauthorized          |                           |
| Unpredictable errors                               | 500 Internal Server error | 500 Internal Server error | Bugs, DB related errors   |
| Request valid but no updates made                  | 150 Semantic Error        | 403 Forbidden             |                           |
| A combination of success/error results             | Included in each message  | 207 MultiStatus           |                           |
| PUT payload contains multiple structures           | 160 Conflict              | 409 Conflict              |                           |
| PUT payload doesn't match the resource identification | N/A                    | 301 Moved Permanently     |                           |

## REST

The methods and resources used in SDMX RI WS are the following. The resource paths are relevant to the SDMX RI REST base path.

| HTTP Method | Resource    | Description                                                                            |
|-------------|-------------|----------------------------------------------------------------------------------------|
| POST        | /structure                  | Append new artefacts, replace existing artefacts                                                 |
| PUT         | /{structureType}/{agencyId}/{maintainableId}/{version}     | Final artefacts replace non-final information, Non-Final artefacts replace completely. If an artefact doesn't exist is added  |
| DELETE      | /{structureType}/{agencyId}/{maintainableId}/{version}           | Final artefacts replace non-final information, Non-Final artefacts replace completely  |

Below is an example request and error response.

The request to delete a DSD:

```http
DELETE http://nsiwsurl/datastructure/TEST_AGENCY/STS/2.0 HTTP/1.1
```

```http
HTTP/1.1 409 CONFLICT

Content-type: application/xml
```

The error response:

```xml
<?xml version="1.0" encoding="utf-8"?>
<message:Error xmlns:common="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common" xmlns:message="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message">
    <message:ErrorMessage code="160">
        <!--Conflict-->
        <common:Text xml:lang="en">urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=TEST_AGENCY:SSTSCONS_PROD_A(2.0)</common:Text>
    </message:ErrorMessage>
    <message:ErrorMessage code="160">
        <!--Conflict-->
        <common:Text xml:lang="en">Failure:  Action Delete for Data Structure Definition TEST_AGENCY:STS (v2.0) cannot be performed. REASON: Cannot replace artefact. Non-Final artefact is used by other artefacts</common:Text>
    </message:ErrorMessage>
</message:Error>
```

### Multi Status

Since a structure submission may contain multiple artefact it is possible to have different results per artefact. For example when submitting a structure file containing a DSD with it's dependencies and one or more `Codelist` already exist.
When multiple structural metadata is submitted and/or deleted, and only for some artefacts there was a failure while other actions were successful.

The HTTP Code will be 207 Multi-Status. Only the failures will be recorded. If an artefact is not referenced then it didn't fail.

In general multi-status will appear only if one of the following conditions are met:

1. Two or more errors with different error codes.
1. For one request, at least one operation succeeded but at least one other operation failed.

## SOAP

For SOAP SDMX v2.1 `RegistryInterface`/`SubmitStructureRequests` requests have to be used.

Below is an example request and response.

The request:

```xml
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:reg="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/webservices/registry" xmlns:mes="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message" xmlns:com="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common" xmlns:reg1="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry" xmlns:str="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure" xmlns:foot="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message/footer">
   <soapenv:Header/>
   <soapenv:Body>
      <reg:RegistryInterfaceRequest>
        <RegistryInterface xsi:schemaLocation="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message SDMXMessage.xsd" xmlns="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message" xmlns:reg="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry" xmlns:foot="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message/footer" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            <Header>
               <ID>RI_2_1</ID>
               <Test>true</Test>
               <Prepared>2009-03-23</Prepared>
               <Sender id="ESTAT"/>
               <Receiver id="ECB"/>
            </Header>
            <SubmitStructureRequest action="Append" externalDependencies="false">
               <Structures xmlns="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure">
                  <Codelists>
                     <Codelist id="CL_FREQ" urn="urn:sdmx:org.sdmx.infomodel.codelist.Codelist=TEST_AGENCY:CL_FREQ(1.11)" agencyID="TEST_AGENCY" version="1.11" isFinal="true">
                        <Name xml:lang="it" xmlns="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common">Periodicità</Name>
                        <Name xml:lang="en" xmlns="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common">Frequency</Name>
                        <Code id="A" urn="urn:sdmx:org.sdmx.infomodel.codelist.Code=TEST_AGENCY:CL_FREQ(1.11).A">
                           <Name xml:lang="it" xmlns="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common">annuale</Name>
                           <Name xml:lang="en" xmlns="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common">annual</Name>
                        </Code>
                        <Code id="M" urn="urn:sdmx:org.sdmx.infomodel.codelist.Code=TEST_AGENCY:CL_FREQ(1.11).M">
                           <Name xml:lang="it" xmlns="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common">mensile</Name>
                           <Name xml:lang="en" xmlns="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common">monthly</Name>
                        </Code>
                        <Code id="Q" urn="urn:sdmx:org.sdmx.infomodel.codelist.Code=TEST_AGENCY:CL_FREQ(1.11).Q">
                           <Name xml:lang="it" xmlns="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common">trimestrale</Name>
                           <Name xml:lang="en" xmlns="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common">quarterly</Name>
                        </Code>
                        <Code id="W" urn="urn:sdmx:org.sdmx.infomodel.codelist.Code=TEST_AGENCY:CL_FREQ(1.11).W">
                           <Name xml:lang="it" xmlns="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common">settimanale</Name>
                           <Name xml:lang="en" xmlns="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common">weekly</Name>
                        </Code>
                     </Codelist>
                  </Codelists>
               </Structures>
            </SubmitStructureRequest>
         </RegistryInterface>
      </reg:RegistryInterfaceRequest>
   </soapenv:Body>
</soapenv:Envelope>
```

The response:

```xml
<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
   <s:Body>
      <web:RegistryInterfaceResponse xmlns:web="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/webservices/registry">
         <message:RegistryInterface xmlns:message="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message" xmlns:common="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common" xmlns:registry="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry" xmlns:structure="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure">
            <message:Header>
               <message:ID>IREF9e17d1c602b74796943832232cf8da7c</message:ID>
               <message:Test>true</message:Test>
               <message:Prepared>2017-12-04T11:00:59</message:Prepared>
               <message:Sender id="SOME_NSI"/>
               <message:Receiver id="ESTAT"/>
            </message:Header>
            <message:SubmitStructureResponse>
               <registry:SubmissionResult>
                  <registry:SubmittedStructure action="Append">
                     <registry:MaintainableObject>
                        <URN>urn:sdmx:org.sdmx.infomodel.codelist.Codelist=TEST_AGENCY:CL_FREQ(1.11)</URN>
                     </registry:MaintainableObject>
                  </registry:SubmittedStructure>
                  <registry:StatusMessage status="Failure">
                     <registry:MessageText code="150">
                        <!--Semantic Error-->
                        <common:Text xml:lang="en">Failure: Codelist TEST_AGENCY:CL_FREQ (v1.11) cannot be inserted. REASON: Cannot add artefact, already exists</common:Text>
                     </registry:MessageText>
                  </registry:StatusMessage>
               </registry:SubmissionResult>
            </message:SubmitStructureResponse>
         </message:RegistryInterface>
      </web:RegistryInterfaceResponse>
   </s:Body>
</s:Envelope>
```

## Special Behavior

Due to specific needs in SDMX RI the following behaviors exist:

1. Deleting an artefact, will delete also `Categorisations` that categorize this artefact
1. Deleting a Category Scheme, will delete also `Categorisations` that relate to its categories
1. Dataflow cannot be deleted if it has a Mapping Set
1. Replacing a Category Scheme will add/remove categories.

Special handling for SDMX RI/Mapping Assistant (Normally in SDMX Informational means no action shall be taken). This is not accessible neither via REST or SOAP.

| Requested Action | REST Action | Exists | Is Final | Is used | Dependencies exist or provided | Result                                              |
|------------------|-------------|--------|----------|---------|--------------------------------|-----------------------------------------------------|
| Information      | N/A         | No     | -        | -       | Yes                            | Success - insert artefact                           |
| Information      | N/A         | Yes    | No       | No      | Yes                            | Success - Delete and Insert artefact                |
| Information      | N/A         | Yes    | Yes      | *       | *                              | Success - Update non final attributes               |
| Information      | N/A         | No     | -        | -       | No                             | No Results - include missing references in response |
| Information      | N/A         | Yes    | No       | Yes     | Yes                            | Conflict                                            |