# Un-installation procedure

To remove completely the SRI Web Service, it needs first to be removed form IIS and then physically from the windows file system.

## Remove from IIS 7.x

_This does not apply to IIS 7.5 Express edition. Please refer to IIS 7.5 Express instructions on how to remove an application by editing its configuration file._

1. Open IIS Manager and navigate to the level you want to manage. For information about opening IIS Manager, see [Open IIS Manager (IIS 7)](http://technet.microsoft.com/en-us/library/cc770472%28v=ws.10%29.aspx)
1. Expand the server name.
1. Expand the `Sites`, `Default Web Site`
1. In the left pane, right-click at the application (such as NSIWS) and then click Remove.
1. Click yes at the dialog presented by the tool

_*Note:* Removing an application in IIS does not delete the physical content from the Windows file system; it removes the relationship of the content as an application under a site._

## Remove the content folder from Windows file system (all IIS versions)

After un-installing the NSI Web Service from the IIS, delete the content folder from the Windows file system (such as C:\Inetpub\wwwroot\nsiws) using for example Windows Explorer.
