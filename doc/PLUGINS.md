# Plugins

## Overview

The Web Service supports using different types of plugins. Those plugins are loaded from DLLs found in the plug-ins folder. The default plug-ins folder is `Plugins` under the `bin` folder.  This folder can be modified by modifying the corresponding section in [Main configuration file] (more specifically, the `pluginFolder` under `<appSettings>` in [Main configuration file]).

```xml
<appSettings>
 <add key="pluginFolder" value="Plugins"/>
```

## Supported plugins

### Retriever plugins

First type of plugins are the providers for retrieving SDMX data and structures.All providers for retrieving SDMX data and structures will be tried until it one is found that can serve the request. To determine which provider for retrieving SDMX data and structures is tried first, which second etc. the “Id” of each data provider must be placed in the desired order under the `<estat.sri>/<module>/<retrievers>` section under the [Main configuration file]

```xml
<estat.sri>
...
            <module>
            <retrievers>
                <!-- First provider to be tried -->
                <add name="MappingStoreRetrieversFactory"/>
                <!-- Second provider to be tried. -->
  <!-- It will used only if first cannot serve the result -->
...
            </retrievers>
        </module>
```

In case no configuration is found the Web Service will always default at the SDMX-RI implementation “MappingStoreRetrieversFactory” as the first provider to be tried.

### Format plugins

Second type of plugins is the output format plugin, which add support for additional data and structure formats for REST requests. Also for SOAP requests existing supported formats can be replaced. By default the NSI Web Service includes three format plugins. One that supports most SDMX-ML data & structure formats for SDMX v2.0 and SDMX v2.1. Another that support SDMX-JSON for data (REST Only).

### Submit structure plugin

Third type of plugins is the support of importing structural metadata. Unlike other plugins only one instance of this type of plugin should be available. By default the NSI Web Service includes a plugin that will store structural metadata inside the Mapping Store database.

### Dataflow authorization plugin

The fourth type of plugins is the Dataflow Authorization plugins. By default the NSI Web Service will include a plugin that uses the NSI Auth Module but additional plugins can be added.

### Default SDMX Header plugin for data messages

By default the NSI Web Service includes a plugin for reading the default SDMX Header format from configuration. Unlike other plugins, NSI WS supports only one default header plugin at a time.

```xml
   <!-- Default header retriever. Should match the class name of the IHeaderRetrievalManager implementation. The DLL is expected to be found in the Plugins folder-->
    <add key="defaultHeaderRetriever" value="SettingsHeaderRetriever"/>
```

### DDB Connection builder plugin

The Dissamination Database (DDB) connection builder plugin is responsible to build the connection to the DDB given the information in MSDB.

Only one DDB Connection builder is supported at the time. The DLL containing the plugin can be configured in the [Main configuration file] under `<appSettings>` at the `dbConnectionBuilderDll` setting.

```xml
    <add key="dbConnectionBuilderDll" value="Estat.Sri.Mapping.MappingStore"/>
```

## Plugin Dependencies

All plugin dlls should be copied in the 'bin\Plugins' folder of the web service.If plugin A depends on plugin B both dlls should be copied to the 'bin\Plugins'.
Other dll dependencies of the plugins should be copied to the 'bin' folder.

## Plugin development/examples

### Format plugins development

For a output format plugin example please see:
[SampleFormatPlugin](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/nsiws.net/browse/ReUsingExamples/SampleFormatPlugin)

The default SDMX RI format plugins implementation can be found below:

| Format DLL                               | Supported Formats            | SOAP/REST                                | Data/Structure                           | Source code repository |
|------------------------------------------|------------------------------|------------------------------------------|------------------------------------------|------------------------|
| Estat.SdmxSource.Extension.RDFPlugin.dll | RDF                          | REST only                                | both                                     | [Bitbucket](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/estat_sdmxsource_extension.net/browse/Estat.SdmxSource.Extension.RDFPlugin)              |
| Estat.Sri.Ws.Format.Json.dll             | SDMX-JSON draft and v1.0     | REST only                                | both                                     | [BitBucket](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/nsiws.net/browse/src/JsonFormatPlugin)              |
| Estat.Sri.Ws.Format.Sdmx.dll             | SDMX-ML v2.0, v2.1, SDMX-CSV | Both for SDMX-ML, REST only for SDMX-CSV | Both for SDMX-ML, data only for SDMX-CSV | [BitBucket](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/nsiws.net/browse/src/OutputFormat)              |

### Retriever plugins sample implementation

For a retriever plugin example please see:
[SampleRetrieverPlugin](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/nsiws.net/browse/ReUsingExamples/SampleRetrieverPlugin)

### Submit Structure plugins

To add your own plug-in for submitting structural metadata, a plugin assembly with the implementation of the interface `IStructureSubmitFactory` and `IStructureSubmitterEngine` from `Estat.Sdmxsource.Extension` needs to be implemented.

Please see the general plugin section on where to copy the plugin assembly (dll).

An example implementation can be found at  [StructureSubmitMappingStoreFactory](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/maapi.net/browse/src/Estat.Sri.MappingStore.Store/Factory/StructureSubmitMappingStoreFactory.cs?at=refs%2Fheads%2F1.7.0) and[StructureSubmitterMappingStore](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/maapi.net/browse/src/Estat.Sri.MappingStore.Store/Engine/StructureSubmitterMappingStore.cs?at=refs%2Fheads%2F1.7.0)

### DDB Connection builder plugin development

To add your own plug-in for DDB Connection builder, a plugin assembly with the implementation of the interface `IBuilder<DbConnection, DdbConnectionEntity>`  from `Estat.Sri.Mapping.Api` needs to be implemented.

The default implementation included in NSI WS can be found at [DdbConnectionBuilder](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/maapi.net/browse/src/Estat.Sri.Mapping.MappingStore/Builder/DDbConnectionBuilder.cs)

The NSI WS can be configured to look for an implementation in a different DLL in [Main Configuration file] under `appSettings`:

```xml
 <add key="dbConnectionBuilderDll" value="Estat.Sri.Mapping.MappingStore"/>
 ```

## MiddleWare plugins

In ASP.NET Core the .NET Framework [HTTP Modules](https://docs.microsoft.com/en-us/dotnet/api/system.web.ihttpmodule?view=netframework-4.7.2) have been replaced with [Middleware](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/middleware/?view=aspnetcore-2.2).

One main difference between HTTP Modules and Middleware is that, adding/removing HTTP Module required only changes in `Web.Config` while in ASP.NET Core it requires code changes and re-compiling.

In order to maintain this HTTP Module feature, 3rd party Middleware can be used as plugins and copied to the Web Service `Plugins` folder.

To add your own Middleware plug-in, a plugin assembly with the implementation of the interface `Estat.Sdmxsource.Extension.Builder.IMiddlewareBuilder` from `Estat.Sdmxsource.Extension` needs to be implemented.

Please see the general plugin section on where to copy the plugin assembly (dll).
Example implementations can be found at the [NSIWS MiddlewareBuilder Project](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/nsiws.net/browse/src/MIddlewareBuilder?at=refs%2Fheads%2Fdevelop)

The order of the middlere is configurable. It can be configured in [Main configuration file] under `<appSettings>` in `middlewareImplementation` by specifying the name of the class. E.g.

```xml
    <!--A comma separated string that determines the order for the middleware implementations -->
   <add key="middlewareImplementation" value="CorsMiddlewareBuilder,AuthMiddlewareBuilder,PolicyModuleMiddlewareBuilder" />
```

*Important* for simplicity it expects all class names to be unique.

## Links

[Main configuration file]: CONFIGURATION.md#Main-Configuration-file
