# BUILDING NSIWS

## PREREQUISITES

### DOTNET 6.0 SDK
In order to build NSIWS we first need to have installed the dotnet 6.0 sdk. You can check if/what version of sdk is installed on the computer by running the following command
```console
dotnet --info
```
![DOTNET_INFO](figures/DOTNET_INFO.png)

if the dotnet command line tool is not available

![SDK_NOT_INSTALLED_1](figures/SDK_NOT_INSTALLED_1.png)

please check that the path `c:\Program Files\dotnet\dotnet.exe` exists and that it is added to the path environment variable.

if the directory does not exist you can download and install the SDK from the following link, [SDK 6.0](https://dotnet.microsoft.com/download/dotnet/6.0)

### NUGET PACKAGES SOURCE
The second requirement for building NSIWS is that we have the nuget.org source configured

#### COMMAND LINE

To get a list of package sources please run the command
```console
dotnet nuget list source
```
Note: This command needs dotnet core version newer than 3.1.100. Avoid running this command from a folder that contains a global.json file (like.eg maws.net project folder) because this will make the command run with the version included in the global.json that may 3.1.100 or older and therefore fail even if you have installed a newer version in your PC.

![NUGET_NOT_CONFIGURED](figures/NUGET_NOT_CONFIGURED.png)

If the nuget.org source is missing, like in the above picture, you can add the nuget.org source using the following command
```console
dotnet nuget add source https://api.nuget.org/v3/index.json -n nuget.org
```

![ADD_NUGET_SOURCE](figures/ADD_NUGET_SOURCE.png)

#### VISUAL STUDIO 2019

To check if nuget.org source is configured you can go to Tools>NuGet Package Manager>Package Manager Settings>Package Sources
If the nuget.org source is missing, you can add the nuget.org source using the + button at the top right and put `nuget.org` as Name and `https://api.nuget.org/v3/index.json` as Source.

![VS_ADD_NUGET_SOURCE](figures/VS_ADD_NUGET_SOURCE.png)

### NUGET PACKAGES RESTORE

#### COMMAND LINE
The second requirement for building NSIWS is that we need to get the dependency packages. To do this we need to go to the directory containing the solution file `NSIWebServices.sln` and run the following command
```console
dotnet restore NSIWebServices.sln
```

#### VISUAL STUDIO 2019

We need to go to Tools>NuGet Package Manager>Package Manager Settings and select `Allow NuGet to download missing packages` and `Automatically check for missing packages during build in Visual Studio`, this will restore the required packages during build or you can restore them manually by going in Solution Explorer, right click the solution and select Restore NuGet Packages.

![VS_CONFIG_RESTORE](figures/VS_CONFIG_RESTORE.png)
## BUILDING

### COMAND LINE
Go to the directory containing the solution file `NSIWebServices.sln` and run the following command
```console
dotnet build --configuration Release NSIWebServices.sln
```

#### VISUAL STUDIO 2019
Load `NSIWebServices.sln` then go to Solution Explorer, right click the solution and select Build Solution.
