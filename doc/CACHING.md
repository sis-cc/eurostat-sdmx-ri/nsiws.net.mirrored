# Caching support

## Vary header configuration

By default the entries under `httpHeaders.varyHeader.varyBy` in `config/httpHeaders.json` will be used in the response in a `Vary` header if they are present in the request.
If the `Vary` header is already present, any additional values that are not already present in the `Vary` header will be appended.
To disable this functionality simply remove or change the extension of the json file.

```json
{
  "httpHeaders": {
    "varyHeader": {
      "varyBy": ["Accept", "Accept-Language", "Accept-Encoding"]
    }
  }
}
```

Note `Vary` HTTP response header is also added by `CORS` and Server Side caching.

## Browser side caching

In the `web.config` file it is possible to enable client side caching.
Enabling those settings allow browsers or other client software to cache results.

By default caching is disabled.

### Configuration for enabling Browser

Browser side caching can be enabled by adding two settings under `<appSettings>` in [Main configuration file](CONFIGURATION.md#main-configuration-file) file, see below.

```xml
 <add key="data.cache-control.max-age" value="3600"/>
 <add key="structure.cache-control.max-age" value="86400"/>
```

The values are in seconds.

More information can be found at [Cache-Control documentation](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Cache-Control#Cache_response_directives)

## Server side caching

Output caching is supported in NSI WS REST data and structure services using HTTP GET method.
The default streaming functionality will only work when the cache is not used - the two cannot work together.

The caching is disabled by default.

### Configuration

In order to enable caching, a JSON file with the following contents must be created in the `config` folder:

```json
{
  "outputcache": {
    "max-age": 3600
  }
}
```

The duration in "max-age" is given in seconds.
