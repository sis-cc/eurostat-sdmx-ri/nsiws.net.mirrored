﻿// -----------------------------------------------------------------------
// <copyright file="SampleDataWriterEngine.cs" company="EUROSTAT">
//   Date Created : 2016-08-12
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Samples.Ws.SampleFormatPlugin.Engine
{
    using System;
    using System.IO;
    using System.Text;
    using System.Xml;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;

    /// <summary>
    /// The sample data writer engine.
    /// </summary>
    public class SampleDataWriterEngine : IDataWriterEngine
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SampleDataWriterEngine"/> class.
        /// </summary>
        /// <param name="outputStream">The output stream.</param>
        /// <param name="outputEncoding">The output encoding.</param>
        /// <remarks>This is used for REST.</remarks>
        public SampleDataWriterEngine(Stream outputStream, Encoding outputEncoding)
        {
            if (outputStream == null)
            {
                throw new ArgumentNullException(nameof(outputStream));
            }

            if (outputEncoding == null)
            {
                throw new ArgumentNullException(nameof(outputEncoding));
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SampleDataWriterEngine"/> class.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <remarks>This is used for SOAP. SOAP is limited to XML</remarks>
        public SampleDataWriterEngine(XmlWriter writer)
        {
            if (writer == null)
            {
                throw new ArgumentNullException(nameof(writer));
            }

            //// TODO This is needed only when overriding existing SDMX XML formats to use under SOAP endpoints. If this is not the case remove it.
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
        }

        /// <summary>
        /// (Optional) Writes a header to the message, any missing mandatory attributes are defaulted.  If a header is required
        /// for a message, then this
        /// call should be the first call on a WriterEngine
        /// <p />
        /// If this method is not called, and a message requires a header, a default header will be generated.
        /// </summary>
        /// <param name="header">The header.</param>
        public void WriteHeader(IHeader header)
        {
            if (header == null)
            {
                throw new ArgumentNullException(nameof(header));
            }

            throw new NotImplementedException();
        }

        /// <summary>
        /// Writes the footer message (if supported by the writer implementation), and then completes the XML document, closes
        /// off all the tags, closes any resources.
        /// <b>NOTE</b> It is very important to close off a completed DataWriterEngine,
        /// as this ensures any output is written to the given location, and any resources are closed.  If this call
        /// is not made, the output document may be incomplete.
        /// <p><b> NOTE: </b> If writing a footer is not supported, then the footer will be silently ignored
        /// </p>
        /// Close also validates that the last series key or group key has been completed.
        /// </summary>
        /// <param name="footer">The footer.</param>
        public void Close(params IFooterMessage[] footer)
        {
            // Handle footers if format supports footers
            // then dispose
            this.Dispose();
        }

        /// <summary>
        /// Starts a dataset with the data conforming to the DSD
        /// </summary>
        /// <param name="dataflow">Optional. The dataflow can be provided to give extra information about the dataset.</param>
        /// <param name="dsd">The data structure is used to know the dimensionality of the data</param>
        /// <param name="header">Dataset header containing, amongst others, the dataset action, reporting dates,
        /// dimension at observation if null then the dimension at observation is assumed to be TIME_PERIOD and the dataset
        /// action is assumed to be INFORMATION</param>
        /// <param name="annotations">Any additional annotations that are attached to the dataset, can be null
        /// if no annotations exist</param>
        public void StartDataset(
            IDataflowObject dataflow,
            IDataStructureObject dsd,
            IDatasetHeader header,
            params IAnnotation[] annotations)
        {
            if (dsd == null)
            {
                throw new ArgumentNullException(nameof(dsd));
            }

            // all other parameters can be null
            throw new NotImplementedException();
        }

        /// <summary>
        /// Starts a group with the given id, the subsequent calls to <c>writeGroupKeyValue</c> will write the id/values to
        /// this group.  After
        /// the group key is complete <c>writeAttributeValue</c> may be called to add attributes to this group.
        /// <p /><b>Example Usage</b>
        /// A group 'demo' is made up of 3 concepts (Country/Sex/Status), and has an attribute (Collection).
        /// <code>
        /// DataWriterEngine dre = //Create instance
        /// dre.startGroup("demo");
        /// dre.writeGroupKeyValue("Country", "FR");
        /// dre.writeGroupKeyValue("Sex", "M");
        /// dre.writeGroupKeyValue("Status", "U");
        /// dre.writeAttributeValue("Collection", "A");
        /// </code>
        /// Any subsequent calls, for example to start a series, or to start a new group, will close off this exiting group.
        /// </summary>
        /// <param name="groupId">Group Id</param>
        /// <param name="annotations">Annotations any additional annotations that are attached to the group, can be null if no
        /// annotations exist</param>
        public void StartGroup(string groupId, params IAnnotation[] annotations)
        {
            if (groupId == null)
            {
                throw new ArgumentNullException(nameof(groupId));
            }

            throw new NotImplementedException();
        }

        /// <summary>
        /// Starts a new series, closes off any existing series keys or attribute/observations.
        /// </summary>
        /// <param name="annotations">Any additional annotations that are attached to the series, can be null if no annotations
        /// exist</param>
        public void StartSeries(params IAnnotation[] annotations)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Writes an attribute value for the given id.
        /// <p />
        /// If this method is called immediately after a <c>writeSeriesKeyValue</c> method call then it will write
        /// the attribute at the series level.  If it is called after a <c>writeGroupKeyValue</c> it will write the attribute
        /// against the group.
        /// <p />
        /// If this method is called immediately after a <c>writeObservation</c> method call then it will write the attribute
        /// at the observation level.
        /// </summary>
        /// <param name="id">the id of the given value for example 'OBS_STATUS'</param>
        /// <param name="valueRen">The value.</param>
        public void WriteAttributeValue(string id, string valueRen)
        {
            if (id == null)
            {
                throw new ArgumentNullException(nameof(id));
            }

            throw new NotImplementedException();
        }

        /// <summary>
        /// Writes a group key value, for example 'Country' is 'France'.  A group may contain multiple id/value pairs in the
        /// key, so this method may be called consecutively with an id / value for each key item.
        /// <p />
        /// If this method is called consecutively multiple times and a duplicate id is passed in, then an exception will be
        /// thrown, as a group can only declare one value for a given id.
        /// <p />
        /// The <c>startGroup</c> method must be called before calling this method to add the first id/value, as the
        /// WriterEngine needs to know what group to assign the id/values to.
        /// </summary>
        /// <param name="id">the id of the concept or dimension</param>
        /// <param name="valueRen">The value.</param>
        public void WriteGroupKeyValue(string id, string valueRen)
        {
            if (id == null)
            {
                throw new ArgumentNullException(nameof(id));
            }

            if (valueRen == null)
            {
                throw new ArgumentNullException(nameof(valueRen));
            }

            throw new NotImplementedException();
        }

        /// <summary>
        /// Writes an observation, the observation concept is assumed to be that which has been defined to be at the
        /// observation level (as declared in the start dataset method DatasetHeaderObject).
        /// </summary>
        /// <param name="obsConceptValue">May be the observation time, or the cross section value</param>
        /// <param name="obsValue">The observation value - can be numerical</param>
        /// <param name="annotations">Any additional annotations that are attached to the observation, can be null if no
        /// annotations exist</param>
        public void WriteObservation(string obsConceptValue, string obsValue, params IAnnotation[] annotations)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Writes an Observation value against the current series.
        /// <p />
        /// The current series is determined by the latest writeKeyValue call,
        /// If this is a cross sectional dataset, then the observation Concept is expected to be the cross sectional concept
        /// value - for
        /// example if this is cross sectional on Country the id may be "FR"
        /// If this is a time series dataset then the observation Concept is expected to be the observation time - for example
        /// 2006-12-12
        /// <p />
        /// Validates the following:
        /// <ul><li>The observation Time string format is one of an allowed SDMX time format</li><li>The observation Time does not replicate a previously reported observation Time for the current series</li></ul>
        /// </summary>
        /// <param name="observationConceptId">the concept id for the observation, for example 'COUNTRY'.  If this is a Time
        /// Series, then the id will be DimensionBean.TIME_DIMENSION_FIXED_ID.</param>
        /// <param name="obsConceptValue">may be the observation time, or the cross section value</param>
        /// <param name="obsValue">The observation value - can be numerical</param>
        /// <param name="annotations">Any additional annotations that are attached to the observation, can be null if no
        /// annotations exist</param>
        public void WriteObservation(
            string observationConceptId,
            string obsConceptValue,
            string obsValue,
            params IAnnotation[] annotations)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Writes an Observation value against the current series
        /// <p />
        /// The date is formatted as a string, the format rules are determined by the TIME_FORMAT argument
        /// <p />
        /// Validates the following:
        /// <ul><li>The observation Time does not replicate a previously reported observation Time for the current series</li></ul>
        /// </summary>
        /// <param name="obsTime">the time of the observation</param>
        /// <param name="obsValue">the observation value - can be numerical</param>
        /// <param name="sdmxSwTimeFormat">the time format to format the observation Time in when converting to a string</param>
        /// <param name="annotations">The annotations.</param>
        public void WriteObservation(DateTime obsTime, string obsValue, TimeFormat sdmxSwTimeFormat, params IAnnotation[] annotations)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Writes a series key value.  This will have the effect of closing off any observation, or attribute values if they
        /// are any present
        /// <p />
        /// If this method is called after calling writeAttribute or writeObservation, then the engine will start a new series.
        /// </summary>
        /// <param name="id">the id of the value for example 'Country'</param>
        /// <param name="valueRen">The value.</param>
        public void WriteSeriesKeyValue(string id, string valueRen)
        {
            if (id == null)
            {
                throw new ArgumentNullException(nameof(id));
            }

            throw new NotImplementedException();
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="managed"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool managed)
        {
            if (managed)
            {
               // TODO dispose anything that needs to. 
            }
        }
    }
}