﻿// -----------------------------------------------------------------------
// <copyright file="SampleDataWritingContract.cs" company="EUROSTAT">
//   Date Created : 2016-08-12
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Samples.Ws.SampleFormatPlugin.Factory
{
    using System;
    using System.Linq;
    using System.Text;
    using System.Xml;

    using Estat.Sdmxsource.Extension.Factory;
    using Estat.Sdmxsource.Extension.Model;
    using Estat.Sri.Samples.Ws.SampleFormatPlugin.Model;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;

    /// <summary>
    /// The sample data writing contract.
    /// </summary>
    public class SampleDataWritingContract : AbstractDataWriterPluginContract
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SampleDataWritingContract"/> class.
        /// </summary>
        public SampleDataWritingContract()

            // TODO CHANGE THIS the supported formats
            : base(
                new[]
                    {
                        "application/vnd+myformat",
                        /* add your supported media types e.g. "mytype/subtype+a;version=2.3", "othertype/someothersubtype" */
                    })
        {
        }

        /// <summary>
        /// Gets the data format.
        /// </summary>
        /// <param name="soapRequest">
        /// Type of the data.
        /// </param>
        /// <param name="xmlWriter">
        /// The XML writer.
        /// </param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Data.IDataFormat"/>.
        /// </returns>
        public override IDataFormat GetDataFormat(ISoapRequest<DataType> soapRequest, XmlWriter xmlWriter)
        {
            ///// Used in SOAP data requests. If you don't need SOAP support, just return null ////

            //// SOAP can be used only with SDMX-ML (XML) formats. If it is not XML and not SDMX it then this is not needed.

            /*
            if (soapRequest == null)
            {
                throw new ArgumentNullException(nameof(soapRequest));
            }

            if (xmlWriter == null)
            {
                throw new ArgumentNullException(nameof(xmlWriter));
            }


            // CHANGE THIS sample list of supported formats
            var supportedFormatDataTypes = new[] { DataEnumType.Generic21Xs, DataEnumType.Compact10, };

            // get the web operation if needed
            var operation = soapRequest.Operation;

            // the format type, e.g. CompactData v2.0, GenericData v2.0, GenericData v2.1, StructureSpecificData (CompactData) v2.1
            var formatType = soapRequest.FormatType;

            // the SDMX Extensions, normally not used for data
            var sdmxExtension = soapRequest.Extensions;

            if (!formatType.SchemaVersion.IsXmlFormat())
            {
                return null;
            }

            // Check if we support this requested format
            if (!supportedFormatDataTypes.Contains(formatType.EnumType))
            {
                return null;
            }

            return new SampleDataSoapFormat(formatType, xmlWriter);
            */
            return null;
        }

        /// <summary>
        /// Builds the format.
        /// </summary>
        /// <param name="mediaType">Type of the media.</param>
        /// <param name="encoding">The encoding.</param>
        /// <param name="dataRequest">The rest data request.</param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Data.IDataFormat" />.
        /// </returns>
        protected override IDataFormat BuildFormat(
            string mediaType,
            Encoding encoding,
            IRestDataRequest dataRequest)
        {
            // In case we need to, we can check the requested media types sorted by quality 
            var acceptHeaderValue = dataRequest.SortedAcceptHeaders.ContentTypes;

            // get requested languages if needed sorted by quality
            var languages = dataRequest.SortedAcceptHeaders.Languages;
            //// -or-
            // override and use the this.GetLanguage method
            var language = this.GetLanguage(dataRequest);

            // get the requested dataflow if needed
            var dataflow = dataRequest.DataQuery.Dataflow;

            // get the requested dsd if needed
            var dsd = dataRequest.DataQuery.DataStructure;

            // get the Structure retriever manager in case we need other structural metadata
            var retrieverManager = dataRequest.RetrievalManager;

            return new SampleDataRestFormat(encoding);
        }

        /// <summary>
        /// Builds the encoding.  Override this when you need to override the way the <see cref="T:System.Text.Encoding" /> is selected.
        /// </summary>
        /// <param name="restDataRequest">The rest data request.</param>
        /// <returns>
        /// The <see cref="T:System.Text.Encoding" />.
        /// </returns>
        protected override Encoding BuildEncoding(IRestDataRequest restDataRequest)
        {
            return base.BuildEncoding(restDataRequest);
        }

        /// <summary>
        /// Gets the language.  Override this to return a different language
        /// </summary>
        /// <param name="restDataRequest">The rest data request.</param>
        /// <returns>
        /// The <see cref="T:System.String" />.
        /// </returns>
        protected override string GetLanguage(IRestDataRequest restDataRequest)
        {
            return base.GetLanguage(restDataRequest);
        }
    }
}