﻿// -----------------------------------------------------------------------
// <copyright file="SampleStructureWriterRestFactory.cs" company="EUROSTAT">
//   Date Created : 2016-08-12
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Samples.Ws.SampleFormatPlugin.Factory
{
    using System.IO;

    using Estat.Sri.Samples.Ws.SampleFormatPlugin.Engine;
    using Estat.Sri.Samples.Ws.SampleFormatPlugin.Model;

    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;

    /// <summary>
    /// REST Structure factory
    /// </summary>
    /// <seealso cref="Org.Sdmxsource.Sdmx.Api.Factory.IStructureWriterFactory" />
    public class SampleStructureWriterRestFactory : IStructureWriterFactory
    {
        /// <summary>
        /// Obtains a StructureWritingEngine engine for the given output format
        /// </summary>
        /// <param name="structureFormat">An implementation of the StructureFormat to describe the output format for the structures
        /// (required)</param>
        /// <param name="streamWriter">The output stream to write to (can be null if it is not required)</param>
        /// <returns>
        /// Null if this factory is not capable of creating a data writer engine in the requested format
        /// </returns>
        public IStructureWriterEngine GetStructureWriterEngine(IStructureFormat structureFormat, Stream streamWriter)
        {
            var specializedFormat = structureFormat as SampleStructureRestFormat;
            if (specializedFormat != null)
            {
                return new SampleStructureWritingEngine(streamWriter, specializedFormat.Encoding);
            }

            return null;
        }
    }
}