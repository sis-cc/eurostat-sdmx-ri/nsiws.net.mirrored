﻿// -----------------------------------------------------------------------
// <copyright file="DataWriterSoapFactory.cs" company="EUROSTAT">
//   Date Created : 2016-08-12
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Samples.Ws.SampleFormatPlugin.Factory
{
    using System;
    using System.IO;

    using Estat.Sri.Samples.Ws.SampleFormatPlugin.Engine;
    using Estat.Sri.Samples.Ws.SampleFormatPlugin.Model;

    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;

    /// <summary>
    /// The Data Writer factory for REST.
    /// </summary>
    public class DataWriterSoapFactory : IDataWriterFactory
    {
        /// <summary>
        /// Gets the data writer engine.
        /// </summary>
        /// <param name="dataFormat">The data format.</param>
        /// <param name="outStream">The output stream.</param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.Api.Engine.IDataWriterEngine" />.
        /// </returns>
        public IDataWriterEngine GetDataWriterEngine(IDataFormat dataFormat, Stream outStream)
        {
            var specializedFormat = dataFormat as SampleDataSoapFormat;
            if (specializedFormat != null)
            {
                return new SampleDataWriterEngine(specializedFormat.Writer);
            }

            return null;
        }
    }
}