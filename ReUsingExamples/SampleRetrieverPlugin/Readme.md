﻿# Sample retriever plug-in for NSI Web Service

This a template project for building data and structure retrievers for SDMX RI NSI Web Service.

## How to use
1. Clone it from : https://webgate.ec.europa.eu/CITnet/stash/scm/sdmxri/nsiws.net.git
2. Open SampleRetrieverPlugin.sln with Visual Studio
3. Change namespace in the classes to avoid collisions with other plugins. [Instructions how to quickly change the namespace without any Visual Studio extension](http://stackoverflow.com/questions/2871314/change-project-namespace-in-visual-studio)
4. Open the [Sample Retriever Plugin][SampleRetrieverFactory] and change the Factory ID to a unique value which identifies uniquely your retriever factory:
```c#
 private const string FactoryId = "ENTER UNIQUE VALUE HERE";
```
5. Again in [Sample Retriever Plugin][SampleRetrieverFactory] the Get.. methods return sample implementations of each Manager. The available sample manager implementations should be modified to return actual data and structures. The sample implementations include examples how to use the various related SdmxSource classes. 
In this sample only for data only [SampleAdvancedSdmxDataRetriever][] and [SampleXsDataRetriever][] need to be modified while for structure queries only [SampleMutableObjectRetriever][] and [SampleCrossReferenceManager][] need to be modified. 

6. Build and copy to the bin\\Plugins folder of NSI WS. Any dependencies should be copied in the NSI WS bin folder.
7. Use a REST and/or SOAP client to test

## Data Retrievers

Data Retrievers are responsible for SDMX SOAP or REST data requests. Because of differences between SDMX data queries, between interfaces and expected behaviour, i.e. SDMX SOAP v2.0 vs v2.1 and SOAP vs REST, there are different implementation of data retrievers. But because the functionality is nearly the same, it is possible to implement only one or two implementations and use those for everything. 

In this sample only the [SDMX SOAP v2.1][SampleAdvancedSdmxDataRetriever] and [SDMX v2.0 Cross Sectional (SOAP/REST)][SampleXsDataRetriever] touch the fake "data store". For SDMX V2.0 and REST we re-use the class for the [SDMX SOAP v2.1][SampleAdvancedSdmxDataRetriever] since SDMX v2.1 SOAP data queries are more or less a super set of the SDMX v2.0 and REST data queries in terms of functionality. 

Below are the implementations related to Data Retrieval and could be modified to access a real data store.


| Sample implementation            | Interface                            | Description                                                                                                                                  |
|----------------------------------|--------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------|
| [SampleAdvancedSdmxDataRetriever][]  | IAdvancedSdmxDataRetrievalWithWriter | In this sample there is an example parsing an SDMX SOAP v2.1 data query and write data. Only SDMX v2.1 SOAP requests will be routed here. Also by using the decorator class DataRetrieverWithWriterWrapper it will also handle also requests from SDMX SOAP v2.0 and REST    |
| [SampleXsDataRetriever][]            | ISdmxDataRetrievalWithCrossWriter    | IIn this sample there is an example parsing a SDMX v2.0 or REST data query and write SDMX v2.0 Cross Sectional data.                         |
| [SampleDataHeaderRetrievalManager][] | IHeaderRetrievalManager              |  In this sample there is an example parsing build a header which will be used with datasets.                                                 |                |



## Structure Retrievers

Structure Retrievers are reponsible for SDMX SOAP or REST structure requests and getting required structural metadata for data requests.  Because of differences between SDMX structure queries, between interfaces and expected behaviour, i.e. SDMX SOAP v2.0 vs v2.1 and SOAP vs REST, there are different implementation of structure retrievers. But because the functionality is nearly the same, it is possible to implement only one or two implementations and use those for everything. 

In this sample only the [SampleMutableObjectRetriever][] and the [SampleCrossReferenceManager][] touch the fake "structure store". Everything else will use those 2 classes. 

Below are the implementations related to Structure Retrieval and could be modified to access a real structure store.

| Sample implementation                        | Interface                              | Description                                                                                                                                                                                                                                                                                                                  |
|----------------------------------------------|----------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| SampleIAdvancedMutableStructureSearchManager | IAdvancedMutableStructureSearchManager | This is used for SDMX v2.1 SOAP structure requests only. In this sample it doesn't use any special features of SDMX v2.1 SOAP and instead passed the requests to the [SampleMutableStructureSearchManager][]                                                                                                                 |
| SampleMutableObjectRetriever                 | ISdmxMutableObjectRetrievalManager     | This class is used by every other class in this sample to retrieve structures from the fake "structure store".                                                                                                                                                                                                               |
| SampleMutableStructureSearchManager          | IMutableStructureSearchManager         | This is used for SDMX v2.0 SOAP and,REST structure requests. Internally it uses [SampleMutableObjectRetriever][] and [SampleCrossReferenceManager][]                                                                                                                                                                         |
| SampleCrossReferenceManager                  | ICrossReferenceMutableRetrievalManager | This is used for resolving parents and children of an SDMX structure. It uses [SampleMutableObjectRetriever][] but it also access the fake "structure store" directly. Production implementation would need to do this as well as resolving parents and children in some cases  is not possible without access to the store. |


[SampleRetrieverFactory]: src/SampleRetrieverPlugin/Factory/SampleRetrieverFactory.cs
[SampleAdvancedSdmxDataRetriever]: src/SampleRetrieverPlugin/Manager/SampleAdvancedSdmxDataRetriever.cs
[SampleCrossReferenceManager]: src/SampleRetrieverPlugin/Manager/SampleCrossReferenceManager.cs
[SampleDataHeaderRetrievalManager]: src/SampleRetrieverPlugin/Manager/SampleDataHeaderRetrievalManager.cs
[SampleIAdvancedMutableStructureSearchManager]: src/SampleRetrieverPlugin/Manager/SampleIAdvancedMutableStructureSearchManager.cs
[SampleMutableObjectRetriever]: src/SampleRetrieverPlugin/Manager/SampleMutableObjectRetriever.cs
[SampleMutableStructureSearchManager]: src/SampleRetrieverPlugin/Manager/SampleMutableStructureSearchManager.cs
[SampleXsDataRetriever]: src/SampleRetrieverPlugin/Manager/SampleXsDataRetriever.cs
