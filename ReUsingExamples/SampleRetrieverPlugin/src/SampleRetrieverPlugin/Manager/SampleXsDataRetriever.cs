﻿// -----------------------------------------------------------------------
// <copyright file="SampleXsDataRetriever.cs" company="EUROSTAT">
//   Date Created : 2016-09-22
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace SampleRetrieverPlugin.Manager
{
    using System;
    using System.Linq;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.DataParser.Engine;
    using Org.Sdmxsource.Sdmx.DataParser.Manager;

    using SampleRetrieverPlugin.Constant;

    /// <summary>
    /// Sample Data Retriever for Cross Sectional Data
    /// </summary>
    /// <seealso cref="Org.Sdmxsource.Sdmx.DataParser.Manager.ISdmxDataRetrievalWithCrossWriter" />
    public class SampleXsDataRetriever : ISdmxDataRetrievalWithCrossWriter
    {
        /// <summary>
        /// The header retrieval manager
        /// </summary>
        private readonly IHeaderRetrievalManager _headerRetrievalManager = new SampleDataHeaderRetrievalManager();

        /// <summary>
        /// Retrieves data requested in <paramref name="dataQuery" /> and write it to <paramref name="dataWriter" />
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <param name="dataWriter">The data writer.</param>
        public void GetData(IDataQuery dataQuery, ICrossSectionalWriterEngine dataWriter)
        {
            if (dataQuery == null)
            {
                throw new ArgumentNullException(nameof(dataQuery));
            }

            if (dataWriter == null)
            {
                throw new ArgumentNullException(nameof(dataWriter));
            }

            // the most important part, the dataflow
            var dataFlow = dataQuery.Dataflow;

            // write the data header
            var messageHeader = this._headerRetrievalManager.Header;
            dataWriter.WriteHeader(messageHeader);

            dataWriter.StartDataset(null, dataQuery.DataStructure as ICrossSectionalDataStructureObject, null);

            // TODO replace the following with actual code for retrieving and writing data.
            // It is just an example to show how to use IDataWriteEngine and IDataQuery
            WriteData(dataQuery, dataWriter);
        }

        /// <summary>
        /// Writes the data. TODO Just an example how to work with data
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <param name="dataWriter">The data writer.</param>
        private static void WriteData(IDataQuery dataQuery, ICrossSectionalWriterEngine dataWriter)
        {
            //// The following just shows how to use IDataWriterEngine. 
            //// It make a lot of assumptions in order to remain simple

            // For cross sectional data we need a cross sectional DSD
            var crossDsd = (ICrossSectionalDataStructureObject)dataQuery.DataStructure;

            for (int index0 = 0; index0 < SampleData.Dataset.GetLength(0); index0++)
            {
                // We check against criteria although in production this could be done more efficiently in a SQL Query
                if (!MatchQuery(dataQuery, index0))
                {
                    continue;
                }

                // retrieve and store the current series key. This is needed in order to avoid writing one series per observation
                var crossGroupDimensions = crossDsd.GetCrossSectionalAttachGroup(true, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dimension));
                var crossSectionDimensions = crossDsd.GetCrossSectionalAttachSection(true, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dimension));
                var crossObsDimensions = crossDsd.GetCrossSectionalAttachObservation(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dimension));

                // normally here we check if we have a new XS group or an old one by comparing the current values of the XS Group dimensions with the previous
                // provided the data is ordered first by XS Dataset dimensions, XS Group dimensions and then by Section dimensions
                dataWriter.StartXSGroup();
                foreach (var dimension in crossGroupDimensions)
                {
                    dataWriter.WriteXSGroupKeyValue(dimension.Id, SampleData.GetValue(index0, dimension.Id));
                }

                foreach (var attribute in crossDsd.GetCrossSectionalAttachGroup(true, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DataAttribute)))
                {
                    dataWriter.WriteAttributeValue(attribute.Id, SampleData.GetValue(index0, attribute.Id));
                }

                // normally here we check if we have a new XS section or an old one by comparing the current values of the XS section dimensions with the previous
                // provided the data is ordered first by XS DataSet dimensions, XS Group dimensions and then by Section dimensions
                dataWriter.StartSection();
                foreach (var dimension in crossSectionDimensions)
                {
                    dataWriter.WriteSectionKeyValue(dimension.Id, SampleData.GetValue(index0, dimension.Id));
                }

                foreach (var attribute in crossDsd.GetCrossSectionalAttachSection(true, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DataAttribute)))
                {
                    dataWriter.WriteAttributeValue(attribute.Id, SampleData.GetValue(index0, attribute.Id));
                }

                if (crossDsd.CrossSectionalMeasures.Count > 0)
                {
                    foreach (var crossSectionalMeasure in crossDsd.CrossSectionalMeasures)
                    {
                        dataWriter.StartXSObservation(
                            crossSectionalMeasure.Code,
                            SampleData.GetValue(index0, crossSectionalMeasure.Id));
                        foreach (var crossObsDimension in crossObsDimensions)
                        {
                            dataWriter.WriteXSObservationKeyValue(crossObsDimension.Id, SampleData.GetValue(index0, crossObsDimension.Id));
                        }

                        foreach (var attribute in crossDsd.GetCrossSectionalAttachObservation(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DataAttribute)))
                        {
                            if (
                                crossDsd.GetAttachmentMeasures((IAttributeObject)attribute)
                                    .Contains(crossSectionalMeasure))
                            {
                                dataWriter.WriteAttributeValue(attribute.Id, SampleData.GetValue(index0, attribute.Id));
                            }
                        }
                    }
                }
                else
                {
                   dataWriter.StartXSObservation(crossDsd.PrimaryMeasure.Id, SampleData.GetValue(index0, crossDsd.PrimaryMeasure.Id));
                    foreach (var crossObsDimension in crossObsDimensions)
                    {
                        dataWriter.WriteXSObservationKeyValue(crossObsDimension.Id, SampleData.GetValue(index0, crossObsDimension.Id));
                    }

                    foreach (var attribute in crossDsd.GetCrossSectionalAttachObservation(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DataAttribute)))
                    {
                        dataWriter.WriteAttributeValue(attribute.Id, SampleData.GetValue(index0, attribute.Id));
                    }
                }
            }
        }

        /// <summary>
        /// Matches the query.
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <param name="rowNumber">The row number.</param>
        /// <returns>
        /// True if this row is queried false otherwise
        /// </returns>
        private static bool MatchQuery(IDataQuery dataQuery, int rowNumber)
        {
            // We could check against criteria although this could be done more efficiently in a SQL Query
            var dataQuerySelectionGroup = dataQuery.SelectionGroups.FirstOrDefault();
            if (dataQuerySelectionGroup == null)
            {
                return true;
            }

            foreach (var dimension in dataQuery.DataStructure.GetDimensions(SdmxStructureEnumType.Dimension, SdmxStructureEnumType.MeasureDimension))
            {
                var criteria = dataQuerySelectionGroup.GetSelectionsForConcept(dimension.Id);
                if (!criteria.Values.Contains(SampleData.GetValue(rowNumber, dimension.Id)))
                {
                    return false;
                }
            }

            return true;
        }
    }
}