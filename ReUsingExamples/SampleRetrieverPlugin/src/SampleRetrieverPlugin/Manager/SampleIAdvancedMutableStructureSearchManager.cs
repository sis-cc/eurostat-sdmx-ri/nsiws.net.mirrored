﻿// -----------------------------------------------------------------------
// <copyright file="SampleIAdvancedMutableStructureSearchManager.cs" company="EUROSTAT">
//   Date Created : 2016-09-23
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace SampleRetrieverPlugin.Manager
{
    using Estat.Sdmxsource.Extension.Extension;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    /// The sample i advanced mutable structure search manager.
    /// </summary>
    public class SampleIAdvancedMutableStructureSearchManager : IAdvancedMutableStructureSearchManager
    {
        /// <summary>
        /// The mutable structure search manager
        /// </summary>
        private readonly IMutableStructureSearchManager _mutableStructureSearchManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="SampleIAdvancedMutableStructureSearchManager"/> class.
        /// </summary>
        /// <param name="mutableStructureSearchManager">The mutable structure search manager.</param>
        public SampleIAdvancedMutableStructureSearchManager(IMutableStructureSearchManager mutableStructureSearchManager)
        {
            this._mutableStructureSearchManager = mutableStructureSearchManager;
        }

        /// <summary>
        /// Process the specified <paramref name="structureQuery" /> returning an <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.IMutableObjects" /> container
        /// which contains the Maintainable Structure hat correspond to the <paramref name="structureQuery" /> query
        /// parameters.
        /// </summary>
        /// <param name="structureQuery">The structure query.</param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.IMutableObjects" />.
        /// </returns>
        public IMutableObjects GetMaintainables(IComplexStructureQuery structureQuery)
        {
            var askedForWhat = structureQuery.StructureReference.ReferencedStructureType;
            SdmxStructureType maintainableType = askedForWhat.MaintainableStructureType;
            IMutableObjects objects = new MutableObjectsImpl();

            var structureDetail = ToSimple(structureQuery.StructureQueryMetadata.StructureQueryDetail, structureQuery.StructureQueryMetadata.ReferencesQueryDetail);
            StructureReferenceDetail structureReferenceDetail = structureQuery.StructureQueryMetadata.StructureReferenceDetail;

            // SdmxStructureType specificStructureReference = structureQuery.StructureQueryMetadata.ReferenceSpecificStructures;

            // In this sample we support only the subset of criteria supported by REST
            var maintainableRef = structureQuery.StructureReference.GetMaintainableRefObject();
            var returnLatest = structureQuery.StructureReference.VersionReference.IsReturnLatest;
            var structureReference = new StructureReferenceImpl(
                   maintainableRef.AgencyId,
                   maintainableRef.MaintainableId,
                   maintainableRef.Version,
                   maintainableType);
            if (structureReferenceDetail.EnumType != StructureReferenceDetailEnumType.Specific)
            {
                IRestStructureQuery restQuery = new RESTStructureQueryCore(
                    structureDetail,
                    structureReferenceDetail,
                    null,
                    structureReference,
                    returnLatest.IsTrue);
                return this._mutableStructureSearchManager.GetMaintainables(restQuery);
            }

            foreach (var specificStructure in structureQuery.StructureQueryMetadata.ReferenceSpecificStructures)
            {
                IRestStructureQuery restQuery = new RESTStructureQueryCore(
                    structureDetail,
                    structureReferenceDetail,
                    specificStructure,
                    structureReference,
                    returnLatest.IsTrue);
                objects.AddIdentifiables(this._mutableStructureSearchManager.GetMaintainables(restQuery).AllMaintainables);
            }

            return objects;
        }

        /// <summary>
        /// Convert SOAP v2.1 Query details to REST like.
        /// </summary>
        /// <param name="complexStructureQueryDetail">The complex structure query detail.</param>
        /// <param name="referenceComplexStructureQueryDetail">The reference complex structure query detail.</param>
        /// <returns>The REST structure query details</returns>
        private static StructureQueryDetail ToSimple(ComplexStructureQueryDetail complexStructureQueryDetail, ComplexMaintainableQueryDetail referenceComplexStructureQueryDetail)
        {
            StructureQueryDetailEnumType type = StructureQueryDetailEnumType.Full;
            switch (complexStructureQueryDetail.EnumType)
            {
                case ComplexStructureQueryDetailEnumType.Null:
                case ComplexStructureQueryDetailEnumType.Full:
                    switch (referenceComplexStructureQueryDetail.EnumType)
                    {
                        case ComplexMaintainableQueryDetailEnumType.Null:
                        case ComplexMaintainableQueryDetailEnumType.Full:
                            type = StructureQueryDetailEnumType.Full;
                            break;
                        case ComplexMaintainableQueryDetailEnumType.Stub:
                        case ComplexMaintainableQueryDetailEnumType.CompleteStub:
                            type = StructureQueryDetailEnumType.ReferencedStubs;
                            break;
                    }

                    break;
                case ComplexStructureQueryDetailEnumType.Stub:
                case ComplexStructureQueryDetailEnumType.CompleteStub:
                    type = StructureQueryDetailEnumType.AllStubs;
                    break;
            }

            return StructureQueryDetail.GetFromEnum(type);
        }
    }
}