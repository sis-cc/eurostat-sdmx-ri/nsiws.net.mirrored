﻿// -----------------------------------------------------------------------
// <copyright file="SampleCrossReferenceManager.cs" company="EUROSTAT">
//   Date Created : 2016-10-04
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace SampleRetrieverPlugin.Manager
{
    using System.Collections.Generic;
    using System.Linq;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;

    using SampleRetrieverPlugin.Constant;

    /// <summary>
    /// The sample cross reference manager. This manager is not used directly by NSI WS and doesn't have to be implemented. 
    /// But an optimized way to retrieve parents, children and specific structures is needed to avoid performance issues. This sample accesses the <see cref="SampleStructuralMetadata"/> 
    /// either directly or via a <see cref="ISdmxMutableObjectRetrievalManager"/>
    /// </summary>
    public class SampleCrossReferenceManager : ICrossReferenceMutableRetrievalManager
    {
        /// <summary>
        /// The mutable object retrieval manager
        /// </summary>
        private readonly ISdmxMutableObjectRetrievalManager _mutableObjectRetrievalManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="SampleCrossReferenceManager"/> class.
        /// </summary>
        /// <param name="mutableObjectRetrievalManager">The mutable object retrieval manager.</param>
        public SampleCrossReferenceManager(ISdmxMutableObjectRetrievalManager mutableObjectRetrievalManager)
        {
            this._mutableObjectRetrievalManager = mutableObjectRetrievalManager;
        }

        /// <summary>
        /// Returns a list of MaintainableObject that cross reference the structure(s) that match the reference parameter
        /// </summary>
        /// <param name="structureReference">- What Do I Reference?</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <param name="structures">an optional parameter to further filter the list by structure type</param>
        /// <returns>
        /// The <see cref="T:System.Collections.Generic.IList`1" />.
        /// </returns>
        public IList<IMaintainableMutableObject> GetCrossReferencedStructures(
            IStructureReference structureReference,
            bool returnStub,
            params SdmxStructureType[] structures)
        {
            if (!ObjectUtil.ValidArray(structures))
            {
                return this.GetChildren(structureReference, returnStub);
            }

            return this.GetSpecificFromDescendants(structureReference, returnStub, structures);
        }

        /// <summary>
        /// Returns a list of MaintainableObject that are cross referenced by the given identifiable structure
        /// </summary>
        /// <param name="identifiable">the identifiable bean to retrieve the references for - What Do I Reference?</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <param name="structures">an optional parameter to further filter the list by structure type</param>
        /// <returns>
        /// The <see cref="T:System.Collections.Generic.IList`1" />.
        /// </returns>
        public IList<IMaintainableMutableObject> GetCrossReferencedStructures(
            IIdentifiableMutableObject identifiable,
            bool returnStub,
            params SdmxStructureType[] structures)
        {
            return this.GetCrossReferencedStructures(GetAsStructureReference(identifiable), returnStub, structures);
        }

        /// <summary>
        /// Returns a tree structure representing the <paramref name="maintainableObject" /> and all the structures that cross
        /// reference it, and the structures that reference them, and so on.
        /// </summary>
        /// <param name="maintainableObject">The maintainable object to build the tree for.</param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.ICrossReferencingTree" />.
        /// </returns>
        /// <exception cref="System.NotImplementedException">Not implemented in this sample. Not needed by NSI WS</exception>
        public IMutableCrossReferencingTree GetCrossReferenceTree(IMaintainableMutableObject maintainableObject)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Returns a list of MaintainableObject that cross reference the structure(s) that match the reference parameter
        /// </summary>
        /// <param name="structureReference">Who References Me?</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <param name="structures">an optional parameter to further filter the list by structure type</param>
        /// <returns>
        /// The <see cref="T:System.Collections.Generic.IList`1" />.
        /// </returns>
        public IList<IMaintainableMutableObject> GetCrossReferencingStructures(
                    IStructureReference structureReference,
                    bool returnStub,
                    params SdmxStructureType[] structures)
        {
            if (!ObjectUtil.ValidArray(structures))
            {
                return this.GetParents(structureReference, returnStub);
            }

            return this.GetSpecificFromParentsOrSiblings(structureReference, returnStub, structures);
        }

        /// <summary>
        /// Returns a list of MaintainableObject that cross reference the given identifiable structure
        /// </summary>
        /// <param name="identifiable">the identifiable bean to retrieve the references for - Who References Me?</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <param name="structures">an optional parameter to further filter the list by structure type</param>
        /// <returns>
        /// The <see cref="T:System.Collections.Generic.IList`1" />.
        /// </returns>
        public IList<IMaintainableMutableObject> GetCrossReferencingStructures(
            IIdentifiableMutableObject identifiable,
            bool returnStub,
            params SdmxStructureType[] structures)
        {
            return this.GetCrossReferencingStructures(GetAsStructureReference(identifiable), returnStub, structures);
        }

        /// <summary>
        /// Gets as structure reference.
        /// </summary>
        /// <param name="identifiable">The identifiable.</param>
        /// <returns>The <see cref="IStructureReference"/> of the <paramref name="identifiable"/></returns>
        /// <exception cref="SdmxNotImplementedException">
        /// Only maintainable are needed and supported. It is not possible to extract this information from a non-identifiable mutable.
        /// </exception>
        private static IStructureReference GetAsStructureReference(IIdentifiableMutableObject identifiable)
        {
            if (!identifiable.StructureType.IsMaintainable)
            {
                throw new SdmxNotImplementedException();
            }

            IMaintainableMutableObject maintainable = identifiable as IMaintainableMutableObject;
            if (maintainable == null)
            {
                throw new SdmxNotImplementedException();
            }

            return new StructureReferenceImpl(maintainable.AgencyId, maintainable.Id, maintainable.Version, maintainable.StructureType);
        }

        /// <summary>
        /// Gets the parents.
        /// </summary>
        /// <param name="reference">The reference.</param>
        /// <param name="returnStub">The return stub.</param>
        /// <returns>The parents</returns>
        private IList<IMaintainableMutableObject> GetParents(IStructureReference reference, bool returnStub)
        {
            List<IMaintainableMutableObject> parents = new List<IMaintainableMutableObject>();
            switch (reference.MaintainableStructureEnumType.EnumType)
            {
                case SdmxStructureEnumType.Dsd:
                    // return dataflows. Here we have DSD identification = the DATAFLOW one
                    var dataflows = this._mutableObjectRetrievalManager.GetMutableDataflowObjects(reference, false, returnStub);

                    parents.AddRange(dataflows);
                    break;

                case SdmxStructureEnumType.ConceptScheme:
                    {
                        // return DSD. All DSD in this example use the same concept scheme so we return everything
                        var dsds = this._mutableObjectRetrievalManager.GetMutableDataStructureObjects(new MaintainableRefObjectImpl(), false, returnStub);

                        parents.AddRange(dsds);
                    }

                    break;
                case SdmxStructureEnumType.CodeList:
                    // return DSD, a more complex example where we assume we might have more than one DSD
                    for (int i = 0; i < SampleStructuralMetadata.GetComponentCount(); i++)
                    {
                        var codelistId = SampleStructuralMetadata.GetCodelistIdFromComponent(i);
                        if (codelistId.Equals(reference.MaintainableId))
                        {
                            var dsdReference = new MaintainableRefObjectImpl(null, SampleStructuralMetadata.GetDsdIdFromComponent(i), null);
                            var dsds = this._mutableObjectRetrievalManager.GetMutableDataStructureObjects(dsdReference, false, returnStub);
                            parents.AddRange(dsds);
                        }
                    }

                    break;
                case SdmxStructureEnumType.Dataflow:
                    // normally categorisations, provision agreement, content constraint but we don't support those in this sample
                    break;
            }

            return parents;
        }

        /// <summary>
        /// Gets the children.
        /// </summary>
        /// <param name="reference">
        /// The reference.
        /// </param>
        /// <param name="returnStub">
        /// if set to <c>true</c> [return stub].
        /// </param>
        /// <returns>
        /// Returns the children
        /// </returns>
        private IList<IMaintainableMutableObject> GetChildren(IStructureReference reference, bool returnStub)
        {
            var children = new List<IMaintainableMutableObject>();
            switch (reference.MaintainableStructureEnumType.EnumType)
            {
                case SdmxStructureEnumType.Dsd:
                    // get concept scheme and codelist

                    // get all referenced codelists. 
                    this.GetCodelistsForDsd(reference, returnStub, children);

                    // get the one concept scheme we have in the sample
                    children.AddRange(this._mutableObjectRetrievalManager.GetMutableConceptSchemeObjects(new MaintainableRefObjectImpl(), false, returnStub));

                    break;
                case SdmxStructureEnumType.Dataflow:
                    // get dsd
                    children.AddRange(this._mutableObjectRetrievalManager.GetMutableDataStructureObjects(reference, false, returnStub));
                    break;
            }

            return children;
        }

        /// <summary>
        /// Gets the codelists for DSD.
        /// </summary>
        /// <param name="reference">
        /// The reference.
        /// </param>
        /// <param name="returnStub">
        /// if set to <c>true</c> [return stub].
        /// </param>
        /// <param name="children">
        /// The children.
        /// </param>
        private void GetCodelistsForDsd(
            IStructureReference reference,
            bool returnStub,
            List<IMaintainableMutableObject> children)
        {
            for (int i = 0; i < SampleStructuralMetadata.GetComponentCount(); i++)
            {
                var dsdId = SampleStructuralMetadata.GetDsdIdFromComponent(i);
                if (dsdId.Equals(reference.MaintainableId))
                {
                    var codelistReference = new MaintainableRefObjectImpl(null, SampleStructuralMetadata.GetCodelistIdFromComponent(i), null);
                    var codelists = this._mutableObjectRetrievalManager.GetMutableCodelistObjects(codelistReference, false, returnStub);

                    children.AddRange(codelists);
                }
            }
        }

        /// <summary>
        /// Gets the structures of a specific type specified in <paramref name="specificReferences" />.
        /// </summary>
        /// <param name="reference">The reference.</param>
        /// <param name="returnStub">if set to <c>true</c> [return stub].</param>
        /// <param name="specificReferences">The specific references.</param>
        /// <returns>
        /// The list of specific structures
        /// </returns>
        private IList<IMaintainableMutableObject> GetSpecificFromParentsOrSiblings(
            IStructureReference reference,
            bool returnStub,
            params SdmxStructureType[] specificReferences)
        {
            var specific = new List<IMaintainableMutableObject>();
            foreach (var specificReference in specificReferences)

            {
                switch (reference.MaintainableStructureEnumType.EnumType)
                {
                    case SdmxStructureEnumType.Dsd:
                        switch (specificReference.EnumType)
                        {
                            case SdmxStructureEnumType.Dataflow:
                                var dataflows = this.GetParents(reference, returnStub);
                                specific.AddRange(dataflows);
                                break;
                        }

                        break;
                   
                    case SdmxStructureEnumType.CodeList:
                    case SdmxStructureEnumType.ConceptScheme:
                        switch (specificReference.EnumType)
                        {
                            case SdmxStructureEnumType.CodeList:
                            case SdmxStructureEnumType.ConceptScheme:
                                {
                                    // 1st get parent and then siblings
                                    var parents = this.GetParents(reference, true);

                                    var collection =
                                        parents.SelectMany(
                                            parent =>
                                            this.GetChildren(GetAsStructureReference(parent), returnStub)
                                                .Where(o => o.StructureType == specificReference));
                                    specific.AddRange(collection);
                                    break;
                                }

                            case SdmxStructureEnumType.Dsd:
                                {
                                    specific.AddRange(
                                        this.GetParents(reference, returnStub)
                                            .Where(o => o.StructureType == specificReference));
                                    break;
                                }
                        }

                        break;
                }
            }

            return specific;
        }

        /// <summary>
        /// Gets the structures of a specific type specified in <paramref name="specificReferences" />.
        /// </summary>
        /// <param name="reference">The reference.</param>
        /// <param name="returnStub">if set to <c>true</c> [return stub].</param>
        /// <param name="specificReferences">The specific references.</param>
        /// <returns>
        /// The list of specific structures
        /// </returns>
        private IList<IMaintainableMutableObject> GetSpecificFromDescendants(
            IStructureReference reference,
            bool returnStub,
            params SdmxStructureType[] specificReferences)
        {
            var specific = new List<IMaintainableMutableObject>();
            foreach (var specificReference in specificReferences)

            {
                switch (reference.MaintainableStructureEnumType.EnumType)
                {
                    case SdmxStructureEnumType.Dsd:
                        switch (specificReference.EnumType)
                        {
                            case SdmxStructureEnumType.ConceptScheme:
                            case SdmxStructureEnumType.CodeList:

                                var children =
                                    this.GetChildren(reference, returnStub).Where(o => o.StructureType == specificReference);
                                specific.AddRange(children);
                                break;
                        }

                        break;
                    case SdmxStructureEnumType.Dataflow:
                        switch (specificReference.EnumType)
                        {
                            case SdmxStructureEnumType.Dsd:
                                specific.AddRange(
                                    this.GetChildren(reference, returnStub)
                                        .Where(o => o.StructureType.EnumType == SdmxStructureEnumType.Dsd));
                                break;
                            case SdmxStructureEnumType.ConceptScheme:
                            case SdmxStructureEnumType.CodeList:
                                {
                                    var dsd =
                                        this.GetChildren(reference, returnStub)
                                            .Where(o => o.StructureType.EnumType == SdmxStructureEnumType.Dsd);
                                    var collection = dsd.SelectMany(d => this.GetChildren(GetAsStructureReference(d), returnStub)
                                                                                 .Where(o => o.StructureType.EnumType == specificReference));
                                    specific.AddRange(collection);
                                    break;
                                }
                        }

                        break;
                }
            }

            return specific;
        }
    }
}