﻿namespace SampleRetrieverPlugin.Manager
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Process;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;

    using SampleRetrieverPlugin.Constant;

    /// <summary>
    /// A sample Mutable object retriever. Note while this sample contains support only for DSD, Dataflow, Concept Scheme and CodeLists
    /// </summary>
    /// <seealso cref="Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Mutable.ISdmxMutableObjectRetrievalManager" />
    public class SampleMutableObjectRetriever : ISdmxMutableObjectRetrievalManager
    {
        /// <summary>
        /// Gets a single Agency Scheme, this expects the ref object either to contain
        /// a URN or all the attributes required to uniquely identify the object.  If version information
        /// is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">
        /// The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        /// if set to <c>true</c> [return latest].
        /// </param>
        /// <param name="returnStub">
        /// The return Stub.
        /// </param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base.IAgencySchemeMutableObject"/> .
        /// </returns>
        public IAgencySchemeMutableObject GetMutableAgencyScheme(
            IMaintainableRefObject maintainableReference,
            bool returnLatest,
            bool returnStub)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets AgencySchemeMutableObject that match the parameters in the ref @object.  If the ref @object is null or
        /// has no attributes set, then this will be interpreted as a search for all CodelistObjects
        /// </summary>
        /// <param name="maintainableReference">
        /// The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        /// The return Latest.
        /// </param>
        /// <param name="returnStub">
        /// The return Stub.
        /// </param>
        /// <returns>
        /// list of sdmxObjects that match the search criteria
        /// </returns>
        public ISet<IAgencySchemeMutableObject> GetMutableAgencySchemeObjects(
            IMaintainableRefObject maintainableReference,
            bool returnLatest,
            bool returnStub)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets a single Categorisation, this expects the ref object either to contain
        /// a URN or all the attributes required to uniquely identify the object.  If version information
        /// is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">
        /// The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        /// if set to <c>true</c> [return latest].
        /// </param>
        /// <param name="returnStub">
        /// The return Stub.
        /// </param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme.ICategorisationMutableObject"/> .
        /// </returns>
        public ICategorisationMutableObject GetMutableCategorisation(
            IMaintainableRefObject maintainableReference,
            bool returnLatest,
            bool returnStub)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets CategorisationObjects that match the parameters in the ref @object.  If the ref @object is null or
        /// has no attributes set, then this will be interpreted as a search for all CodelistObjects
        /// </summary>
        /// <param name="maintainableReference">
        /// The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        /// The return Latest.
        /// </param>
        /// <param name="returnStub">
        /// The return Stub.
        /// </param>
        /// <returns>
        /// list of sdmxObjects that match the search criteria
        /// </returns>
        public ISet<ICategorisationMutableObject> GetMutableCategorisationObjects(
            IMaintainableRefObject maintainableReference,
            bool returnLatest,
            bool returnStub)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets a single CategoryScheme , this expects the ref object either to contain
        /// a URN or all the attributes required to uniquely identify the object.  If version information
        /// is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">
        /// The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        /// if set to <c>true</c> [return latest].
        /// </param>
        /// <param name="returnStub">
        /// The return Stub.
        /// </param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme.ICategorySchemeMutableObject"/> .
        /// </returns>
        public ICategorySchemeMutableObject GetMutableCategoryScheme(
            IMaintainableRefObject maintainableReference,
            bool returnLatest,
            bool returnStub)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets CategorySchemeObjects that match the parameters in the ref @object.  If the ref @object is null or
        /// has no attributes set, then this will be interpreted as a search for all CategorySchemeObjects
        /// </summary>
        /// <param name="maintainableReference">
        /// The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        /// The return Latest.
        /// </param>
        /// <param name="returnStub">
        /// The return Stub.
        /// </param>
        /// <returns>
        /// list of sdmxObjects that match the search criteria
        /// </returns>
        public ISet<ICategorySchemeMutableObject> GetMutableCategorySchemeObjects(
            IMaintainableRefObject maintainableReference,
            bool returnLatest,
            bool returnStub)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets a single CodeList , this expects the ref object either to contain
        /// a URN or all the attributes required to uniquely identify the object.  If version information
        /// is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">
        /// The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        /// if set to <c>true</c> [return latest].
        /// </param>
        /// <param name="returnStub">
        /// The return Stub.
        /// </param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist.ICodelistMutableObject"/> .
        /// </returns>
        public ICodelistMutableObject GetMutableCodelist(
            IMaintainableRefObject maintainableReference,
            bool returnLatest,
            bool returnStub)
        {
            return this.GetMutableCodelistObjects(maintainableReference, returnLatest, returnStub).FirstOrDefault();
        }

        /// <summary>
        /// Gets CodelistObjects that match the parameters in the ref @object.  If the ref @object is null or
        /// has no attributes set, then this will be interpreted as a search for all CodelistObjects
        /// </summary>
        /// <param name="maintainableReference">
        /// The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        /// The return Latest.
        /// </param>
        /// <param name="returnStub">
        /// The return Stub.
        /// </param>
        /// <returns>
        /// list of sdmxObjects that match the search criteria
        /// </returns>
        public ISet<ICodelistMutableObject> GetMutableCodelistObjects(
            IMaintainableRefObject maintainableReference,
            bool returnLatest,
            bool returnStub)
        {
            var codelists =
                SampleStructuralMetadata.GetArtefact<CodelistMutableCore>(
                    SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CodeList),
                    maintainableReference);
            if (!returnStub)
            {
                foreach (var codelist in codelists)
                {
                    SampleStructuralMetadata.AddChildren(codelist);
                }
            }
            else
            {
                SetupStub(codelists);
            }

            return new HashSet<ICodelistMutableObject>(codelists);
        }

        /// <summary>
        /// Gets ConceptSchemeObjects that match the parameters in the ref @object.  If the ref @object is null or
        /// has no attributes set, then this will be interpreted as a search for all ConceptSchemeObjects
        /// </summary>
        /// <param name="maintainableReference">
        /// The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        /// The return Latest.
        /// </param>
        /// <param name="returnStub">
        /// The return Stub.
        /// </param>
        /// <returns>
        /// list of sdmxObjects that match the search criteria
        /// </returns>
        public ISet<IConceptSchemeMutableObject> GetMutableConceptSchemeObjects(
            IMaintainableRefObject maintainableReference,
            bool returnLatest,
            bool returnStub)
        {
            var artefacts =
                SampleStructuralMetadata.GetArtefact<ConceptSchemeMutableCore>(
                    SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ConceptScheme),
                    maintainableReference);
            if (!returnStub)
            {
                foreach (var artefact in artefacts)
                {
                    SampleStructuralMetadata.AddChildren(artefact);
                }
            }

            return new HashSet<IConceptSchemeMutableObject>(artefacts);
        }

        /// <summary>
        /// Returns a single Content Constraint, this expects the ref object either to contain
        /// a URN or all the attributes required to uniquely identify the object.  If version information
        /// is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">
        /// The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        /// if set to <c>true</c> [return latest].
        /// </param>
        /// <param name="returnStub">
        /// The return Stub.
        /// </param>
        /// <returns>
        /// The Content constraint.
        /// </returns>
        public IContentConstraintMutableObject GetMutableContentConstraint(
            IMaintainableRefObject maintainableReference,
            bool returnLatest,
            bool returnStub)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Returns ContentConstraintBeans that match the parameters in the ref bean.  If the ref bean is null or
        /// has no attributes set, then this will be interpreted as a search for all ContentConstraintObjects
        /// </summary>
        /// <param name="maintainableReference">
        /// the reference object defining the search parameters, can be empty or null
        /// </param>
        /// <param name="returnLatest">
        /// The return Latest.
        /// </param>
        /// <param name="returnStub">
        /// The return Stub.
        /// </param>
        /// <returns>
        /// list of objects that match the search criteria
        /// </returns>
        public ISet<IContentConstraintMutableObject> GetMutableContentConstraintObjects(
            IMaintainableRefObject maintainableReference,
            bool returnLatest,
            bool returnStub)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets a single data consumer scheme, this expects the ref object either to contain
        /// a URN or all the attributes required to uniquely identify the object.  If version information
        /// is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">
        /// The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        /// if set to <c>true</c> [return latest].
        /// </param>
        /// <param name="returnStub">
        /// The return Stub.
        /// </param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base.IDataConsumerSchemeMutableObject"/> .
        /// </returns>
        public IDataConsumerSchemeMutableObject GetMutableDataConsumerScheme(
            IMaintainableRefObject maintainableReference,
            bool returnLatest,
            bool returnStub)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets DataConsumerSchemeMutableObjects that match the parameters in the ref @object.  If the ref @object is null or
        /// has no attributes set, then this will be interpreted as a search for all CodelistObjects
        /// </summary>
        /// <param name="maintainableReference">
        /// The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        /// The return Latest.
        /// </param>
        /// <param name="returnStub">
        /// The return Stub.
        /// </param>
        /// <returns>
        /// list of sdmxObjects that match the search criteria
        /// </returns>
        public ISet<IDataConsumerSchemeMutableObject> GetMutableDataConsumerSchemeObjects(
            IMaintainableRefObject maintainableReference,
            bool returnLatest,
            bool returnStub)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets a single Dataflow , this expects the ref object either to contain
        /// a URN or all the attributes required to uniquely identify the object.  If version information
        /// is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">
        /// The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        /// if set to <c>true</c> [return latest].
        /// </param>
        /// <param name="returnStub">
        /// The return Stub.
        /// </param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure.IDataflowMutableObject"/> .
        /// </returns>
        public IDataflowMutableObject GetMutableDataflow(
            IMaintainableRefObject maintainableReference,
            bool returnLatest,
            bool returnStub)
        {
            return this.GetMutableDataflowObjects(maintainableReference, returnLatest, returnStub).FirstOrDefault();
        }

        /// <summary>
        /// Gets DataflowObjects that match the parameters in the ref @object.  If the ref @object is null or
        /// has no attributes set, then this will be interpreted as a search for all DataflowObjects
        /// </summary>
        /// <param name="maintainableReference">
        /// The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        /// The return Latest.
        /// </param>
        /// <param name="returnStub">
        /// The return Stub.
        /// </param>
        /// <returns>
        /// list of sdmxObjects that match the search criteria
        /// </returns>
        public ISet<IDataflowMutableObject> GetMutableDataflowObjects(
            IMaintainableRefObject maintainableReference,
            bool returnLatest,
            bool returnStub)
        {
            var artefacts =
                SampleStructuralMetadata.GetArtefact<DataflowMutableCore>(
                    SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dsd),
                    maintainableReference);
            if (!returnStub)
            {
                foreach (var artefact in artefacts)
                {
                    SampleStructuralMetadata.BuildDsdReference(artefact);
                }
            }

            return new HashSet<IDataflowMutableObject>(artefacts);
        }

        /// <summary>
        /// Gets a single Data Provider Scheme, this expects the ref object either to contain
        /// a URN or all the attributes required to uniquely identify the object.  If version information
        /// is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">
        /// The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        /// if set to <c>true</c> [return latest].
        /// </param>
        /// <param name="returnStub">
        /// The return Stub.
        /// </param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base.IDataProviderSchemeMutableObject"/> .
        /// </returns>
        public IDataProviderSchemeMutableObject GetMutableDataProviderScheme(
            IMaintainableRefObject maintainableReference,
            bool returnLatest,
            bool returnStub)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets DataProviderSchemeMutableObjects that match the parameters in the ref @object.  If the ref @object is null or
        /// has no attributes set, then this will be interpreted as a search for all CodelistObjects
        /// </summary>
        /// <param name="maintainableReference">
        /// The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        /// The return Latest.
        /// </param>
        /// <param name="returnStub">
        /// The return Stub.
        /// </param>
        /// <returns>
        /// list of sdmxObjects that match the search criteria
        /// </returns>
        public ISet<IDataProviderSchemeMutableObject> GetMutableDataProviderSchemeObjects(
            IMaintainableRefObject maintainableReference,
            bool returnLatest,
            bool returnStub)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets a single DataStructure.
        /// This expects the ref object either to contain a URN or all the attributes required to uniquely identify the object.
        /// If version information is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">
        /// The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        /// if set to <c>true</c> [return latest].
        /// </param>
        /// <param name="returnStub">
        /// The return Stub.
        /// </param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure.IDataStructureMutableObject"/> .
        /// </returns>
        public IDataStructureMutableObject GetMutableDataStructure(
            IMaintainableRefObject maintainableReference,
            bool returnLatest,
            bool returnStub)
        {
            return this.GetMutableDataStructureObjects(maintainableReference, returnLatest, returnStub).FirstOrDefault();
        }

        /// <summary>
        /// Gets DataStructureObjects that match the parameters in the ref @object.  If the ref @object is null or
        /// has no attributes set, then this will be interpreted as a search for all dataStructureObjects
        /// </summary>
        /// <param name="maintainableReference">
        /// The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        /// The return Latest.
        /// </param>
        /// <param name="returnStub">
        /// The return Stub.
        /// </param>
        /// <returns>
        /// list of sdmxObjects that match the search criteria
        /// </returns>
        public ISet<IDataStructureMutableObject> GetMutableDataStructureObjects(
            IMaintainableRefObject maintainableReference,
            bool returnLatest,
            bool returnStub)
        {
            var artefacts =
                SampleStructuralMetadata.GetArtefact<DataStructureMutableCore>(
                    SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dsd),
                    maintainableReference);
            if (!returnStub)
            {
                foreach (var artefact in artefacts)
                {
                    SampleStructuralMetadata.AddChildren(artefact);
                }
            }

            return new HashSet<IDataStructureMutableObject>(artefacts);
        }

        /// <summary>
        /// Gets a single HierarchicCodeList , this expects the ref object either to contain
        /// a URN or all the attributes required to uniquely identify the object.  If version information
        /// is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">
        /// The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        /// if set to <c>true</c> [return latest].
        /// </param>
        /// <param name="returnStub">
        /// The return Stub.
        /// </param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist.IHierarchicalCodelistMutableObject"/> .
        /// </returns>
        public IHierarchicalCodelistMutableObject GetMutableHierarchicCodeList(
            IMaintainableRefObject maintainableReference,
            bool returnLatest,
            bool returnStub)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets HierarchicalCodelistObjects that match the parameters in the ref @object.  If the ref @object is null or
        /// has no attributes set, then this will be interpreted as a search for all HierarchicalCodelistObjects
        /// </summary>
        /// <param name="maintainableReference">
        /// The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        /// The return Latest.
        /// </param>
        /// <param name="returnStub">
        /// The return Stub.
        /// </param>
        /// <returns>
        /// list of sdmxObjects that match the search criteria
        /// </returns>
        public ISet<IHierarchicalCodelistMutableObject> GetMutableHierarchicCodeListObjects(
            IMaintainableRefObject maintainableReference,
            bool returnLatest,
            bool returnStub)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets a set of maintainable objects which includes the maintainable being queried for, defined by the
        /// StructureQueryObject parameter.
        /// <p/>
        /// Expects only ONE maintainable to be returned from this query
        /// </summary>
        /// <param name="query">
        /// The query.
        /// </param>
        /// <param name="returnLatest">
        /// if set to <c>true</c> [return latest].
        /// </param>
        /// <param name="returnStub">
        /// The return Stub.
        /// </param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base.IMaintainableMutableObject"/> .
        /// </returns>
        public IMaintainableMutableObject GetMutableMaintainable(
            IStructureReference query,
            bool returnLatest,
            bool returnStub)
        {
            return this.GetMutableMaintainables(query, returnLatest, returnStub).FirstOrDefault();
        }

        /// <summary>
        /// Gets a set of maintainable objects which includes the maintainable being queried for, defined by the
        /// StructureQueryObject parameter.
        /// </summary>
        /// <param name="query">
        /// The query.
        /// </param>
        /// <param name="returnLatest">
        /// The return Latest.
        /// </param>
        /// <param name="returnStub">
        /// The return Stub.
        /// </param>
        /// <returns>
        /// The <see cref="T:System.Collections.Generic.ISet`1"/> .
        /// </returns>
        public ISet<IMaintainableMutableObject> GetMutableMaintainables(
            IStructureReference query,
            bool returnLatest,
            bool returnStub)
        {
            switch (query.MaintainableStructureEnumType.EnumType)
            {
                case SdmxStructureEnumType.Dsd:
                    return
                        new HashSet<IMaintainableMutableObject>(
                            this.GetMutableDataStructureObjects(query, returnLatest, returnStub));
                case SdmxStructureEnumType.Dataflow:
                    return
                        new HashSet<IMaintainableMutableObject>(
                            this.GetMutableDataflowObjects(query, returnLatest, returnStub));
                case SdmxStructureEnumType.CodeList:
                    return
                        new HashSet<IMaintainableMutableObject>(
                            this.GetMutableCodelistObjects(query, returnLatest, returnStub));
                case SdmxStructureEnumType.ConceptScheme:
                    return
                        new HashSet<IMaintainableMutableObject>(
                            this.GetMutableConceptSchemeObjects(query, returnLatest, returnStub));
                default:
                    throw new SdmxNotImplementedException(
                        "Not implememented. Queries against  " + query.TargetReference);
            }
        }

        /// <summary>
        /// Gets a single Metadataflow , this expects the ref object either to contain
        /// a URN or all the attributes required to uniquely identify the object.  If version information
        /// is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">
        /// The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        /// if set to <c>true</c> [return latest].
        /// </param>
        /// <param name="returnStub">
        /// The return Stub.
        /// </param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.MetadataStructure.IMetadataFlowMutableObject"/> .
        /// </returns>
        public IMetadataFlowMutableObject GetMutableMetadataflow(
            IMaintainableRefObject maintainableReference,
            bool returnLatest,
            bool returnStub)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets MetadataFlowObjects that match the parameters in the ref @object.  If the ref @object is null or
        /// has no attributes set, then this will be interpreted as a search for all MetadataFlowObjects
        /// </summary>
        /// <param name="maintainableReference">
        /// The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        /// The return Latest.
        /// </param>
        /// <param name="returnStub">
        /// The return Stub.
        /// </param>
        /// <returns>
        /// list of sdmxObjects that match the search criteria
        /// </returns>
        public ISet<IMetadataFlowMutableObject> GetMutableMetadataflowObjects(
            IMaintainableRefObject maintainableReference,
            bool returnLatest,
            bool returnStub)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets a single MetadataStructure , this expects the ref object either to contain
        /// a URN or all the attributes required to uniquely identify the object.  If version information
        /// is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">
        /// The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        /// if set to <c>true</c> [return latest].
        /// </param>
        /// <param name="returnStub">
        /// The return Stub.
        /// </param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.MetadataStructure.IMetadataStructureDefinitionMutableObject"/> .
        /// </returns>
        public IMetadataStructureDefinitionMutableObject GetMutableMetadataStructure(
            IMaintainableRefObject maintainableReference,
            bool returnLatest,
            bool returnStub)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets MetadataStructureObjects that match the parameters in the ref @object.  If the ref @object is null or
        /// has no attributes set, then this will be interpreted as a search for all MetadataStructureObjects
        /// </summary>
        /// <param name="maintainableReference">
        /// The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        /// The return Latest.
        /// </param>
        /// <param name="returnStub">
        /// The return Stub.
        /// </param>
        /// <returns>
        /// list of sdmxObjects that match the search criteria
        /// </returns>
        public ISet<IMetadataStructureDefinitionMutableObject> GetMutableMetadataStructureObjects(
            IMaintainableRefObject maintainableReference,
            bool returnLatest,
            bool returnStub)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets a single organization scheme, this expects the ref object either to contain
        /// a URN or all the attributes required to uniquely identify the object.  If version information
        /// is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">
        /// The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        /// if set to <c>true</c> [return latest].
        /// </param>
        /// <param name="returnStub">
        /// The return Stub.
        /// </param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base.IOrganisationUnitSchemeMutableObject"/> .
        /// </returns>
        public IOrganisationUnitSchemeMutableObject GetMutableOrganisationUnitScheme(
            IMaintainableRefObject maintainableReference,
            bool returnLatest,
            bool returnStub)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets OrganisationUnitSchemeMutableObject that match the parameters in the ref @object.  If the ref @object is null
        /// or
        /// has no attributes set, then this will be interpreted as a search for all OrganisationUnitSchemeMutableObject
        /// </summary>
        /// <param name="maintainableReference">
        /// The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        /// The return Latest.
        /// </param>
        /// <param name="returnStub">
        /// The return Stub.
        /// </param>
        /// <returns>
        /// list of sdmxObjects that match the search criteria
        /// </returns>
        public ISet<IOrganisationUnitSchemeMutableObject> GetMutableOrganisationUnitSchemeObjects(
            IMaintainableRefObject maintainableReference,
            bool returnLatest,
            bool returnStub)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets a process @object, this expects the ref object either to contain
        /// a URN or all the attributes required to uniquely identify the object.  If version information
        /// is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">
        /// The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        /// if set to <c>true</c> [return latest].
        /// </param>
        /// <param name="returnStub">
        /// The return Stub.
        /// </param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.Process.IProcessMutableObject"/> .
        /// </returns>
        public IProcessMutableObject GetMutableProcessObject(
            IMaintainableRefObject maintainableReference,
            bool returnLatest,
            bool returnStub)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets ProcessObjects that match the parameters in the ref @object.  If the ref @object is null or
        /// has no attributes set, then this will be interpreted as a search for all IProcessObject
        /// </summary>
        /// <param name="maintainableReference">
        /// The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        /// The return Latest.
        /// </param>
        /// <param name="returnStub">
        /// The return Stub.
        /// </param>
        /// <returns>
        /// list of sdmxObjects that match the search criteria
        /// </returns>
        public ISet<IProcessMutableObject> GetMutableProcessObjects(
            IMaintainableRefObject maintainableReference,
            bool returnLatest,
            bool returnStub)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Returns a provision agreement bean, this expects the ref object to contain
        /// all the attributes required to uniquely identify the object.  If version information
        /// is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">
        /// The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        /// if set to <c>true</c> [return latest].
        /// </param>
        /// <param name="returnStub">
        /// The return Stub.
        /// </param>
        /// <returns>
        /// list of sdmxObjects that match the search criteria
        /// </returns>
        public IProvisionAgreementMutableObject GetMutableProvisionAgreement(
            IMaintainableRefObject maintainableReference,
            bool returnLatest,
            bool returnStub)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Returns ProvisionAgreement beans that match the parameters in the ref bean. If the ref bean is null or
        /// has no attributes set, then this will be interpreted as a search for all ProvisionAgreement beans.
        /// </summary>
        /// <param name="maintainableReference">
        /// the reference object defining the search parameters, can be empty or null
        /// </param>
        /// <param name="returnLatest">
        /// The return Latest.
        /// </param>
        /// <param name="returnStub">
        /// The return Stub.
        /// </param>
        /// <returns>
        /// list of objects that match the search criteria
        /// </returns>
        public ISet<IProvisionAgreementMutableObject> GetMutableProvisionAgreementObjects(
            IMaintainableRefObject maintainableReference,
            bool returnLatest,
            bool returnStub)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets a reporting taxonomy @object, this expects the ref object either to contain
        /// a URN or all the attributes required to uniquely identify the object.  If version information
        /// is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">
        /// The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        /// if set to <c>true</c> [return latest].
        /// </param>
        /// <param name="returnStub">
        /// The return Stub.
        /// </param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme.IReportingTaxonomyMutableObject"/> .
        /// </returns>
        public IReportingTaxonomyMutableObject GetMutableReportingTaxonomy(
            IMaintainableRefObject maintainableReference,
            bool returnLatest,
            bool returnStub)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets ReportingTaxonomyObjects that match the parameters in the ref @object.  If the ref @object is null or
        /// has no attributes set, then this will be interpreted as a search for all ReportingTaxonomyObjects
        /// </summary>
        /// <param name="maintainableReference">
        /// The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        /// The return Latest.
        /// </param>
        /// <param name="returnStub">
        /// The return Stub.
        /// </param>
        /// <returns>
        /// list of sdmxObjects that match the search criteria
        /// </returns>
        public ISet<IReportingTaxonomyMutableObject> GetMutableReportingTaxonomyObjects(
            IMaintainableRefObject maintainableReference,
            bool returnLatest,
            bool returnStub)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets a structure set @object, this expects the ref object either to contain
        /// a URN or all the attributes required to uniquely identify the object.  If version information
        /// is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">
        /// The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        /// if set to <c>true</c> [return latest].
        /// </param>
        /// <param name="returnStub">
        /// The return Stub.
        /// </param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.Mapping.IStructureSetMutableObject"/> .
        /// </returns>
        public IStructureSetMutableObject GetMutableStructureSet(
            IMaintainableRefObject maintainableReference,
            bool returnLatest,
            bool returnStub)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets StructureSetObjects that match the parameters in the ref @object.  If the ref @object is null or
        /// has no attributes set, then this will be interpreted as a search for all StructureSetObjects
        /// </summary>
        /// <param name="maintainableReference">
        /// The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        /// The return Latest.
        /// </param>
        /// <param name="returnStub">
        /// The return Stub.
        /// </param>
        /// <returns>
        /// list of sdmxObjects that match the search criteria
        /// </returns>
        public ISet<IStructureSetMutableObject> GetMutableStructureSetObjects(
            IMaintainableRefObject maintainableReference,
            bool returnLatest,
            bool returnStub)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets a single ConceptScheme , this expects the ref object either to contain
        /// a URN or all the attributes required to uniquely identify the object.  If version information
        /// is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">
        /// The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        /// if set to <c>true</c> [return latest].
        /// </param>
        /// <param name="returnStub">
        /// The return Stub.
        /// </param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.ConceptScheme.IConceptSchemeMutableObject"/> .
        /// </returns>
        public IConceptSchemeMutableObject GetMutableConceptScheme(
            IMaintainableRefObject maintainableReference,
            bool returnLatest,
            bool returnStub)
        {
            return this.GetMutableConceptSchemeObjects(maintainableReference, returnLatest, returnStub).FirstOrDefault();
        }

        /// <summary>
        /// Setups the specified <paramref name="maintainableMutableObjects"/> as stub.
        /// </summary>
        /// <param name="maintainableMutableObjects">
        /// The maintainable mutable objects.
        /// </param>
        private static void SetupStub(IEnumerable<IMaintainableMutableObject> maintainableMutableObjects)
        {
            foreach (var mutableCore in maintainableMutableObjects)
            {
                mutableCore.Stub = true;
                mutableCore.StructureURL = new Uri("http://changeme.net");
            }
        }
    }
}