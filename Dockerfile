#https://docs.microsoft.com/en-us/aspnet/core/host-and-deploy/docker/building-net-docker-images?view=aspnetcore-3.1
#https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet-build
#https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet-publish

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /app

ARG CONFIG
 
COPY NSIWebServices.sln Directory.Build.props GlobalAssemblyInfo.cs ./
COPY docke[r]/${CONFIG}/config  ./config
COPY docke[r]/${CONFIG}/NuGet.Config  ./
COPY src ./src

RUN dotnet build -c Release -p:WarningsAsErrors= -nowarn:NU1605
RUN dotnet publish /app/src/NSIWebServiceCore -c Release -o /out --no-build -p:WarningsAsErrors= -nowarn:NU1605
RUN ls /out/Plugins/*.dll
# Copy must be done before starting the app
RUN if [ -d config ]; then echo 'Using config' ; cp -a config/* /out/config/ ; cp /out/config/app.config /out/NSIWebServiceCore.dll.config; fi

FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS runtime
WORKDIR /app
COPY --from=build /out .

ENTRYPOINT ["dotnet", "NSIWebServiceCore.dll"]
