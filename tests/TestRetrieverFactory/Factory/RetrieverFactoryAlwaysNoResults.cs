// -----------------------------------------------------------------------
// <copyright file="RetrieverFactoryTest1.cs" company="EUROSTAT">
//   Date Created : 2015-12-23
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Test.Factory
{
    using System.Collections.Generic;
    using System.Security.Principal;

    using Estat.Sdmxsource.Extension.Factory;
    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Nsi.StructureRetriever.Manager;
    using Estat.Nsi.StructureRetriever.Model;
    using Estat.Sri.MappingStoreRetrieval.Factory;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.Ws.Test.Manager;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Data;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.DataParser.Manager;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager.CrossReference;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;

    internal class RetrieverFactoryAlwaysNoResults : IRetrieverFactory
    {
        #region Fields
        private readonly string _id;

        /// <summary>
        ///     The _mutable structure search manager
        /// </summary>
        private readonly SampleStructureSearchManager _mutableStructureSearchManager;

        /// <summary>
        ///     The _sample advanced mutable structure search manager
        /// </summary>
        private readonly SampleAdvancedMutableStructureSearchManager _sampleAdvancedMutableStructureSearchManager;

        /// <summary>
        ///     The _sample authentication advanced structure search manager
        /// </summary>
        private readonly SampleAuthAdvancedStructureSearchManager _sampleAuthAdvancedStructureSearchManager;

        /// <summary>
        ///     The _SDMX object retrieval manager
        /// </summary>
        private readonly ISdmxObjectRetrievalManager _sdmxObjectRetrievalManager;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public RetrieverFactoryAlwaysNoResults()
        {
            this._id = "ImplementsNothing";
            MappingStoreIoc.Register<RetrievalEngineContainerFactory>(this._id);

            ISdmxObjects sdmxObjects = new SdmxObjectsImpl();

            InMemoryRetrievalManager retrievalManager = new InMemoryRetrievalManager(sdmxObjects);
            retrievalManager.CrossReferenceRetrievalManager = new CrossReferencedRetrievalManager(retrievalManager);
            this._sdmxObjectRetrievalManager = retrievalManager;
            this._mutableStructureSearchManager = new SampleStructureSearchManager(this._sdmxObjectRetrievalManager);
            this._sampleAdvancedMutableStructureSearchManager = new SampleAdvancedMutableStructureSearchManager(this._mutableStructureSearchManager);
            this._sampleAuthAdvancedStructureSearchManager = new SampleAuthAdvancedStructureSearchManager(this._sampleAdvancedMutableStructureSearchManager);
        }

        public ISdmxDataRetrievalWithWriter GetDataRetrieval(SdmxSchema sdmxSchema, IPrincipal principal)
        {
            return new SampleDataRetriever(sdmxSchema);
        }

        public ISdmxDataRetrievalWithCrossWriter GetDataRetrievalWithCross(SdmxSchema sdmxSchema, IPrincipal principal)
        {
            return new SampleDataRetrieverCross();
        }

        public IAdvancedSdmxDataRetrievalWithWriter GetAdvancedDataRetrieval(IPrincipal principal)
        {
            return new SampleDataRetriever(SdmxSchemaEnumType.VersionTwoPointOne);
        }

        public IAdvancedMutableStructureSearchManager GetAdvancedMutableStructureSearchManager(IPrincipal principal)
        {
            return this._sampleAdvancedMutableStructureSearchManager;
        }

        public IAdvancedMutableStructureSearchManager GetAuthAdvancedMutableStructureSearch(IList<IMaintainableRefObject> authorizedDataflows)
        {
            return new AdvancedMutableStructureSearchManagerWithAuth(this._sampleAuthAdvancedStructureSearchManager, authorizedDataflows);
        }

        public IMutableStructureSearchManager GetAuthMutableStructureSearch(SdmxSchema sdmxSchema, IList<IMaintainableRefObject> authorizedDataflows)
        {
            return new MutableStructureSearchManagerWithAuth(new SampleAuthStructureSearchManager(this._mutableStructureSearchManager), authorizedDataflows);
        }

        public IMutableStructureSearchManager GetMutableStructureSearch(SdmxSchema sdmxSchema, IPrincipal principal)
        {
            return this._mutableStructureSearchManager;
        }

        public ISdmxObjectRetrievalManager GetSdmxObjectRetrieval(IPrincipal principal)
        {
            return this._sdmxObjectRetrievalManager;
        }

        public int GetNumberOfObservations(IDataQuery dataQuery)
        {
            throw new System.NotImplementedException();
        }

        public IAvailableDataManager GetAvailableDataManager(SdmxSchema sdmxVersion, IPrincipal principal)
        {
            return new AvailableDataManager(new StructureRetrieverSettings()); //throw new System.NotImplementedException();
        }

        public ICommonSdmxObjectRetrievalManager GetCommonSdmxObjectRetrievalManager(IPrincipal principal)
        {
            throw new System.NotImplementedException();
        }

        public ICommonSdmxObjectRetrievalManagerSoap20 GetCommonSdmxObjectRetrievalManagerSoap20(IPrincipal principal)
        {
            throw new System.NotImplementedException();
        }

        public string Id
        {
            get
            {
                return this._id;
            }
        }
    }
}