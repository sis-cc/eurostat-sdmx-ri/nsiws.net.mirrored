// -----------------------------------------------------------------------
// <copyright file="TestWsRetrieverFactory.cs" company="EUROSTAT">
//   Date Created : 2015-12-21
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Test.Factory
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Security.Principal;
    using Estat.Nsi.StructureRetriever.Manager;
    using Estat.Nsi.StructureRetriever.Model;
    using Estat.Sdmxsource.Extension.Factory;
    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sri.MappingStoreRetrieval.Factory;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.Ws.Test.Constant;
    using Estat.Sri.Ws.Test.Manager;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Data;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.DataParser.Manager;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager.CrossReference;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util.Io;

    /// <summary>
    ///     A sample implementation of the <see cref="IRetrieverFactory" />
    /// </summary>
    public class TestWsRetrieverFactory : IRetrieverFactory
    {
        #region Fields

        /// <summary>
        ///     The _mutable structure search manager
        /// </summary>
        private readonly SampleStructureSearchManager _mutableStructureSearchManager;

        /// <summary>
        ///     The _sample advanced mutable structure search manager
        /// </summary>
        private readonly SampleAdvancedMutableStructureSearchManager _sampleAdvancedMutableStructureSearchManager;

        /// <summary>
        ///     The _sample authentication advanced structure search manager
        /// </summary>
        private readonly SampleAuthAdvancedStructureSearchManager _sampleAuthAdvancedStructureSearchManager;

        /// <summary>
        ///     The _SDMX object retrieval manager
        /// </summary>
        private readonly ISdmxObjectRetrievalManager _sdmxObjectRetrievalManager;


        private readonly IAvailableDataManager _availableDataManager;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="TestWsRetrieverFactory" /> class.
        /// </summary>
        public TestWsRetrieverFactory()
        {
            MappingStoreIoc.Register<RetrievalEngineContainerFactory>(this.Id);

            ISdmxObjects sdmxObjects = new SdmxObjectsImpl();
            using (var stream = GetResource("ESTAT+STS+2.0.xml"))
            {
                Merge(stream, sdmxObjects);
            }

            using (var stream = GetResource("ESTAT+DEMOGRAPHY+2.1.xml"))
            {
                Merge(stream, sdmxObjects);
            }

            var sts = sdmxObjects.GetDataStructures(new MaintainableRefObjectImpl(null, "STS", null)).Select(o => o.AsReference).FirstOrDefault();
            var demo = sdmxObjects.GetDataStructures(new MaintainableRefObjectImpl(null, "DEMOGRAPHY", null)).Select(o => o.AsReference).FirstOrDefault();
            sdmxObjects.AddDataflow(BuildDataflow(Dataflows.NoResults, sts));
            sdmxObjects.AddDataflow(BuildDataflow(Dataflows.ResponseExceedsLimit, sts));
            sdmxObjects.AddDataflow(BuildDataflow(Dataflows.ResponseTooLarge, sts));
            sdmxObjects.AddDataflow(BuildDataflow(Dataflows.Semantic, sts));
            sdmxObjects.AddDataflow(BuildDataflow(Dataflows.TimeSeries, sts));
            sdmxObjects.AddDataflow(BuildDataflow(Dataflows.CrossV20Only, demo));

            InMemoryRetrievalManager retrievalManager = new InMemoryRetrievalManager(sdmxObjects);
            retrievalManager.CrossReferenceRetrievalManager = new CrossReferencedRetrievalManager(retrievalManager);
            this._sdmxObjectRetrievalManager = retrievalManager;
            this._mutableStructureSearchManager = new SampleStructureSearchManager(this._sdmxObjectRetrievalManager);
            this._sampleAdvancedMutableStructureSearchManager = new SampleAdvancedMutableStructureSearchManager(this._mutableStructureSearchManager);
            this._sampleAuthAdvancedStructureSearchManager = new SampleAuthAdvancedStructureSearchManager(this._sampleAdvancedMutableStructureSearchManager);
            this._availableDataManager = new SampleAvailableDataManager(sdmxObjects);
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        public string Id
        {
            get
            {
                return "SampleFactory";
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Gets the advanced data retrieval.
        /// </summary>
        /// <param name="principal">The principal.</param>
        /// <returns>The <see cref="IAdvancedSdmxDataRetrievalWithWriter" />.</returns>
        public IAdvancedSdmxDataRetrievalWithWriter GetAdvancedDataRetrieval(IPrincipal principal)
        {
            return new SampleDataRetriever(SdmxSchemaEnumType.VersionTwoPointOne);
        }

        /// <summary>
        ///     Gets the advanced mutable structure search manager.
        /// </summary>
        /// <returns>The <see cref="IAdvancedMutableStructureSearchManager" />.</returns>
        public IAdvancedMutableStructureSearchManager GetAdvancedMutableStructureSearchManager(IPrincipal principal)
        {
            return this._sampleAdvancedMutableStructureSearchManager;
        }

        /// <summary>
        ///     Gets the authentication advanced mutable structure search.
        /// </summary>
        /// <returns>The <see cref="IAuthAdvancedMutableStructureSearchManager" />.</returns>
        public IAdvancedMutableStructureSearchManager GetAuthAdvancedMutableStructureSearch(IList<IMaintainableRefObject> authorizedDataflows)
        {
            return new AdvancedMutableStructureSearchManagerWithAuth(this._sampleAuthAdvancedStructureSearchManager, authorizedDataflows);
        }

        /// <summary>
        ///     Gets the authentication mutable structure search.
        /// </summary>
        /// <param name="sdmxSchema">The SDMX schema.</param>
        /// <returns>The <see cref="IAuthMutableStructureSearchManager" />.</returns>
        public IMutableStructureSearchManager GetAuthMutableStructureSearch(SdmxSchema sdmxSchema, IList<IMaintainableRefObject> authorizedDataflows)
        {
            return new MutableStructureSearchManagerWithAuth(new SampleAuthStructureSearchManager(this._mutableStructureSearchManager), authorizedDataflows);
        }

        /// <summary>
        ///     Gets the data retrieval.
        /// </summary>
        /// <param name="sdmxSchema">The SDMX schema.</param>
        /// <returns>The <see cref="ISdmxDataRetrievalWithWriter" />.</returns>
        public ISdmxDataRetrievalWithWriter GetDataRetrieval(SdmxSchema sdmxSchema, IPrincipal principal)
        {
            return new SampleDataRetriever(sdmxSchema);
        }

        /// <summary>
        ///     Gets the data retrieval with cross.
        /// </summary>
        /// <param name="sdmxSchema">The SDMX schema.</param>
        /// <returns>The <see cref="ISdmxDataRetrievalWithCrossWriter" />.</returns>
        public ISdmxDataRetrievalWithCrossWriter GetDataRetrievalWithCross(SdmxSchema sdmxSchema, IPrincipal principal)
        {
            return new SampleDataRetrieverCross();
        }

        /// <summary>
        ///     Gets the mutable structure search.
        /// </summary>
        /// <param name="sdmxSchema">The SDMX schema.</param>
        /// <returns>The <see cref="IMutableStructureSearchManager" />.</returns>
        public IMutableStructureSearchManager GetMutableStructureSearch(SdmxSchema sdmxSchema, IPrincipal principal)
        {
            return this._mutableStructureSearchManager;
        }

        /// <summary>
        ///     Gets the SDMX object retrieval.
        /// </summary>
        /// <returns>The <see cref="ISdmxObjectRetrievalManager" />.</returns>
        public ISdmxObjectRetrievalManager GetSdmxObjectRetrieval(IPrincipal principal)
        {
            return this._sdmxObjectRetrievalManager;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Builds the dataflow.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="dsdReference">The Data Structure Definition reference.</param>
        /// <returns>The <see cref="IDataflowObject" />.</returns>
        private static IDataflowObject BuildDataflow(string id, IStructureReference dsdReference)
        {
            IDataflowMutableObject dataflow = new DataflowMutableCore { Id = id, AgencyId = "TEST", Version = "1.1", DataStructureRef = dsdReference };
            dataflow.AddName("en", "Test dataflow");
            return dataflow.ImmutableInstance;
        }

        /// <summary>
        /// Gets the resource stream.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns>The <see cref="Stream"/>.</returns>
        private static Stream GetResource(string name)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var xmlFile = string.Format("Estat.Sri.Ws.Test.{0}", name);
            return assembly.GetManifestResourceStream(xmlFile);
        }

        /// <summary>
        ///     Merges the specified file information.
        /// </summary>
        /// <param name="stream">The file information.</param>
        /// <param name="sdmxObjects">The SDMX objects.</param>
        private static void Merge(Stream stream, ISdmxObjects sdmxObjects)
        {
            IStructureWorkspace structureWorkspace;
            StructureParsingManager parsingManager = new StructureParsingManager();
            IReadableDataLocationFactory dataLocationFactory = new ReadableDataLocationFactory();
            using (var readableDataLocation = dataLocationFactory.GetReadableDataLocation(stream))
            {
                structureWorkspace = parsingManager.ParseStructures(readableDataLocation);
            }

            sdmxObjects.Merge(structureWorkspace.GetStructureObjects(false));
        }

        public int GetNumberOfObservations(IDataQuery dataQuery)
        {
            throw new System.NotImplementedException();
        }

        public IAvailableDataManager GetAvailableDataManager(SdmxSchema sdmxVersion, IPrincipal principal)
        {
            return this._availableDataManager;
        }

        public ICommonSdmxObjectRetrievalManager GetCommonSdmxObjectRetrievalManager(IPrincipal principal)
        {
            throw new System.NotImplementedException();
        }

        public ICommonSdmxObjectRetrievalManagerSoap20 GetCommonSdmxObjectRetrievalManagerSoap20(IPrincipal principal)
        {
            throw new System.NotImplementedException();
        }

        #endregion
    }
}