﻿// -----------------------------------------------------------------------
// <copyright file="SampleDataflowAuthorizationEngine.cs" company="EUROSTAT">
//   Date Created : 2016-02-15
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Test.Engine
{
    using System.Collections.Generic;

    using Estat.Sdmxsource.Extension.Constant;
    using Estat.Sdmxsource.Extension.Engine;
    using Estat.Sri.Ws.Test.Constant;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    public class SampleDataflowAuthorizationEngine : IDataflowAuthorizationEngine
    {
        /// <summary>
        /// Gets the authorization.
        /// </summary>
        /// <param name="dataflowReference">The dataflow reference.</param>
        /// <returns>The <see cref="AuthorizationStatus"/>.</returns>
        public AuthorizationStatus GetAuthorization(IMaintainableRefObject dataflowReference)
        {
            return AuthorizationStatus.Authorized;
        }

        /// <summary>
        /// Gets the authorization.
        /// </summary>
        /// <param name="structureReferences">The structure references.</param><param name="permission">The permission.</param>
        /// <returns>
        /// The <see cref="T:Estat.Sdmxsource.Extension.Constant.AuthorizationStatus"/>.
        /// </returns>
        public AuthorizationStatus GetAuthorization(IEnumerable<IStructureReference> structureReferences, PermissionType permission)
        {
            return AuthorizationStatus.Authorized;
        }

        /// <summary>
        /// Gets the authorization.
        /// </summary>
        /// <param name="structureReference">The structure reference.</param><param name="permission">The permission.</param>
        /// <returns>
        /// The <see cref="T:Estat.Sdmxsource.Extension.Constant.AuthorizationStatus"/>.
        /// </returns>
        public AuthorizationStatus GetAuthorization(IStructureReference structureReference, PermissionType permission)
        {
            return AuthorizationStatus.Authorized;
        }

        /// <summary>
        /// Retrieves the authorized dataflows.
        /// </summary>
        /// <returns>The <see cref="IList&lt;IMaintainableRefObject&gt;"/>.</returns>
        public IList<IMaintainableRefObject> RetrieveAuthorizedDataflows()
        {
            List<IMaintainableRefObject> authorizedDataflows = new List<IMaintainableRefObject>();
            var authorizedIds = new[] { Dataflows.CrossV20Only, Dataflows.NoResults, Dataflows.ResponseExceedsLimit, Dataflows.ResponseTooLarge, Dataflows.Semantic, Dataflows.TimeSeries, Dataflows.DoesNotExist };
            for (int i = 0; i < authorizedIds.Length; i++)
            {
                authorizedDataflows.Add(new MaintainableRefObjectImpl(null, authorizedIds[i], "1.0"));
            }

            return authorizedDataflows;
        }
    }
}