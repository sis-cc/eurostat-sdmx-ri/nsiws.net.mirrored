﻿// -----------------------------------------------------------------------
// <copyright file="Dataflows.cs" company="EUROSTAT">
//   Date Created : 2015-12-23
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Test.Constant
{
    /// <summary>
    ///     Class Dataflows.
    /// </summary>
    public static class Dataflows
    {
        #region Constants

        /// <summary>
        ///     The cross V20 only
        /// </summary>
        public const string CrossV20Only = "CROSSV2";

        /// <summary>
        ///     The no results
        /// </summary>
        public const string NoResults = "NO_RESULTS";

        /// <summary>
        ///     The response exceeds limit
        /// </summary>
        public const string ResponseExceedsLimit = "RESPONSE_EXCEEDS_LIMIT";

        /// <summary>
        ///     The response too large
        /// </summary>
        public const string ResponseTooLarge = "RESPONSE_TOO_LARGE";

        /// <summary>
        ///     The semantic
        /// </summary>
        public const string Semantic = "SEMANTIC";

        /// <summary>
        ///     The time series
        /// </summary>
        public const string TimeSeries = "TimeSeries";

        /// <summary>
        /// The does not exist
        /// </summary>
        public const string DoesNotExist = "DOES_NOT_EXIST";

        /// <summary>
        /// The unauthorized
        /// </summary>
        public const string Unauthorized = "NO_AUTH";

        #endregion
    }
}