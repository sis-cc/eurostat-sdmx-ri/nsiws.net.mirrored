﻿// -----------------------------------------------------------------------
// <copyright file="SampleAdvancedMutableStructureSearchManager.cs" company="EUROSTAT">
//   Date Created : 2015-12-22
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Test.Manager
{
    using System.Linq;

    using Estat.Sdmxsource.Extension.Extension;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    /// A sample implementation of <see cref="IAdvancedMutableStructureSearchManager"/>
    /// </summary>
    public class SampleAdvancedMutableStructureSearchManager : IAdvancedMutableStructureSearchManager
    {
        /// <summary>
        /// The _mutable structure search manager
        /// </summary>
        private readonly IMutableStructureSearchManager _mutableStructureSearchManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="SampleAdvancedMutableStructureSearchManager"/> class.
        /// </summary>
        /// <param name="mutableStructureSearchManager">The mutable structure search manager.</param>
        public SampleAdvancedMutableStructureSearchManager(IMutableStructureSearchManager mutableStructureSearchManager)
        {
            this._mutableStructureSearchManager = mutableStructureSearchManager;
        }

        /// <summary>
        /// Gets the maintainables.
        /// </summary>
        /// <param name="structureQuery">The structure query.</param>
        /// <returns>The <see cref="IMutableObjects"/>.</returns>
        public IMutableObjects GetMaintainables(IComplexStructureQuery structureQuery)
        {
            // In this sample we convert the SDMX v2.1 SOAP request to REST.
            var maintainableRefObject = structureQuery.StructureReference.GetMaintainableRefObject();
            var structureReference = new StructureReferenceImpl(maintainableRefObject, structureQuery.StructureReference.ReferencedStructureType);
            StructureQueryDetail structureQueryDetail = Convert(structureQuery.StructureQueryMetadata.StructureQueryDetail, structureQuery.StructureQueryMetadata.ReferencesQueryDetail);

            StructureReferenceDetail reference = structureQuery.StructureQueryMetadata.StructureReferenceDetail;

            SdmxStructureType specificStructureReference = null;
            if (structureQuery.StructureQueryMetadata.ReferenceSpecificStructures != null)
            {
                specificStructureReference = structureQuery.StructureQueryMetadata.ReferenceSpecificStructures.FirstOrDefault();
            }

            bool returnLatest = structureQuery.StructureReference.VersionReference.IsReturnLatest.IsTrue;
            IRestStructureQuery restStructureQuery = new RESTStructureQueryCore(structureQueryDetail, reference, specificStructureReference, structureReference, returnLatest);
            return this._mutableStructureSearchManager.GetMaintainables(restStructureQuery);
        }

        /// <summary>
        /// Converts the specified complex structure query detail.
        /// </summary>
        /// <param name="complexStructureQueryDetail">The complex structure query detail.</param>
        /// <param name="referenceDetail">The reference detail.</param>
        /// <returns>The <see cref="StructureQueryDetail"/>.</returns>
        private static StructureQueryDetail Convert(ComplexStructureQueryDetail complexStructureQueryDetail, ComplexMaintainableQueryDetail referenceDetail)
        {
            StructureQueryDetailEnumType structureQueryDetail;
            switch (complexStructureQueryDetail.EnumType)
            {
                case ComplexStructureQueryDetailEnumType.Full:
                    if (referenceDetail.EnumType == ComplexMaintainableQueryDetailEnumType.Full || referenceDetail.EnumType == ComplexMaintainableQueryDetailEnumType.Null)
                    {
                        structureQueryDetail = StructureQueryDetailEnumType.Full;
                    }
                    else
                    {
                        structureQueryDetail = StructureQueryDetailEnumType.ReferencedStubs;
                    }

                    break;
                case ComplexStructureQueryDetailEnumType.CompleteStub:
                case ComplexStructureQueryDetailEnumType.Stub:
                    structureQueryDetail = StructureQueryDetailEnumType.AllStubs;
                    break;
                default:
                    structureQueryDetail = StructureQueryDetailEnumType.Null;
                    break;
            }

            return StructureQueryDetail.GetFromEnum(structureQueryDetail);
        }
    }
}