// -----------------------------------------------------------------------
// <copyright file="SampleAuthStructureSearchManager.cs" company="EUROSTAT">
//   Date Created : 2015-12-22
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Test.Manager
{
    using System.Collections.Generic;
    using System.Linq;

    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sri.Ws.Test.Extension;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;

    /// <summary>
    ///     A Sample implementation of <see cref="IAuthMutableStructureSearchManager" />
    /// </summary>
    public class SampleAuthStructureSearchManager : IAuthMutableStructureSearchManager
    {
        #region Fields

        /// <summary>
        ///     The _mutable structure search manager
        /// </summary>
        private readonly IMutableStructureSearchManager _mutableStructureSearchManager;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="SampleAuthStructureSearchManager" /> class.
        /// </summary>
        /// <param name="mutableStructureSearchManager">The mutable structure search manager.</param>
        public SampleAuthStructureSearchManager(IMutableStructureSearchManager mutableStructureSearchManager)
        {
            this._mutableStructureSearchManager = mutableStructureSearchManager;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Gets the latest.
        /// </summary>
        /// <param name="maintainableObject">The maintainable object.</param>
        /// <param name="allowedDataflows">The allowed dataflows.</param>
        /// <returns>The <see cref="IMaintainableMutableObject" />.</returns>
        public IMaintainableMutableObject GetLatest(IMaintainableMutableObject maintainableObject, IList<IMaintainableRefObject> allowedDataflows)
        {
            var latest = this._mutableStructureSearchManager.GetLatest(maintainableObject);
            if (allowedDataflows != null)
            {
                return allowedDataflows.Any(o => o.IsMatch(latest)) ? latest : null;
            }

            return latest;
        }

        /// <summary>
        ///     Gets the maintainables.
        /// </summary>
        /// <param name="structureQuery">The structure query.</param>
        /// <param name="allowedDataflows">The allowed dataflows.</param>
        /// <returns>The <see cref="IMutableObjects" />.</returns>
        public IMutableObjects GetMaintainables(IRestStructureQuery structureQuery, IList<IMaintainableRefObject> allowedDataflows)
        {
            var mutableObjects = this._mutableStructureSearchManager.GetMaintainables(structureQuery);
            if (allowedDataflows != null)
            {
                return new MutableObjectsImpl(mutableObjects.AllMaintainables.Where(o => allowedDataflows.Any(refObject => refObject.IsMatch(o))));
            }

            return mutableObjects;
        }

        /// <summary>
        ///     Retrieves the structures.
        /// </summary>
        /// <param name="queries">The queries.</param>
        /// <param name="resolveReferences">if set to <c>true</c> [resolve references].</param>
        /// <param name="returnStub">if set to <c>true</c> [return stub].</param>
        /// <param name="allowedDataflows">The allowed dataflows.</param>
        /// <returns>The <see cref="IMutableObjects" />.</returns>
        public IMutableObjects RetrieveStructures(IList<IStructureReference> queries, bool resolveReferences, StructureQueryDetail structureQueryDetails, IList<IMaintainableRefObject> allowedDataflows)
        {
            var mutableObjects = this._mutableStructureSearchManager.RetrieveStructures(queries, resolveReferences, structureQueryDetails.EnumType != StructureQueryDetailEnumType.Full);
            if (allowedDataflows != null)
            {
                return new MutableObjectsImpl(mutableObjects.AllMaintainables.Where(o => allowedDataflows.Any(refObject => refObject.IsMatch(o))));
            }

            return mutableObjects;
        }

        #endregion
    }
}