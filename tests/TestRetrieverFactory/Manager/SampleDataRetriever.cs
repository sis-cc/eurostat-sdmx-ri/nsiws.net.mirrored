﻿// -----------------------------------------------------------------------
// <copyright file="SampleDataRetriever.cs" company="EUROSTAT">
//   Date Created : 2015-12-21
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Threading.Tasks;

namespace Estat.Sri.Ws.Test.Manager
{
    using System;

    using Estat.Sri.Ws.Test.Constant;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query.Complex;

    /// <summary>
    ///     Class SampleDataRetriever.
    /// </summary>
    public class SampleDataRetriever : ISdmxDataRetrievalWithWriter, IAdvancedSdmxDataRetrievalWithWriter
    {
        #region Fields

        /// <summary>
        ///     The SDMX schema
        /// </summary>
        private readonly SdmxSchemaEnumType _sdmxSchema;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="SampleDataRetriever" /> class.
        /// </summary>
        /// <param name="sdmxSchema">The SDMX schema.</param>
        public SampleDataRetriever(SdmxSchemaEnumType sdmxSchema)
        {
            this._sdmxSchema = sdmxSchema;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Gets the data.
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <param name="dataWriter">The data writer.</param>
        /// <exception cref="Org.Sdmxsource.Sdmx.Api.Exception.SdmxNoResultsException">0 observations found (SDMX v2.1 behavior)</exception>
        /// <exception cref="Org.Sdmxsource.Sdmx.Api.Exception.SdmxResponseTooLargeException">Emulating default limit</exception>
        /// <exception cref="Org.Sdmxsource.Sdmx.Api.Exception.SdmxResponseSizeExceedsLimitException">Emulating server side limit</exception>
        /// <exception cref="Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException">Invalid component in query</exception>
        /// <exception cref="SdmxNoResultsException">Invalid component in query</exception>
        public void GetData(IDataQuery dataQuery, IDataWriterEngine dataWriter)
        {
            dataWriter.StartDataset(null, dataQuery.DataStructure, null);
            this.Validate(dataQuery, dataWriter);

            WriteData(dataQuery, dataWriter);
        }

        /// <summary>
        ///     Gets the data asynchronous.
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <param name="dataWriter">The data writer.</param>
        /// <exception cref="Org.Sdmxsource.Sdmx.Api.Exception.SdmxNoResultsException">0 observations found (SDMX v2.1 behavior)</exception>
        /// <exception cref="Org.Sdmxsource.Sdmx.Api.Exception.SdmxResponseTooLargeException">Emulating default limit</exception>
        /// <exception cref="Org.Sdmxsource.Sdmx.Api.Exception.SdmxResponseSizeExceedsLimitException">Emulating server side limit</exception>
        /// <exception cref="Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException">Invalid component in query</exception>
        /// <exception cref="SdmxNoResultsException">Invalid component in query</exception>
        public async Task GetDataAsync(IDataQuery dataQuery, IDataWriterEngine dataWriter)
        {
            this.GetData(dataQuery,dataWriter);
        }

        /// <summary>
        ///     Gets the data.
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <param name="dataWriter">The data writer.</param>
        /// <exception cref="SdmxNoResultsException">0 observations found (SDMX v2.1 behavior)</exception>
        /// <exception cref="SdmxResponseTooLargeException">Emulating default limit</exception>
        /// <exception cref="SdmxResponseSizeExceedsLimitException">Emulating server side limit</exception>
        /// <exception cref="SdmxSemmanticException">Invalid component in query</exception>
        public void GetData(IComplexDataQuery dataQuery, IDataWriterEngine dataWriter)
        {
            dataWriter.StartDataset(null, dataQuery.DataStructure, null);
            this.Validate(dataQuery, dataWriter);

            WriteData(dataQuery, dataWriter);
        }

        /// <summary>
        ///     Gets the data asynchronous.
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <param name="dataWriter">The data writer.</param>
        /// <exception cref="SdmxNoResultsException">0 observations found (SDMX v2.1 behavior)</exception>
        /// <exception cref="SdmxResponseTooLargeException">Emulating default limit</exception>
        /// <exception cref="SdmxResponseSizeExceedsLimitException">Emulating server side limit</exception>
        /// <exception cref="SdmxSemmanticException">Invalid component in query</exception>
        public async Task GetDataAsync(IComplexDataQuery dataQuery, IDataWriterEngine dataWriter)
        {
            this.GetData(dataQuery,dataWriter);
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Writes the data.
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <param name="dataWriter">The data writer.</param>
        private static void WriteData(IBaseDataQuery dataQuery, IDataWriterEngine dataWriter)
        {
            if (dataQuery.DataQueryDetail.EnumType == DataQueryDetailEnumType.NoData || dataQuery.DataQueryDetail.EnumType == DataQueryDetailEnumType.Full)
            {
                if (dataQuery.Dataflow.Id.Equals("TEST_GROUPS"))
                {
                    dataWriter.StartGroup("Sibling");
                    dataWriter.WriteGroupKeyValue("REF_AREA", "LU");
                    dataWriter.WriteGroupKeyValue("ADJUSTMENT", "N");
                    dataWriter.WriteGroupKeyValue("STS_INDICATOR", "PROD");
                    dataWriter.WriteGroupKeyValue("STS_ACTIVITY", "NS0030");
                    dataWriter.WriteGroupKeyValue("STS_INSTITUTION", "1");
                    dataWriter.WriteGroupKeyValue("STS_BASE_YEAR", "2000");

                    dataWriter.WriteAttributeValue("DECIMALS", "1");
                    dataWriter.WriteAttributeValue("UNIT", "UNIT");
                }
            }

            // TODO support dimension at observation != TIME_PERIOD
            dataWriter.StartSeries();
            dataWriter.WriteSeriesKeyValue("FREQ", "M");
            dataWriter.WriteSeriesKeyValue("REF_AREA", "LU");
            dataWriter.WriteSeriesKeyValue("ADJUSTMENT", "N");
            dataWriter.WriteSeriesKeyValue("STS_INDICATOR", "PROD");
            dataWriter.WriteSeriesKeyValue("STS_ACTIVITY", "NS0030");
            dataWriter.WriteSeriesKeyValue("STS_INSTITUTION", "1");
            dataWriter.WriteSeriesKeyValue("STS_BASE_YEAR", "2000");
            if (dataQuery.DataQueryDetail.EnumType == DataQueryDetailEnumType.NoData || dataQuery.DataQueryDetail.EnumType == DataQueryDetailEnumType.Full)
            {
                dataWriter.WriteAttributeValue("TIME_FORMAT", "P1M");
            }

            var startDate = new DateTime(2010, 01, 01);
            if (dataQuery.DataQueryDetail.EnumType == DataQueryDetailEnumType.Full || dataQuery.DataQueryDetail.EnumType == DataQueryDetailEnumType.DataOnly)
            {
                int firstObsCount = 5;
                if (dataQuery.FirstNObservations.HasValue)
                {
                    firstObsCount = Math.Min(firstObsCount, dataQuery.FirstNObservations.Value);
                }

                for (int i = 0; i < firstObsCount; i++)
                {
                    dataWriter.WriteObservation(startDate.AddMonths(i), "1.0", TimeFormat.GetFromEnum(TimeFormatEnumType.Month));
                    if (dataQuery.DataQueryDetail.EnumType == DataQueryDetailEnumType.Full)
                    {
                        dataWriter.WriteAttributeValue("OBS_STATUS", "A");
                    }
                }

                if (dataQuery.LastNObservations.HasValue)
                {
                    int lastObsStart = Math.Max(5 - dataQuery.LastNObservations.Value, firstObsCount);

                    for (int i = lastObsStart; i < 5; i++)
                    {
                        dataWriter.WriteObservation(startDate.AddMonths(i), "1.0", TimeFormat.GetFromEnum(TimeFormatEnumType.Month));
                        if (dataQuery.DataQueryDetail.EnumType == DataQueryDetailEnumType.Full)
                        {
                            dataWriter.WriteAttributeValue("OBS_STATUS", "A");
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     Validates the specified data query.
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <param name="dataWriter">The data writer.</param>
        /// <exception cref="Org.Sdmxsource.Sdmx.Api.Exception.SdmxNoResultsException">0 observations found (SDMX v2.1 behavior)</exception>
        /// <exception cref="Org.Sdmxsource.Sdmx.Api.Exception.SdmxResponseTooLargeException">Emulating default limit</exception>
        /// <exception cref="Org.Sdmxsource.Sdmx.Api.Exception.SdmxResponseSizeExceedsLimitException">Emulating server side limit</exception>
        /// <exception cref="Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException">Invalid component in query</exception>
        private void Validate(IBaseDataQuery dataQuery, IDataWriterEngine dataWriter)
        {
            switch (dataQuery.Dataflow.Id)
            {
                case Dataflows.NoResults:
                    if (this._sdmxSchema == SdmxSchemaEnumType.VersionTwoPointOne)
                    {
                        throw new SdmxNoResultsException("0 observations found (SDMX v2.1 behavior)");
                    }

                    break;
                case Dataflows.ResponseTooLarge:
                    WriteData(dataQuery, dataWriter);
                    throw new SdmxResponseTooLargeException("Emulating default limit");
                case Dataflows.ResponseExceedsLimit:
                    WriteData(dataQuery, dataWriter);
                    throw new SdmxResponseSizeExceedsLimitException("Emulating server side limit");
                case Dataflows.Semantic:
                    throw new SdmxSemmanticException("Invalid component in query");
            }
        }

        #endregion
    }
}