﻿// -----------------------------------------------------------------------
// <copyright file="MappingStoreIdRepository.cs" company="EUROSTAT">
//   Date Created : 2017-06-07
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Test.AuthModule
{
    using Estat.Nsi.AuthModule;
    using System.Configuration;
    using System.Text.RegularExpressions;
    using System.Web;

    /// <summary>
    /// A helper class for storing the mapping store id
    /// </summary>
    public class MappingStoreIdRepositoryTest : IMappingStoreIdRepository
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="MappingStoreIdRepository"/> class.
        /// </summary>
        public MappingStoreIdRepositoryTest()
        {
        }

        /// <summary>
        /// Gets the connection identifier from URL.
        /// </summary>
        /// <returns>The store Id from the URL; otherwise the default id</returns>
        public string GetConnectionName()
        {
            return "sqlserver";
        }

    }
}