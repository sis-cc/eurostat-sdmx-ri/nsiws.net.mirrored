﻿using Estat.Nsi.AuthModule.Engine;
using Estat.Nsi.AuthModule.Factory;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Estat.Nsi.AuthModule;

namespace Estat.Sri.Ws.Test.AuthModule
{
    using Estat.Sri.Mapping.Api.Factory;
    using Estat.Sri.Mapping.Api.Manager;

    [TestFixture]
    public class MappingStoreAuth
    {

        public MappingStoreAuth()
        {

        }

        [Test]
        public void ExternalConfigFile()
        {
            var configurationStoreManager = new ConfigurationStoreManager(new AppConfigStoreFactory());
            MappingStoreAuthenticationFactory authFactory = new MappingStoreAuthenticationFactory(new MappingStoreIdRepositoryTest(), configurationStoreManager);
            var auth = (MappingStoreAuthenticationProvider)authFactory.CreateProvider("MappingStoreAuthenticationProvider");

            var user = auth.GetAnonymous("user1");

            Assert.That(user, Is.Not.Null);
        }
    }
}
