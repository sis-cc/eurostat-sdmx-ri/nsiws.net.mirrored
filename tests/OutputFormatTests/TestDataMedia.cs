﻿// -----------------------------------------------------------------------
// <copyright file="TestDataMedia.cs" company="EUROSTAT">
//   Date Created : 2016-07-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace OutputFormatTests
{
    using System.Globalization;
    using System.Net.Mime;

    using Estat.Sri.Ws.Format.Sdmx.Constants;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// The test structure media.
    /// </summary>
    [TestFixture]
    internal class TestDataMedia
    {
        /// <summary>
        /// Tests the <see cref="DataMediaType.MediaType"/>.
        /// </summary>
        /// <param name="structureMedia">Type of the media .</param>
        /// <param name="expectedMediaName">Expected name of the media.</param>
        /// <param name="expectedVersion">The expected version.</param>
        [TestCase(DataMediaEnumType.StructureSpecificData, SdmxMedia.StructureSpecificData, SdmxSchemaEnumType.VersionTwoPointOne)]
        [TestCase(DataMediaEnumType.GenericData, SdmxMedia.GenericData, SdmxSchemaEnumType.VersionTwoPointOne)]
        [TestCase(DataMediaEnumType.GenericDataV20, SdmxMedia.GenericData, SdmxSchemaEnumType.VersionTwo)]
        [TestCase(DataMediaEnumType.CompactData, SdmxMedia.CompactData, SdmxSchemaEnumType.VersionTwo)]
        [TestCase(DataMediaEnumType.CrossSectionalData, SdmxMedia.CrossSectionalData, SdmxSchemaEnumType.VersionTwo)]
        [TestCase(DataMediaEnumType.ApplicationXml, SdmxMedia.ApplicationXml, SdmxSchemaEnumType.VersionTwoPointOne)]
        [TestCase(DataMediaEnumType.TextXml, SdmxMedia.TextXml, SdmxSchemaEnumType.VersionTwoPointOne)]
        public void TestMedia(DataMediaEnumType structureMedia, string expectedMediaName, SdmxSchemaEnumType expectedVersion)
        {
            var sdmxSchema = SdmxSchema.GetFromEnum(expectedVersion);
            var expected =
                new ContentType(
                    string.Format(
                        CultureInfo.InvariantCulture,
                        "{0};version={1}",
                        expectedMediaName,
                        sdmxSchema));
            var dataMediaType = DataMediaType.GetFromEnum(structureMedia);
            Assert.AreEqual(expectedMediaName, dataMediaType.MediaType.MediaType);
            Assert.IsNotNull(dataMediaType.MediaType.Parameters);
            Assert.AreEqual(sdmxSchema.ToString(), dataMediaType.MediaType.Parameters["version"]);
            Assert.AreEqual(expected, dataMediaType.MediaType);
        }

        /// <summary>
        /// Tests the <see cref="DataMediaType.Format"/>.
        /// </summary>
        /// <param name="mediaEnumType">Type of the media.</param>
        /// <param name="expectedFormat">The expected format.</param>
        [TestCase(DataMediaEnumType.StructureSpecificData, DataEnumType.Compact21)]
        [TestCase(DataMediaEnumType.GenericData, DataEnumType.Generic21)]
        [TestCase(DataMediaEnumType.GenericDataV20, DataEnumType.Generic20)]
        [TestCase(DataMediaEnumType.CompactData, DataEnumType.Compact20)]
        [TestCase(DataMediaEnumType.CrossSectionalData, DataEnumType.CrossSectional20)]
        [TestCase(DataMediaEnumType.ApplicationXml, DataEnumType.Generic21)]
        [TestCase(DataMediaEnumType.TextXml, DataEnumType.Generic21)]
        public void TestFormat(DataMediaEnumType mediaEnumType, DataEnumType expectedFormat)
        {
            var dataMediaType = DataMediaType.GetFromEnum(mediaEnumType);
            Assert.AreEqual(expectedFormat, dataMediaType.Format.EnumType);
       }

        /// <summary>
        /// Tests the <see cref="DataMediaType.Format"/>.
        /// </summary>
        /// <param name="mediaEnumType">Type of the media .</param>
        [TestCase(DataMediaEnumType.StructureSpecificData)]
        [TestCase(DataMediaEnumType.GenericData)]
        [TestCase(DataMediaEnumType.GenericDataV20)]
        [TestCase(DataMediaEnumType.CompactData)]
        [TestCase(DataMediaEnumType.CrossSectionalData)]
        [TestCase(DataMediaEnumType.ApplicationXml)]
        [TestCase(DataMediaEnumType.TextXml)]
        public void TestGetFormat(DataMediaEnumType mediaEnumType)
        {
            var structureMediaType = DataMediaType.GetFromEnum(mediaEnumType);
            var contentType = structureMediaType.MediaType;
            var roundTripFormat = DataMediaType.GetFormat(contentType);
            
            Assert.AreEqual(structureMediaType.Format.EnumType, roundTripFormat.EnumType);
        }
    }
}