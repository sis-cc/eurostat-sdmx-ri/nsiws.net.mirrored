// -----------------------------------------------------------------------
// <copyright file="TestSriFormatFactories.cs" company="EUROSTAT">
//   Date Created : 2016-07-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.ServiceModel;

namespace OutputFormatTests
{
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Mime;
    using System.Xml;

    using Estat.Sdmxsource.Extension.Builder;
    using Estat.Sdmxsource.Extension.Constant;
    using Estat.Sdmxsource.Extension.Factory;
    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sdmxsource.Extension.Model;
    using Estat.Sri.Ws.Format.Sdmx.Factory;
    using Estat.Sri.Ws.Format.Sdmx.Model;

    using NSubstitute;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Builder.Common;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model;

    /// <summary>
    /// Test SDMX RI Data Format Factory
    /// </summary>
    [TestFixture]
    public class TestSriFormatFactories
    {
        /// <summary>
        /// The _data format factory
        /// </summary>
        private readonly IDataWriterPluginContract _dataFormatFactory = new SriDataFormatFactory();

        /// <summary>
        /// The _structure format factory
        /// </summary>
        private readonly IStructureWriterPluginContract _structureFormatFactory = new SriStructureFormatFactory();

        /// <summary>
        /// The _resolver manager
        /// </summary>
        private readonly IFormatResolverManager _resolverManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="TestSriFormatFactories"/> class.
        /// </summary>
        public TestSriFormatFactories()
        {
            this._resolverManager = new FormatResolverManager(new[] { this._structureFormatFactory }, new[] { this._dataFormatFactory }, new SortedAcceptHeadersBuilder());
        }

        /// <summary>
        /// Tests for rest.
        /// </summary>
        /// <param name="acceptValue">The accept value.</param>
        /// <param name="expectedContentTypeValue">The expected content type value.</param>
        /// <param name="acceptCharSet">The accept character set.</param>
        [TestCase("*/*", "application/vnd.sdmx.genericdata+xml;version=2.1; charset=utf-8", "utf-8")]
        [TestCase("text/xml;q=0.3,*/*,application/*", "application/vnd.sdmx.genericdata+xml; version=2.1; charset=utf-8;", "utf-8")]
        [TestCase("text/*;q=0.3,*/*,application/*", "application/vnd.sdmx.genericdata+xml;version=2.1; charset=utf-8", "utf-8")]
        [TestCase("text/*;q=0.3,*/*;q=0.0001,application/*;q=1", "application/vnd.sdmx.genericdata+xml;version=2.1; charset=utf-8", "utf-8")]
        [TestCase("text/xml, */*", "text/xml;version=2.1; charset=utf-8", "utf-8")]
        [TestCase("text/xml, paok/ole, */*", "text/xml;version=2.1; charset=utf-8", "utf-8")]
        [TestCase("text/xml;q=0.1, paok/ole;q=0.9, */*;q=0.0001", "text/xml;version=2.1; charset=utf-8", null)]
        [TestCase("text/xml, */*,application/vnd.sdmx.genericdata+xml;version=2.1", "application/vnd.sdmx.genericdata+xml;version=2.1; charset=utf-8", "utf-8")]
        [TestCase("text/xml, */*,application/vnd.sdmx.genericdata+xml;version=2.1", "application/vnd.sdmx.genericdata+xml;version=2.1; charset=utf-8", "invalid-encoding")]
        [TestCase("text/xml, */*,application/vnd.sdmx.genericdata+xml;version=2.0", "application/vnd.sdmx.genericdata+xml;version=2.0; charset=utf-8", "utf-8")]
        [TestCase("application/vnd.sdmx.structurespecificdata+xml;version=2.1;, */*;q=0.0001,application/vnd.sdmx.genericdata+xml;version=2.1", "application/vnd.sdmx.structurespecificdata+xml;version=2.1; charset=utf-8", "utf-8")]
        [TestCase("application/vnd.sdmx.structurespecificdata+xml;version=2.1;q=0.1, */*;q=0.0001,application/vnd.sdmx.genericdata+xml;version=2.1;q=0.01", "application/vnd.sdmx.structurespecificdata+xml;version=2.1; charset=utf-8", "utf-8")]
        [TestCase("application/vnd.sdmx.compactdata+xml;version=2.0;q=0.1, */*;q=0.0001,application/vnd.sdmx.genericdata+xml;version=2.1;q=0.01", "application/vnd.sdmx.compactdata+xml;version=2.0; charset=utf-16", "utf-16")]
        //[TestCase("application/vnd.sdmx.compactdata+xml;version=2.0;q=0.1, */*;q=0.0001,application/vnd.sdmx.genericdata+xml;version=2.1;q=0.01", "application/vnd.sdmx.compactdata+xml;version=2.0; charset=utf-16", "utf-8", ExpectedException = typeof(AssertionException))]
        public void TestForRestDataContentType(string acceptValue, string expectedContentTypeValue, string acceptCharSet)
        {
            var headerCollection = new UnRestrictedWebHeaderCollection();
            headerCollection.Add("Accept", acceptValue);
            if (acceptCharSet != null)
            {
                headerCollection.Add("Accept-CharSet", acceptCharSet);
            }

            IDataQuery dataQuery = Substitute.For<IDataQuery>();
            ISdmxObjectRetrievalManager retrievalManager = Substitute.For<ISdmxObjectRetrievalManager>();
            var restResponse = this._resolverManager.GetDataFormat(
                headerCollection,
                retrievalManager,
                dataQuery,
                RestApiVersion.V1);
            Assert.NotNull(restResponse);
            Assert.AreEqual(new ContentType(expectedContentTypeValue), restResponse.ResponseContentHeader.MediaType);
        }

        /// <summary>
        /// Tests for rest.
        /// </summary>
        /// <param name="acceptValue">The accept value.</param>
        /// <param name="expectedContentTypeValue">The expected content type value.</param>
        /// <param name="acceptCharSet">The accept character set.</param>
        [TestCase("*/*", "application/vnd.sdmx.structure+xml;version=2.1; charset=utf-8", "utf-8")]
        [TestCase("text/xml;q=0.3,*/*,application/*", "application/vnd.sdmx.structure+xml; version=2.1; charset=utf-8", "utf-8")]
        [TestCase("text/*;q=0.3,*/*,application/*", "application/vnd.sdmx.structure+xml;version=2.1; charset=utf-8", "utf-8")]
        [TestCase("text/*;q=0.3,*/*,application/*;q=1", "application/vnd.sdmx.structure+xml;version=2.1; charset=utf-8", "utf-8")]
        [TestCase("text/xml, */*", "text/xml;version=2.1; charset=utf-8", "utf-8")]
        [TestCase("text/xml, paok/ole, */*", "text/xml;version=2.1; charset=utf-8", "utf-8")]
        [TestCase("text/xml;q=0.1, paok/ole;q=0.9", "text/xml;version=2.1; charset=utf-8", null)]
        [TestCase("text/xml, */*,application/vnd.sdmx.structure+xml;version=2.1", "application/vnd.sdmx.structure+xml;version=2.1; charset=utf-8", "utf-8")]
        [TestCase("text/xml, */*,application/vnd.sdmx.structure+xml;version=2.1", "application/vnd.sdmx.structure+xml;version=2.1; charset=utf-8", "invalid-encoding")]
        [TestCase("text/xml, */*,application/vnd.sdmx.structure+xml;version=2.0", "application/vnd.sdmx.structure+xml;version=2.0; charset=utf-8", "utf-8")]
        [TestCase("application/vnd.sdmx.structure+xml;version=2.1;, */*,application/vnd.sdmx.structure+xml;version=2.1", "application/vnd.sdmx.structure+xml;version=2.1; charset=utf-8", "utf-8")]
        [TestCase("application/vnd.sdmx.structure+xml;version=2.1;q=0.1", "application/vnd.sdmx.structure+xml;version=2.1; charset=utf-8", "utf-8")]
        [TestCase("application/vnd.sdmx.structure+xml;version=2.0;q=0.1", "application/vnd.sdmx.structure+xml;version=2.0; charset=utf-16", "utf-16")]
       // [TestCase("application/vnd.sdmx.structure+xml;version=2.0;q=0.1", "application/vnd.sdmx.structure+xml;version=2.0; charset=utf-16", "utf-8", ExpectedException = typeof(AssertionException))]
        public void TestForRestStructureContentType(string acceptValue, string expectedContentTypeValue, string acceptCharSet)
        {
            var headerCollection = new UnRestrictedWebHeaderCollection();
            headerCollection.Add("Accept", acceptValue);
            if (acceptCharSet != null)
            {
                headerCollection.Add("Accept-CharSet", acceptCharSet);
            }

            var restResponse = this._resolverManager.GetStructureFormat(headerCollection,RestApiVersion.V1);
            Assert.NotNull(restResponse);
            Assert.AreEqual(new ContentType(expectedContentTypeValue), restResponse.ResponseContentHeader.MediaType);
        }

        /// <summary>
        /// Tests for rest.
        /// </summary>
        /// <param name="acceptValue">The accept value.</param>
        /// <param name="expectedFormat">The expected format.</param>
        [TestCase("*/*", DataEnumType.Generic21)]
        [TestCase("text/xml;q=0.3,*/*,application/*", DataEnumType.Generic21)]
        [TestCase("text/*;q=0.3,*/*,application/*", DataEnumType.Generic21)]
        [TestCase("text/*;q=0.3,*/*,application/*;q=1", DataEnumType.Generic21)]
        [TestCase("text/xml, */*", DataEnumType.Generic21)]
        [TestCase("text/xml, paok/ole, */*", DataEnumType.Generic21)]
        [TestCase("text/xml;q=0.1, paok/ole;q=0.9, */*", DataEnumType.Generic21)]
        [TestCase("text/xml, */*,application/vnd.sdmx.genericdata+xml;version=2.1", DataEnumType.Generic21)]
        [TestCase("text/xml, */*,application/vnd.sdmx.genericdata+xml;version=2.1", DataEnumType.Generic21)]
        [TestCase("text/xml, */*,application/vnd.sdmx.genericdata+xml;version=2.0", DataEnumType.Generic20)]
        [TestCase("application/vnd.sdmx.structurespecificdata+xml;version=2.1;, application/vnd.sdmx.genericdata+xml;version=2.1", DataEnumType.Compact21)]
        [TestCase("application/vnd.sdmx.structurespecificdata+xml;version=2.1;q=0.1, application/vnd.sdmx.genericdata+xml;version=2.1;q=0.01", DataEnumType.Compact21)]
        [TestCase("application/vnd.sdmx.compactdata+xml;version=2.0;q=0.1, application/vnd.sdmx.genericdata+xml;version=2.1;q=0.01", DataEnumType.Compact20)]
        [TestCase("application/vnd.sdmx.crosssectionaldata+xml;version=2.0;q=0.1, application/vnd.sdmx.genericdata+xml;version=2.1;q=0.01", DataEnumType.CrossSectional20)]
        public void TestForRestDataType(string acceptValue, DataEnumType expectedFormat)
        {
            var headerCollection = new UnRestrictedWebHeaderCollection();
            headerCollection.Add("Accept", acceptValue);

            IDataQuery dataQuery = Substitute.For<IDataQuery>();
            ISdmxObjectRetrievalManager retrievalManager = Substitute.For<ISdmxObjectRetrievalManager>();
            var restResponse = this._resolverManager.GetDataFormat(headerCollection, retrievalManager, dataQuery, RestApiVersion.V1);
            Assert.NotNull(restResponse);
            Assert.AreEqual(expectedFormat, restResponse.Format.SdmxDataFormat.EnumType);
        }

        /// <summary>
        /// Tests for rest.
        /// </summary>
        /// <param name="acceptValue">The accept value.</param>
        [TestCase("*/*")]
        [TestCase("text/xml;q=0.3,*/*,application/*")]
        [TestCase("text/*;q=0.3,*/*,application/*")]
        [TestCase("text/*;q=0.3,*/*,application/*;q=1")]
        [TestCase("text/xml, */*")]
        [TestCase("text/xml, paok/ole, */*")]
        [TestCase("text/xml;q=0.1, paok/ole;q=0.9, */*")]
        [TestCase("text/xml, */*,application/vnd.sdmx.genericdata+xml;version=2.1")]
        [TestCase("text/xml, */*,application/vnd.sdmx.genericdata+xml;version=2.1")]
        [TestCase("text/xml, */*,application/vnd.sdmx.genericdata+xml;version=2.0")]
        [TestCase("application/vnd.sdmx.structurespecificdata+xml;version=2.1;, */*,application/vnd.sdmx.genericdata+xml;version=2.1")]
        [TestCase("application/vnd.sdmx.structurespecificdata+xml;version=2.1;q=0.1, */*,application/vnd.sdmx.genericdata+xml;version=2.1;q=0.01")]
        [TestCase("application/vnd.sdmx.compactdata+xml;version=2.0;q=0.1, */*,application/vnd.sdmx.genericdata+xml;version=2.1;q=0.01")]
        [TestCase("application/vnd.sdmx.crosssectionaldata+xml;version=2.0;q=0.1, */*,application/vnd.sdmx.genericdata+xml;version=2.1;q=0.01")]
        public void TestForRestDataFormat(string acceptValue)
        {
            var headerCollection = new UnRestrictedWebHeaderCollection();
            headerCollection.Add("Accept", acceptValue);

            var dataRequest = Substitute.For<IRestDataRequest>();
            var restResponse = this._dataFormatFactory.GetDataFormats(dataRequest);
            Assert.NotNull(restResponse);
            Assert.IsInstanceOf<SriRestDataFormat>(restResponse.First().Format);
        }

        /// <summary>
        /// Tests for rest.
        /// </summary>
        /// <param name="acceptValue">The accept value.</param>
        /// <param name="expectedFormat">The expected format.</param>
        [TestCase("*/*", StructureOutputFormatEnumType.SdmxV21StructureDocument)]
        [TestCase("text/xml;q=0.3,*/*,application/*", StructureOutputFormatEnumType.SdmxV21StructureDocument)]
        [TestCase("text/*;q=0.3,*/*,application/*", StructureOutputFormatEnumType.SdmxV21StructureDocument)]
        [TestCase("text/*;q=0.3,*/*,application/*;q=1", StructureOutputFormatEnumType.SdmxV21StructureDocument)]
        [TestCase("text/xml, */*", StructureOutputFormatEnumType.SdmxV21StructureDocument)]
        [TestCase("text/xml, paok/ole, */*", StructureOutputFormatEnumType.SdmxV21StructureDocument)]
        [TestCase("text/xml;q=0.1, paok/ole;q=0.9, */*", StructureOutputFormatEnumType.SdmxV21StructureDocument)]
        [TestCase("text/xml, */*,application/vnd.sdmx.structure+xml;version=2.1", StructureOutputFormatEnumType.SdmxV21StructureDocument)]
        [TestCase("text/xml, */*,application/vnd.sdmx.structure+xml;version=2.1", StructureOutputFormatEnumType.SdmxV21StructureDocument)]
        [TestCase("text/xml, */*,application/vnd.sdmx.structure+xml;version=2.0", StructureOutputFormatEnumType.SdmxV2StructureDocument)]
        [TestCase("application/vnd.sdmx.structure+xml;version=2.1, application/vnd.sdmx.structure+xml;version=2.0", StructureOutputFormatEnumType.SdmxV21StructureDocument)]
        [TestCase("application/vnd.sdmx.structure+xml;version=2.1;q=0.1,application/vnd.sdmx.structure+xml;version=2.0;q=0.01", StructureOutputFormatEnumType.SdmxV21StructureDocument)]
        [TestCase("application/vnd.sdmx.structure+xml;version=2.0;q=0.1,application/vnd.sdmx.structure+xml;version=2.1;q=0.01", StructureOutputFormatEnumType.SdmxV2StructureDocument)]
        [TestCase("application/vnd.sdmx.structure+xml;version=2.0;q=0.1,application/vnd.sdmx.structure+xml;version=2.1;q=0.01", StructureOutputFormatEnumType.SdmxV2StructureDocument)]
        public void TestForRestStructureOutputFormat(string acceptValue, StructureOutputFormatEnumType expectedFormat)
        {
            var headerCollection = new UnRestrictedWebHeaderCollection();
            headerCollection.Add("Accept", acceptValue);

            var restResponse = this._resolverManager.GetStructureFormat(headerCollection, RestApiVersion.V1);
            Assert.NotNull(restResponse);
            Assert.AreEqual(expectedFormat, restResponse.Format.SdmxOutputFormat.EnumType);
        }

        /// <summary>
        /// Tests for rest.
        /// </summary>
        /// <param name="acceptValue">The accept value.</param>
        [TestCase("*/*")]
        [TestCase("text/xml;q=0.3,*/*,application/*")]
        [TestCase("text/*;q=0.3,*/*,application/*")]
        [TestCase("text/*;q=0.3,*/*,application/*;q=1")]
        [TestCase("text/xml, */*")]
        [TestCase("text/xml, paok/ole, */*")]
        [TestCase("text/xml;q=0.1, paok/ole;q=0.9, */*")]
        [TestCase("text/xml, */*,application/vnd.sdmx.structure+xml;version=2.1")]
        [TestCase("text/xml, */*,application/vnd.sdmx.structure+xml;version=2.1")]
        [TestCase("text/xml, */*,application/vnd.sdmx.structure+xml;version=2.0")]
        [TestCase("application/vnd.sdmx.structure+xml;version=2.1;, */*,application/vnd.sdmx.structure+xml;version=2.0")]
        [TestCase("application/vnd.sdmx.structure+xml;version=2.1;q=0.1, */*,application/vnd.sdmx.structure+xml;version=2.0;q=0.01")]
        [TestCase("application/vnd.sdmx.structure+xml;version=2.0;q=0.1, */*,application/vnd.sdmx.structure+xml;version=2.1;q=0.01")]
        [TestCase("application/vnd.sdmx.structure+xml;version=2.0;q=0.1, */*,application/vnd.sdmx.structure+xml;version=2.1;q=0.01")]
        public void TestForRestStructureFormat(string acceptValue)
        {
            var headerCollection = new UnRestrictedWebHeaderCollection();
            headerCollection.Add("Accept", acceptValue);

            var soapAcceptHeader = Substitute.For<ISortedAcceptHeaders>();
            var restResponse = this._structureFormatFactory.GetStructureFormats(headerCollection, soapAcceptHeader);
            Assert.NotNull(restResponse);
            Assert.IsInstanceOf<SdmxStructureFormat>(restResponse.First().Format);
        }

        /// <summary>
        /// Tests for rest failure.
        /// </summary>
        /// <param name="acceptValue">The accept value.</param>
        [TestCase("paok/ole", typeof(FaultException<string>))]
        [TestCase("application/ole", typeof(FaultException<string>))]
        [TestCase("foobar/xml",  typeof(FaultException<string>))]
        [TestCase("application/vnd.sdmx.compactdata+xml;version=2.0;q=0.1", typeof(AssertionException))]
        public void TestForRestDataFailure(string acceptValue,Type exceptionType)
        {
            
            Assert.Throws(exceptionType, () =>
            {
                var headerCollection = new UnRestrictedWebHeaderCollection();
                headerCollection.Add("Accept", acceptValue);

                IDataQuery dataQuery = Substitute.For<IDataQuery>();
                ISdmxObjectRetrievalManager retrievalManager = Substitute.For<ISdmxObjectRetrievalManager>();
                var restResponse = this._resolverManager.GetDataFormat(headerCollection, retrievalManager, dataQuery, RestApiVersion.V1);
                Assert.IsNull(restResponse);
            });
        }

        /// <summary>
        /// Tests the SOAP.
        /// </summary>
        /// <param name="operation">The operation. We don't currently care about it.</param>
        /// <param name="dataEnumType">Type of the data.</param>
        [TestCase("GetGenericData", DataEnumType.Generic21)]
        [TestCase("GetGenericData", DataEnumType.Generic20)]
        [TestCase("GetStructureSpecificData", DataEnumType.Compact21)]
        [TestCase("GetCompactData", DataEnumType.Compact20)]
        [TestCase("GetCrossSectionalData", DataEnumType.CrossSectional20)]
        //[TestCase("GetEdiData", DataEnumType.EdiTs, ExpectedException = typeof(AssertionException))]
        public void TestSoapData(string operation, DataEnumType dataEnumType)
        {
            var soapDataRequest = new SoapDataRequest(operation, DataType.GetFromEnum(dataEnumType), SdmxExtension.None);
            var dataFormat = this._dataFormatFactory.GetDataFormat(soapDataRequest, XmlWriter.Create(Stream.Null));
            Assert.NotNull(dataFormat);
            Assert.AreEqual(dataEnumType, dataFormat.SdmxDataFormat.EnumType);
        }

        /// <summary>
        /// Tests the SOAP.
        /// </summary>
        /// <param name="operation">The operation. We don't currently care about it.</param>
        /// <param name="dataEnumType">Type of the data.</param>
        [TestCase("GetGenericData", DataEnumType.Generic21)]
        [TestCase("GetGenericData", DataEnumType.Generic20)]
        [TestCase("GetStructureSpecificData", DataEnumType.Compact21)]
        [TestCase("GetCompactData", DataEnumType.Compact20)]
        [TestCase("GetCrossSectionalData", DataEnumType.CrossSectional20)]
        //[TestCase("GetEdiData", DataEnumType.EdiTs, ExpectedException = typeof(AssertionException))]
        public void TestSoapDataFormat(string operation, DataEnumType dataEnumType)
        {
            var soapDataRequest = new SoapDataRequest(operation, DataType.GetFromEnum(dataEnumType), SdmxExtension.None);
            var dataFormat = this._dataFormatFactory.GetDataFormat(soapDataRequest, XmlWriter.Create(Stream.Null));
            Assert.NotNull(dataFormat);
            Assert.IsInstanceOf<SriSoapDataFormat>(dataFormat);
        }

        /// <summary>
        /// Tests the SOAP.
        /// </summary>
        /// <param name="operation">The operation. We don't currently care about it.</param>
        /// <param name="structureOutputFormat">Type of the data.</param>
        [TestCase("QueryStructureRequest", StructureOutputFormatEnumType.SdmxV2RegistryQueryResponseDocument)]
        [TestCase("GetDataflow", StructureOutputFormatEnumType.SdmxV21StructureDocument)]
        [TestCase("GetDataflow", StructureOutputFormatEnumType.SdmxV2StructureDocument)]
        //[TestCase("GetEdiData", StructureOutputFormatEnumType.Edi, ExpectedException = typeof(AssertionException))]
        public void TestSoapStructure(string operation, StructureOutputFormatEnumType structureOutputFormat)
        {
            var soapDataRequest = new SoapStructureRequest(operation, StructureOutputFormat.GetFromEnum(structureOutputFormat), SdmxExtension.None);
            var dataFormat = this._structureFormatFactory.GetStructureFormat(soapDataRequest, XmlWriter.Create(Stream.Null));
            Assert.NotNull(dataFormat);
            Assert.AreEqual(structureOutputFormat, dataFormat.SdmxOutputFormat.EnumType);
        }

        /// <summary>
        /// Tests the SOAP.
        /// </summary>
        /// <param name="operation">The operation. We don't currently care about it.</param>
        /// <param name="structureOutputFormat">Type of the data.</param>
        [TestCase("QueryStructureRequest", StructureOutputFormatEnumType.SdmxV2RegistryQueryResponseDocument)]
        [TestCase("GetDataflow", StructureOutputFormatEnumType.SdmxV21StructureDocument)]
        [TestCase("GetDataflow", StructureOutputFormatEnumType.SdmxV2StructureDocument)]
        //[TestCase("GetEdiData", StructureOutputFormatEnumType.Edi, ExpectedException = typeof(AssertionException))]
        public void TestSoapStructureFormat(string operation, StructureOutputFormatEnumType structureOutputFormat)
        {
            var soapDataRequest = new SoapStructureRequest(operation, StructureOutputFormat.GetFromEnum(structureOutputFormat), SdmxExtension.None);
            var structureFormat = this._structureFormatFactory.GetStructureFormat(soapDataRequest, XmlWriter.Create(Stream.Null));
            Assert.NotNull(structureFormat);
            Assert.IsInstanceOf<SdmxXmlStructureFormat>(structureFormat);
        }

        /// <summary>
        /// Tests the SOAP.
        /// </summary>
        /// <param name="operation">The operation. We don't currently care about it.</param>
        /// <param name="structureOutputFormat">Type of the data.</param>
        //[TestCase("GetDataflow", StructureOutputFormatEnumType.SdmxV21StructureDocument, ExpectedException = typeof(AssertionException))]
        [TestCase("GetEdiData", StructureOutputFormatEnumType.Edi)]
        [TestCase("GetEdiData", StructureOutputFormatEnumType.Csv)]
        [TestCase("GetEdiData", StructureOutputFormatEnumType.Xlsx)]
        public void TestSoapStructureFailure(string operation, StructureOutputFormatEnumType structureOutputFormat)
        {
            var soapDataRequest = new SoapStructureRequest(operation, StructureOutputFormat.GetFromEnum(structureOutputFormat), SdmxExtension.None);
            var dataFormat = this._structureFormatFactory.GetStructureFormat(soapDataRequest, XmlWriter.Create(Stream.Null));
            Assert.Null(dataFormat);
        }

        /// <summary>
        /// Tests the SOAP.
        /// </summary>
        /// <param name="operation">The operation. We don't currently care about it.</param>
        /// <param name="dataEnumType">Type of the data.</param>
        //[TestCase("GetCrossSectionalData", DataEnumType.CrossSectional20, ExpectedException = typeof(AssertionException))]
        [TestCase("GetEdiData", DataEnumType.EdiTs)]
        [TestCase("GetEdiData", DataEnumType.CsvDatesXAxis)]
        [TestCase("GetEdiData", DataEnumType.Json)]
        public void TestSoapDataFailure(string operation, DataEnumType dataEnumType)
        {
            var soapDataRequest = new SoapDataRequest(operation, DataType.GetFromEnum(dataEnumType), SdmxExtension.None);
            var dataFormat = this._dataFormatFactory.GetDataFormat(soapDataRequest, XmlWriter.Create(Stream.Null));
            Assert.Null(dataFormat);
        }

        /// <summary>
        /// An unrestricted web header collection. Not mocking too simple.
        /// </summary>
        private class UnRestrictedWebHeaderCollection : WebHeaderCollection
        {
            /// <summary>
            /// Inserts a header with the specified name and value into the collection.
            /// </summary>
            /// <param name="name">The header to add to the collection.</param>
            /// <param name="value">The content of the header.</param>
            public override void Add(string name, string value)
            {
                this.AddWithoutValidate(name, value);
            }
        }
    }
}
