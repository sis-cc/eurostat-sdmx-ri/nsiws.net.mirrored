﻿// -----------------------------------------------------------------------
// <copyright file="TestWriterFactories.cs" company="EUROSTAT">
//   Date Created : 2016-07-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace OutputFormatTests
{
    using System.IO;
    using System.Text;
    using System.Xml;

    using Estat.Sri.SdmxStructureMutableParser.Engine.V2;
    using Estat.Sri.SdmxStructureMutableParser.Model;
    using Estat.Sri.Ws.Format.Sdmx.Factory;
    using Estat.Sri.Ws.Format.Sdmx.Model;

    using NSubstitute;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;
    using Org.Sdmxsource.Sdmx.DataParser.Engine;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model;
    using Org.Sdmxsource.Sdmx.Structureparser.Engine.Writing;
    using Org.Sdmxsource.Sdmx.Structureparser.Factory;

    /// <summary>
    /// The test writer factories.
    /// </summary>
    public class TestWriterFactories
    {
        /// <summary>
        /// Tests the SOAP structure factory21.
        /// </summary>
        /// <param name="formatType">Type of the format.</param>
        [TestCase(StructureOutputFormatEnumType.SdmxV21StructureDocument)]
        public void TestSoapStructureFactory21(StructureOutputFormatEnumType formatType)
        {
            var factory = new SriStructureWriterFactory();
            var outputFormatType = StructureOutputFormat.GetFromEnum(formatType);
            var format = new SdmxXmlStructureFormat(outputFormatType,  XmlWriter.Create(Stream.Null));
            var engine = factory.GetStructureWriterEngine(format, null);
            Assert.IsInstanceOf<StructureWriterEngineV21>(engine);
        }

        /// <summary>
        /// Tests the SOAP structure factory20.
        /// </summary>
        /// <param name="formatType">Type of the format.</param>
        [TestCase(StructureOutputFormatEnumType.SdmxV2StructureDocument)]
        public void TestSoapStructureFactory20(StructureOutputFormatEnumType formatType)
        {
            var factory = new SriStructureWriterFactory();
            var outputFormatType = StructureOutputFormat.GetFromEnum(formatType);
            var format = new SdmxXmlStructureFormat(outputFormatType, XmlWriter.Create(Stream.Null));
            var engine = factory.GetStructureWriterEngine(format, null);
            Assert.IsInstanceOf<StructureWriterEngineV2>(engine);
        }

        /// <summary>
        /// Tests the SOAP structure factory20 registry.
        /// </summary>
        /// <param name="formatType">Type of the format.</param>
        [TestCase(StructureOutputFormatEnumType.SdmxV2RegistryQueryResponseDocument)]
        public void TestSoapStructureFactory20Registry(StructureOutputFormatEnumType formatType)
        {
            var factory = new SriStructureWriterFactory();
            var outputFormatType = StructureOutputFormat.GetFromEnum(formatType);
            var format = new SdmxXmlStructureFormat(outputFormatType, XmlWriter.Create(Stream.Null));
            var engine = factory.GetStructureWriterEngine(format, null);
            Assert.IsInstanceOf<RegistryQueryResponseWriterEngineV2>(engine);
        }

        /// <summary>
        /// Tests the SOAP structure factory20 extensions.
        /// </summary>
        /// <param name="formatType">Type of the format.</param>
        [TestCase(StructureOutputFormatEnumType.SdmxV2StructureDocument)]
        public void TestSoapStructureFactory20Extensions(StructureOutputFormatEnumType formatType)
        {
            var factory = new SriEstatExtensionsStructureWriterFactory();
            var outputFormatType = StructureOutputFormat.GetFromEnum(formatType);
            var format = new SdmxXmlStructureV2Format(
                outputFormatType,
                XmlWriter.Create(Stream.Null));
            var engine = factory.GetStructureWriterEngine(format, null);
            Assert.IsInstanceOf<StructureWriterV2>(engine);
        }

        /// <summary>
        /// Tests the SOAP structure factory20 extensions registry.
        /// </summary>
        /// <param name="formatType">Type of the format.</param>
        [TestCase(StructureOutputFormatEnumType.SdmxV2RegistryQueryResponseDocument)]
        public void TestSoapStructureFactory20ExtensionsRegistry(StructureOutputFormatEnumType formatType)
        {
            var factory = new SriEstatExtensionsStructureWriterFactory();
            var outputFormatType = StructureOutputFormat.GetFromEnum(formatType);
            var format = new SdmxXmlStructureV2Format(
                outputFormatType,
                XmlWriter.Create(Stream.Null));
            var engine = factory.GetStructureWriterEngine(format, null);
            Assert.IsInstanceOf<RegistryInterfaceWriterV2>(engine);
        }

        /// <summary>
        /// Tests the rest structure factory21.
        /// </summary>
        /// <param name="formatType">Type of the format.</param>
        [TestCase(StructureOutputFormatEnumType.SdmxV21StructureDocument)]
        public void TestRestStructureFactory21(StructureOutputFormatEnumType formatType)
        {
            var factory = new SriStructureWriterFactory();
            var outputFormatType = StructureOutputFormat.GetFromEnum(formatType);
            var format = new SdmxStructureFormat(outputFormatType, Encoding.UTF8);
            var engine = factory.GetStructureWriterEngine(format, Stream.Null);
            Assert.IsInstanceOf<StructureWriterEngineV21>(engine);
        }

        /// <summary>
        /// Tests the rest structure factory20.
        /// </summary>
        /// <param name="formatType">Type of the format.</param>
        [TestCase(StructureOutputFormatEnumType.SdmxV2StructureDocument)]
        public void TestRestStructureFactory20(StructureOutputFormatEnumType formatType)
        {
            var factory = new SdmxStructureWriterFactory();
            var outputFormatType = StructureOutputFormat.GetFromEnum(formatType);
            var sriSoapStructureFormat = new SdmxStructureFormat(outputFormatType, Encoding.UTF8);
            var structureWriterEngine = factory.GetStructureWriterEngine(sriSoapStructureFormat, Stream.Null);
            Assert.IsInstanceOf<StructureWriterEngineV2>(structureWriterEngine);
        }

        /// <summary>
        /// Tests the SOAP data factory compact.
        /// </summary>
        /// <param name="formatType">Type of the format.</param>
        [TestCase(DataEnumType.Compact21)]
        [TestCase(DataEnumType.Compact20)]
        public void TestSoapDataFactoryCompact(DataEnumType formatType)
        {
            var factory = new SriSoapDataWriterFactory();
            var outputFormatType = DataType.GetFromEnum(formatType);
            var format = new SriSoapDataFormat(outputFormatType, XmlWriter.Create(Stream.Null));
            var engine = factory.GetDataWriterEngine(format, null);
            Assert.IsInstanceOf<CompactDataWriterEngine>(engine);
        }

        /// <summary>
        /// Tests the SOAP data factory generic.
        /// </summary>
        /// <param name="formatType">Type of the format.</param>
        [TestCase(DataEnumType.Generic21)]
        [TestCase(DataEnumType.Generic20)]
        public void TestSoapDataFactoryGeneric(DataEnumType formatType)
        {
            var factory = new SriSoapDataWriterFactory();
            var outputFormatType = DataType.GetFromEnum(formatType);
            var format = new SriSoapDataFormat(outputFormatType, XmlWriter.Create(Stream.Null));
            var engine = factory.GetDataWriterEngine(format, null);
            Assert.IsInstanceOf<GenericDataWriterEngine>(engine);
        }

        /// <summary>
        /// Tests the rest data factory compact.
        /// </summary>
        /// <param name="formatType">Type of the format.</param>
        [TestCase(DataEnumType.Compact21)]
        [TestCase(DataEnumType.Compact20)]
        public void TestRestDataFactoryCompact(DataEnumType formatType)
        {
            var factory = new SriRestDataWriterFactory();
            var outputFormatType = DataType.GetFromEnum(formatType);
            var format = new SriRestDataFormat(outputFormatType, Encoding.UTF8);
            var engine = factory.GetDataWriterEngine(format, Stream.Null);
            Assert.IsInstanceOf<CompactDataWriterEngine>(engine);
        }

        /// <summary>
        /// Tests the rest data factory generic.
        /// </summary>
        /// <param name="formatType">Type of the format.</param>
        [TestCase(DataEnumType.Generic21)]
        [TestCase(DataEnumType.Generic20)]
        public void TestRestDataFactoryGeneric(DataEnumType formatType)
        {
            var factory = new SriRestDataWriterFactory();
            var outputFormatType = DataType.GetFromEnum(formatType);
            var format = new SriRestDataFormat(outputFormatType, Encoding.UTF8);
            var engine = factory.GetDataWriterEngine(format, Stream.Null);
            Assert.IsInstanceOf<GenericDataWriterEngine>(engine);
        }

        /// <summary>
        /// Tests the SOAP structure factory estat no match.
        /// </summary>
        /// <param name="formatType">Type of the format.</param>
        [TestCase(StructureOutputFormatEnumType.Csv)]
        [TestCase(StructureOutputFormatEnumType.Edi)]
        [TestCase(StructureOutputFormatEnumType.Xlsx)]
        public void TestSoapStructureFactoryEstatNoMatch(StructureOutputFormatEnumType formatType)
        {
            var factory = new SriEstatExtensionsStructureWriterFactory();
            var outputFormatType = StructureOutputFormat.GetFromEnum(formatType);
            var format = Substitute.For<IStructureFormat>();
            format.SdmxOutputFormat.Returns(outputFormatType);
            format.FormatAsString.Returns("Mocked format");
            var engine = factory.GetStructureWriterEngine(format, null);
            Assert.IsNull(engine);
        }

        /// <summary>
        /// Tests the SOAP structure factory no match.
        /// </summary>
        /// <param name="formatType">Type of the format.</param>
        [TestCase(StructureOutputFormatEnumType.Csv)]
        [TestCase(StructureOutputFormatEnumType.Edi)]
        [TestCase(StructureOutputFormatEnumType.Xlsx)]
        public void TestSoapStructureFactoryNoMatch(StructureOutputFormatEnumType formatType)
        {
            var factory = new SdmxStructureWriterFactory();
            var outputFormatType = StructureOutputFormat.GetFromEnum(formatType);
            var format = Substitute.For<IStructureFormat>();
            format.SdmxOutputFormat.Returns(outputFormatType);
            format.FormatAsString.Returns("Mocked format");
            var engine = factory.GetStructureWriterEngine(format, null);
            Assert.IsNull(engine);
        }

        /// <summary>
        /// Tests the rest structure factory no match.
        /// </summary>
        /// <param name="formatType">Type of the format.</param>
        [TestCase(StructureOutputFormatEnumType.Csv)]
        [TestCase(StructureOutputFormatEnumType.Xlsx)]
        public void TestRestStructureFactoryNoMatch(StructureOutputFormatEnumType formatType)
        {
            var factory = new SdmxStructureWriterFactory();
            var outputFormatType = StructureOutputFormat.GetFromEnum(formatType);
            var format = Substitute.For<IStructureFormat>();
            format.SdmxOutputFormat.Returns(outputFormatType);
            format.FormatAsString.Returns("Mocked format");
            var engine = factory.GetStructureWriterEngine(format, Stream.Null);
            Assert.IsNull(engine);
        }

        /// <summary>
        /// Tests the SOAP data factory no match.
        /// </summary>
        /// <param name="formatType">Type of the format.</param>
        [TestCase(DataEnumType.Json)]
        [TestCase(DataEnumType.EdiTs)]
        [TestCase(DataEnumType.Utility20)]
        public void TestSoapDataFactoryNoMatch(DataEnumType formatType)
        {
            var factory = new SriSoapDataWriterFactory();
            var outputFormatType = DataType.GetFromEnum(formatType);
            var format = new SriSoapDataFormat(outputFormatType, XmlWriter.Create(Stream.Null));
            var engine = factory.GetDataWriterEngine(format, null);
            Assert.IsNull(engine);
        }

        /// <summary>
        /// Tests the rest data factory no match.
        /// </summary>
        /// <param name="formatType">Type of the format.</param>
        [TestCase(DataEnumType.Json)]
        [TestCase(DataEnumType.Utility20)]
        public void TestRestDataFactoryNoMatch(DataEnumType formatType)
        {
            var factory = new SriRestDataWriterFactory();
            var outputFormatType = DataType.GetFromEnum(formatType);
            var format = new SriRestDataFormat(outputFormatType, Encoding.UTF8);
            var engine = factory.GetDataWriterEngine(format, Stream.Null);
            Assert.IsNull(engine);
        }
    }
}