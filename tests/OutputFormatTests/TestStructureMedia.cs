﻿// -----------------------------------------------------------------------
// <copyright file="TestStructureMedia.cs" company="EUROSTAT">
//   Date Created : 2016-07-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace OutputFormatTests
{
    using System.Globalization;
    using System.Net.Mime;

    using Estat.Sri.Ws.Format.Sdmx.Constants;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// The test structure media.
    /// </summary>
    [TestFixture]
    internal class TestStructureMedia
    {
        /// <summary>
        /// Tests the <see cref="StructureMediaType.MediaType"/>.
        /// </summary>
        /// <param name="structureMedia">Type of the media .</param>
        /// <param name="expectedMediaName">Expected name of the media.</param>
        /// <param name="expectedVersion">The expected version.</param>
        [TestCase(StructureMediaEnumType.Structure, SdmxMedia.Structure, SdmxSchemaEnumType.VersionTwoPointOne)]
        [TestCase(StructureMediaEnumType.StructureV20, SdmxMedia.Structure, SdmxSchemaEnumType.VersionTwo)]
        [TestCase(StructureMediaEnumType.ApplicationXml, SdmxMedia.ApplicationXml, SdmxSchemaEnumType.VersionTwoPointOne)]
        [TestCase(StructureMediaEnumType.TextXml, SdmxMedia.TextXml, SdmxSchemaEnumType.VersionTwoPointOne)]
        public void TestMedia(StructureMediaEnumType structureMedia, string expectedMediaName, SdmxSchemaEnumType expectedVersion)
        {
            var sdmxSchema = SdmxSchema.GetFromEnum(expectedVersion);
            var expected =
                new ContentType(
                    string.Format(
                        CultureInfo.InvariantCulture,
                        "{0};version={1}",
                        expectedMediaName,
                        sdmxSchema));
            var structureMediaType = StructureMediaType.GetFromEnum(structureMedia);
            Assert.AreEqual(expectedMediaName, structureMediaType.MediaType.MediaType);
            Assert.IsNotNull(structureMediaType.MediaType.Parameters);
            Assert.AreEqual(sdmxSchema.ToString(), structureMediaType.MediaType.Parameters["version"]);
            Assert.AreEqual(expected, structureMediaType.MediaType);
        }

        /// <summary>
        /// Tests the <see cref="StructureMediaType.Format"/>.
        /// </summary>
        /// <param name="mediaEnumType">Type of the media.</param>
        /// <param name="expectedFormat">The expected format.</param>
        [TestCase(StructureMediaEnumType.Structure, StructureOutputFormatEnumType.SdmxV21StructureDocument)]
        [TestCase(StructureMediaEnumType.StructureV20, StructureOutputFormatEnumType.SdmxV2StructureDocument)]
        [TestCase(StructureMediaEnumType.ApplicationXml, StructureOutputFormatEnumType.SdmxV21StructureDocument)]
        [TestCase(StructureMediaEnumType.TextXml, StructureOutputFormatEnumType.SdmxV21StructureDocument)]
        public void TestFormat(StructureMediaEnumType mediaEnumType, StructureOutputFormatEnumType expectedFormat)
        {
            var structureMediaType = StructureMediaType.GetFromEnum(mediaEnumType);
            Assert.AreEqual(expectedFormat, structureMediaType.Format.EnumType);
       }

        /// <summary>
        /// Tests the <see cref="StructureMediaType.Format"/>.
        /// </summary>
        /// <param name="mediaEnumType">Type of the media .</param>
        [TestCase(StructureMediaEnumType.Structure)]
        [TestCase(StructureMediaEnumType.StructureV20)]
        [TestCase(StructureMediaEnumType.ApplicationXml)]
        [TestCase(StructureMediaEnumType.TextXml)]
        public void TestGetFormat(StructureMediaEnumType mediaEnumType)
        {
            var structureMediaType = StructureMediaType.GetFromEnum(mediaEnumType);
            var contentType = structureMediaType.MediaType;
            var roundTripFormat = StructureMediaType.GetFormat(contentType);
            
            Assert.AreEqual(structureMediaType.Format.EnumType, roundTripFormat.EnumType);
        }
    }
}