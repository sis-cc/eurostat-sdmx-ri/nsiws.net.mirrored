﻿// -----------------------------------------------------------------------
// <copyright file="StaticWsdlService.cs" company="EUROSTAT">
//   Date Created : 2013-10-21
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Reflection;
using Controllers.Utils;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace Estat.Sri.Ws.Wsdl
{
    using System;
    using System.Globalization;
    using System.IO;
    using System.Net;
    using System.Text;
    using System.Web;
    using System.Xml;

    using log4net;

    /// <summary>
    ///     The static WSDL service.
    /// </summary>
    public class StaticWsdlService : IStaticWsdlService
    {
        /// <summary>
        /// The _log.
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(StaticWsdlService));

        /// <summary>
        /// The _WSDL registry
        /// </summary>
        private readonly WsdlRegistry _wsdlRegistry;

        private readonly IHttpContextAccessor _contextAccessor;
        private readonly IHostingEnvironment _hostingEnvironment;

        /// <summary>
        /// Initializes a new instance of the <see cref="StaticWsdlService"/> class.
        /// </summary>
        /// <param name="wsdlRegistry">The WSDL registry.</param>
        public StaticWsdlService(WsdlRegistry wsdlRegistry, IHttpContextAccessor contextAccessor, IHostingEnvironment  hostingEnvironment)
        {
            this._wsdlRegistry = wsdlRegistry;
            this._contextAccessor = contextAccessor;
            this._hostingEnvironment = hostingEnvironment;
        }

        /// <summary>
        /// Gets the WSDL.
        /// </summary>
        /// <param name="name">
        /// The service name.
        /// </param>
        /// <returns>
        /// The stream to the WSDL.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "It is OK. We return this stream to WCF")]
        public Stream GetWsdl(string name)
        {
            string prefix = null;
            string localName = null;
            string namespaceUri = null;
            try
            {
                var context = this._contextAccessor.HttpContext;
                var wsdlInfo = this._wsdlRegistry.GetWsdlInfo(name);
                if (wsdlInfo == null)
                {
                    throw new ServiceException(HttpStatusCode.InternalServerError);
                }

                //var uriString = VirtualPathUtility.ToAbsolute("~/");
                var baseAddress = new Uri($"{context.Request.Scheme}://{context.Request.Host}{context.Request.PathBase}");
                var originalPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), Path.Combine(wsdlInfo.OriginalPath.Split('/')));
                var stream = new MemoryStream();

                using (XmlReader reader = XmlReader.Create(originalPath, new XmlReaderSettings { IgnoreWhitespace = true, CloseInput = true, IgnoreComments = true }))
                using (XmlWriter writer = XmlWriter.Create(stream, new XmlWriterSettings { Indent = true, Encoding = Encoding.UTF8 }))
                {
                    while (reader.Read())
                    {
                        switch (reader.NodeType)
                        {
                            case XmlNodeType.Element:
                                bool isEmptyElement = reader.IsEmptyElement;
                                prefix = reader.Prefix;
                                localName = reader.LocalName;
                                namespaceUri = reader.NamespaceURI;
                                writer.WriteStartElement(prefix, localName, namespaceUri);
                                if (!AddBaseAddress(reader, writer, "address", "http://schemas.xmlsoap.org/wsdl/soap/", "location", baseAddress) && !AddBaseAddress(reader, writer, "import", "http://www.w3.org/2001/XMLSchema", "schemaLocation", baseAddress))
                                {
                                    while (reader.MoveToNextAttribute())
                                    {
                                        writer.WriteAttributeString(reader.Prefix, reader.LocalName, reader.NamespaceURI, reader.Value);
                                    }
                                }

                                if (isEmptyElement)
                                {
                                    writer.WriteEndElement();
                                }

                                break;
                            case XmlNodeType.None:
                                break;
                            case XmlNodeType.EndElement:
                                writer.WriteEndElement();
                                break;
                            case XmlNodeType.Text:
                                writer.WriteValue(reader.Value);
                                break;
                        }
                    }
                }

                stream.Position = 0;
                if (context != null)
                {
                    context.Response.ContentType = "text/xml; charset=utf-8";
                }

                return stream;
            }
            catch (Exception e)
            {
                _log.ErrorFormat("Element: {0}, Prefix : {1} , NS {2}", localName, prefix, namespaceUri);
                _log.Error("Error while converting the WSDL", e);
                throw;
            }
        }

        /// <summary>
        /// Adds the base address.
        /// </summary>
        /// <param name="reader">
        /// The reader.
        /// </param>
        /// <param name="writer">
        /// The writer.
        /// </param>
        /// <param name="element">
        /// The element.
        /// </param>
        /// <param name="targetNs">
        /// The target constant.
        /// </param>
        /// <param name="targetAttribute">
        /// The target attribute.
        /// </param>
        /// <param name="baseAddress">
        /// The base address.
        /// </param>
        /// <returns>
        /// <c>True if there was a match; otherwise false</c>
        /// </returns>
        private static bool AddBaseAddress(XmlReader reader, XmlWriter writer, string element, string targetNs, string targetAttribute, Uri baseAddress)
        {
            string localName = reader.LocalName;
            string ns = reader.NamespaceURI;
            bool ret = false;
            if (element.Equals(localName) && targetNs.Equals(ns))
            {
                while (reader.MoveToNextAttribute())
                {
                    string attribute = reader.LocalName;
                    string value = reader.Value;

                    writer.WriteAttributeString(attribute, targetAttribute.Equals(attribute) ? $"{baseAddress.AbsoluteUri.TrimEnd('/')}/{value.TrimStart('/')}" : value);
                }

                ret = true;
            }

            return ret;
        }
    }
}