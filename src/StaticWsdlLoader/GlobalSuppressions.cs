// -----------------------------------------------------------------------
// <copyright file="GlobalSuppressions.cs" company="EUROSTAT">
//   Date Created : 2016-04-11
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Microsoft.Design", "CA1019:DefineAccessorsForAttributeArguments", Scope = "type", Target = "Estat.Sri.Ws.Wsdl.DispatchBodyElementAttribute", Justification = "It is OK. The other parameters are used to construct the field")]
[assembly: SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Scope = "member", Target = "Estat.Sri.Ws.Wsdl.DispatchByBodyElementOperationSelector.#CreateMessageCopy(System.ServiceModel.Channels.Message,System.Xml.XmlDictionaryReader)", Justification = "It is OK. Disposable returned.")]
[assembly: SuppressMessage("Microsoft.Usage", "CA2234:PassSystemUriObjectsInsteadOfStrings", Scope = "member", Target = "Estat.Sri.Ws.Wsdl.StaticWsdlService.#GetWsdl(System.String)", Justification = "It is OK. We want to use URI.")]