﻿// -----------------------------------------------------------------------
// <copyright file="SchemaCache.cs" company="EUROSTAT">
//   Date Created : 2010-06-15
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.SdmxSoapValidatorExtension
{
    using System;
    using System.IO;
    using System.Reflection;
    using System.Web.Services.Protocols;
    using System.Xml;
    using System.Xml.Schema;

    using Estat.Nsi.SdmxSoapValidatorExtension.Properties;

    /// <summary>
    /// A helper class that keeps a cached copy of the various schema sets
    /// </summary>
    public class SchemaCache
    {
        /// <summary>
        /// Constant string that contains the resource location of soap11 XSD
        /// </summary>
        private const string Soap11Location = "Estat.Nsi.SdmxSoapValidatorExtension.soap11.xsd";

        /// <summary>
        /// Constant string that contains the resource location of soap12 XSD
        /// </summary>
        private const string Soap12Location = "Estat.Nsi.SdmxSoapValidatorExtension.soap12.xsd";

        /// <summary>
        /// The singleton instance
        /// </summary>
        private static readonly SchemaCache _instance = new SchemaCache();

        /// <summary>
        /// This XmlSchemaSet field is used to hold the soap 1.1 and soap 1.2 schemas
        /// </summary>
        private readonly XmlSchemaSet _soapSchema;

        /// <summary>
        /// Prevents a default instance of the <see cref="SchemaCache"/> class from being created. 
        /// Initialize a new instance of the <see cref="SchemaCache"/> class
        /// </summary>
        private SchemaCache()
        {
            var schemaSet = new XmlSchemaSet();
            schemaSet.ValidationEventHandler -= this.SchemaSetValidationEventHandler;
            schemaSet.ValidationEventHandler += this.SchemaSetValidationEventHandler;
            schemaSet.Add(this.LoadXsdFromResource(GetSoapXsdLocation(SoapProtocolVersion.Soap11)));
            schemaSet.Add(this.LoadXsdFromResource(GetSoapXsdLocation(SoapProtocolVersion.Soap12)));
            this._soapSchema = schemaSet;
            this._soapSchema.Compile();
        }

        /// <summary>
        /// Gets or sets the soap 1.1, soap 1.2, the web server WSDL and sdmx schemes
        /// </summary>
        public static XmlSchemaSet SoapBodySchema { get; set; }

        /// <summary>
        /// Gets the soap 1.1 and soap 1.2 schemas
        /// </summary>
        public static XmlSchemaSet SoapSchema
        {
            get
            {
                return _instance._soapSchema;
            }
        }

        /// <summary>
        /// This getter returns the resource location of the soap schemas
        /// </summary>
        /// <param name="version">
        /// The Soap version
        /// </param>
        /// <returns>
        /// The resource location
        /// </returns>
        public static string GetSoapXsdLocation(SoapProtocolVersion version)
        {
            switch (version)
            {
                case SoapProtocolVersion.Soap11:
                    return Soap11Location;
                default:
                    return Soap12Location;
            }
        }

        /// <summary>
        /// Loads a XSD file from an embedded resource to a <see cref="System.Xml.Schema.XmlSchema"/> object
        /// </summary>
        /// <param name="name">
        /// The resource location of the XSD 
        /// </param>
        /// <returns>
        /// The XSD file as a <see cref="System.Xml.Schema.XmlSchema"/> object
        /// </returns>
        private XmlSchema LoadXsdFromResource(string name)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            XmlSchema schema;

            using (Stream manifestResourceStream = assembly.GetManifestResourceStream(name))
            {
                if (manifestResourceStream != null)
                {
                    using (XmlReader xr = XmlReader.Create(manifestResourceStream))
                    {
                        schema = XmlSchema.Read(xr, this.SchemaSetValidationEventHandler);
                    }
                }
                else
                {
                    schema = new XmlSchema();
                }
            }

            return schema;
        }

        /// <summary>
        /// The handler for schema validation errors. It throws a server Soap Exception
        /// </summary>
        /// <param name="sender">
        /// The XmlSchemaSet object that initiated this event
        /// </param>
        /// <param name="e">
        /// The event argument
        /// </param>
        private void SchemaSetValidationEventHandler(object sender, ValidationEventArgs e)
        {
            throw this.ThrowServerSoapException(e.Exception);
        }

        /// <summary>
        /// Creates a server SoapException
        /// </summary>
        /// <param name="ex">
        /// The exception that causes this error
        /// </param>
        /// <returns>
        /// A SoapException
        /// </returns>
        private SoapException ThrowServerSoapException(Exception ex)
        {
            // TODO log server exceptions
            string source = this.GetType().Name;
            return SoapFaultFactory.CreateSoapException(source, string.Empty, ex.ToString(), Resources.error_server_code, source, false, Resources.error_server_msg);
        }
    }
}