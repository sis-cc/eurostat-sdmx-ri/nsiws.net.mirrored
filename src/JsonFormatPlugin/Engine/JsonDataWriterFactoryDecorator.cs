// -----------------------------------------------------------------------
// <copyright file="JsonDataWriterFactoryDecorator.cs" company="EUROSTAT">
//   Date Created : 2016-07-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Format.Json.Engine
{
    using System.IO;

    using Estat.Sri.Ws.Configuration;
    using Estat.Sri.Ws.Format.Json.Model;

    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.DataParser.Factory;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;

    /// <summary>
    /// This class decorates the <see cref="SdmxJsonDataWriterFactory"/> mainly to register automatically and second to avoid throwing exceptions when it is not supported.
    /// </summary>
    /// <seealso cref="Org.Sdmxsource.Sdmx.Api.Factory.IDataWriterFactory" />
    public class JsonDataWriterFactoryDecorator : IDataWriterFactory
    {
        private readonly SdmxJsonDataWriterFactory _dataWriterFactory;

        public JsonDataWriterFactoryDecorator(SettingsFromConfigurationManager configuration)
        {
            _dataWriterFactory = new SdmxJsonDataWriterFactory()
            {
                UseHierarchicalCodelists = configuration.UseHierarchicalCodelists
            };
        }

        /// <summary>
        /// Gets the data writer engine.
        /// </summary>
        /// <param name="dataFormat">The data format.</param>
        /// <param name="outStream">The output stream.</param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.Api.Engine.IDataWriterEngine" />.
        /// </returns>
        public IDataWriterEngine GetDataWriterEngine(IDataFormat dataFormat, Stream outStream)
        {
            if (outStream == null)
            {
                // We don't support SOAP so we return null
                return null;
            }

            var format = dataFormat as JsonRestDataFormat;

            if (format != null)
            {
                var sdmxJsonDataFormat = new SdmxJsonDataFormat(
                    format.Retriever.RetrievalManager, 
                    format.SdmxDataFormat, 
                    format.Retriever.Translator,
                    format.RetrieverManager
                );
                  
                // Only proceed when we have a JSON format.
                return this._dataWriterFactory.GetDataWriterEngine(sdmxJsonDataFormat, outStream);
            }

            return null;
        }
    }
}