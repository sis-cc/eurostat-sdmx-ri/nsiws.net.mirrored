// -----------------------------------------------------------------------
// <copyright file="DependencyRegisterManager.cs" company="EUROSTAT">
//   Date Created : 2015-12-18
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------



namespace Estat.Sri.Ws.Format.Json.Model
{
    using System.Text;
    using Sdmxsource.Extension.Model;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model;
    using Org.Sdmxsource.Translator;
    using System.Collections.Generic;
    using System.Globalization;


    public class JsonRestStructureFormat : SdmxStructureJsonFormat, IFormatWithTranslator
    {

        /// <summary>
        /// The SDMX data format
        /// </summary>
        private readonly DataType _sdmxDataFormat = DataType.GetFromEnum(DataEnumType.Json);


        /// <summary>
        /// Initializes a new instance of the <see cref="JsonRestDataFormat" /> class.
        /// </summary>
        /// <param name="encoding">The encoding.</param>
        /// <param name="requestedStructureType"></param>
        public JsonRestStructureFormat(Encoding encoding, StructureOutputFormatEnumType structureOutputFormatEnumType)
            : this(encoding, new BestMatchLanguageTranslator(new List<CultureInfo>(), new List<CultureInfo>(), CultureInfo.CurrentCulture), structureOutputFormatEnumType)
        {
        }

        public JsonRestStructureFormat(Encoding encoding, ITranslator translator, StructureOutputFormatEnumType structureOutputFormatEnumType)
            : base( translator, structureOutputFormatEnumType)
        {
        }

        /// <summary>
        /// Gets the SDMX data format that this interface is describing.
        /// If this is not describing an SDMX message then this will return null and the implementation class will be expected
        /// to expose additional methods to understand the data
        /// </summary>
        public DataType SdmxDataFormat
        {
            get
            {
                // if there is not corresponding data type for this format; return null
                return this._sdmxDataFormat;
            }
        }

        public new ITranslator Translator { set; get; }
    }
}
