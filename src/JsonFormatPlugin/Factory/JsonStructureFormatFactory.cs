// -----------------------------------------------------------------------
// <copyright file="DependencyRegisterManager.cs" company="EUROSTAT">
//   Date Created : 2015-12-18
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------



namespace Estat.Sri.Ws.Format.Json.Factory
{
    using System.Collections.Generic;
    using System.Net;
    using System.Text;
    using Estat.Sri.Ws.Format.Json.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;
    using Estat.Sdmxsource.Extension.Factory;
    using Estat.Sdmxsource.Extension.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using System.Xml;
    using System.Net.Mime;

    public class JsonStructureFormatFactory : AbstractStructureWriterPluginContract
    {
        /// <summary>
        /// The _content list
        /// </summary>
        private static readonly IList<string> _contentList = new[]
        {
            "application/vnd.sdmx.structure+json;version=2",
            "application/vnd.sdmx.structure+json;version=2.0",
            "application/vnd.sdmx.structure+json;version=2.0.0",
            "application/vnd.sdmx.structure+json;version=1.0",
            "application/vnd.sdmx.structure+json;version=1.0.0-wd",
            "text/json", "application/json"
        };

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Estat.Sdmxsource.Extension.Factory.AbstractStructureWriterPluginContract"/> class.
        /// </summary>
        public JsonStructureFormatFactory()
            : base(_contentList)
        {
        }

        /// <summary>
        /// Builds the format. This is used only for REST. This needs to be overridden. The <paramref name="mediaType"/> and the <paramref name="encoding"/> are the most important.
        /// </summary>
        /// <param name="mediaType">Type of the media.</param><param name="header">The header.</param><param name="encoding">The encoding.</param><param name="sortedAcceptHeaders">The sorted accept headers.</param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Format.IStructureFormat"/>.
        /// </returns>
        protected override IStructureFormat BuildFormat(string mediaType, WebHeaderCollection header, Encoding encoding, ISortedAcceptHeaders sortedAcceptHeaders)
        {
            var contentType = new ContentType(mediaType) { CharSet = encoding.WebName };

            if (contentType.Parameters.ContainsKey("version") && (contentType.Parameters["version"] == "1.0" || contentType.Parameters["version"] == "1.0.0-wd"))
            {
                return new JsonRestStructureFormat(encoding, StructureOutputFormatEnumType.JsonV10);
            }
            else
            {
                return new JsonRestStructureFormat(encoding, StructureOutputFormatEnumType.Json);
            }
        }
    }
}