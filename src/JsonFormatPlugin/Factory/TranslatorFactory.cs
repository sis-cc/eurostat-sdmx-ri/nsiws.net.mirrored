// -----------------------------------------------------------------------
// <copyright file="DependencyRegisterManager.cs" company="EUROSTAT">
//   Date Created : 2015-12-18
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Configuration;
using Org.Sdmxsource.Sdmx.Structureparser.Builder;
using Org.Sdmxsource.Sdmx.Util.Objects;
using Org.Sdmxsource.Sdmx.Util.Objects.Container;

namespace Estat.Sri.Ws.Format.Json.Factory
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using Estat.Sdmxsource.Extension.Factory;
    using Estat.Sdmxsource.Extension.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects;
    using Org.Sdmxsource.Translator;


    public class TranslatorFactory : ITranslatorFactory
    {
        public ITranslator GetTranslator(ISdmxObjects sdmxObjects, IList<HeaderValueWithQuality<CultureInfo>> requestedLanguages)
        {
            if (sdmxObjects == null)
            {
                throw new ArgumentNullException("sdmxObjects");
            }

            if (requestedLanguages == null)
            {
                throw new ArgumentNullException("requestedLanguages");
            }

            ITranslator translator = 
                new BestMatchLanguageTranslator(
                    requestedLanguages.Select(quality => quality.Value).ToArray(),
                    sdmxObjects,
                    CultureInfo.CurrentCulture); 
            return translator;
        }

        public ITranslator GetTranslator(ISuperObjects superObjects, IList<HeaderValueWithQuality<CultureInfo>> requestedLanguages)
        {
            if (superObjects == null)
            {
                throw new ArgumentNullException("superObjects");
            }

            if (requestedLanguages == null)
            {
                throw new ArgumentNullException("requestedLanguages");
            }

            return GetTranslator(new SdmxObjectsImpl(null, superObjects.AllMaintainables.Select(m => m.BuiltFrom)), requestedLanguages);
        }
    }
}