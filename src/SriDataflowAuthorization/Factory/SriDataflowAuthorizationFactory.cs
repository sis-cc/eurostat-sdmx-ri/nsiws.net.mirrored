// -----------------------------------------------------------------------
// <copyright file="SriDataflowAuthorizationFactory.cs" company="EUROSTAT">
//   Date Created : 2016-02-12
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Linq;

namespace Estat.Sri.DataflowAuthorization.Factory
{
    using System;
    using System.Security.Principal;

    using DryIoc;

    using Estat.Nsi.AuthModule.Engine;
    using Estat.Nsi.AuthModule.Manager;
    using Estat.Nsi.AuthModule.Model;
    using Estat.Sdmxsource.Extension.Engine;
    using Estat.Sdmxsource.Extension.Factory;
    using Estat.Sri.DataflowAuthorization.Engine;
    using Estat.Sri.Mapping.Api.Manager;

    /// <summary>
    /// The SDMX-RI Authorization factory for <see cref="SriDataflowAuthorizationEngine"/>
    /// </summary>
    public class SriDataflowAuthorizationFactory : IDataflowAuthorizationFactory
    {
        /// <summary>
        /// The authorization provider
        /// </summary>
        private readonly Lazy<IAuthorizationProvider> _authorizationProvider;

        /// <summary>
        /// Initializes a new instance of the <see cref="SriDataflowAuthorizationFactory"/> class.
        /// </summary>
        public SriDataflowAuthorizationFactory(Lazy<IConfigurationStoreManager> configManager)
        {
            this._authorizationProvider = new Lazy<IAuthorizationProvider>(() => this.GetAuthorizationProvider(configManager));
        }

        /// <summary>
        /// Gets the engine.
        /// </summary>
        /// <param name="principal">The principal.</param>
        /// <returns>The <see cref="IDataflowAuthorizationEngine"/>.</returns>
        public IDataflowAuthorizationEngine GetEngine(IPrincipal principal)
        {
            DataflowPrincipal dataflowPrincipal = principal as DataflowPrincipal;
            if (dataflowPrincipal != null)
            {
                return new SriDataflowAuthorizationEngine(dataflowPrincipal, this._authorizationProvider.Value);
            }

            return null;
        }

        /// <summary>
        /// Gets the authorization provider.
        /// </summary>
        /// <returns>
        /// The <see cref="IAuthorizationProvider"/>.
        /// </returns>
        private IAuthorizationProvider GetAuthorizationProvider(Lazy<IConfigurationStoreManager> configManager)
        {
            using (var container = new Container(rules => rules.With(FactoryMethod.ConstructorWithResolvableArguments)))
            {
                // TODO check performance
                container.RegisterInstance(configManager.Value);
                container.RegisterMany(new[] { typeof(IAuthorizationProvider).Assembly }, serviceTypeCondition: type => type.IsPublic );
                var providerResolverManager = container.Resolve<ProviderResolverManager>();
                return providerResolverManager.CreateAuthorizationProvider();
            }
        }
    }
}