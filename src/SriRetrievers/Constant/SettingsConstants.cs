// -----------------------------------------------------------------------
// <copyright file="SettingsConstants.cs" company="EUROSTAT">
//   Date Created : 2011-09-09
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Retrievers.Constant
{
    using Estat.Sri.Ws.Retrievers.Manager;

    /// <summary>
    ///     Constant values used in <see cref="MappingStoreSettingsManager" />
    /// </summary>
    internal static class SettingsConstants
    {
        /// <summary>
        ///     The web.config appSettings variable name for the default DDB Oracle Provider
        /// </summary>
        public const string DefaultDdbOracleProvider = "defaultDDBOracleProvider";

        /// <summary>
        ///     The Name of the mapping store connection string. The default value is MappingStoreServer
        /// </summary>
        public const string MappingStoreConnectionName = "MappingStoreServer";
    }
}