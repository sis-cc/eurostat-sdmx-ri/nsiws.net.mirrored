﻿// -----------------------------------------------------------------------
// <copyright file="Startup.cs" company="EUROSTAT">
//   Date Created : 2019-01-30
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using DryIoc;
using estat.sri.ws.auth.api;
using estat.sri.ws.auth.generic;
using Microsoft.Extensions.Configuration;
using System.Linq;

namespace NSIWebServiceCore
{
    internal class ExtraConfiguration
    {
        public ExtraConfiguration(IContainer container, IConfiguration configuration)
        {

            var ruleConfig = configuration.GetSection(RuleAuthorizationConfiguration.DefaultSection)?.Get<RuleAuthorizationConfiguration>();
            if (ruleConfig != null && ruleConfig.Enabled && ruleConfig.Method != null)
            {
                // possible HACK It is expected that IRuleProviderConfiguration implementations get what they need from the constructor parameters
                // e.g. IContainer & IConfiguration
                // resolve only
                container.ResolveMany<IRuleProviderConfiguration>().ToArray();
                // run after all other registrations in order to keep any injected
                container.Register<IGenericAuthorization, GenericAuthorizationDefault>(ifAlreadyRegistered: IfAlreadyRegistered.Keep);
            }
        }
    }
}