﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.ActionConstraints;
using Microsoft.AspNetCore.Routing;

namespace Estat.Sri.Ws.NSIWebServiceCore.Controllers
{
    public class QueryStringConstraint : ActionMethodSelectorAttribute
    {
        private readonly string _valueName;

        public QueryStringConstraint(string valueName)
        {
            this._valueName = valueName;
        }

        public override bool IsValidForRequest(RouteContext routeContext, ActionDescriptor action)
        {
            IList<ParameterDescriptor> methodParameters = action.Parameters;

            ICollection<string> queryStringKeys = routeContext.HttpContext.Request.Query.Keys.Select(q => q.ToLower()).ToList();
            if (queryStringKeys.Contains(this._valueName) && queryStringKeys.Count == 1)
            {
                return true;
            }

            return false;
        }
    }
}