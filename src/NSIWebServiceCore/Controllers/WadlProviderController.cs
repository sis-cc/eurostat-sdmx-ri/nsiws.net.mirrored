﻿using System.IO;
using Estat.Sri.Ws.Rest;
using Microsoft.AspNetCore.Mvc;

namespace NSIWebServiceCore.Controllers
{
    [Route("rest/application.wadl")]
    [ApiController]
    public class WadlProviderController : Controller
    {
        private readonly IWadlProvider _wadlProvider;

        public WadlProviderController(IWadlProvider wadlProvider)
        {
            this._wadlProvider = wadlProvider;
        }

        [HttpGet]
        public void GetWadl()
        {
            this._wadlProvider.GetWadl();
        }
    }
}