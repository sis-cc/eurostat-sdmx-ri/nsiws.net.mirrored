﻿using Estat.Sri.Ws.LoginService;
using Estat.Sri.Ws.LoginService.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Estat.Sri.Ws.NSIWebServiceCore.Controllers
{
    [Route("login/")]
    public class LoginController : Controller
    {
        private readonly ILoginResource _service;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public LoginController(ILoginResource service,IHttpContextAccessor httpContextAccessor)
        {
            this._service = service;
            this._httpContextAccessor = httpContextAccessor;
        }

        [HttpPost("token")]
        public AccessTokenResponse PostGrantRequest()
        {
            return this._service.PostGrantRequest(this.HttpContext.Request.Body, this._httpContextAccessor.HttpContext);
        }
    }
}