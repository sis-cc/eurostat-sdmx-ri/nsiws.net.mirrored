﻿using System.Threading.Tasks;
using Estat.Sri.Ws.Rest;
using Microsoft.AspNetCore.Mvc;

namespace NSIWebServiceCore.Controllers
{
    [Route("data")]
    public class DataResourceOnTheFlyAnnotationsController : Controller
    {
        private readonly IDataResource _dataResource;

        public DataResourceOnTheFlyAnnotationsController(IDataResource availableResource)
        {
            this._dataResource = availableResource;
        }

        [HttpGet("{flowRef}/{key=ALL}/{providerRef=ALL}/")]
        [ResponseCache(CacheProfileName = "CustomCacheProfile")]
        public async Task GetGenericData(string flowRef, string key, string providerRef)
        {
            await this._dataResource.GetGenericDataAsync(flowRef, key, providerRef);
        }
    }
}