﻿using System.Threading;
using System.Threading.Tasks;
using Estat.Sri.Ws.Controllers.Manager;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Estat.Sri.Ws.NSIWebServiceCore
{
    public sealed class StructureObserverBackgroundService : BackgroundService
    {
        private readonly StructureSubmitterObserverDecorator.StructureEventQueue _backgroundQueue;

        public StructureObserverBackgroundService(StructureSubmitterObserverDecorator.StructureEventQueue backgroundQueue)
        {
            _backgroundQueue = backgroundQueue;
        }

        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                if (_backgroundQueue.TaskInQueue == 0)
                    await Task.Delay(_backgroundQueue.MillisecondsToWaitBeforePickingUpTask, cancellationToken);
                else
                    await _backgroundQueue.Dequeue(cancellationToken);
            }
        }

        public static void Register(IServiceCollection services)
        {
            services.AddSingleton(new StructureSubmitterObserverDecorator.StructureEventQueue(500));
            services.AddSingleton<IHostedService, StructureObserverBackgroundService>();
        }
    }
}
