// -----------------------------------------------------------------------
// <copyright file="Program.cs" company="EUROSTAT">
//   Date Created : 2019-01-30
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using Estat.Sri.Ws.NSIWebServiceCore;
using log4net;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ConfigurationManager = System.Configuration.ConfigurationManager;

namespace NSIWebServiceCore
{
    public class Program
    {
        private const string ConfigDir = "config";
        private static readonly ILog _log = LogManager.GetLogger(typeof(Program));
        public static readonly string ConfigDirPath;

        static Program()
        {
            var rootDirPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            ConfigDirPath = Path.Combine(rootDirPath, ConfigDir);
            SetAppConfigPath();
        }

        /// <summary>
        /// Overwrites the path where System.Configuration.ConfigurationManager is looking for a config file
        /// By default it reads from NSIWebServiceCore.dll.config (next to executable assembly, located at the root)
        /// See more here https://github.com/dotnet/runtime/blob/main/src/libraries/System.Configuration.ConfigurationManager/src/System/Configuration/ClientConfigPaths.cs#L87
        /// </summary>
        private static void SetAppConfigPath()
        {
            var configFilePath = Path.Combine(ConfigDirPath, "app.config");

            if (!File.Exists(configFilePath))
            {
                return;
            }

            AppDomain.CurrentDomain.SetData("APP_CONFIG_FILE", configFilePath);

            typeof(ConfigurationManager)
                .GetField("s_initState", BindingFlags.NonPublic | BindingFlags.Static)
                .SetValue(null, 0);

            typeof(ConfigurationManager)
                .GetField("s_configSystem", BindingFlags.NonPublic | BindingFlags.Static)
                .SetValue(null, null);

            typeof(ConfigurationManager)
                .Assembly.GetTypes()
                .Where(x => x.FullName =="System.Configuration.ClientConfigPaths")
                .First()
                .GetField("s_current", BindingFlags.NonPublic |BindingFlags.Static)
                .SetValue(null, null);
        }

        public static void Main(string[] args)
        {
            try
            {
                var host = CreateWebHostBuilder(args)
                        .Build();

                LogHelper.SetupLog4Net(host.Services);
                _log.Info("Application started");

                host.Run();
            }
            catch (Exception e)
            {
                _log.Error("Error starting application", e);
                Console.WriteLine(e);
                throw;
            }
        }

        //https://github.com/aspnet/MetaPackages/blob/release/2.2/src/Microsoft.AspNetCore/WebHost.cs
        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
        {
            return WebHost.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    var configDir = new DirectoryInfo(ConfigDirPath);

                    if (!configDir.Exists)
                        throw new InvalidOperationException($"Config directory not found: {ConfigDirPath}");

                    foreach (var json in configDir.GetFiles("*.json"))
                    {
                        config.AddJsonFile(json.FullName);
                    }

                    config.AddEnvironmentVariables();
                })
                .ConfigureServices((context, services) =>
                {
                    services.Configure<KestrelServerOptions>(
                        context.Configuration.GetSection("Kestrel"));
                })
                .UseStartup<Startup>();
        }
    }
}