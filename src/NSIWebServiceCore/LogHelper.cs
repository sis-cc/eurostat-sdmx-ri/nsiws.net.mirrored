// -----------------------------------------------------------------------
// <copyright file="LogHelper.cs" company="EUROSTAT">
//   Date Created : 2019-02-01
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.IO;
using System.Linq;
using System.Xml;
using Google.Cloud.Logging.Log4Net;
using log4net.Appender;
using log4net.Core;
using log4net.Layout;
using log4net.Repository.Hierarchy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NSIWebServiceCore;

namespace Estat.Sri.Ws.NSIWebServiceCore
{
    public static class LogHelper
    {
        public const string GoogleLoggingSection = "googleLogging";

        public static void SetupLog4Net(IServiceProvider serviceProvider)
        {
            var logInfo = new FileInfo(Path.Combine(Program.ConfigDirPath, "log4net.config"));

            if (!logInfo.Exists)
            {
                Console.WriteLine("Could not find log4net.config at " + logInfo.FullName);
                return;
            }

            XmlDocument log4netConfig = new XmlDocument();

            using (var fileStream = logInfo.OpenRead())
            {
                log4netConfig.Load(fileStream);
            }

            var repo = log4net.LogManager.CreateRepository(
                typeof(LogHelper).Assembly, 
                typeof(Hierarchy)
            );

            var xmlElement = log4netConfig["log4net"];

            log4net.Config.XmlConfigurator.Configure(repo, xmlElement);

            var googleConfig = serviceProvider.GetRequiredService<IConfiguration>()
                .GetSection(GoogleLoggingSection)
                .Get<GoogleLoggingConfig>();
            
            if(googleConfig!=null && !string.IsNullOrEmpty(googleConfig.ProjectId) && googleConfig.Enabled)
            {
                ((Hierarchy) repo).Root.AddAppender(CreateGoogleAppender(googleConfig));
            }
        }

        static IAppender CreateGoogleAppender(GoogleLoggingConfig config)
        {
            var appender = new GoogleStackdriverAppender()
            {
                ProjectId = config.ProjectId,
                LogId = config.LogId ?? "NSI",
                Threshold = StringToLogLevel(config.LogLevel, Level.Debug),
                UsePatternWithinCustomLabels = true,
                Layout = new PatternLayout("%logger - %message"),
                DisableResourceTypeDetection = true
            };

            appender.AddCustomLabel(new GoogleStackdriverAppender.Label()
            {
                Key = "thread",
                Value = "%thread"
            });

            appender.AddCustomLabel(new GoogleStackdriverAppender.Label()
            {
                Key = "logger",
                Value = "%logger"
            });

            appender.AddCustomLabel(new GoogleStackdriverAppender.Label()
            {
                Key = "exception",
                Value = "%exception"
            });

            appender.AddCustomLabel(new GoogleStackdriverAppender.Label()
            {
                Key = "user",
                Value = "%property{User}"
            });

            appender.AddCustomLabel(new GoogleStackdriverAppender.Label()
            {
                Key = "server",
                Value = "%property{log4net:HostName}"
            });

            appender.ActivateOptions();

            return appender;
        }

        static Level StringToLogLevel(string logLevel, Level defaultLevel)
        {
            var level = defaultLevel;

            var loggerRepository = LoggerManager.GetAllRepositories().FirstOrDefault();

            if (loggerRepository != null)
            {
                var levelMatch = loggerRepository.LevelMap[logLevel];

                if (levelMatch != null)
                {
                    level = levelMatch;
                }
            }

            return level;
        }
    }

    public class GoogleLoggingConfig
    {
        public bool Enabled { get; set; }
        public string ProjectId { get; set; }
        public string LogId { get; set; }
        public string LogLevel { get; set; }
    }
}