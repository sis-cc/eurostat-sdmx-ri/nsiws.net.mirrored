﻿// -----------------------------------------------------------------------
// <copyright file="SoapHelper.cs" company="EUROSTAT">
//   Date Created : 2019-02-01
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Estat.Sri.Ws.Controllers.Constants;
using Estat.Sri.Ws.Soap;
using Microsoft.AspNetCore.Http;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Factory;
using Org.Sdmxsource.Sdmx.Api.Util;
using Org.Sdmxsource.Sdmx.Util.Xml;
using Org.Sdmxsource.Util.Extensions;
using Org.Sdmxsource.Util.Io;

namespace Estat.Sri.Ws.NSIWebServiceCore
{
    internal static class SoapHelper
    {
        private static readonly IReadableDataLocationFactory _dataLocationFactory = new ReadableDataLocationFactory();
        public static async Task InvokeSoapService<TSoapService>(string soapServiceNamespace, TSoapService soapService, HttpContext context) where TSoapService : ISoapService
        {
            // use readable data location so we can re-read the request multiple times
            using(Stream inputStream = context.Request.Body)
            using (IReadableDataLocation soapRequestLocation = _dataLocationFactory.GetReadableDataLocation(inputStream))
            {
                SoapOperation operation = GetSoapOperation(soapServiceNamespace, soapRequestLocation);

                if (operation == SoapOperation.Null)
                {
                    throw new SdmxSemmanticException("Could not find request for " + soapServiceNamespace);
                }

                if (!operation.IsOneOf(SoapOperation.RegistryInterfaceRequest, SoapOperation.SubmitStructure))
                {
                    await SmallRequestHandler(soapService, context, soapRequestLocation, operation);
                }
                else
                {
                    await BigRequestHandler(soapService, context, soapRequestLocation, operation);
                }
            }
        }

        /// <summary>
        /// Use a temporary file to store the SDMX part of the request, this is for SubmitStructure or Registry Interface requests
        /// </summary>
        /// <typeparam name="TSoapService">The SDMX SOAP Service type</typeparam>
        /// <param name="soapService">The SOAP service</param>
        /// <param name="outputStream">The output stream</param>
        /// <param name="soapRequestLocation">The SOAP request location</param>
        /// <param name="operation">The SOAP operation</param>
        private static async Task BigRequestHandler<TSoapService>(TSoapService soapService, HttpContext context, IReadableDataLocation soapRequestLocation, SoapOperation operation) where TSoapService : ISoapService
        {
            string tempFile = Path.GetTempFileName();
            try
            {
                using (Stream sdmxRequest = soapRequestLocation.InputStream)
                using (XmlWriter writer = XmlWriter.Create(tempFile, XmlHelper.GetXmlWriterSettings()))
                {
                    SoapUtils.ExtractSdmxMessage(sdmxRequest, writer);
                }

                using (IReadableDataLocation sdmxRequestReadbleLocation = _dataLocationFactory.GetReadableDataLocation(new FileInfo(tempFile)))
                    await soapService.HandleRequest(sdmxRequestReadbleLocation, operation, context);
            }
            finally
            {
                if (File.Exists(tempFile))
                {
                    File.Delete(tempFile);
                }
            }
        }

        /// <summary>
        /// Use a memory buffer to store the SDMX part of the request, this is for normal structure or data queries
        /// </summary>
        /// <typeparam name="TSoapService">The SDMX SOAP Service type</typeparam>
        /// <param name="soapService">The SOAP service</param>
        /// <param name="outputStream">The output stream</param>
        /// <param name="soapRequestLocation">The SOAP request location</param>
        /// <param name="operation">The SOAP operation</param>
        private static async Task SmallRequestHandler<TSoapService>(TSoapService soapService, HttpContext context, IReadableDataLocation soapRequestLocation, SoapOperation operation) where TSoapService : ISoapService
        {
            byte[] requestAsBytes;
            using(MemoryStream tempBuffer = new MemoryStream())
            using (Stream sdmxRequest = soapRequestLocation.InputStream)
            {
                var xmlWriterSettings = XmlHelper.GetXmlWriterSettings();

                // TODO for some reason utf-16 is used...
                //xmlWriterSettings.Encoding = Encoding.Unicode;
                using (XmlWriter writer = XmlWriter.Create(tempBuffer, xmlWriterSettings))
                {
                    SoapUtils.ExtractSdmxMessage(sdmxRequest, writer);
                }

                requestAsBytes = tempBuffer.ToArray();
            }

            using (IReadableDataLocation sdmxRequestReadbleLocation = _dataLocationFactory.GetReadableDataLocation(requestAsBytes))
            {
                await soapService.HandleRequest(sdmxRequestReadbleLocation, operation, context);
            }
        }

        /// <summary>
        /// Get the SOAP operation
        /// </summary>
        /// <param name="soapServiceNamespace">The SOAP service namespace</param>
        /// <param name="soapRequestLocation">The SOAP request location</param>
        /// <returns></returns>
        private static SoapOperation GetSoapOperation(string soapServiceNamespace, IReadableDataLocation soapRequestLocation)
        {
            SoapOperation operation = SoapOperation.Null;
            using (var readableStream = soapRequestLocation.InputStream)
            using (var xmlTextReader = XmlReader.Create(readableStream))
            {
                while (xmlTextReader.Read())
                {
                    var nodeType = xmlTextReader.NodeType;
                    if (nodeType == XmlNodeType.Element)
                    {
                        var localName = xmlTextReader.LocalName;
                        var namespaceUri = xmlTextReader.NamespaceURI;
                        if (string.Equals(soapServiceNamespace, namespaceUri))
                        {
                            if (!System.Enum.TryParse(localName, out operation))
                            {
                                // TODO change to FaultException or something like that
                                throw new SdmxSemmanticException("Unknown operation " + localName);
                            }

                            break;
                        }
                    }
                }
            }

            return operation;
        }
    }
}