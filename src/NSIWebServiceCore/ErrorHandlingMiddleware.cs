using System;
using System.Net;
using System.Threading.Tasks;
using System.Xml;
using Controllers.Utils;
using Estat.Sri.Ws.Controllers.Engine;
using Estat.Sri.Ws.Controllers.Exception;
using Estat.Sri.Ws.Controllers.Model;
using log4net;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;

namespace NSIWebServiceCore
{
    public class ErrorHandlingMiddleware
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(ErrorHandlingMiddleware));

        public static async Task Invoke(HttpContext context)
        {
            var exception = context.Features.Get<IExceptionHandlerFeature>()?.Error;

            if (exception == null)
                return;

            _log.Error("Unhandled exception", exception);

            HttpStatusCode code = HttpStatusCode.InternalServerError;

            var exceptionMessage = exception.Message;
            if (exception is ServiceException serviceException)
            {
                code = serviceException.HttpStatusCode;
            }

            //this is because of IMessageFaultSoapBuilder and should be checked
            // TODO use a specific exception for Soap errors
            if (exception is FaultException<SdmxFault> faultException)
            {
                try
                {
                    WriteSoapFault(context, faultException);

                    return;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    return;
                }
            }
            else
            {
                context.Response.ContentType = "text/plain";
            }

            if (context.Response.HasStarted)
            {
                //we need to actually write even if the response has started

                return;
            }


            context.Response.StatusCode = (int)code;

            //var result = JsonConvert.SerializeObject(new {error = exceptionMessage});
            await context.Response.WriteAsync(exceptionMessage);
        }

        private static void WriteSoapFault(HttpContext context, FaultException<SdmxFault> faultException)
        {
// We need SOAP namespaces we cannot serialize a random object
            var settings = new XmlWriterSettings { OmitXmlDeclaration = true };
            settings.NamespaceHandling = NamespaceHandling.OmitDuplicates;
            settings.Indent = true;

            context.Response.StatusCode = 500;
            context.Response.ContentType = "application/xml";
            using (var responseBody = context.Response.Body)
            {
                using (var writer = XmlWriter.Create(responseBody, settings))
                {
                    var soapWriter = new SoapWriter(writer);
                    soapWriter.WriteStartEnvelope();
                    soapWriter.WriteFault(faultException.Code, faultException.Message, faultException.Detail);
                    soapWriter.CloseDocument();
                }

                responseBody.Flush();
            }
        }
    }
}