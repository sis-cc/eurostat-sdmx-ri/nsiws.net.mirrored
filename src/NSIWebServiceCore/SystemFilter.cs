﻿using log4net;
using log4net.Core;
using log4net.Filter;

namespace Estat.Sri.Ws.NSIWebServiceCore
{
    public class SystemFilter: FilterSkeleton
    {
        public override FilterDecision Decide(LoggingEvent loggingEvent)
        {
            return LogicalThreadContext.Properties["User"] == null ? FilterDecision.Accept : FilterDecision.Deny;
        }
    }
}
