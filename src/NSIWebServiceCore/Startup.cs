// -----------------------------------------------------------------------
// <copyright file="Startup.cs" company="EUROSTAT">
//   Date Created : 2019-01-30
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using DryIoc;
using DryIoc.Microsoft.DependencyInjection;
using Estat.Sdmxsource.Extension.Builder;
using Estat.Sri.Ws.Controllers.Manager;
using Estat.Sri.Ws.Dependency.Manager;
using Estat.Sri.Ws.NSIWebServiceCore;
using Estat.Sri.Ws.NSIWebServiceCore.HealthCheck;
using Estat.Sri.Ws.Wsdl;
using log4net;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Util.ResourceBundle;
using SameSiteMode = Microsoft.AspNetCore.Http.SameSiteMode;
using ServiceInfo = Estat.Sri.Ws.Wsdl.ServiceInfo;
using Microsoft.Extensions.Hosting;
using estat.sri.ws.auth.api;
using Estat.Sri.Ws.Configuration;
using Estat.Sri.Ws.Rest.Utils.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using Microsoft.OpenApi.Models;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.StaticFiles;
using Estat.Sri.Ws.Rest.Utils.ApiDocumentation;
using Microsoft.AspNetCore.ResponseCompression;

namespace NSIWebServiceCore
{
    public class Startup
    {
        private readonly IConfiguration _configuration;
        private List<IMiddlewareBuilder> _availableMiddlewares;
        private static readonly ILog _log = LogManager.GetLogger(typeof(Startup));
        private bool _disseminationDbConfigSet;
        private bool _enableResponseCompression;
        private CultureInfo _defaultCultureInfo;

        public Startup(IConfiguration configuration)
        {
            _log.Debug("Starting application");

            SdmxException.SetMessageResolver(new MessageDecoder());
            this._configuration = configuration;

            ReadConfiguration(configuration);
        }


        private void ReadConfiguration(IConfiguration configuration)
        {
            var settingsFromConfigurationManager = new SettingsFromConfigurationManager(configuration);

            Estat.Sri.Utils.Config.ConfigurationProvider.CreateStubCategory = settingsFromConfigurationManager.CreateStubCategories;
            Estat.Sri.Utils.Config.ConfigurationProvider.AutoDeleteMappingSets = settingsFromConfigurationManager.AutoDeleteMappingSets;

            if (settingsFromConfigurationManager.DisseminationDbConfig != null)
            {
                Estat.Sri.Utils.Config.ConfigurationProvider.DisseminationDbConfig = new Tuple<string, string>(
                    settingsFromConfigurationManager.DisseminationDbConfig.DbType, 
                    settingsFromConfigurationManager.DisseminationDbConfig.ConnectionString);

                this._disseminationDbConfigSet = true;
            }

            this._enableResponseCompression = settingsFromConfigurationManager.EnableResponseCompression;
            this._defaultCultureInfo = settingsFromConfigurationManager.DefaultCultureInfo ?? CultureInfo.CurrentCulture;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            if (this._enableResponseCompression)
            {
                services.AddResponseCompression(
                    options =>
                    {
                        options.MimeTypes = ResponseCompressionDefaults.MimeTypes.Concat(
                            new[]
                            {
                                "application/vnd.sdmx.data+xml",
                                "application/vnd.sdmx.data+json",
                                "application/vnd.sdmx.data+csv",
                                "application/vnd.sdmx.structure+xml",
                                "application/vnd.sdmx.structure+json"
                            });
                    });
            }

            services.AddResponseCaching();
            services.Configure<CookiePolicyOptions>(
                options =>
                {
                    // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                    options.CheckConsentNeeded = context => true;
                    options.MinimumSameSitePolicy = SameSiteMode.None;
                });

            services.AddHttpContextAccessor();
            // If using Kestrel:
            services.Configure<KestrelServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
            });

            // If using IIS:
            services.Configure<IISServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
                options.MaxRequestBodySize = int.MaxValue;
            });
            var varyByHeader = VaryByHeaders(this._configuration);

            services.AddMvc(
                options =>
                {
                    //options.OutputFormatters.Add(new XmlSerializerOutputFormatter());
                    //options.FormatterMappings.SetMediaTypeMappingForFormat
                    //    ("xml", MediaTypeHeaderValue.Parse("application/xml"));
                    int duration = GetOutputCacheMaxAge();
                    options.CacheProfiles.Add("CustomCacheProfile",
                        duration == 0 ?
                        new CacheProfile()
                        {
                            Duration = 0,
                            Location = ResponseCacheLocation.None,
                            NoStore = true
                        } :
                        new CacheProfile()
                        {
                            Duration = duration,
                            Location = ResponseCacheLocation.Any,
                            VaryByHeader = varyByHeader
                        }) ;
                    
                }).SetCompatibilityVersion(CompatibilityVersion.Version_3_0)/*.AddXmlSerializerFormatters()*/;
            
            //https://docs.microsoft.com/en-us/aspnet/core/host-and-deploy/health-checks?view=aspnetcore-2.2
            services.AddHealthChecks()
                .AddCheck<ServiceHealthCheck>("service")
                .AddCheck<DbHealthCheck>("db")
                .AddCheck<MemoryHealthCheck>("memory");

            if (this._disseminationDbConfigSet)
            {
                services.AddHealthChecks().AddCheck<DisseminationDbHealthCheck>("disseminationDb");
            }

            StructureObserverBackgroundService.Register(services);

            return this.ConfigureDependencyInjectionAndWsdlRegistry(services);
        }
        private static string VaryByHeaders(IConfiguration configuration)
        {
            IConfigurationSection configurationSection = configuration.GetSection("httpHeaders:varyHeader:varyBy");
            if (configurationSection.Exists())
            {
                return string.Join(';', configurationSection.GetChildren().Select(x => x.Value));
            }

            return null;
        }

        private int GetOutputCacheMaxAge()
        {
            // first try to the json configuration
            var jsonConfig =_configuration.GetSection("outputcache");
            if (jsonConfig != null)
            {
                return jsonConfig.GetValue<int>("max-age");
            }

            // then app.config one
            var setting = System.Configuration.ConfigurationManager.AppSettings["outputcache.max-age"];
            if (setting != null && int.TryParse(setting, NumberStyles.None, CultureInfo.InvariantCulture, out int maxAge)) {
                return maxAge;
            }

            return 0;
        }

        private IServiceProvider ConfigureDependencyInjectionAndWsdlRegistry(IServiceCollection services)
        {
            var container = new Container(rules => rules.With(FactoryMethod.ConstructorWithResolvableArguments));

            DependencyRegisterManager.SetDefaultImplementations(container);
            this.RegisterPluginsAndMiddleware(services);

            var wsdlRegistry = DependencyRegisterManager.Container.Resolve<WsdlRegistry>();
            wsdlRegistry.Add(new ServiceInfo {Name = "NSIEstatV20Service", OriginalPath = "wwwroot/sdmx_estat/NSI.wsdl", Description = Messages.label_estatv20, Order = 0, SchemaPath = "sdmx_estat", Namespace = "http://ec.europa.eu/eurostat/sri/service/2.0/extended"});
            wsdlRegistry.Add(new ServiceInfo {Name = "NSIStdV20Service", OriginalPath = "wwwroot/sdmx_org/NSI.wsdl", Description = Messages.label_stdv20, Order = 1, SchemaPath = "sdmx_org", Namespace = "http://ec.europa.eu/eurostat/sri/service/2.0"});
            wsdlRegistry.Add(new ServiceInfo {Name = "SdmxService", OriginalPath = "wwwroot/sdmxv21/SDMX-WS.wsdl", Description = Messages.label_stdv21, Order = 2, SchemaPath = "sdmxv21", Namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/webservices"});
            wsdlRegistry.Add(new ServiceInfo {Name = "SdmxRegistryService", OriginalPath = "wwwroot/sdmxv21/SDMX-RR.wsdl", Description = Messages.label_rrv21, Order = 3, SchemaPath = "sdmxv21", Namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/webservices"});

            return container
                        .WithDependencyInjectionAdapter(services)
                        .ConfigureServiceProvider<ExtraConfiguration>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (this._enableResponseCompression)
            {
                app.UseResponseCompression();
            }

            // Global exception handler
            app.UseExceptionHandler(new ExceptionHandlerOptions
            {
                ExceptionHandler = ErrorHandlingMiddleware.Invoke
            });

            if (!env.IsDevelopment())
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseFileServer();
            app.UseCookiePolicy();
            app.UseMiddleware<LoggingMiddleware>();
            this.ConfigureDynamicMiddleware(app);

            app.UseResponseCaching();

            app.UseHealthChecks("/health", new NsiHealthCheckOptions());
            app.UseSwagger();
            //add swagger endpoints
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("v1/swagger.json", ApiConstants.SupportedApiVersion.V1.ToString().ToLower());
                c.SwaggerEndpoint("v2/swagger.json", ApiConstants.SupportedApiVersion.V2.ToString().ToLower());
                c.InjectStylesheet("ui/custom.css");
            });
            
            var extensionProvider = new FileExtensionContentTypeProvider();
            extensionProvider.Mappings.Add(".yaml", "text/vnd.yaml");
            app.UseStaticFiles(new StaticFileOptions
            {
                ContentTypeProvider = extensionProvider
            });

            app.UseRouting();
            app.UseEndpoints(endpoints => { 
                endpoints.MapControllers();
                endpoints.MapRazorPages();
            });

            CultureInfo.DefaultThreadCurrentCulture = this._defaultCultureInfo;
            CultureInfo.DefaultThreadCurrentUICulture = this._defaultCultureInfo;
        }

        private void RegisterPluginsAndMiddleware(IServiceCollection services)
        {
            this._availableMiddlewares = new List<IMiddlewareBuilder>();
            var directory = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            DependencyRegisterManager.RegisterPlugins(directory);

            var middlewareImplementation = SettingsManager.MiddlewareImplementation;

            if (middlewareImplementation == null)
            {
                _log.Warn("Configuration does not contain a middlewareImplementation key");
                return;
            }

            var middlewareOrder = middlewareImplementation.Split(',');
            var middlewareBuilders  = DependencyRegisterManager.Container
                .ResolveMany<IMiddlewareBuilder>()
                .ToDictionary(builder => builder.GetType().Name);

            foreach (var type in middlewareOrder)
            {
                IMiddlewareBuilder middlewareBuilder;

                if (!middlewareBuilders.TryGetValue(type, out middlewareBuilder))
                {
                    _log.Warn($"could not find the middleware builder {type}");
                }
                else
                {
                    this._availableMiddlewares.Add(middlewareBuilder);
                    middlewareBuilder.ConfigureServices(services, this._configuration);
                }
            }

            services.AddApiVersioning(config =>
            {
                config.DefaultApiVersion = new ApiVersion(1, 0);
                config.AssumeDefaultVersionWhenUnspecified = true;
                config.ReportApiVersions = true;
            });
            //configure swagger documentation generation
            services.AddSwaggerGen(c =>
            {
                c.DocumentFilter<SwaggerDocumentFilter>();
                c.OperationFilter<SwaggerApiParameterFilter>();
                c.OperationFilter<SwaggerPathParameterFilter>();
                c.OperationFilter<SwaggerResponseTypesFilter>();
                c.SwaggerDoc(ApiConstants.SupportedApiVersion.V1.ToString().ToLower(),
                 new OpenApiInfo
                 {
                     Version = ApiConstants.SupportedApiVersion.V1.ToString().ToLower(),
                     Title = "SDMX RESTful API, v1.5.0",
                     Description = "<p>The SDMX RESTful API, released in September 2020.</p><p>For additional information, check the" +
                                   " <a rel='noopener noreferrer' target='_blank' href='https://github.com/sdmx-twg/sdmx-rest/tree/" +
                                   "master/v2_1/ws/rest/docs'>official sdmx-rest specification</a> or the <a rel='noopener noreferrer' " +
                                   "target='_blank' href='https://github.com/sdmx-twg/sdmx-rest/wiki'>dedicated Wiki</a>.</p>"
                 });
                c.SwaggerDoc(ApiConstants.SupportedApiVersion.V2.ToString().ToLower(),
                 new OpenApiInfo
                 {
                    Version = ApiConstants.SupportedApiVersion.V2.ToString().ToLower(),
                    Title = "SDMX RESTful API, v2.0.0",
                    Description = "<p>The RESTful API for SDMX 3.0.</p><p>For additional information, check the <a rel='noopener " +
                                  "noreferrer' target='_blank' href='https://github.com/sdmx-twg/sdmx-rest/tree/develop/v2_1/ws/rest/docs'>documentation</a>.</p>"
                 });
                c.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());
                c.DocumentFilter<ReplaceVersionWithExactValueInPath>();
                c.DocInclusionPredicate((docName, apiDesc) =>
                    { return SwaggerInclusionPredicate.IsTrueFor(docName, apiDesc); });
                
                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
                c.EnableAnnotations();
            });
        }

        private void ConfigureDynamicMiddleware(IApplicationBuilder app)
        {
            foreach(var middlewareBuilder in this._availableMiddlewares)
                middlewareBuilder.Configure(app);
        }
    }
}