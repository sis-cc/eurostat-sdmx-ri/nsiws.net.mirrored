﻿namespace Estat.Sri.Ws.NSIWebServiceCore.HealthCheck
{
    using Estat.Sri.Ws.Configuration;

    using Microsoft.Extensions.Diagnostics.HealthChecks;

    using System.Data.SqlClient;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// DisseminationDb health check.
    /// </summary>
    public class DisseminationDbHealthCheck : IHealthCheck
    {
        private readonly SettingsFromConfigurationManager _settingsFromConfigurationManager;

        /// <summary>
        /// DisseminationDb health check.
        /// </summary>
        /// <param name="settingsFromConfigurationManager">Settings from configurationManager.</param>
        public DisseminationDbHealthCheck(SettingsFromConfigurationManager settingsFromConfigurationManager)
        {
            this._settingsFromConfigurationManager = settingsFromConfigurationManager;
        }

        /// <summary>
        /// Check health.
        /// </summary>
        /// <param name="context">Context.</param>
        /// <param name="cancellationToken">CancellationToken.</param>
        /// <returns></returns>
        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = new CancellationToken())
        {
            bool isHealthy;

            using (var connection = new SqlConnection(this._settingsFromConfigurationManager.DisseminationDbConfig.ConnectionString))
            {
                try
                {
                    connection.Open();
                    isHealthy = true;
                }
                catch (SqlException)
                {
                    isHealthy = false;
                }
            }

            return isHealthy
               ? HealthCheckResult.Healthy()
               : HealthCheckResult.Unhealthy();
        }
    }
}
