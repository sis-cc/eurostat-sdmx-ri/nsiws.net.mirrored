﻿// -----------------------------------------------------------------------
// <copyright file="WebServiceType.cs" company="EUROSTAT">
//   Date Created : 2016-08-17
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.DataDisseminationWS.WebServices
{
    /// <summary>
    /// The web service type.
    /// </summary>
    public enum WebServiceType
    {
        /// <summary>
        /// The rest
        /// </summary>
        Rest,

        /// <summary>
        /// The SOAP
        /// </summary>
        Soap,

        /// <summary>
        /// The ASMX SOAP
        /// </summary>
        AsmxSoap
    }
}