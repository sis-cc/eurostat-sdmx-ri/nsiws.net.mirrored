﻿// -----------------------------------------------------------------------
// <copyright file="IWebServiceMetadata.cs" company="EUROSTAT">
//   Date Created : 2016-08-17
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.DataDisseminationWS.WebServices
{
    /// <summary>
    /// The WebServiceInfo interface.
    /// </summary>
    public interface IWebServiceMetadata
    {
        /// <summary>
        ///     Gets the description
        /// </summary>
        string Description { get; }

        /// <summary>
        /// Gets a value indicating whether this <see cref="NsiStdV20WebServiceMetadata"/> is enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        bool Enabled { get; }

        /// <summary>
        ///     Gets the Name
        /// </summary>
        string Name { get; }

        /// <summary>
        ///     Gets the Namespace
        /// </summary>
        string Namespace { get; }

        /// <summary>
        /// Gets the order.
        /// </summary>
        /// <value>
        /// The order.
        /// </value>
        int Order { get; }

        /// <summary>
        /// Gets the route prefix.
        /// </summary>
        /// <value>
        /// The route prefix.
        /// </value>
        string RoutePrefix { get; }

        /// <summary>
        ///     Gets the SDMXMessage.XSD path
        /// </summary>
        string SchemaPath { get; }

        /// <summary>
        /// Gets the type of the service.
        /// </summary>
        /// <value>
        /// The type of the service.
        /// </value>
        WebServiceType ServiceType { get; }

        /// <summary>
        /// Gets the WSDL path.
        /// </summary>
        /// <value>
        /// The WSDL path.
        /// </value>
        string WsdlPath { get; }

        /// <summary>
        /// Registers this instance.
        /// </summary>
        void Register();
    }
}