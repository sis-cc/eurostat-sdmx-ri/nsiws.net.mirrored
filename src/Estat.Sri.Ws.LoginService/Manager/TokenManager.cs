﻿// -----------------------------------------------------------------------
// <copyright file="TokenManager.cs" company="EUROSTAT">
//   Date Created : 2017-06-10
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;

namespace Estat.Sri.Ws.LoginService.Manager
{
    using System;
    using System.Globalization;
    using System.Security.Cryptography;
    using System.Security.Principal;
    using System.Web;

    using Estat.Sdmxsource.Extension.Extension;
    using Estat.Sri.Ws.LoginService.Model;

    /// <summary>
    /// An OAuth2 client_credentials response builder
    /// </summary>
    public class TokenManager
    {
        /// <summary>
        /// The crypto service provider
        /// </summary>
        private readonly RNGCryptoServiceProvider _cryptoServiceProvider;

        /// <summary>
        /// The maximum seconds inactive
        /// </summary>
        private readonly int _maxSecondsInactive;

        /// <summary>
        /// Initializes a new instance of the <see cref="TokenManager"/> class.
        /// </summary>
        /// <param name="cryptoServiceProvider">The crypto service provider.</param>
        public TokenManager(RNGCryptoServiceProvider cryptoServiceProvider)
        {
            this._cryptoServiceProvider = cryptoServiceProvider;
            var inactive = ConfigurationManager.AppSettings["maxInactiveAccesTokenInSeconds"];
            int value;
            if (int.TryParse(inactive, NumberStyles.AllowLeadingWhite | NumberStyles.AllowTrailingWhite, CultureInfo.InvariantCulture, out value) && value > 0)
            {
                this._maxSecondsInactive = value;
            }
            else
            {
                this._maxSecondsInactive = 3600;
            }

        }

        /// <summary>
        /// Gets the token.
        /// </summary>
        /// <param name="principal">The principal.</param>
        /// <param name="context"></param>
        /// <returns>The <see cref="AccessTokenResponse"/>.</returns>
        public AccessTokenResponse CreateAccessToken(IPrincipal principal, HttpContext context)
        {
            if (principal == null)
            {
                throw new ArgumentNullException(nameof(principal));
            }
            IMemoryCache cache = (IMemoryCache)context.RequestServices.GetService(typeof(IMemoryCache));

            var tokenBytes = new byte[32];
            string accessToken;
            object accessTokenValue;
            do
            {
                this._cryptoServiceProvider.GetBytes(tokenBytes);
                accessToken = Convert.ToBase64String(tokenBytes).ToBase64UrlSafe();
                cache.TryGetValue(accessToken, out accessTokenValue);
            }
            while (accessTokenValue != null);

            var cacheEntryOptions = new MemoryCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromSeconds(this._maxSecondsInactive));


            cache.Set(accessToken, principal, cacheEntryOptions);

            return new AccessTokenResponse() { AccessToken = accessToken, ExpiresIn = (long)Math.Round(cacheEntryOptions.SlidingExpiration.Value.TotalSeconds), TokenType = "bearer" };
        }
    }
}