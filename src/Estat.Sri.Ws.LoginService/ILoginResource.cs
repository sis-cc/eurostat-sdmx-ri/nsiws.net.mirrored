﻿// -----------------------------------------------------------------------
// <copyright file="ILoginResource.cs" company="EUROSTAT">
//   Date Created : 2017-06-09
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Microsoft.AspNetCore.Http;

namespace Estat.Sri.Ws.LoginService
{
    using System.IO;

    using Estat.Sri.Ws.LoginService.Model;

    /// <summary>
    /// The Login resource which is used to retrieve access_token
    /// </summary>
    public interface ILoginResource
    {
        /// <summary>
        /// Posts the grant request.
        /// </summary>
        /// <param name="grantType">
        ///     Type of the grant.
        /// </param>
        /// <returns>
        /// The <see cref="AccessTokenResponse"/>.
        /// </returns>
        AccessTokenResponse PostGrantRequest(Stream grantType, HttpContext context);
    }
}