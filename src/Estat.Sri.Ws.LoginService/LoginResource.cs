﻿// -----------------------------------------------------------------------
// <copyright file="LoginResource.cs" company="EUROSTAT">
//   Date Created : 2017-06-10
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Controllers.Utils;
using Microsoft.AspNetCore.Http;

namespace Estat.Sri.Ws.LoginService
{
    using System.IO;
    using System.Net;
    using System.Text;
    using System.Web;

    using Estat.Sdmxsource.Extension.Constant;
    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sri.Ws.LoginService.Manager;
    using Estat.Sri.Ws.LoginService.Model;

    /// <summary>
    /// The login resource.
    /// </summary>
    public class LoginResource : ILoginResource
    {
        /// <summary>
        /// The parameter name
        /// </summary>
        private const string ParameterName = "grant_type";

        /// <summary>
        /// The parameter value
        /// </summary>
        private const string ParameterValue = "client_credentials";

        /// <summary>
        /// The dataflow principal manager
        /// </summary>
        private readonly IDataflowPrincipalManager _dataflowPrincipalManager;

        /// <summary>
        /// The token manager
        /// </summary>
        private readonly TokenManager _tokenManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoginResource"/> class.
        /// </summary>
        /// <param name="dataflowPrincipalManager">The dataflow principal manager.</param>
        /// <param name="tokenManager">The token manager.</param>
        public LoginResource(IDataflowPrincipalManager dataflowPrincipalManager, TokenManager tokenManager)
        {
            this._dataflowPrincipalManager = dataflowPrincipalManager;
            this._tokenManager = tokenManager;
        }

        /// <summary>
        /// Posts the grant request.
        /// </summary>
        /// <param name="grantType">Type of the grant.</param>
        /// <returns>
        /// The <see cref="AccessTokenResponse" />.
        /// </returns>
        /// <exception>
        /// Access forbidden
        /// </exception>
        public AccessTokenResponse PostGrantRequest(Stream grantType, HttpContext context)
        {
            using (var streamReader = new StreamReader(grantType, Encoding.UTF8))
            {
                var buffer = new char[128];

                var len = streamReader.Read(buffer, 0, 128);
                var nameValueCollection = HttpUtility.ParseQueryString(new string(buffer, 0, len));
                if (!ParameterValue.Equals(nameValueCollection.Get(ParameterName)))
                {
                    throw new ServiceException(HttpStatusCode.Forbidden, "Incorrect Message");
                }
            }

            var principal = this._dataflowPrincipalManager.GetCurrentPrincipal();
            if (principal == null)
            {
                throw new ServiceException(HttpStatusCode.Forbidden, "Not authenticated");
            }

            return this._tokenManager.CreateAccessToken(principal,context);
        }
    }
}