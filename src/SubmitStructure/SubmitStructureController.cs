﻿// -----------------------------------------------------------------------
// <copyright file="SubmitStructureController.cs" company="EUROSTAT">
//   Date Created : 2016-08-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.SubmitStructure
{

    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Estat.Sdmxsource.Extension.Manager;
    using System.Collections.Generic;
    using Estat.Sdmxsource.Extension.Model.Error;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;

    /// <summary>
    /// The submit structure controller.
    /// </summary>
    public class SubmitStructureController
    {
        /// <summary>
        /// The parsing manager
        /// </summary>
        private readonly IStructureParsingManager _parsingManager = new StructureParsingManager();

        /// <summary>
        /// The structure persistence manager
        /// </summary>
        private readonly IStructureSubmitter _structurePersistenceManager;

        /// <summary>
        /// The response writer manager
        /// </summary>
        private readonly IResponseWriterManager _responseWriterManager;

        /// <summary>
        /// The header retrieval manager
        /// </summary>
        private readonly IHeaderRetrievalManager _headerRetrievalManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="SubmitStructureController" /> class.
        /// </summary>
        /// <param name="structurePersistenceManager">The structure persistence manager.</param>
        /// <param name="responseWriterManager">The response writer manager.</param>
        public SubmitStructureController(IStructureSubmitter structurePersistenceManager, IResponseWriterManager responseWriterManager, IHeaderRetrievalManager headerRetrievalManager)
        {
            if (responseWriterManager == null)
            {
                throw new System.ArgumentNullException(nameof(responseWriterManager));
            }

            if (structurePersistenceManager == null)
            {
                throw new System.ArgumentNullException(nameof(structurePersistenceManager));
            }

            this._structurePersistenceManager = structurePersistenceManager;
            _responseWriterManager = responseWriterManager;
            _headerRetrievalManager = headerRetrievalManager;
        }

        /// <summary>
        /// Submits the specified structural meta-data .
        /// </summary>
        /// <param name="dataLocation">The data location pointing to the structural meta-data.</param>
        /// <param name="actionType">Type of the action.</param>
        /// <param name="storeId">The store identifier.</param>
        /// <returns>
        /// The imported objects
        /// </returns>
        /// <exception cref="SdmxNotImplementedException">action " + actionType.ToString()</exception>
        public ResponseWithSdmxObjects Submit(IReadableDataLocation dataLocation, SubmitStructureConstant.ActionType actionType, string storeId)
        {
            // Parse structures IStructureParsingManager is an instance field.
            IStructureWorkspace structureWorkspace = this._parsingManager.ParseStructures(dataLocation);

            // Get immutable objects from workspace
            ISdmxObjects objects = structureWorkspace.GetStructureObjects(false);

            // use the the IStructurePersistenceManager specified at the constructor
            IStructureSubmitter persistenceManager = this._structurePersistenceManager;

            switch (actionType)
            {
                case SubmitStructureConstant.ActionType.Append:
                    objects.Action = DatasetAction.GetFromEnum(DatasetActionEnumType.Append);
                    break;
                case SubmitStructureConstant.ActionType.Replace:
                    // Save the structure to the mapping store database.
                    objects.Action = DatasetAction.GetFromEnum(DatasetActionEnumType.Replace);
                    break;
                case SubmitStructureConstant.ActionType.Delete:
                    // Delete the structure to the mapping store database.
                    objects.Action = DatasetAction.GetFromEnum(DatasetActionEnumType.Delete);
                    break;
                default:
                    throw new SdmxNotImplementedException("action " + actionType.ToString());
            }

            return new ResponseWithSdmxObjects(persistenceManager.SubmitStructures(storeId, objects), objects, GetResponseHeader(objects.Header));
        }

        /// <summary>
        /// Writes the specified response format.
        /// </summary>
        /// <param name="responseFormat">The response format.</param>
        /// <param name="responses">The responses.</param>
        public void Write(IResponseFormat responseFormat, IList<IResponseWithStatusObject> responses)
        {
            _responseWriterManager.Write(responseFormat, responses);
        }

        /// <summary>
        /// Gets the response header.
        /// </summary>
        /// <param name="requestHeader">The request header.</param>
        /// <returns>The <see cref="IHeader"/> with the sender from <paramref name="requestHeader"/> as receiver.</returns>
        private IHeader GetResponseHeader(IHeader requestHeader)
        {
            var header = _headerRetrievalManager.Header;
            header.Action = null;
            header.AddReciever(requestHeader.Sender);
            return header;
        }
    }
}
