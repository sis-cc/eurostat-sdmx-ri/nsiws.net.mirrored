﻿// -----------------------------------------------------------------------
// <copyright file="SubmitStructureUtil.cs" company="EUROSTAT">
//   Date Created : 2016-08-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Org.Sdmxsource.Sdmx.Api.Util;

namespace Estat.Sri.Ws.SubmitStructure
{
    using System;
    using System.IO;
    using System.Text;
    using System.Xml;

    public static class SubmitStructureUtil
    {

        #region Public Methods

        /// <summary>
        /// Validate v21 Document
        /// </summary>
        /// <param name="xDomSource">Document to validate</param>
        /// <returns>Action value</returns>
        public static string ValidateDocument(XmlDocument xDomSource)
        {
            string actionValue = string.Empty;
            actionValue = GetAction21(xDomSource);

            // Error: Missing tag "SubmitStructureRequest" or Missing attribute "action"
            if (actionValue == string.Empty)
                throw new Exception("Invalid message: missing element 'SubmitStructureRequest' or Missing attribute 'action'");

            // Error: Missing tag "Structures"
            if (xDomSource.SelectSingleNode("//*[local-name()='Structures']") == null)
                throw new Exception("Invalid message: missing element 'Structures'");

            return actionValue;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xDomSource"></param>
        /// <returns></returns>
        public static string GetAction20(XmlDocument xDomSource)
        {
            XmlNodeList nodeList = xDomSource.SelectNodes("//*[local-name()='SubmittedStructure']");

            foreach (XmlNode node in nodeList)
            {
                if (node.NamespaceURI == "http://www.SDMX.org/resources/SDMXML/schemas/v2_0/registry")
                {
                    XmlAttribute xAtt = node.Attributes["action"];
                    if (xAtt != null)
                        return xAtt.Value;
                }
            }
            return "";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xDomSource"></param>
        /// <returns></returns>
        public static string GetAction21(XmlDocument xDomSource)
        {
            XmlNodeList nodeList = xDomSource.SelectNodes("//*[local-name()='SubmitStructureRequest']");

            foreach (XmlNode node in nodeList)
            {
                if (node.NamespaceURI == "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message")
                {
                    XmlAttribute xAtt = node.Attributes["action"];
                    if (xAtt != null)
                        return xAtt.Value;
                }
            }
            return "";
        }

        /// <summary>
        /// Convert a Message to XMLDocument
        /// </summary>
        /// <param name="msg">Message to convert</param>
        /// <returns></returns>
        public static XmlDocument MessageToXDom(IReadableDataLocation msg)
        {
            using (var requestStream = msg.InputStream)
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(requestStream);
                return doc;
            }
        }
       
        /// <summary>
        /// Convert a XMLDocument to Bytes Array
        /// </summary>
        /// <param name="doc">XMLDocument to convert</param>
        /// <returns></returns>
        public static byte[] ConvertToBytes(XmlDocument doc)
        {
            Encoding encoding = Encoding.UTF8;
            byte[] docAsBytes = encoding.GetBytes(doc.OuterXml);
            return docAsBytes;
        }

        public static void SetHeader(XmlDocument xDomTemp, XmlDocument xDomSource)
        {
            XmlNode xNodeSourceHeader = null;
            XmlNode xNodeTempHeader = null;
            XmlNode xNodeTempStructure = null;

            // Trovo nel doc source l'elemento Header con namespace message
            XmlNodeList nodeList = xDomSource.SelectNodes("//*[local-name()='Header']");
            foreach (XmlNode node in nodeList)
            {
                if (node.NamespaceURI == "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message")
                    xNodeSourceHeader = node;
            }

            // Se trovo l'elemento 
            if (xNodeSourceHeader != null)
            {
                // Mi posiziono sul tag Structure del template elimino il figlio e gli aggiungo l'Header del source
                xNodeTempStructure = xDomTemp.SelectSingleNode("//*[local-name()='Structure']");

                xNodeTempStructure.RemoveChild(xNodeTempStructure.FirstChild);
                xNodeTempHeader = xDomTemp.CreateElement("Header", "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message");
                xNodeTempHeader.InnerXml = xNodeSourceHeader.InnerXml;
                xNodeTempStructure.AppendChild(xNodeTempHeader);
            }
        }


        #endregion

    }
}
