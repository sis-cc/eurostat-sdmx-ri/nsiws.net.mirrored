// -----------------------------------------------------------------------
// <copyright file="EstatSriSecurityAuthenticationFactoryXml.cs" company="EUROSTAT">
//   Date Created : 2017-06-06
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Configuration;
using System.Globalization;
using log4net;
using Microsoft.AspNetCore.Http;

namespace Estat.Nsi.AuthModule.Factory
{
    using Estat.Nsi.AuthModule.Engine;
    using Estat.Sri.Security.Engine;
    using Estat.Sri.Security.Model;
    using System.IO;
    using System.Xml;

    /// <summary>
    /// Class AuthSchemaAuthenticationProviderFactory.
    /// </summary>
    public class EstatSriSecurityAuthenticationFactoryXml : AbstractProviderFactory<EstatSriSecurityAuthenticationProviderXml, IAuthenticationProvider>
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(EstatSriSecurityAuthenticationFactory));

        private readonly IHttpContextAccessor _contextAccessor;
        private readonly string _authXmlPath;
        private readonly PrincipalRetrieverXml _principalRetrieverXml;
        
        public EstatSriSecurityAuthenticationFactoryXml(IHttpContextAccessor contextAccessor)
        {
            this._contextAccessor = contextAccessor;
            this._authXmlPath = this.GetAuthXmlLocation(ConfigurationManager.AppSettings["authXmlLocation"]);
            this._principalRetrieverXml = new PrincipalRetrieverXml();
        }

        /// <summary>
        /// Creates the instance.
        /// </summary>
        /// <returns>The provider implementation.</returns>
        protected override EstatSriSecurityAuthenticationProviderXml CreateInstance()
        {
            if (this._authXmlPath == null)
            {
                return null;
            }

            return new EstatSriSecurityAuthenticationProviderXml(this._authXmlPath, this._principalRetrieverXml, this._contextAccessor);
        }

        private string GetAuthXmlLocation(string authXmlPath)
        {
            if (authXmlPath == null || !File.Exists(authXmlPath))
            {
                string fullPath = null;
                if (authXmlPath != null)
                {
                    fullPath = new FileInfo(authXmlPath).FullName;
                }
                _log.ErrorFormat("Auth xml not found. Config path {0}, Full path {1}", authXmlPath, fullPath);
                return null;
            }

            return authXmlPath;
        }
    }
}