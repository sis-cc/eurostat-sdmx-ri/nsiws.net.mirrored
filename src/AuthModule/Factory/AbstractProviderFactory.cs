﻿// -----------------------------------------------------------------------
// <copyright file="AbstractProviderFactory.cs" company="EUROSTAT">
//   Date Created : 2017-06-06
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.AuthModule.Factory
{
    /// <summary>
    /// Class AbstractProviderFactory.
    /// </summary>
    /// <typeparam name="TProviderImpl">The type of the t provider implementation.</typeparam>
    /// <typeparam name="TProvider">The type of the t provider.</typeparam>
    /// <seealso cref="Estat.Nsi.AuthModule.Factory.IProviderFactory{TProvider}" />
    public abstract class AbstractProviderFactory<TProviderImpl, TProvider> : IProviderFactory<TProvider> where TProviderImpl : TProvider
    {
        /// <summary>
        /// The _name.
        /// </summary>
        private readonly string _name;

        /// <summary>
        /// Initializes a new instance of the <see cref="AbstractProviderFactory{TProviderImpl, TProvider}"/> class.
        /// </summary>
        protected AbstractProviderFactory()
        {
            this._name = typeof(TProviderImpl).Name;
        }

        /// <summary>
        /// Creates the provider.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>A provider instance</returns>
        public TProvider CreateProvider(string type)
        {
            // Assembly Qualified names no longer supported
            if (this._name.Equals(type))
            {
                return this.CreateInstance();
            }

            return default(TProviderImpl);
        }

        /// <summary>
        /// Creates the instance.
        /// </summary>
        /// <returns>The provider implementation.</returns>
        protected abstract TProviderImpl CreateInstance();
    }
}