﻿// -----------------------------------------------------------------------
// <copyright file="DbAuthenticationProviderBase.cs" company="EUROSTAT">
//   Date Created : 2017-06-07
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Microsoft.AspNetCore.Http;

namespace Estat.Nsi.AuthModule.Engine
{
    using System.Configuration;
    using System.Data;
    using System.Globalization;
    using System.Security.Principal;

    using Estat.Nsi.AuthModule.Constant;
    using Estat.Nsi.AuthModule.Manager;
    using Estat.Nsi.AuthModule.Model;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    /// <summary>
    /// The db authentication provider base.
    /// </summary>
    public abstract class DbAuthenticationProviderBase : IAuthenticationProvider
    {
        /// <summary>
        /// The SQL query as specified in the config file
        /// </summary>
        private readonly string _selectQuery = ConfigManager.Instance.Config.DBAuth.Authentication.Sql;

        /// <summary>
        /// The authentication database
        /// </summary>
        private readonly Database _database;

        /// <summary>
        /// The _query.
        /// </summary>
        private readonly string _query;

        /// <summary>
        /// Initializes a new instance of the <see cref="DbAuthenticationProviderBase"/> class. 
        /// Initializes a new instance of the <see cref="DbAuthenticationProvider"/> class
        /// </summary>
        /// <exception cref="AuthConfigurationException">
        /// Missing or invalid configuration at .config
        /// </exception>
        protected DbAuthenticationProviderBase()
        {
            AuthUtils.ValidateConfig(ConfigManager.Instance.Config.DBAuth, this.GetType());
            AuthUtils.ValidateConfig(ConfigManager.Instance.Config.DBAuth.Authentication, this.GetType());
            var connectionStringName = ConfigManager.Instance.Config.DBAuth.Authentication.ConnectionStringName;
            if (string.IsNullOrEmpty(connectionStringName))
            {
                throw new AuthConfigurationException(Errors.DbAuthMissingConnectionStringName);
            }

            var connectionStringSettings = ConfigurationManager.ConnectionStrings[connectionStringName];
            if (connectionStringSettings == null)
            {
                throw new AuthConfigurationException(Errors.DbAuthMissingConnectionStringName);
            }

            if (!AuthUtils.ValidateContains(this._selectQuery, DbConstants.UserMacro))
            {
                var invalidQuery = string.Format(CultureInfo.CurrentCulture, Errors.DbAuthenticationInvalidSqlQuery, DbConstants.UserMacro);
                throw new AuthConfigurationException(string.Format(CultureInfo.CurrentCulture, Errors.DbAuthInvalidSqlQuery, this.GetType().Name, invalidQuery));
            }

            this._database = new Database(connectionStringSettings);
            var userParam = this._database.BuildParameterName(DbConstants.UserParamName);

            this._query = this._selectQuery.Replace(DbConstants.UserMacro, userParam);
        }

        /// <summary>
        /// Authenticate the specified user
        /// </summary>
        /// <param name="user">The <see cref="IUser" /> instance containing the user information</param>
        /// <returns>
        /// If the user is authenticated <see cref="IPrincipal" /> else null
        /// </returns>
        public IPrincipal Authenticate(IUser user)
        {
            if (this.Exists(user))
            {
                return new DataflowPrincipal(new GenericIdentity(user.UserName, "basic"), false);
            }

            return null;
        }

        /// <summary>
        /// Gets the anonymous user.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <returns>An implementation specific anonymous <see cref="IPrincipal"/></returns>
        public IPrincipal GetAnonymous(string userName)
        {
            return new DataflowPrincipal(new GenericIdentity(userName, "basic"), true);
        }

        /// <summary>
        /// Checks the password encoding
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="convertDbValue">
        /// The convert database value.
        /// </param>
        /// <returns>
        /// True if the password matches
        /// </returns>
        protected abstract bool CheckPasswordEnc(IUser user, string convertDbValue);

        /// <summary>
        /// Check if the specified user with user name and password exists.
        /// </summary>
        /// <param name="user">
        /// The <see cref="IUser"/> object that holds the user credentials
        /// </param>
        /// <returns>
        /// True if the specified user with user name and password exists. Else false
        /// </returns>
        private bool Exists(IUser user)
        {
            var o = this._database.ExecuteScalar(this._query, new[] { this._database.CreateInParameter(DbConstants.UserParamName, DbType.String, user.UserName) });
            if (o != null)
            {
                var convertDbValue = AuthUtils.ConvertDBValue<string>(o);
                return this.CheckPasswordEnc(user, convertDbValue);
            }

            return false;
        }
    }
}