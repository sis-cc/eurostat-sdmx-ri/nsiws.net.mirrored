﻿// -----------------------------------------------------------------------
// <copyright file="EstatSriSecurityAuthenticationProvider.cs" company="EUROSTAT">
//   Date Created : 2017-06-06
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;

namespace Estat.Nsi.AuthModule.Engine
{
    using System.Net;
    using System.Security.Principal;

    using Estat.Nsi.AuthModule.Model;
    using log4net;

    using Estat.Sri.Security.Engine;
    using System;

    /// <summary>
    /// The mapping store authentication provider.
    /// </summary>
    public class EstatSriSecurityAuthenticationProviderXml : IAuthenticationProvider
    {
        /// <summary>
        /// The log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(EstatSriSecurityAuthenticationProviderXml));

        /// <summary>
        /// The principal retriever
        /// </summary>
        private readonly PrincipalRetrieverXml _principalRetrieverXml;

        private readonly IHttpContextAccessor _contextAccessor;

        private readonly string _authXmlString;

        /// <summary>
        /// Initializes a new instance of the <see cref="EstatSriSecurityAuthenticationProviderXml" /> class.
        /// </summary>
        /// <param name="authXmlString"></param>
        /// <param name="principalRetrieverXml"></param>
        /// <param name="contextAccessor"></param>
        public EstatSriSecurityAuthenticationProviderXml(string authXmlString, PrincipalRetrieverXml principalRetrieverXml, IHttpContextAccessor contextAccessor)
        {
            this._authXmlString = authXmlString;
            this._principalRetrieverXml = principalRetrieverXml;
            this._contextAccessor = contextAccessor;
        }

        /// <summary>
        /// Authenticate the specified user
        /// </summary>
        /// <param name="user">
        /// The <see cref="IUser"/> instance containing the user information
        /// </param>
        /// <returns>
        /// If the user is authenticated <see cref="IPrincipal"/> else null
        /// </returns>
        public IPrincipal Authenticate(IUser user)
        {

            var scope = GetMappingStoreScope();

            try
            {
                return this._principalRetrieverXml.GetPrincipal(user.UserName, user.Password, scope, this._authXmlString);
            }
            catch (Exception ex)
            {
                _log.Error("An error occurred while trying to access the auth.xml", ex);
                CreateServiceUnavailableResponse();
                return null;
            }
        }

        /// <summary>
        /// Gets the anonymous user.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <returns>An implementation specific anonymous <see cref="IPrincipal"/></returns>
        public IPrincipal GetAnonymous(string userName)
        {
            var scope = GetMappingStoreScope();
            try
            {
                return this._principalRetrieverXml.GetPrincipal(userName, string.Empty, scope, this._authXmlString);
            }
            catch (Exception ex)
            {
                _log.Error("An error occurred while trying to access the auth.xml", ex);
                CreateServiceUnavailableResponse();
                return null;
            }
        }


        private  void CreateServiceUnavailableResponse()
        {
            this._contextAccessor.HttpContext.Response.StatusCode = (int)HttpStatusCode.ServiceUnavailable;
        }

        /// <summary>
        /// Gets the mapping store scope.
        /// </summary>
        /// <returns>The MappingStore ID from query parameter; otherwise <c>null</c></returns>
        /// <summary>
        /// Gets the mapping store scope.
        /// </summary>
        /// <returns>The MappingStore ID from query parameter; otherwise <c>null</c></returns>
        private string GetMappingStoreScope()
        {
            var sid = this._contextAccessor.HttpContext.Request.Query["sid"];
            if (!StringValues.IsNullOrEmpty(sid))
            {
                _log.DebugFormat("Got SID = '{0}' from query parameter", sid);
                return sid;
            }

            return null;
        }
    }
}