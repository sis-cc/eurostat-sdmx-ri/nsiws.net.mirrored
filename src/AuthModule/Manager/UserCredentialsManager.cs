﻿// -----------------------------------------------------------------------
// <copyright file="UserCredentialsManager.cs" company="EUROSTAT">
//   Date Created : 2017-06-03
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.AuthModule.Manager
{
    using Estat.Nsi.AuthModule.Engine;
    using Estat.Nsi.AuthModule.Factory;

    /// <summary>
    /// User Credentials Manager.
    /// </summary>
    public class UserCredentialsManager : AbstractManager<IUserCredentials>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserCredentialsManager"/> class.
        /// </summary>
        /// <param name="factories">The factories.</param>
        public UserCredentialsManager(params IProviderFactory<IUserCredentials>[] factories)
            : base(factories)
        {
        }
    }
}