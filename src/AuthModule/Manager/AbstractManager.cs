﻿// -----------------------------------------------------------------------
// <copyright file="AbstractManager.cs" company="EUROSTAT">
//   Date Created : 2017-06-03
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.AuthModule.Manager
{
    using System;
    using System.Linq;

    using Estat.Nsi.AuthModule.Factory;
    using Estat.Sri.MappingStoreRetrieval.Extensions;

    /// <summary>
    /// An abstract provider manager
    /// </summary>
    /// <typeparam name="TProvider">The type of the provider.</typeparam>
    public abstract class AbstractManager<TProvider> : IProviderManager<TProvider>
    {
        /// <summary>
        /// The factories
        /// </summary>
        private readonly IProviderFactory<TProvider>[] _factories;

        /// <summary>
        /// Initializes a new instance of the <see cref="AbstractManager{TProvider}"/> class.
        /// </summary>
        /// <param name="factories">The factories.</param>
        protected AbstractManager(params IProviderFactory<TProvider>[] factories)
        {
            if (factories == null)
            {
                throw new ArgumentNullException(nameof(factories));
            }

            if (factories.Length == 0)
            {
               throw new ArgumentException("No factories found", nameof(factories));
            }

            this._factories = factories.ToArray();
        }

        /// <summary>
        /// Gets the provider.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>A provider instance.</returns>
        /// <exception cref="AuthConfigurationException">Provider not found</exception>
        public TProvider GetProvider(string type)
        {
            foreach (var baseFactory in this._factories)
            {
                var provider = baseFactory.CreateProvider(type);
                if (!provider.IsDefault())
                {
                    return provider;
                }
            }

            throw new AuthConfigurationException("Provider not found");
        }
    }
}