﻿// -----------------------------------------------------------------------
// <copyright file="NsiAuthModule.cs" company="EUROSTAT">
//   Date Created : 2011-06-19
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;

namespace Estat.Nsi.AuthModule
{
    using System;
    using System.Security.Principal;
    using System.Threading;

    using Estat.Nsi.AuthModule.Constant;
    using Estat.Nsi.AuthModule.Engine;
    using Estat.Nsi.AuthModule.Manager;
    using Estat.Nsi.AuthModule.Model;

    using log4net;

    /// <summary>
    /// A HTTP AUTH Module for IIS for restricting access to dataflows for data and structure requests.
    /// </summary>
    /// <remarks>
    /// You will need to configure this module in the web.config file of your
    /// web and register it with IIS before being able to use it. For more information
    /// see the following link: <a href="http://go.microsoft.com/?linkid=8101007">Go</a>
    /// </remarks>
    public class NsiAuthModule 
    {
        private readonly RequestDelegate _next;

        #region Constants and Fields

        /// <summary>
        /// The log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(NsiAuthModule));

        /// <summary>
        /// The _anon user.
        /// </summary>
        private readonly IUser _anonUser;

        /// <summary>
        /// The _authentication.
        /// </summary>
        private readonly IAuthenticationProvider _authentication;

        /// <summary>
        /// The _realm.
        /// </summary>
        private readonly string _realm;

        /// <summary>
        /// The _user cred.
        /// </summary>
        private readonly IUserCredentials _userCred;

        private readonly PrincipalCacheManager _principalCacheManager;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="NsiAuthModule"/> class. 
        /// Create a new instance of the <see cref="NsiAuthModule"/> class
        /// </summary>
        public NsiAuthModule(IHttpContextAccessor httpContextAccessor,IMemoryCache memoryCache, RequestDelegate next,  ProviderResolverManager providerResolverManager)
        {
            this._next = next;
            _log.Debug("Starting SRI Authentication and dataflow authorization module.");
            AuthUtils.ValidateConfig(ConfigManager.Instance.Config, this.GetType());
            try
            {
                this._userCred = providerResolverManager.CreateUserCredentials();

                this._authentication = providerResolverManager.CreateAuthenticationProvider();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            

            this._realm = ConfigManager.Instance.Config.Realm;

            string anonUser = ConfigManager.Instance.Config.AnonymousUser;
            if (!string.IsNullOrEmpty(anonUser))
            {
                this._anonUser = new User();
                if (this._anonUser != null)
                {
                    this._anonUser.UserName = anonUser;
                }
            }

            this._principalCacheManager = new PrincipalCacheManager(httpContextAccessor, memoryCache);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Authentication Request event handler
        /// </summary>
        /// <param name="sender">
        /// The current <see cref="HttpApplication"/> instance
        /// </param>
        /// <param name="e">
        /// Not used
        /// </param>
        public async Task Invoke(HttpContext context)
        {
            _log.Debug("Starting auth request");
            IUser user = new User();

            _log.Debug("Clearing old thread principal");
            Thread.CurrentPrincipal = null;
            IPrincipal principal;
            if (this._userCred.ParseResponse(context, user))
            {
                _log.Debug("Request has user credentials");
                // there were user credentials in the request
                principal = this._authentication.Authenticate(user); // try to authenticate user
                if (principal == null)
                {
                    _log.WarnFormat("Failed to authenticated user {0}", user.UserName);
                    // failed
                    await this._userCred.RequestAuthentication(context, this._realm);
                    return;
                }

                _log.DebugFormat("User {0} authenticated", user.UserName);
            }
            else
            {
                var token = this._principalCacheManager.GetPrincipalFromToken();
                if (!string.IsNullOrWhiteSpace(token))
                {
                    _log.Debug("Found token");
                    principal = this._principalCacheManager.GetPrincipalFromToken(token);
                    if (principal == null)
                    {
                        _log.Debug("Token not the access token");
                        // we found a Authorization header but not the access token
                        await this._userCred.RequestAuthentication(context, this._realm);
                        return;
                    }

                    _log.Debug("Found access token");
                    
                }
                else
                {
                    if (this._anonUser == null)
                    {
                        _log.Debug("anon user not allowed so request authentication (depends on implementation)");
                        // anon user not allowed so request authentication (depends on implementation)
                        await this._userCred.RequestAuthentication(context, this._realm);
                        return;
                    }

                    // introduce special value to allow real anonymous user
                    if (this._anonUser.UserName.Equals("*"))
                    {
                        _log.Debug("Anonymous user set to '*', pass check to the policy module");
                        // in that case we pass the checks to the policy module
                        // TODO use UserCred to add possibly a header e.g. WWW-Authenticate
                        if (!context.Request.Headers.ContainsKey("X-Requested-With"))
                        {
                            context.Response.Headers.Add(HttpConstants.WwwAuthHeader, this._userCred.GetWwwAuthenticationValue(this._realm));
                        }
                        await this._next.Invoke(context);
                        return;
                    }

                    // anon user exists, we check if it exists in the authentication store and if it does use that user.
                    _log.DebugFormat("anon user set to {0}, we check if it exists in the authentication store and if it does use that user.", this._anonUser.UserName);
                    principal = this._authentication.GetAnonymous(this._anonUser.UserName);
                    if (principal == null)
                    {
                        _log.DebugFormat("configured anon user '{0}', doesn't exist in auth store", this._anonUser.UserName);
                        // this is normally a configuration error. But probably we shouldn't allow the anonymous user
                        await this._userCred.RequestAuthentication(context, this._realm);
                        return;
                    }
                }
            }

            // TODO Do we need to do this ?
            // https://docs.microsoft.com/en-us/aspnet/web-api/overview/security/authentication-and-authorization-in-aspnet-web-api 
            _log.DebugFormat("Setting principal to thread {0}", principal.Identity?.Name);
            Thread.CurrentPrincipal = principal;

            await this._next.Invoke(context);
        }

        #endregion
    }
}