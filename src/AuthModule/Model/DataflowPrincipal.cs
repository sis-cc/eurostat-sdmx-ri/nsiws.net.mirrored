﻿// -----------------------------------------------------------------------
// <copyright file="DataflowPrincipal.cs" company="EUROSTAT">
//   Date Created : 2011-06-19
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.AuthModule.Model
{
    using System;
    using System.Security.Principal;

    using Estat.Sdmxsource.Extension.Constant;

    /// <summary>
    /// An implementation of the <see cref="IPrincipal"/> interface that stores that allowed dataflows
    /// </summary>
    public class DataflowPrincipal : IPrincipal
    {
        #region Constants and Fields

        /// <summary>
        /// This field holds the <see cref="IIdentity"/> of this principal
        /// </summary>
        private readonly IIdentity _identity;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DataflowPrincipal" /> class.
        /// Initialize a new instance of the <see cref="DataflowPrincipal" /> class with the specified <paramref name="identity" />
        /// </summary>
        /// <param name="identity">The <see cref="IIdentity" /> of this principal</param>
        /// <param name="isAnonymous">if set to <c>true</c> [is anonymous].</param>
        /// <exception cref="ArgumentNullException"><paramref name="identity" /> is <see langword="null" />.</exception>
        public DataflowPrincipal(IIdentity identity, bool isAnonymous)
        {
            this.IsAnonymous = isAnonymous;
            if (identity == null)
            {
                throw new ArgumentNullException("identity");
            }

            this._identity = identity;
        }

        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets a value indicating whether this instance is anonymous.
        /// </summary>
        /// <value><c>true</c> if this instance is anonymous; otherwise, <c>false</c>.</value>
        public bool IsAnonymous { get; set; }

        /// <summary>
        /// Gets the identity of the current principal.
        /// </summary>
        /// <returns>
        /// The <see cref="T:System.Security.Principal.IIdentity"/> object associated with the current principal.
        /// </returns>
        public IIdentity Identity
        {
            get
            {
                return this._identity;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Determines whether the current principal belongs to the specified role.
        /// </summary>
        /// <returns>
        /// true if the current principal is a member of the specified role; otherwise, false.
        /// </returns>
        /// <param name="role">
        /// The name of the role for which to check membership. 
        /// </param>
        public bool IsInRole(string role)
        {
            if (this.IsAnonymous)
            {
                return false;
            }

            PermissionType permission;
            if (!Enum.TryParse(role, out permission))
            {
                return false;
            }

            switch (permission)
            {
                    case PermissionType.CanImportStructures:
                    case PermissionType.CanReadData:
                    case PermissionType.CanReadStructuralMetadata:
                    return true;
            }

            return false;

        }

        #endregion
    }
}