﻿// -----------------------------------------------------------------------
// <copyright file="MappingStoreIdRepository.cs" company="EUROSTAT">
//   Date Created : 2017-06-07
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Microsoft.AspNetCore.Http;

namespace Estat.Nsi.AuthModule
{
    using log4net;
    using System.Configuration;
    using System.Text.RegularExpressions;
    using System.Web;

    /// <summary>
    /// A helper class for storing the mapping store id
    /// </summary>
    public class MappingStoreIdRepository : IMappingStoreIdRepository
    {
        private readonly IHttpContextAccessor _contextAccessor;

        /// <summary>
        /// The default identifier
        /// </summary>
        private const string DefaultId = "MappingStoreServer";

        /// <summary>
        /// The log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(MappingStoreIdRepository));

        /// <summary>
        /// The regular expression to extract the mapping store id
        /// </summary>
        private readonly Regex _pathRegex;

        /// <summary>
        /// The mapping store id validation
        /// </summary>
        private readonly Regex _sidValidation;
        /// <summary>
        /// Initializes a new instance of the <see cref="MappingStoreIdRepository"/> class.
        /// </summary>
        public MappingStoreIdRepository(IHttpContextAccessor contextAccessor)
        {
            this._contextAccessor = contextAccessor;
            var appSetting = ConfigurationManager.AppSettings["storeIdFromUrl"];
            if (appSetting != null)
            {
                this._pathRegex = new Regex(appSetting, RegexOptions.Compiled);
            }

            this._sidValidation = new Regex("^\\w+[A-Z_a-z0-9.-]*");
        }

        /// <summary>
        /// Gets the connection identifier from URL.
        /// </summary>
        /// <returns>The store Id from the URL; otherwise the default id</returns>
        public string GetConnectionName()
        {
            var fullpath = this._contextAccessor.HttpContext.Request.Path.Value;
            var appPath = this._contextAccessor.HttpContext.Request.PathBase.Value;
            var appPathLen = appPath.Length;
            var path = fullpath.Substring(appPathLen);
            if (this._pathRegex != null)
            {
                _log.Debug("Found regular expression to get the store id from URL");
                var match = this._pathRegex.Match(path);
                if (match.Success)
                {
                    _log.Debug("Regular expression matched path");
                    string sid;
                    if (match.Groups.Count > 1 && match.Groups[1].Success)
                    {
                        sid = match.Groups[1].Value;
                        _log.DebugFormat("Got SID = '{0}' from path", sid);
                    }
                    else
                    {
                        sid = this._contextAccessor.HttpContext.Request.Query["sid"]; 
                        _log.DebugFormat("Got SID = '{0}' from query parameter", sid);
                    }

                    if (!string.IsNullOrWhiteSpace(sid) && this._sidValidation.IsMatch(sid))
                    {
                        return sid;
                    }
                }
            }

            _log.DebugFormat("COuld not get the Store ID from the URL : '{0}'", path);

            return DefaultId;
        }

        /// <summary>
        /// Stores the connection identifier.
        /// </summary>
        public void StoreConnectionId()
        {
            var connectionId = this.GetConnectionName();
            this._contextAccessor.HttpContext.Items[DefaultId] = connectionId;
        }

        /// <summary>
        /// Gets the connection identifier.
        /// </summary>
        /// <returns>The connection identifier</returns>
        public string GetConnectionId()
        {
            return this._contextAccessor.HttpContext.Items[DefaultId] as string;
        }
    }
}