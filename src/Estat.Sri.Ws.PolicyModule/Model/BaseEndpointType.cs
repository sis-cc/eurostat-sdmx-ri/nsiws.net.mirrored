﻿// -----------------------------------------------------------------------
// <copyright file="BaseEndpointType.cs" company="EUROSTAT">
//   Date Created : 2018-02-09
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.PolicyModule.Model
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Text.RegularExpressions;

    public abstract class BaseEndpointType
    {
        /// <summary>
        /// To string
        /// </summary>
        private readonly string _toString;

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseEndpointType"/> class.
        /// </summary>
        /// <param name="pathExpression">The path expression.</param>
        /// <param name="allowAnonymous">if set to <c>true</c> [allow anonymous].</param>
        /// <param name="andSet">The and set.</param>
        /// <param name="orSet">The or set.</param>
        protected BaseEndpointType(string pathExpression, bool allowAnonymous, IEnumerable<string> andSet, IEnumerable<string> orSet)
        {
            if (pathExpression == null)
                throw new ArgumentNullException(nameof(pathExpression));
            if (andSet == null)
                throw new ArgumentNullException(nameof(andSet));
            if (orSet == null)
                throw new ArgumentNullException(nameof(orSet));

            this.PathExpression = new Regex(pathExpression, RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.Singleline);
            this.AllowAnonymous = allowAnonymous;
            this.AndSet = new HashSet<string>(andSet, StringComparer.OrdinalIgnoreCase);
            this.OrSet = new HashSet<string>(orSet, StringComparer.OrdinalIgnoreCase);

            _toString = string.Format(CultureInfo.InvariantCulture, "PathExpression: {0}, AllowAnonymous: {1}, And rules: [{2}], Or rules: [{3}]", pathExpression, allowAnonymous, string.Join("|", AndSet), string.Join("|", OrSet));
        }

        /// <summary>
        /// Gets the path expression.
        /// </summary>
        /// <value>
        /// The path expression.
        /// </value>
        public Regex PathExpression { get; }
        
        /// <summary>
        /// Gets a value indicating whether [allow anonymous].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [allow anonymous]; otherwise, <c>false</c>.
        /// </value>
        public bool AllowAnonymous { get; }

        /// <summary>
        /// Gets the and set.
        /// </summary>
        /// <value>
        /// The and set.
        /// </value>
        public ISet<string> AndSet { get; }

        /// <summary>
        /// Gets the or set.
        /// </summary>
        /// <value>
        /// The or set.
        /// </value>
        public ISet<string> OrSet { get; }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
        public override string ToString()
        {
            return _toString;
        }
    }
}