﻿// -----------------------------------------------------------------------
// <copyright file="RestResource.cs" company="EUROSTAT">
//   Date Created : 2018-02-09
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.PolicyModule.Model
{
    using System.Collections.Generic;

    public class RestResource : BaseEndpointType
    {
        public RestResource(string pathExpression, bool allowAnonymous, IEnumerable<string> andSet, IEnumerable<string> orSet, string method)
            : base(pathExpression, allowAnonymous, andSet, orSet)
        {
            this.Method = method;
        }

        public string Method { get; }

        public override string ToString()
        {
            return base.ToString() + "Method: " + Method;
        }
    }
}