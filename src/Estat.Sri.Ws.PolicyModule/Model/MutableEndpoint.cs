﻿// -----------------------------------------------------------------------
// <copyright file="MutableEndpoint.cs" company="EUROSTAT">
//   Date Created : 2018-02-12
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.PolicyModule.Model
{
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Xml;

    internal class MutableEndpoint
    {
        private readonly List<string> _andRules = new List<string>();

        private readonly List<string> _orRules = new List<string>();

        private readonly NameTable _nameTable = new NameTable();

        private XmlNamespaceManager _namespaceResolver;

        public List<string> AndRules => this._andRules;

        public List<string> OrRules => this._orRules;

        public XmlNamespaceManager NamespaceResolver
        {
            get
            {
                if (this._namespaceResolver == null)
                {
                    this._namespaceResolver = new XmlNamespaceManager(this._nameTable);
                }

                return this._namespaceResolver;
            }
        }

        public string Path { get; set; }

        public bool AllowAnonymous { get; private set; } = false;

        public string Expression { get; set; } = null;

        public string Method { get; set; } = null;

        public bool IsAnd { get; set; }

        public void Add(string permission)
        {
            if (string.IsNullOrWhiteSpace(permission))
            {
                Debug.Fail("permission is null");
                return;
            }

            if (IsAnd)
            {
                this._andRules.Add(permission);
            }
            else
            {
                this._orRules.Add(permission);
            }
        }

        public void Init()
        {
            this._orRules.Clear();
            this._andRules.Clear();
        }

        public void SetAllowAnonymous(string xmlValue)
        {
            if (!string.IsNullOrWhiteSpace(xmlValue))
            {
                AllowAnonymous = XmlConvert.ToBoolean(xmlValue);
            }
            else
            {
                AllowAnonymous = false;
            }
        }

        public RestResource CreateRestResource()
        {
            return new RestResource(this.Path, this.AllowAnonymous, this._andRules, this._orRules, this.Method);
        }

        public SoapEndpoint CreateSoapEndpoint()
        {
            XPathType xPathType = new XPathType(this.NamespaceResolver, this.Expression);
            return new SoapEndpoint(this.Path, this.AllowAnonymous, this._andRules, this._orRules, xPathType);
        }
    }
}