﻿// -----------------------------------------------------------------------
// <copyright file="NamespacePrefix.cs" company="EUROSTAT">
//   Date Created : 2018-02-09
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.PolicyModule.Model
{
    using System;

    /// <summary>
    /// Model class for holding the XML prefix and namespace pairs
    /// </summary>
    public class NamespacePrefix
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NamespacePrefix"/> class.
        /// </summary>
        /// <param name="prefix">The prefix.</param>
        /// <param name="ns">The ns.</param>
        public NamespacePrefix(string prefix, Uri ns)
        {
            this.Prefix = prefix;
            this.Namespace = ns;
        }

        /// <summary>
        /// Gets the prefix.
        /// </summary>
        /// <value>
        /// The prefix.
        /// </value>
        public string Prefix { get; }

        /// <summary>
        /// Gets the namespace.
        /// </summary>
        /// <value>
        /// The namespace.
        /// </value>
        public Uri Namespace { get; }
    }
}