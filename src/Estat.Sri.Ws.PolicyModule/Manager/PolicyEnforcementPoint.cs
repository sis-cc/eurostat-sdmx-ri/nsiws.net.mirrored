﻿// -----------------------------------------------------------------------
// <copyright file="PolicyEnforcementPoint.cs" company="EUROSTAT">
//   Date Created : 2018-02-12
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace Estat.Sri.Ws.PolicyModule.Manager
{
    using System;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Web;
    using System.Xml;
    using Estat.Sri.Ws.PolicyModule.Constant;
    using Estat.Sri.Ws.PolicyModule.Engine;
    using log4net;

    /// <summary>
    /// Policy enforcement point http module
    /// </summary>
    /// <seealso />
    public class PolicyEnforcementPoint
    {
        private readonly RequestDelegate _next;
        private readonly IHostingEnvironment _hostingEnvironment;
        private ILog _log = LogManager.GetLogger(typeof(PolicyEnforcementPoint));
        private PolicyDecisionPointManager _decisionPointManager;

        public PolicyEnforcementPoint(RequestDelegate next, IHostingEnvironment hostingEnvironment)
        {
            this._next = next;
            this._hostingEnvironment = hostingEnvironment;
            this.CreatePolicyDecisionPoint();
        }

        private void CreatePolicyDecisionPoint()
        {
            // TODO from configuration
            var assemblyPath = new FileInfo(Assembly.GetEntryAssembly().Location);
            var assemblyDir = assemblyPath.Directory?.FullName ?? ".";
            var rulesPath = Path.Combine( assemblyDir, "App_Data", "nsiws.xml");
            var schemaPath = Path.Combine(assemblyDir, "App_Data", "rules.xsd");
            if (rulesPath == null || !File.Exists(rulesPath))
            {
                _log.ErrorFormat(CultureInfo.InvariantCulture, "Could not find the rules file at {0}", rulesPath);
                throw new FileNotFoundException(rulesPath);
            }

            if (!File.Exists(schemaPath))
            {
                schemaPath = null;
            }

            PolicyInformationFromXml policyInformation = new PolicyInformationFromXml();
            using (var stream = File.OpenRead(rulesPath))
            {
                try
                {
                    var rules = policyInformation.Parse(stream, schemaPath);
                    this._decisionPointManager = new PolicyDecisionPointManager(rules);
                }
                catch (XmlException e)
                {

                    _log.ErrorFormat(CultureInfo.InvariantCulture, "An XML/Validation error occured while parsing the rules file '{0}' at line {1} column {2}", rulesPath, e.LineNumber, e.LinePosition);
                    _log.Error(e);
                    throw;
                }
            }
        }

        /// <summary>
        /// Contexts the on authorize request.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="eventArgs">The <see cref="EventArgs"/> instance containing the event data.</param>
        public async Task Invoke(HttpContext context)
        {

            AccessResponseType accessResponseType = this._decisionPointManager.CanAccess(context);
            switch (accessResponseType)
            {
                case AccessResponseType.Success:
                    break;
                case AccessResponseType.AnonymousUserDenied:
                    _log.DebugFormat(CultureInfo.InvariantCulture, "Anonymous access denied for path {0}", context.Request.QueryString.Value);
                    CreateAnonymousDenieddResponse(context);
                    return;
                case AccessResponseType.InsufficientPermissions:
                    _log.DebugFormat(CultureInfo.InvariantCulture, "Access denied for path {0}", context.Request.QueryString.Value);
                    CreateDeniedResponse(context);
                    return;
                default:
                    _log.DebugFormat(CultureInfo.InvariantCulture, "Rule not found for path {0}", context.Request.QueryString.Value);
                    CreateNotFound(context);
                    return;
            }

            await this._next.Invoke(context);
        }

        /// <summary>
        /// Create an 403 (Access Denied) HTTP response
        /// </summary>
        /// <param name="httpContext">
        /// The current <see /> instance
        /// </param>
        private static void CreateDeniedResponse(HttpContext httpContext)
        {
            httpContext.Response.StatusCode = 403;
            var data = Encoding.UTF8.GetBytes("403 Forbidden");
            httpContext.Response.Body.Write(data, 0, data.Length);
        }

        /// <summary>
        /// Create an 404 HTTP response
        /// </summary>
        /// <param name="httpContext"></param>
        private static void CreateNotFound(HttpContext httpContext)
        {
            httpContext.Response.StatusCode = 404;
            var data = Encoding.UTF8.GetBytes("404 Not found");
            httpContext.Response.Body.Write(data, 0, data.Length);
        }

        /// <summary>
        /// Create an 403 (Access Denied) HTTP response
        /// </summary>
        /// <param name="application">
        /// The current <see cref="HttpApplication"/> instance
        /// </param>
        private static void CreateAnonymousDenieddResponse(HttpContext httpContext)
        {
            // we cannot respond with 401 because we don't know what WWW-Authentication header value to include
            if (httpContext.Response.Headers.Any(h => h.Key.Equals("WWW-Authenticate")))
            {
                httpContext.Response.StatusCode = 401;
            }
            else
            {
                httpContext.Response.StatusCode = 403;
            }

            var data = Encoding.UTF8.GetBytes("The requested resource requires user authentication.");

            httpContext.Response.Body.Write(data, 0, data.Length);
        }

        /// <summary>
        /// Disposes of the resources (other than memory) used by the module that implements <see cref="T:System.Web.IHttpModule" />.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
        }

        /// <summary>
        /// Disposes of the resources (other than memory) used by the module that implements <see cref="T:System.Web.IHttpModule" />.
        /// </summary>
        /// <param name="managed">The managed.</param>
        private void Dispose(bool managed)
        {
            if (managed)
            {

            }
        }
    }
}