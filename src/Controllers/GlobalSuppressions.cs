// -----------------------------------------------------------------------
// <copyright file="GlobalSuppressions.cs" company="EUROSTAT">
//   Date Created : 2016-04-11
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Scope = "member", Target = "Estat.Sri.Ws.Controllers.Builder.CrossDataWriterBuilder.#Build(System.Xml.XmlWriter,System.Collections.Generic.Queue`1<System.Action>)", Justification = "It is OK. We build and return a disposable object")]
[assembly: SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed", Scope = "member", Target = "Estat.Sri.Ws.Controllers.Factory.DataQueryVisitorFactory.#.ctor(Estat.Sdmxsource.Extension.Manager.IDataflowAuthorizationManager,Estat.Sri.Ws.Controllers.Manager.IDataflowPrincipalManager)", Justification = "It is OK. Not a library so cleaner code is better than overloads")]
[assembly: SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed", Scope = "member", Target = "Estat.Sri.Ws.Controllers.Builder.CrossDataWriterBuilder.#.ctor(Estat.Sri.Ws.Controllers.Constants.DataWriterDelayBehavior,System.Text.Encoding)", Justification = "It is OK. Not a library so cleaner code is better than overloads")]
[assembly: SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Scope = "member", Target = "Estat.Sri.Ws.Controllers.Factory.LazyDataWriterFactory.#GetDataWriterEngine(Org.Sdmxsource.Sdmx.Api.Model.Data.IDataFormat,System.IO.Stream)", Justification = "It is OK. We build and return a disposable object")]
[assembly: SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Scope = "member", Target = "Estat.Sri.Ws.Controllers.Manager.RetrieverManager.#BuildDataResponse(Org.Sdmxsource.Sdmx.Api.Model.Query.IRestDataQuery,Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.ISdmxObjectRetrievalManager,Estat.Sdmxsource.Extension.Factory.IRetrieverFactory)", Justification = "It is OK. We build and return a disposable object")]
[assembly: SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Scope = "member", Target = "Estat.Sri.Ws.Controllers.Manager.RetrieverManager.#BuildDataResponseGeneratorCross(Org.Sdmxsource.Sdmx.Api.Model.Query.IRestDataQuery,Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.ISdmxObjectRetrievalManager,Estat.Sdmxsource.Extension.Factory.IRetrieverFactory,Org.Sdmxsource.Sdmx.Api.Constants.SdmxSchema)", Justification = "It is OK. We build and return a disposable object")]
[assembly: SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Scope = "member", Target = "Estat.Sri.Ws.Controllers.Constants.SoapOperationExtension.#GetResponse(Estat.Sri.Ws.Controllers.Constants.SoapOperation)", Justification = "It is OK we have big switch")]