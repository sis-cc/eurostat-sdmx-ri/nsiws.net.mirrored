﻿using estat.sri.ws.auth.generic;
using Estat.Sri.Mapping.Api.Model;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Text;

namespace Estat.Sri.Ws.Controllers.Utils
{
    public class SdmxAuthorizationImpl : ISdmxAuthorization
    {
        private readonly IGenericAuthorization genericAuthorization;
        private readonly IPrincipal principal;
        private readonly string storeId;

        public SdmxAuthorizationImpl(IPrincipal principal, string storeId, IGenericAuthorization genericAuthorization)
        {
            this.principal = principal ?? throw new ArgumentNullException(nameof(principal));
            this.storeId = storeId ?? throw new ArgumentNullException(nameof(storeId));
            this.genericAuthorization = genericAuthorization ?? throw new ArgumentNullException(nameof(genericAuthorization));
        }

        public bool CanDelete(IMaintainableObject maintainableObject)
        {
            return genericAuthorization.IsAuthorized(principal, storeId, maintainableObject, StructureAction.Delete);
        }

        public bool CanInsert(IMaintainableObject maintainableObject)
        {
            return genericAuthorization.IsAuthorized(principal, storeId, maintainableObject, StructureAction.Insert);
        }

        public bool CanRead(IStructureReference structureReference)
        {
            return genericAuthorization.CanReadStructure(principal, storeId, structureReference);
        }

        public bool CanReadData(IStructureReference structureReference, bool isPit)
        {
            return genericAuthorization.CanReadData(principal, storeId, structureReference, isPit); 
        }

        public bool CanUpdate(IMaintainableObject maintainableObject)
        {
            return genericAuthorization.IsAuthorized(principal, storeId, maintainableObject, StructureAction.UpdateFinal);
        }

        public ISet<IStructureReference> GetAllowedStructureReferences()
        {
            return genericAuthorization.GetReadableArtefacts(principal, storeId);
        }
    }
}
