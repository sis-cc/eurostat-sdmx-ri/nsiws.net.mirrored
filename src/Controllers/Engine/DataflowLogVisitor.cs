﻿// -----------------------------------------------------------------------
// <copyright file="DataflowLogVisitor.cs" company="EUROSTAT">
//   Date Created : 2015-12-09
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Controllers.Engine
{
    using System;

    using Estat.Sdmxsource.Extension.Engine;
    using Estat.Sdmxsource.Extension.Manager;

    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;

    /// <summary>
    /// Visit a DataQuery and log the dataflow.
    /// </summary>
    public class DataflowLogVisitor : IDataQueryVisitor
    {
        /// <summary>
        /// The _dataflow log manager
        /// </summary>
        private readonly IDataflowLogManager _dataflowLogManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataflowLogVisitor"/> class.
        /// </summary>
        /// <param name="dataflowLogManager">The dataflow log manager.</param>
        public DataflowLogVisitor(IDataflowLogManager dataflowLogManager)
        {
            this._dataflowLogManager = dataflowLogManager;
        }

        /// <summary>
        /// Visits the specified data query.
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <exception cref="ArgumentNullException"><paramref name="dataQuery"/> is <see langword="null" />.</exception>
        public void Visit(IBaseDataQuery dataQuery)
        {
            if (dataQuery == null)
            {
                throw new ArgumentNullException("dataQuery");
            }

            this._dataflowLogManager.Log(dataQuery.Dataflow);
        }
    }
}