﻿// -----------------------------------------------------------------------
// <copyright file="DataQueryVisitorComposite.cs" company="EUROSTAT">
//   Date Created : 2015-12-15
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Controllers.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Estat.Sdmxsource.Extension.Engine;

    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;

    /// <summary>
    ///     The default SDMX RI implementation for <see cref="IDataQueryVisitor" />.
    /// </summary>
    public class DataQueryVisitorComposite : IDataQueryVisitor
    {
        /// <summary>
        ///     The _data query visitors
        /// </summary>
        private readonly IList<IDataQueryVisitor> _dataQueryVisitors;

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataQueryVisitorComposite" /> class.
        /// </summary>
        /// <param name="dataQueryVisitors">The data query visitors.</param>
        /// <exception cref="System.ArgumentNullException">dataQueryVisitors is null</exception>
        public DataQueryVisitorComposite(IList<IDataQueryVisitor> dataQueryVisitors)
        {
            if (dataQueryVisitors == null)
            {
                throw new ArgumentNullException("dataQueryVisitors");
            }

            this._dataQueryVisitors = dataQueryVisitors.ToArray();
        }

        /// <summary>
        ///     Visits the specified data query.
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        public void Visit(IBaseDataQuery dataQuery)
        {
            foreach (var visitor in this._dataQueryVisitors)
            {
                visitor.Visit(dataQuery);
            }
        }
    }
}