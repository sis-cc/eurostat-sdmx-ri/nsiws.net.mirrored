﻿// -----------------------------------------------------------------------
// <copyright file="DataQueryVisitorFactory.cs" company="EUROSTAT">
//   Date Created : 2015-12-15
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.Ws.Configuration;
using Microsoft.AspNetCore.Http;

namespace Estat.Sri.Ws.Controllers.Factory
{
    using System.Collections.Generic;
    using estat.sri.ws.auth.generic;
    using Estat.Sdmxsource.Extension.Engine;
    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sri.Ws.Controllers.Controller;
    using Estat.Sri.Ws.Controllers.Engine;
    using Estat.Sri.Ws.Controllers.Manager;
    using Microsoft.Extensions.Configuration;
    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// The SDMX RI implementation of <see cref="IDataQueryVisitorFactory"/>. It will return the <see cref="DataQueryVisitorComposite"/> with
    /// <see cref="AuthDataQueryVisitor"/>, <see cref="DataQueryValidatingVisitor"/> and finally <see cref="DataflowLogVisitor"/>.
    /// </summary>
    public class DataQueryVisitorFactory : IDataQueryVisitorFactory
    {
        /// <summary>
        /// The _dataflow authorization manager
        /// </summary>
        private readonly IDataflowAuthorizationManager _dataflowAuthorizationManager;

        /// <summary>
        ///     The _dataflow principal manager
        /// </summary>
        private readonly IDataflowPrincipalManager _dataflowPrincipalManager;

        /// <summary>
        /// The configuration
        /// </summary>
        private readonly SettingsFromConfigurationManager _configuration;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataQueryVisitorFactory" /> class.
        /// </summary>
        /// <param name="dataflowAuthorizationManager">The dataflow authorization manager.</param>
        /// <param name="dataflowPrincipalManager">The dataflow principal manager.</param>
        public DataQueryVisitorFactory(IDataflowAuthorizationManager dataflowAuthorizationManager, IDataflowPrincipalManager dataflowPrincipalManager, IHttpContextAccessor httpContextAccessor, SettingsFromConfigurationManager configuration)
        {
            this._dataflowAuthorizationManager = dataflowAuthorizationManager;
            this._dataflowPrincipalManager = dataflowPrincipalManager ?? new DataflowPrincipalManager(httpContextAccessor);
            this._configuration = configuration;
        }

        /// <summary>
        ///     Gets the data query visitor.
        /// </summary>
        /// <param name="dataFormat">The data format.</param>
        /// <returns>The <see cref="IDataQueryVisitor" />.</returns>
        public IDataQueryVisitor GetDataQueryVisitor(DataType dataFormat)
        {
            List<IDataQueryVisitor> dataQueryVisitors = new List<IDataQueryVisitor>();

            // first check if the current user is authorized to access this dataflow
            IDataQueryVisitor authenticationVisitor = new AuthDataQueryVisitor(_dataflowPrincipalManager, _dataflowAuthorizationManager, _configuration.EnableReleaseManagement);
            dataQueryVisitors.Add(authenticationVisitor);

            // then check if the data request is valid
            IDataQueryVisitor validatingVisitor = new DataQueryValidatingVisitor(new DataRequestValidator(dataFormat, _configuration.AllowSdmx21Cross));
            dataQueryVisitors.Add(validatingVisitor);

            // finally log the data request
            IDataQueryVisitor dataflowLogVisitor = new DataflowLogVisitor(new DataflowLogManager(dataFormat));
            dataQueryVisitors.Add(dataflowLogVisitor);

            return new DataQueryVisitorComposite(dataQueryVisitors);
        }
    }
}