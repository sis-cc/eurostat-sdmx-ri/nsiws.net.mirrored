// -----------------------------------------------------------------------
// <copyright file="MessageFaultSoapBuilderFactory.cs" company="EUROSTAT">
//   Date Created : 2015-12-11
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Controllers.Factory
{
    using Estat.Sri.Ws.Controllers.Builder;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// A factory to get a instance of <see cref="IMessageFaultSoapBuilder"/>
    /// </summary>
    public class MessageFaultSoapBuilderFactory : IMessageFaultSoapBuilderFactory
    {
        /// <summary>
        /// The fault SDMX SOAP v20 builder
        /// </summary>
        private readonly MessageFaultSoapv20Builder _faultSoapv20Builder;

        /// <summary>
        /// The fault SDMX SOAP v21 builder
        /// </summary>
        private readonly MessageFaultSoapv21Builder _faultSoapv21Builder;

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageFaultSoapBuilderFactory"/> class.
        /// </summary>
        public MessageFaultSoapBuilderFactory()
        {
            this._faultSoapv21Builder = new MessageFaultSoapv21Builder();
            this._faultSoapv20Builder = new MessageFaultSoapv20Builder();
        }

        /// <summary>
        ///     Gets the message fault SOAP builder.
        /// </summary>
        /// <param name="sdmxSchema">The SDMX schema.</param>
        /// <returns>The <see cref="IMessageFaultSoapBuilder" />.</returns>
        public IMessageFaultSoapBuilder GetMessageFaultSoapBuilder(SdmxSchemaEnumType sdmxSchema)
        {
            switch (sdmxSchema)
            {
                case SdmxSchemaEnumType.VersionTwoPointOne:
                    return this._faultSoapv21Builder;
                case SdmxSchemaEnumType.VersionTwo:
                    return this._faultSoapv20Builder;
            }

            return null;
        }

        /// <summary>
        ///     Gets the message fault SOAP builder.
        /// </summary>
        /// <param name="sdmxSchema">The SDMX schema.</param>
        /// <returns>The <see cref="IMessageFaultSoapBuilder" />.</returns>
        public IMessageFaultSoapBuilder GetMessageFaultSoapBuilder(SdmxSchemaEnumType sdmxSchema, string soap20namespace)
        {
            switch (sdmxSchema)
            {
                case SdmxSchemaEnumType.VersionTwoPointOne:
                    return this._faultSoapv21Builder;
                case SdmxSchemaEnumType.VersionTwo:
                    return new MessageFaultSoapv20Builder(soap20namespace);
            }

            return null;
        }
    }
}