﻿// -----------------------------------------------------------------------
// <copyright file="StreamController.cs" company="EUROSTAT">
//   Date Created : 2013-10-10
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.IO;
using System.IO.Compression;
using System.Threading.Tasks;

namespace Estat.Sri.Ws.Controllers.Controller
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The stream controller.
    /// </summary>
    /// <typeparam name="TWriter">
    /// The type of the writer.
    /// </typeparam>
    public class ZipStreamController : StreamController<Stream>
    {
        private readonly string _filename;
        
        public ZipStreamController(Func<Stream, Queue<Action>, Task> function, string filename) : base(function)
        {
            _filename = filename;
        }

        public override void StreamTo(Stream writer, Queue<Action> actions)
        {
            using (ZipArchive archive = new ZipArchive(writer, ZipArchiveMode.Create, false))
            {
                var zipEntry = archive.CreateEntry(_filename);

                base.StreamTo(zipEntry.Open(), actions);
            }
        }

        public override async Task StreamToAsync(Stream writer, Queue<Action> actions)
        {
            using (ZipArchive archive = new ZipArchive(writer, ZipArchiveMode.Create, false))
            {
                var zipEntry = archive.CreateEntry(_filename);

                await base.StreamToAsync(zipEntry.Open(), actions);
            }
        }
    }
}