// -----------------------------------------------------------------------
// <copyright file="IStructureRequestController.cs" company="EUROSTAT">
//   Date Created : 2015-12-15
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Microsoft.AspNetCore.Http;
using Org.Sdmxsource.Sdmx.Api.Util;

namespace Estat.Sri.Ws.Controllers.Controller
{
    using System;
    using System.Xml;

    using Estat.Sri.Ws.Controllers.Constants;
    using Estat.Sri.Ws.Controllers.Model;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Builder.Common;

    /// <summary>
    /// Interface IStructureRequestController
    /// </summary>
    public interface IStructureRequestController
    {
        /// <summary>
        /// Parse the structure request for SOAP 2.0
        /// </summary>
        /// <param name="request">The SDMX part of the request </param>
        /// <param name="webServiceNamespace">The namespace of the webservice </param>
        /// <param name="queryParameter">Th query parameter in case of a </param>
        /// <returns>The <see cref="StreamController{XmlWriter}" />.</returns>
        IStreamController<XmlWriter> ParseRequest(IReadableDataLocation request, WebServiceEndpoint endpoint, string webServiceNamespace, string queryParameter);

        /// <summary>
        /// Handles the structure request.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="headers">The headers.</param>
        /// <param name="ctx"></param>
        /// <returns>
        /// The <see cref="ResponseActionWithHeader" />.
        /// </returns>
        [Obsolete("Use the overload with the ICommonStructureQuery")]
        // If no external application is using this, it can be deleted
        ResponseActionWithHeader ParseRequest(IRestStructureQuery query, IHeaderDictionary headers, HttpContext ctx);

        /// <summary>
        /// Handles the structure request.
        /// </summary>
        /// <param name="query">The method to get the query.</param>
        /// <param name="headers">The headers.</param>
        /// <param name="ctx"></param>
        /// <param name="restApiVersion">The REST api version</param>
        /// <returns>
        /// The <see cref="ResponseActionWithHeader" />.
        /// </returns>
        ResponseActionWithHeader ParseRequest(RestStructureQueryParams query, IHeaderDictionary headers, HttpContext ctx, RestApiVersion restApiVersion);

        /// <summary>
        /// Handles the structure request SOAP V21.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="soapOperation">The SOAP operation.</param>
        /// <returns>
        /// The <see cref="Message" />.
        /// </returns>
        IStreamController<XmlWriter> ParseRequest(IReadableDataLocation request, SoapOperation soapOperation);
    }
}