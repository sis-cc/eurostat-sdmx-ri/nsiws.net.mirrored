﻿// -----------------------------------------------------------------------
// <copyright file="RangeHeaderHandler.cs" company="EUROSTAT">
//   Date Created : 2018-04-16
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using Controllers.Utils;
using Estat.Sdmxsource.Extension.Model;
using Estat.Sdmxsource.Extension.Util;
using Microsoft.AspNetCore.Http;
using Org;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.ConceptScheme;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
using Org.Sdmxsource.Sdmx.Util.Objects.Container;
using Org.Sdmxsource.Util;

namespace Estat.Sri.Ws.Controllers.Controller
{
    /// <summary>
    /// Handles the Range header
    /// </summary>
    /// <seealso cref="Estat.Sri.Ws.Controllers.Controller.IRangeHeaderHandler" />
    public class RangeHeaderHandler : IRangeHeaderHandler
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public RangeHeaderHandler(IHttpContextAccessor httpContextAccessor)
        {
            this._httpContextAccessor = httpContextAccessor;
        }

        public void AddHeader(string rangeHeader, int count)
        {
            var headerString = HeaderScope.ParseHeaderString(rangeHeader);
            if (headerString == null)
            {
                RequestRangeNotSatisfiable(count);
            }
            var lowerLimit = headerString.Item1;
            var upperLimit = headerString.Item2;

            if (lowerLimit > count-1 || lowerLimit < 0)
            {
                RequestRangeNotSatisfiable(count);
            }

            var endIndex = upperLimit > count - 1 ? count - 1 : upperLimit;

            this._httpContextAccessor.HttpContext.Response.StatusCode = (int)HttpStatusCode.PartialContent;
            var contentRangeValue = HeaderScope.Unit + " " + lowerLimit + "-" + endIndex + "/" + count;

            this._httpContextAccessor.HttpContext.Response.Headers.Add("Content-Range", contentRangeValue);
        }

        private void RequestRangeNotSatisfiable(int count)
        {
            this._httpContextAccessor.HttpContext.Response.StatusCode = (int) HttpStatusCode.RequestedRangeNotSatisfiable;
            var contentRangeValue = HeaderScope.Unit + " " + "*/" + count;
            this._httpContextAccessor.HttpContext.Response.Headers.Add("Content-Range", contentRangeValue);
            throw new ServiceException(HttpStatusCode.RequestedRangeNotSatisfiable, "Please check Content-Range value");
        }

        public void AddHeaderForData(string rangeHeader, DataRequest request, SdmxSchema sdmxSchema)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var nrRecords = request.RetrieverFactory.GetNumberOfObservations(request.DataQuery);
            if (nrRecords == 0)
            {
                throw new SdmxNoResultsException("No data found");
            }
            this.AddHeader(rangeHeader, nrRecords);
        }

        public IMutableObjects HandleStructures(IMutableObjects mutableObjects, SdmxStructureEnumType queryStructureType)
        {
            if (HeaderScope.Range == null)
            {
                return mutableObjects;
            }

            if (mutableObjects == null)
            {
                throw new ArgumentNullException(nameof(mutableObjects));
            }

            var itemSchemes = new List<IMaintainableMutableObject>();
            switch (queryStructureType)
            {
                case SdmxStructureEnumType.CategoryScheme:
                    itemSchemes = this.ApplyRange(mutableObjects.CategorySchemes.Cast<IItemSchemeMutableObject<ICategoryMutableObject>>().ToList());
                    break;
                case SdmxStructureEnumType.ConceptScheme:
                    itemSchemes = this.ApplyRange(mutableObjects.ConceptSchemes.Cast<IItemSchemeMutableObject<IConceptMutableObject>>().ToList());
                    break;
                case SdmxStructureEnumType.CodeList:
                    itemSchemes = this.ApplyRange(mutableObjects.Codelists.Cast<IItemSchemeMutableObject<ICodeMutableObject>>().ToList());
                    break;
                case SdmxStructureEnumType.OrganisationScheme:
                    itemSchemes = this.ApplyRange(mutableObjects.OrganisationUnitSchemes.Cast<IItemSchemeMutableObject<IOrganisationUnitMutableObject>>().ToList());
                    break;
                case SdmxStructureEnumType.AgencyScheme:
                    itemSchemes = this.ApplyRange(mutableObjects.AgencySchemeMutableObjects.Cast<IItemSchemeMutableObject<IAgencyMutableObject>>().ToList());
                    break;
                case SdmxStructureEnumType.DataProviderScheme:
                    itemSchemes = this.ApplyRange(mutableObjects.DataProviderSchemeMutableObjects.Cast<IItemSchemeMutableObject<IDataProviderMutableObject>>().ToList());
                    break;
                case SdmxStructureEnumType.DataConsumerScheme:
                    itemSchemes = this.ApplyRange(mutableObjects.DataConsumberSchemeMutableObjects.Cast<IItemSchemeMutableObject<IDataConsumerMutableObject>>().ToList());
                    break;
                case SdmxStructureEnumType.OrganisationUnitScheme:
                    itemSchemes = this.ApplyRange(mutableObjects.OrganisationUnitSchemes.Cast<IItemSchemeMutableObject<IOrganisationUnitMutableObject>>().ToList());
                    break;
                case SdmxStructureEnumType.ReportingTaxonomy:
                    itemSchemes = this.ApplyRange(mutableObjects.ReportingTaxonomys.Cast<IItemSchemeMutableObject<IReportingCategoryMutableObject>>().ToList());
                    break;

                default:
                    throw new ServiceException(HttpStatusCode.BadRequest, "Range used on non item scheme");
            }
            return new MutableObjectsImpl(itemSchemes);
        }

        private List<IMaintainableMutableObject> ApplyRange<T>(List<IItemSchemeMutableObject<T>> itemSchemes) where T : IItemMutableObject
        {
            itemSchemes.Sort(new ItemSortComparer());
            HeaderScope.NumberOfStructureRecords = itemSchemes.SelectMany(x => x.Items).Count();
            var itemIndex = 0;
            var itemSchemesInRange = new List<IMaintainableMutableObject>();

            foreach (var itemScheme in itemSchemes.ToList())
            {
                var originalItems = itemScheme.Items.ToList();
                var initialItemCount = itemScheme.Items.Count;
                itemScheme.Items.Clear();
                var itemSchemeIsInRange = false;
                foreach (var item in originalItems)
                {
                    if (itemIndex >= HeaderScope.Range.Item1 && itemIndex <= HeaderScope.Range.Item2)
                    {
                        itemScheme.Items.Add(item);
                        itemSchemeIsInRange = true;
                    }
                    itemIndex++;
                }

                itemScheme.IsPartial = itemScheme.Items.Count != initialItemCount;
                if (itemSchemeIsInRange)
                {
                    itemSchemesInRange.Add(itemScheme);
                }
            }

            return itemSchemesInRange;
        }
    }
}