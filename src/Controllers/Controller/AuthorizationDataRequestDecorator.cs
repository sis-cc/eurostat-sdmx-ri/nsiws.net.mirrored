﻿// -----------------------------------------------------------------------
// <copyright file="AuthorizationDataRequestDecorator.cs" company="EUROSTAT">
//   Date Created : 2020-9-10
//   Copyright (c) 2009, 2020 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sdmxsource.Extension.Model;
using Estat.Sri.Mapping.Api.Utils;
using Estat.Sri.Ws.Controllers.Factory;
using Estat.Sri.Ws.Controllers.Model;
using Microsoft.AspNetCore.Http;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Query;
using Org.Sdmxsource.Sdmx.Api.Util;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Estat.Sri.Ws.Controllers.Controller
{
    public class AuthorizationDataRequestDecorator : IDataRequestController
    {
        private readonly IDataRequestController _decorated;
        private readonly ISdmxAuthorizationScopeFactory authorizationScopeFactory;

        public AuthorizationDataRequestDecorator(IDataRequestController decorated, ISdmxAuthorizationScopeFactory authorizationScopeFactory)
        {
            _decorated = decorated ?? throw new ArgumentNullException(nameof(decorated));
            this.authorizationScopeFactory = authorizationScopeFactory ?? throw new ArgumentNullException(nameof(authorizationScopeFactory));
        }

        public IStreamController<XmlWriter> ParseRequest(IReadableDataLocation request, ISoapRequest<DataType> dataType, string webServiceNamespace, string queryParameter)
        {
            using (SdmxAuthorizationScope scope = authorizationScopeFactory.Create())
            {
                return _decorated.ParseRequest(request, dataType, webServiceNamespace, queryParameter);
            }
        }

        public ResponseActionWithHeader ParseRequest(IRestDataQuery request, IHeaderDictionary headers)
        {
            using (SdmxAuthorizationScope scope = authorizationScopeFactory.Create())
            {
                return _decorated.ParseRequest(request, headers);
            }
        }
    }
}
