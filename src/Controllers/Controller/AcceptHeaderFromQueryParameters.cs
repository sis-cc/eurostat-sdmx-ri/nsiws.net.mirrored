using System.Linq;
using Estat.Sri.Ws.Controllers.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;

namespace Estat.Sri.Ws.Controllers.Controller
{
    using System;
    using System.Collections.Generic;

    public static class AcceptHeaderFromQueryParameters
    {
        private static readonly Dictionary<string, string> _formatMapping;

        static AcceptHeaderFromQueryParameters()
        {
            var config = System.Configuration.ConfigurationManager.GetSection(FormatMapping.SECTION_NAME) as FormatMapping;
            if (config != null)
            {
                _formatMapping = config.Mappings.Cast<FormatMappingElement>().ToDictionary(k => k.Format, v => v.AcceptHeader, StringComparer.Ordinal);
            }
            else
            {
                _formatMapping = new Dictionary<string, string>();
            }

        }

        public static void Update(IQueryCollection queryParameters, IHeaderDictionary headers)
        {
            if (queryParameters == null)
            {
                return;
            }

            if (headers == null)
            {
                throw new ArgumentNullException(nameof(headers));
            }

            if (!queryParameters.TryGetValue("format", out var requestFormat) || StringValues.IsNullOrEmpty(requestFormat))
            {
                return;
            }

            string acceptHeaderValue = GetAcceptHeader(requestFormat);
            if (acceptHeaderValue != null)
            {
                if (!headers.TryGetValue("Accept", out var acceptFromHttpHeader))
                {
                    headers.Add("Accept", acceptHeaderValue);
                }
                else if (string.IsNullOrWhiteSpace(acceptFromHttpHeader))
                {
                    headers["Accept"] = acceptHeaderValue;
                }
                else
                {
                    headers["Accept"] = StringValues.Concat(acceptHeaderValue, acceptFromHttpHeader);
                }
            }
        }

        public static string GetAcceptHeader(string requestFormat)
        {
            if (requestFormat != null && _formatMapping.TryGetValue(requestFormat, out string acceptHeader))
            {
                return acceptHeader;
            }

            return null;
        }
    }
}
