﻿// -----------------------------------------------------------------------
// <copyright file="IRestStructureSubmitController.cs" company="EUROSTAT">
//   Date Created : 2017-03-19
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Controllers.Controller
{
    using System.IO;
    using System.Net.Mime;

    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Estat.Sdmxsource.Extension.Model.Error;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// REST submit SDMX structural metadata controller
    /// </summary>
    public interface IRestStructureSubmitController
    {
        /// <summary>
        /// Stores the specified structure.
        /// </summary>
        /// <param name="structureStream">The structure stream.</param>
        /// <param name="storeId">The store identifier.</param>
        /// <param name="contentType">Type of the content.</param>
        IList<IResponseWithStatusObject> Post(Stream structureStream, string storeId, ContentType contentType);

        /// <summary>
        /// Stores the specified structure.
        /// </summary>
        /// <param name="structureStream">The structure stream.</param>
        /// <param name="storeId">The store identifier.</param>
        /// <param name="contentType">Type of the content.</param>
        IList<IResponseWithStatusObject> Put(Stream structureStream, IRestStructureQuery query, string storeId, ContentType contentType);

        /// <summary>
        /// Patches the specified structure.
        /// </summary>
        /// <param name="structureStream">The structure stream.</param>
        /// <param name="storeId">The store identifier.</param>
        IList<IResponseWithStatusObject> Patch(Stream structureStream, IRestStructureQuery query, string storeId);

        /// <summary>
        /// Deletes the specified query.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="storeId">The store identifier.</param>
        IList<IResponseWithStatusObject> Delete(IRestStructureQuery query, string storeId);

        /// <summary>
        /// Writes the specified responces.
        /// </summary>
        /// <param name="responces">The responces.</param>
        /// <param name="stream">The stream.</param>
        /// <param name="encoding">The encoding.</param>
        void Write(IList<IResponseWithStatusObject> responces, Stream stream, Encoding encoding);
    }
}