// -----------------------------------------------------------------------
// <copyright file="RestStructureSubmitController.cs" company="EUROSTAT">
//   Date Created : 2017-03-19
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Controllers.Controller
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Mime;
    using System.Text;

    using Estat.Sdmxsource.Extension.Constant;
    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sdmxsource.Extension.Model.Error;

    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;

    /// <summary>
    /// Class RestStructureSubmitController.
    /// </summary>
    /// <seealso cref="Estat.Sri.Ws.Controllers.Controller.IRestStructureSubmitController" />
    public class RestStructureSubmitController : IRestStructureSubmitController
    {
        /// <summary>
        /// The data location factory
        /// </summary>
        private readonly IReadableDataLocationFactory _dataLocationFactory;

        /// <summary>
        /// The parsing manager
        /// </summary>
        private readonly IStructureParsingManager _parsingManager;

        /// <summary>
        /// The structure persistence manager
        /// </summary>
        private readonly IStructureSubmitter _structurePersistenceManager;

        /// <summary>
        /// The stub builder
        /// </summary>
        private readonly IBuilder<IMaintainableObject, IStructureReference> _stubBuilder;

        /// <summary>
        /// The response writer manager
        /// </summary>
        private readonly IResponseWriterManager _responseWriterManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="RestStructureSubmitController" /> class.
        /// </summary>
        /// <param name="dataLocationFactory">The data location factory.</param>
        /// <param name="parsingManager">The parsing manager.</param>
        /// <param name="structurePersistenceManager">The structure persistence manager.</param>
        /// <param name="stubBuilder">The stub builder.</param>
        /// <param name="responseWriterManager">The response writer manager.</param>
        public RestStructureSubmitController(IReadableDataLocationFactory dataLocationFactory, IStructureParsingManager parsingManager, IStructureSubmitter structurePersistenceManager, IBuilder<IMaintainableObject, IStructureReference> stubBuilder, IResponseWriterManager responseWriterManager)
        {
            this._dataLocationFactory = dataLocationFactory;
            this._parsingManager = parsingManager;
            this._structurePersistenceManager = structurePersistenceManager;
            this._stubBuilder = stubBuilder;
            this._responseWriterManager = responseWriterManager;
        }

        /// <summary>
        /// Saves the specified structure stream.
        /// </summary>
        /// <param name="structureStream">The structure stream.</param>
        /// <param name="storeId">The store identifier.</param>
        /// <param name="contentType">Type of the content.</param>
        public IList<IResponseWithStatusObject> Post(Stream structureStream, string storeId, ContentType contentType)
        {
            return this.SaveInternal(storeId, structureStream, DatasetActionEnumType.Append);
        }

        /// <summary>
        /// replaces the specified structure stream.
        /// </summary>
        /// <param name="structureStream">The structure stream.</param>
        /// <param name="storeId">The store identifier.</param>
        /// <param name="contentType">Type of the content.</param>
        public IList<IResponseWithStatusObject> Put(Stream structureStream, IRestStructureQuery query, string storeId, ContentType contentType)
        {
            return this.SaveInternal(storeId, structureStream, query, DatasetActionEnumType.Replace);
        }

        /// <summary>
        /// Patches the specified structure stream.
        /// </summary>
        /// <param name="structureStream">The structure stream.</param>
        /// <param name="storeId">The store identifier.</param>
        public IList<IResponseWithStatusObject> Patch(Stream structureStream, IRestStructureQuery query, string storeId)
        {
            return this.SaveInternal(storeId, structureStream, query, DatasetActionEnumType.Information);
        }

        /// <summary>
        /// Deletes the specified structural metadata specified by the query.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="storeId">The store identifier.</param>
        public IList<IResponseWithStatusObject> Delete(IRestStructureQuery query, string storeId)
        {
            if (query == null)
            {
                throw new ArgumentNullException(nameof(query));
            }

            var sdmxObject = this._stubBuilder.Build(query.StructureReference);
            var sdmxObjects = new SdmxObjectsImpl(DatasetActionEnumType.Delete);
            sdmxObjects.AddIdentifiable(sdmxObject);
            return this._structurePersistenceManager.SubmitStructures(storeId, sdmxObjects);
        }

        /// <summary>
        /// Writes the specified responces.
        /// </summary>
        /// <param name="responces">The responces.</param>
        /// <param name="stream">The stream.</param>
        /// <param name="encoding">The encoding.</param>
        public void Write(IList<IResponseWithStatusObject> responces, Stream stream, Encoding encoding)
        {
            var format = new ErrorMessageFormat(stream, encoding);
            _responseWriterManager.Write(format, responces);
        }

        /// <summary>
        /// Saves the specified objects
        /// </summary>
        /// <param name="storeId">
        /// The store identifier.
        /// </param>
        /// <param name="structureStream">
        /// The structure stream.
        /// </param>
        /// <param name="query">
        /// The query.
        /// </param>
        /// <param name="action">
        /// The action.
        /// </param>
        /// <returns>
        /// The responses
        /// </returns>
        private IList<IResponseWithStatusObject> SaveInternal(string storeId, Stream structureStream, IRestStructureQuery query, DatasetAction action)
        {
            ISdmxObjects objects;
            try
            {
                objects = GetSdmxObjects(structureStream);
            }
            catch (SdmxException ex)
            {
                return BuildSdmxErrorResponse(action, ex, query.StructureReference);
            }

            objects.Action = action;
            if (objects.SdmxObjectsInfo.NumberMaintainables != 1)
            {
                var message = new MessageObject(new ITextTypeWrapper[] { new TextTypeWrapperImpl(null, "This method expects a single SDMX maintainable artefact.", null) }, SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.Conflict));

                IResponseWithStatusObject conflict = new ResponseWithStatusObject(new IMessageObject[] { message }, ResponseStatus.Failure, query.StructureReference, action);
                return new List<IResponseWithStatusObject>() { conflict };
            }

            var submittedArtefact = objects.GetAllMaintainables().First();
            if (!query.StructureReference.IsMatch(submittedArtefact))
            {
                var message = new MessageObject(new ITextTypeWrapper[] { new TextTypeWrapperImpl(null, "This method expects the submitted artefact and the target resource URI to match.", null) }, HttpStatusCode.MovedPermanently);

                IResponseWithStatusObject conflict = new ResponseWithStatusObject(new IMessageObject[] { message }, ResponseStatus.Failure, submittedArtefact.AsReference, action);
                return new List<IResponseWithStatusObject>() { conflict };
            }

            return this._structurePersistenceManager.SubmitStructures(storeId, objects);
        }

        /// <summary>
        /// Saves the specified objects
        /// </summary>
        /// <param name="storeId">The store identifier.</param>
        /// <param name="structureStream">The structure stream.</param>
        /// <param name="actiom">The actiom.</param>
        /// <returns>The responses</returns>
        private IList<IResponseWithStatusObject> SaveInternal(string storeId, Stream structureStream, DatasetAction actiom)
        {
            ISdmxObjects objects;
            try
            {
                objects = GetSdmxObjects(structureStream);
            }
            catch (SdmxException ex)
            {
                return BuildSdmxErrorResponse(actiom, ex);
            }

            objects.Action = actiom;


            return this._structurePersistenceManager.SubmitStructures(storeId, objects);
        }

        /// <summary>
        /// Builds the SDMX error response.
        /// </summary>
        /// <param name="actiom">The actiom.</param>
        /// <param name="ex">The ex.</param>
        /// <param name="structureReference">The structure reference.</param>
        /// <returns>The <see cref="IList&lt;IResponseWithStatusObject&gt;" />.</returns>
        private static IList<IResponseWithStatusObject> BuildSdmxErrorResponse(DatasetAction actiom, SdmxException ex, IStructureReference structureReference = null)
        {
            IList<IMessageObject> messages = new IMessageObject[] { new MessageObject(new ITextTypeWrapper[] { new TextTypeWrapperImpl("en", ex.Message, null) }, ex.SdmxErrorCode) };
            return new IResponseWithStatusObject[] { new ResponseWithStatusObject(messages, ResponseStatus.Failure, structureReference, actiom) };
        }

        /// <summary>
        /// Gets the SDMX objects.
        /// </summary>
        /// <param name="structureStream">The structure stream.</param>
        /// <returns>The <see cref="ISdmxObjects" />.</returns>
        private ISdmxObjects GetSdmxObjects(Stream structureStream)
        {
            ISdmxObjects objects;
            using (IReadableDataLocation structureLocation = this._dataLocationFactory.GetReadableDataLocation(structureStream))
            {
                var structureWorkspace = this._parsingManager.ParseStructures(structureLocation);
                objects = structureWorkspace.GetStructureObjects(false);
            }

            return objects;
        }
    }
}