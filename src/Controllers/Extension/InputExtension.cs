﻿// -----------------------------------------------------------------------
// <copyright file="InputExtension.cs" company="EUROSTAT">
//   Date Created : 2013-10-19
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sdmxsource.Extension.Extension;

namespace Estat.Sri.Ws.Controllers.Extension
{
    using System;
    using Estat.Sri.Ws.Controllers.Constants;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;

    /// <summary>
    ///     The input extension.
    /// </summary>
    public static class InputExtension
    {
        /// <summary>
        ///     Gets the SOAP operation.
        /// </summary>
        /// <param name="dataFormat">
        ///     The data format.
        /// </param>
        /// <param name="sdmxSchema">
        ///     The SDMX schema.
        /// </param>
        /// <returns>
        ///     The <see cref="SoapOperation" />.
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="dataFormat"/> is <see langword="null" />.</exception>
        public static SoapOperation GetSoapOperation(this BaseDataFormat dataFormat, SdmxSchemaEnumType sdmxSchema)
        {
            switch (sdmxSchema)
            {
                case SdmxSchemaEnumType.Edi:
                case SdmxSchemaEnumType.Ecv:
                case SdmxSchemaEnumType.Csv:
                case SdmxSchemaEnumType.Json:
                    return SoapOperation.Null;
            }

            if (dataFormat == null)
            {
                throw new ArgumentNullException("dataFormat");
            }

            switch (dataFormat.EnumType)
            {
                case BaseDataFormatEnumType.Generic:
                    return SoapOperation.GetGenericData;
                case BaseDataFormatEnumType.Compact:
                    return sdmxSchema == SdmxSchemaEnumType.VersionTwoPointOne ? SoapOperation.GetStructureSpecificData : SoapOperation.GetCompactData;
                case BaseDataFormatEnumType.Utility:
                    return sdmxSchema == SdmxSchemaEnumType.VersionTwoPointOne ? SoapOperation.Null : SoapOperation.GetUtilityData;
                case BaseDataFormatEnumType.CrossSectional:
                    return sdmxSchema == SdmxSchemaEnumType.VersionTwoPointOne ? SoapOperation.Null : SoapOperation.GetCrossSectionalData;
            }

            return SoapOperation.Null;
        }

        /// <summary>
        /// Get the <see cref="DataType"/> from the specified <paramref name="soapOperation"/> and <paramref name="sdmxSchema"/>
        /// </summary>
        /// <param name="soapOperation">The <see cref="SoapOperation"/>. It must be for data.</param>
        /// <param name="sdmxSchema">The SDMX version, it is only used when it cannot auto-detect if it is 2.0 or 2.1</param>
        /// <returns></returns>
        public static DataType GetDataType(this SoapOperation soapOperation, SdmxSchemaEnumType sdmxSchema)
        {
            switch (soapOperation)
            {
                case SoapOperation.Null:
                    return null;
                case SoapOperation.GetCompactData:
                    return DataType.GetFromEnum(DataEnumType.Compact20);
                case SoapOperation.GetUtilityData:
                    return DataType.GetFromEnum(DataEnumType.Utility20);
                case SoapOperation.GetCrossSectionalData:
                    return DataType.GetFromEnum(DataEnumType.CrossSectional20);
                case SoapOperation.GetGenericData:
                    return BaseDataFormat.GetFromEnum(BaseDataFormatEnumType.Generic).GetDataType(sdmxSchema);
                case SoapOperation.GetGenericTimeSeriesData:
                    return DataType.GetFromEnum(DataEnumType.Generic21);
                case SoapOperation.GetStructureSpecificData:
                    return DataType.GetFromEnum(DataEnumType.Compact21);
                case SoapOperation.GetStructureSpecificTimeSeriesData:
                    return DataType.GetFromEnum(DataEnumType.Compact21);
                case SoapOperation.GetGenericMetadata:
                case SoapOperation.GetStructureSpecificMetadata:
                case SoapOperation.GetStructures:
                case SoapOperation.GetDataflow:
                case SoapOperation.GetMetadataflow:
                case SoapOperation.GetDataStructure:
                case SoapOperation.GetMetadataStructure:
                case SoapOperation.GetCategoryScheme:
                case SoapOperation.GetConceptScheme:
                case SoapOperation.GetCodelist:
                case SoapOperation.GetHierarchicalCodelist:
                case SoapOperation.GetOrganisationScheme:
                case SoapOperation.GetReportingTaxonomy:
                case SoapOperation.GetStructureSet:
                case SoapOperation.GetProcess:
                case SoapOperation.GetCategorisation:
                case SoapOperation.GetProvisionAgreement:
                case SoapOperation.GetConstraint:
                case SoapOperation.GetDataSchema:
                case SoapOperation.GetMetadataSchema:
                case SoapOperation.QueryStructure:
                case SoapOperation.SubmitRegistrations:
                case SoapOperation.QueryRegistration:
                case SoapOperation.SubmitStructure:
                case SoapOperation.SubmitSubscriptions:
                case SoapOperation.QuerySubscription:
                case SoapOperation.RegistryInterfaceRequest:
                    throw new SdmxSemmanticException("Requested non-data in a data request");
                default:
                    throw new ArgumentOutOfRangeException(nameof(soapOperation), soapOperation, null);
            }
        }

        /// <summary>
        /// Handles the SDMX V21 structure document.
        /// </summary>
        /// <param name="structure">The structure.</param>
        /// <returns>The <see cref="SoapOperation" />.</returns>
        private static SoapOperation HandleSdmxV21StructureDocument(SdmxStructureEnumType structure)
        {
            switch (structure)
            {
                case SdmxStructureEnumType.Dataflow:
                    return SoapOperation.GetDataflow;
                case SdmxStructureEnumType.CodeList:
                    return SoapOperation.GetCodelist;
                case SdmxStructureEnumType.Categorisation:
                    return SoapOperation.GetCategorisation;
                case SdmxStructureEnumType.CategoryScheme:
                    return SoapOperation.GetCategoryScheme;
                case SdmxStructureEnumType.ConceptScheme:
                    return SoapOperation.GetConceptScheme;
                case SdmxStructureEnumType.Dsd:
                    return SoapOperation.GetDataStructure;
                case SdmxStructureEnumType.HierarchicalCodelist:
                    return SoapOperation.GetHierarchicalCodelist;
                case SdmxStructureEnumType.ReportingTaxonomy:
                    return SoapOperation.GetReportingTaxonomy;
                case SdmxStructureEnumType.OrganisationScheme:
                    return SoapOperation.GetOrganisationScheme;
                case SdmxStructureEnumType.Constraint:
                    return SoapOperation.GetConstraint;
                case SdmxStructureEnumType.Process:
                    return SoapOperation.GetProcess;
                case SdmxStructureEnumType.ProvisionAgreement:
                    return SoapOperation.GetProvisionAgreement;
                case SdmxStructureEnumType.StructureSet:
                    return SoapOperation.GetStructureSet;
                case SdmxStructureEnumType.MetadataFlow:
                    return SoapOperation.GetMetadataflow;
                case SdmxStructureEnumType.Msd:
                    return SoapOperation.GetMetadataStructure;
                default:
                    return SoapOperation.GetStructures;
            }
        }
    }
}