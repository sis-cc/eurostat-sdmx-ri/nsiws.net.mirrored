﻿// -----------------------------------------------------------------------
// <copyright file="SdmxStructureXmlFormat.cs" company="EUROSTAT">
//   Date Created : 2015-12-17
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Controllers.Model
{
    using System;
    using System.Collections.Generic;
    using System.Xml;

    using Estat.Sri.Ws.Controllers.Constants;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;

    /// <summary>
    /// An Structure format that supports <see cref="XmlWriter"/>
    /// </summary>
    public class SdmxStructureXmlFormat : IStructureFormat
    {
        /// <summary>
        /// Get the queue of pending actions
        /// </summary>
        private readonly Queue<Action> _actions;

        /// <summary>
        /// The _endpoint
        /// </summary>
        private readonly WebServiceEndpoint _endpoint;

        /// <summary>
        /// The _SDMX output format
        /// </summary>
        private readonly StructureOutputFormat _sdmxOutputFormat;

        /// <summary>
        /// The _XML writer
        /// </summary>
        private readonly XmlWriter _xmlWriter;

        /// <summary>
        /// Initializes a new instance of the <see cref="SdmxStructureXmlFormat" /> class.
        /// </summary>
        /// <param name="sdmxOutputFormat">The SDMX output format.</param>
        /// <param name="endpoint">The endpoint.</param>
        /// <param name="xmlWriter">The XML writer.</param>
        /// <param name="actions">The actions.</param>
        /// <exception cref="System.ArgumentNullException">
        /// sdmxOutputFormat
        /// or
        /// xmlWriter
        /// </exception>
        /// <exception cref="ArgumentNullException"><paramref name="sdmxOutputFormat" /> is <see langword="null" />.</exception>
        public SdmxStructureXmlFormat(StructureOutputFormat sdmxOutputFormat, WebServiceEndpoint endpoint, XmlWriter xmlWriter, Queue<Action> actions)
        {
            if (sdmxOutputFormat == null)
            {
                throw new ArgumentNullException("sdmxOutputFormat");
            }

            if (xmlWriter == null)
            {
                throw new ArgumentNullException("xmlWriter");
            }

            this._sdmxOutputFormat = sdmxOutputFormat;
            this._endpoint = endpoint;
            this._xmlWriter = xmlWriter;
            this._actions = actions;
        }

        /// <summary>
        /// Gets the queue of pending actions
        /// </summary>
        public Queue<Action> Actions
        {
            get
            {
                return this._actions;
            }
        }

        /// <summary>
        /// Gets the endpoint.
        /// </summary>
        /// <value>The endpoint.</value>
        public WebServiceEndpoint Endpoint
        {
            get
            {
                return this._endpoint;
            }
        }

        /// <summary>
        /// Gets the format as string.
        /// </summary>
        /// <value>The format as string.</value>
        public string FormatAsString
        {
            get
            {
                return this._sdmxOutputFormat.ToString();
            }
        }

        /// <summary>
        /// Gets the SDMX output format.
        /// </summary>
        /// <value>The SDMX output format.</value>
        public StructureOutputFormat SdmxOutputFormat
        {
            get
            {
                return this._sdmxOutputFormat;
            }
        }

        /// <summary>
        /// Gets the XML writer.
        /// </summary>
        /// <value>The XML writer.</value>
        public XmlWriter XMLWriter
        {
            get
            {
                return this._xmlWriter;
            }
        }
    }
}