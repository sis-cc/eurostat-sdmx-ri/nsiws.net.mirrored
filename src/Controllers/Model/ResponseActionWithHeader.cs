﻿// -----------------------------------------------------------------------
// <copyright file="ResponseActionWithHeader.cs" company="EUROSTAT">
//   Date Created : 2016-07-06
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Threading.Tasks;

namespace Estat.Sri.Ws.Controllers.Model
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    using Estat.Sdmxsource.Extension.Model;

    /// <summary>
    /// The rest response.
    /// </summary>
    public class ResponseActionWithHeader
    {
        /// <summary>
        /// The _function.
        /// </summary>
        private readonly Func<Stream, Queue<Action>, Task> _function;

        /// <summary>
        /// The _response content header.
        /// </summary>
        private readonly IResponseContentHeader _responseContentHeader;

        /// <summary>
        /// Initializes a new instance of the <see cref="ResponseActionWithHeader"/> class.
        /// </summary>
        /// <param name="function">
        /// The action.
        /// </param>
        /// <param name="responseContentHeader">
        /// The response content header.
        /// </param>
        public ResponseActionWithHeader(Func<Stream, Queue<Action>, Task> function, IResponseContentHeader responseContentHeader)
        {
            if (function == null)
            {
                throw new ArgumentNullException("function");
            }

            if (responseContentHeader == null)
            {
                throw new ArgumentNullException("responseContentHeader");
            }

            this._function = function;
            this._responseContentHeader = responseContentHeader;
        }
        
        /// <summary>
        /// Gets the function.
        /// </summary>
        /// <value>
        /// The function.
        /// </value>
        public Func<Stream, Queue<Action>, Task> Function
        {
            get
            {
                return this._function;
            }
        }

        /// <summary>
        /// Gets the response content header.
        /// </summary>
        /// <value>
        /// The response content header.
        /// </value>
        public IResponseContentHeader ResponseContentHeader
        {
            get
            {
                return this._responseContentHeader;
            }
        }
    }
}