﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estat.Sri.Ws.Controllers.Model
{
    public class FormatMapping : ConfigurationSection
    {
        public const string SECTION_NAME = "FormatMapping";
        [ConfigurationProperty("Mappings")]
        public FormatMappingCollection Mappings => base["Mappings"] as FormatMappingCollection;
    }

    public class FormatMappingElement : ConfigurationElement
    {
        [ConfigurationProperty("Format")]
        public string Format => base["Format"] as string;

        [ConfigurationProperty("AcceptHeader")]
        public string AcceptHeader => base["AcceptHeader"] as string;
    }

    [ConfigurationCollection(typeof(FormatMappingElement), AddItemName = "Mapping", CollectionType = ConfigurationElementCollectionType.BasicMap)]
    public class FormatMappingCollection : ConfigurationElementCollection
    {
        public FormatMappingElement this[int index]
        {
            get { return (FormatMappingElement) this.BaseGet(index); }
            set
            {
                if (this.BaseGet(index) != null)
                {
                    this.BaseRemoveAt(index);
                }
                this.BaseAdd(index, value);
            }
        }

        public void Add(FormatMappingElement serviceConfig)
        {
            this.BaseAdd(serviceConfig);
        }

        public void Clear()
        {
            this.BaseClear();
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new FormatMappingElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((FormatMappingElement)element).Format;
        }
    }
}
