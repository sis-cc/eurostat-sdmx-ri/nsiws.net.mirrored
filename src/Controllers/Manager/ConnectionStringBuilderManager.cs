﻿// -----------------------------------------------------------------------
// <copyright file="ConnectionStringBuilderManager.cs" company="EUROSTAT">
//   Date Created : 2014-11-03
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Ws.Controllers.Manager
{
    using System;
    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sri.Mapping.Api.Manager;
    using log4net;
    using System.Configuration;
    using System.Linq;
    using System.Security.Principal;

    /// <summary>
    /// The NSI WS implementation of <see cref="IConnectionStringBuilderManager"/> which uses <see cref="IConfigurationStoreManager"/> and <see cref="IStoreIdBuilderManager"/>
    /// </summary>
    /// <seealso cref="Estat.Sdmxsource.Extension.Manager.IConnectionStringBuilderManager" />
    public class ConnectionStringBuilderManager : IConnectionStringBuilderManager
    {
        /// <summary>
        /// The log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(ConnectionStringBuilderManager));

        /// <summary>
        /// The configuration manager
        /// </summary>
        private readonly IConfigurationStoreManager _configurationManager;

        /// <summary>
        /// The store identifier builder manager
        /// </summary>
        private readonly IStoreIdBuilderManager _storeIdBuilderManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConnectionStringBuilderManager" /> class.
        /// </summary>
        /// <param name="configurationManager">The configuration manager.</param>
        /// <param name="storeIdBuilderManager">The store identifier builder manager.</param>
        /// <exception cref="System.ArgumentNullException">configurationManager
        /// or
        /// storeIdBuilderManager</exception>
        public ConnectionStringBuilderManager(IConfigurationStoreManager configurationManager, IStoreIdBuilderManager storeIdBuilderManager)
        {
            if (configurationManager == null)
            {
                throw new System.ArgumentNullException(nameof(configurationManager));
            }

            if (storeIdBuilderManager == null)
            {
                throw new System.ArgumentNullException(nameof(storeIdBuilderManager));
            }

            _configurationManager = configurationManager;
            _storeIdBuilderManager = storeIdBuilderManager;
        }

        /// <summary>
        /// Builds the <see cref="T:System.Configuration.ConnectionStringSettings" />
        /// </summary>
        /// <returns>The <see cref="T:System.Configuration.ConnectionStringSettings" />.</returns>
        public ConnectionStringSettings Build()
        {
            var id = _storeIdBuilderManager.Build();
            if (id != null)
            {
                _log.DebugFormat("Found store id '{0}'", id);
                return Build(id);
            }

            _log.Debug("Could not find store id");
            return this.Build((string)null);
        }

        /// <summary>
        /// Builds the the <see cref="T:System.Configuration.ConnectionStringSettings" /> from the specified principal.
        /// </summary>
        /// <param name="principal">The principal.</param>
        /// <returns>The <see cref="T:System.Configuration.ConnectionStringSettings" />.</returns>
        public ConnectionStringSettings Build(IPrincipal principal)
        {
            var id = _storeIdBuilderManager.Build(principal);
            if (id != null)
            {
                _log.DebugFormat("Found store id '{0}' using principal", id);
                return Build(id);
            }

            _log.Debug("Could not find store id using principal");
            return this.Build((string)null);
        }

        /// <summary>
        /// Builds the <see cref="T:System.Configuration.ConnectionStringSettings" />
        /// </summary>
        /// <param name="storeId">The store identifier. If it is null then <see cref="Build()"/> will be used</param>
        /// <returns>The <see cref="T:System.Configuration.ConnectionStringSettings" />.</returns>
        public ConnectionStringSettings Build(string storeId)
        {
            if (storeId == null)
            {
                var first = this._configurationManager.GetSettings<ConnectionStringSettings>().FirstOrDefault();
                if (first != null)
                {
                    _log.DebugFormat("Falling back to the first connection = {0}", first.Name);
                    return first;
                }

                _log.Debug("Could not find Connection strings from connection manager");
                return null;
            }

            ConnectionStringSettings connectionStringSettings = this._configurationManager.GetSettings<ConnectionStringSettings>().FirstOrDefault(settings => settings.Name.Equals(storeId, StringComparison.OrdinalIgnoreCase));
            if (connectionStringSettings == null)
            {
                _log.Debug("Could not find Connection strings from connection manager");
                return null;
            }

            _log.Debug("Using Connection strings from connection manager");
            return connectionStringSettings;
        }
    }
}