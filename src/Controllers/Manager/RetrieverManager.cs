// -----------------------------------------------------------------------
// <copyright file="RetrieverManager.cs" company="EUROSTAT">
//   Date Created : 2015-11-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Ws.Controllers.Manager
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Security.Principal;

    using Estat.Sdmxsource.Extension.Builder;
    using Estat.Sdmxsource.Extension.Config;
    using Estat.Sdmxsource.Extension.Constant;
    using Estat.Sdmxsource.Extension.Factory;
    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sdmxsource.Extension.Model;

    using Estat.Sri.CustomRequests.Builder;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.Ws.Configuration;
    using Estat.Sri.Ws.Controllers.Controller;
    using Estat.Sri.Ws.Controllers.Factory;

    using log4net;

    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query.Complex;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.DataParser.Engine;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.Query;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.Structureparser.Workspace;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Util.Sdmx;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Complex;

    /// <summary>
    ///     The default implementation of <see cref="IRetrieverManager" />
    /// </summary>
    public class RetrieverManager : IRetrieverManager
    {
        #region fields

        /// <summary>
        ///     The _log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(RetrieverManager));

        /// <summary>
        ///     The _dataflow authorization manager
        /// </summary>
        private readonly IDataflowAuthorizationManager _dataflowAuthorizationManager;

        private readonly IRangeHeaderHandler _headerHandler;

        /// <summary>
        ///     The _dataflow principal manager
        /// </summary>
        private readonly IDataflowPrincipalManager _dataflowPrincipalManager;

        /// <summary>
        ///     The _data query parse manager
        /// </summary>
        private readonly IDataQueryParseManager _dataQueryParseManager = new DataQueryParseManager(SdmxSchemaEnumType.Null);

        /// <summary>
        ///     The Query Parsing manager.
        /// </summary>
        private readonly IQueryParsingManager _queryParsingManager = new QueryParsingManager(SdmxSchemaEnumType.Null);

        /// <summary>
        ///     The Query Parsing Manager for special requests V20
        /// </summary>
        private readonly IQueryParsingManager _queryParsingManagerCustomRequests;

        /// <summary>
        ///     The _retriever factories
        /// </summary>
        private readonly IList<IRetrieverFactory> _retrieverFactories;

        /// <summary>
        ///     The _structure request behavior
        /// </summary>
        private readonly RequestBehaviorType _structureRequestBehavior;

        /// <summary>
        /// Authorization scope factory
        /// </summary>
        private readonly ISdmxAuthorizationScopeFactory authorizationScopeFactory;

        /// <summary>
        /// The configuration.
        /// </summary>
        private readonly SettingsFromConfigurationManager _configuration;

        #endregion

        /// <summary>
        ///     Initializes a new instance of the <see cref="RetrieverManager" /> class.
        /// </summary>
        /// <param name="retrieverFactories">The retriever factories.</param>
        /// <param name="dataflowPrincipalManager">The dataflow principal manager.</param>
        /// <param name="dataflowAuthorizationManager">The dataflow authorization manager.</param>
        /// <param name="headerHandler"></param>
        /// <exception cref="ArgumentNullException"><paramref name="retrieverFactories" /> is <see langword="null" />.</exception>
        /// <exception cref="ArgumentException"><paramref name="retrieverFactories" /> is empty.</exception>
        public RetrieverManager(
            IEnumerable<IRetrieverFactory> retrieverFactories, IDataflowPrincipalManager dataflowPrincipalManager, IDataflowAuthorizationManager dataflowAuthorizationManager,
            IRangeHeaderHandler headerHandler, ISdmxAuthorizationScopeFactory authorizationScopeFactory, SettingsFromConfigurationManager configuration)
        {
            this._dataflowPrincipalManager = dataflowPrincipalManager;
            this._dataflowAuthorizationManager = dataflowAuthorizationManager;
            this._headerHandler = headerHandler;
            this._queryParsingManagerCustomRequests = new QueryParsingManager(SdmxSchemaEnumType.VersionTwo, new QueryBuilder(null, new ConstrainQueryBuilderV2(), null));
            if (retrieverFactories == null)
            {
                throw new ArgumentNullException("retrieverFactories");
            }

            IBuilder<IList<IRetrieverFactory>, IEnumerable<IRetrieverFactory>> retrieverOrderBuilder = new RetrieverOrderBuilder();
            this._retrieverFactories = retrieverOrderBuilder.Build(retrieverFactories);

            if (this._retrieverFactories.Count == 0)
            {
                throw new ArgumentException(Messages.NoRetrieverFactoriesSpecified, "retrieverFactories");
            }

            this._structureRequestBehavior = ModuleConfigSection.GetSection().StructureRequestBehavior;
            this.authorizationScopeFactory = authorizationScopeFactory;

            this._configuration = configuration;
        }

        #region Data requests

        /// <summary>
        ///     Gets the data response generator complex.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>The <see cref="IDataResponse{TWriter}" />.</returns>
        /// <exception cref="SdmxNoResultsException">
        ///     Could not find Dataflow and/or DSD related
        ///     with this data request
        /// </exception>
        public IDataResponse<IDataWriterEngine> GetDataResponseComplex(IReadableDataLocation input)
        {
            return this.GetDataResponse((manager, factory) => this.BuildDataResponseGeneratorComplex(input, manager, factory));
        }

        /// <summary>
        ///     Gets the data response for SDMX v2.0 Cross Sectional.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>The <see cref="IDataResponse{ICrossSectionalWriterEngine}" />.</returns>
        /// <exception cref="SdmxNoResultsException">
        ///     Could not find Dataflow and/or DSD related
        ///     with this data request
        /// </exception>
        public IDataResponse<ICrossSectionalWriterEngine> GetDataResponseCross(IReadableDataLocation input)
        {
            return this.GetDataResponse((manager, factory) => this.BuildDataResponseGeneratorCross(input, manager, factory, SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwo)));
        }

        /// <summary>
        ///     Gets the SDMX v2.0 CrossSectional data response from a REST request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The <see cref="IDataResponse&lt;ICrossSectionalWriterEngine&gt;" />.</returns>
        /// <exception cref="SdmxNoResultsException">
        ///     Could not find Dataflow and/or DSD related
        ///     with this data request
        /// </exception>
        public IDataResponse<ICrossSectionalWriterEngine> GetDataResponseCross(DataRequest request)
        {
            return this.BuildDataResponseGeneratorCross(request, SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwoPointOne));
        }

        /// <summary>
        ///     Gets the data response simple.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>The <see cref="IDataResponse{IDataWriterEngine}" />.</returns>
        /// <exception cref="SdmxNoResultsException">
        ///     Could not find Dataflow and/or DSD related
        ///     with this data request
        /// </exception>
        public IDataResponse<IDataWriterEngine> GetDataResponseSimple(IReadableDataLocation input)
        {
            return this.GetDataResponse((manager, factory) => this.BuildDataResponseGenerator(input, manager, factory));
        }

        /// <summary>
        ///     Gets the data response simple.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The <see cref="IDataResponse{IDataWriterEngine}" />.</returns>
        /// <exception cref="SdmxNoResultsException">
        ///     Could not find Dataflow and/or DSD related
        ///     with this data request
        /// </exception>
        public IDataResponse<IDataWriterEngine> GetDataResponseSimple(DataRequest request)
        {
            return this.BuildDataResponse(request);
        }

        /// <summary>
        /// Parses the data request.
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <returns>
        /// The <see cref="DataRequest" />.
        /// </returns>
        public DataRequest ParseDataRequest(IRestDataQuery dataQuery)
        {
            return this.GetDataRequest(dataQuery);
        }

        /// <summary>
        ///     Builds the data response.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The <see cref="IDataResponse{IDataWriterEngine}" />.</returns>
        private IDataResponse<IDataWriterEngine> BuildDataResponse(DataRequest request)
        {
            IDataQuery dataQuery = request.DataQuery;
            if (dataQuery.Dataflow != null)
            {
                var retrieverFactory = request.RetrieverFactory;

                //request.SdmxSchema is set in the DataREquestcontroller.GetFunction (from the dataTypeFormat) - it can be NULL and we can set it according to the needs 
                // we want SDMX v2.1 behavior with REST even for 2.0 data . So we need to decide if we have 2.1 or 3.0.0 . We don't care about the schema version but the standard version
                // but because in a lot of code in DR still uses SdmxSchema we 
                SdmxSchemaEnumType workaround = request.SdmxStandard == SdmxStandard.VersionThree ? SdmxSchemaEnumType.VersionThree : SdmxSchemaEnumType.VersionTwoPointOne;

                var dataRetrievalManager = retrieverFactory.GetDataRetrieval(SdmxSchema.GetFromEnum(workaround), this._dataflowPrincipalManager.GetCurrentPrincipal());
                return new DataResponse<IDataWriterEngine, IDataQuery>(new[] { dataQuery }, dataRetrievalManager.GetDataAsync);
            }

            return null;
        }

        /// <summary>
        ///     Builds the data response generator.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="retrievalManager">The retrieval manager.</param>
        /// <param name="retrieverFactory">The retriever factory.</param>
        /// <returns>The <see cref="IDataResponse{IDataWriterEngine}" />.</returns>
        private IDataResponse<IDataWriterEngine> BuildDataResponseGenerator(IReadableDataLocation input, ISdmxObjectRetrievalManager retrievalManager, IRetrieverFactory retrieverFactory)
        {
            var dataQueries = this._dataQueryParseManager.BuildDataQuery(input, retrievalManager);
            if (dataQueries != null && dataQueries.Count > 0)
            {
                // SOAP v2.0 so we always have SDMX v2.0
                var dataRetrievalManager = retrieverFactory.GetDataRetrieval(SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwo), this._dataflowPrincipalManager.GetCurrentPrincipal());
                return new DataResponse<IDataWriterEngine, IDataQuery>(dataQueries, dataRetrievalManager.GetDataAsync);
            }

            return null;
        }

        /// <summary>
        ///     Builds the data response generator complex.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="retrievalManager">The retrieval manager.</param>
        /// <param name="retrieverFactory">The retriever factory.</param>
        /// <returns>The <see cref="IDataResponse{IDataWriterEngine}" />.</returns>
        private IDataResponse<IDataWriterEngine> BuildDataResponseGeneratorComplex(IReadableDataLocation input, ISdmxObjectRetrievalManager retrievalManager, IRetrieverFactory retrieverFactory)
        {
            var complexDataQuery = this._dataQueryParseManager.BuildComplexDataQuery(input, retrievalManager);
            if (complexDataQuery != null && complexDataQuery.Count > 0)
            {
                var dataRetrievalManager = retrieverFactory.GetAdvancedDataRetrieval(this._dataflowPrincipalManager.GetCurrentPrincipal());
                return new DataResponse<IDataWriterEngine, IComplexDataQuery>(complexDataQuery, dataRetrievalManager.GetDataAsync);
            }

            return null;
        }

        /// <summary>
        ///     Builds the data response generator SDMX v2.0 Cross Sectional.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="sdmxSchema">The SDMX schema.</param>
        /// <returns>The <see cref="IDataResponse{ICrossSectionalWriterEngine}" />.</returns>
        private IDataResponse<ICrossSectionalWriterEngine> BuildDataResponseGeneratorCross(DataRequest input, SdmxSchema sdmxSchema)
        {
            IDataQuery dataQuery = input.DataQuery;
            if (dataQuery.Dataflow != null)
            {
                IRetrieverFactory retrieverFactory = input.RetrieverFactory;

                // SOAP v2.0 so we always have SDMX v2.0
                var dataRetrievalManager = retrieverFactory.GetDataRetrievalWithCross(sdmxSchema, this._dataflowPrincipalManager.GetCurrentPrincipal());
                return new DataResponse<ICrossSectionalWriterEngine, IDataQuery>(new[] { dataQuery }, dataRetrievalManager.GetDataAsync);
            }

            return null;
        }

        /// <summary>
        ///     Builds the data response generator SDMX v2.0 Cross Sectional.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="retrievalManager">The retrieval manager.</param>
        /// <param name="retrieverFactory">The retriever factory.</param>
        /// <param name="sdmxSchema">The SDMX schema.</param>
        /// <returns>The <see cref="IDataResponse{ICrossSectionalWriterEngine}" />.</returns>
        private IDataResponse<ICrossSectionalWriterEngine> BuildDataResponseGeneratorCross(IReadableDataLocation input, ISdmxObjectRetrievalManager retrievalManager, IRetrieverFactory retrieverFactory, SdmxSchema sdmxSchema)
        {
            var dataQueries = this._dataQueryParseManager.BuildDataQuery(input, retrievalManager);
            if (dataQueries != null && dataQueries.Count > 0)
            {
                // SOAP v2.0 so we always have SDMX v2.0
                var dataRetrievalManager = retrieverFactory.GetDataRetrievalWithCross(sdmxSchema, this._dataflowPrincipalManager.GetCurrentPrincipal());
                return new DataResponse<ICrossSectionalWriterEngine, IDataQuery>(dataQueries, dataRetrievalManager.GetDataAsync);
            }

            return null;
        }

        /// <summary>
        /// Gets the data request.
        /// </summary>
        /// <param name="dataQuery">
        /// The data query.
        /// </param>
        /// <returns>
        /// The <see cref="DataRequest"/>.
        /// </returns>
        private DataRequest GetDataRequest(IRestDataQuery dataQuery)
        {
            Func<ISdmxObjectRetrievalManager, IRetrieverFactory, DataRequest> func = (manager, factory) =>
            {
                IDataQuery query;
                SdmxStandard sdmxStandard;

                if (dataQuery is IRestDataQueryV2 dataQueryV2)
                {
                    query = new DataQueryV2Impl(dataQueryV2, manager, this._configuration.ApplyContentConstraintsOnDataQueries);
                    sdmxStandard = SdmxStandard.VersionThree;
                }
                else
                {
                    query = new DataQueryImpl(dataQuery, manager, this._configuration.ApplyContentConstraintsOnDataQueries);
                    sdmxStandard = SdmxStandard.VersionTwoPointOne;
                }

                if (query.Dataflow != null)
                {
                    return new DataRequest(query, factory, manager, sdmxStandard);
                }

                return null;
            };

            return GetDataResponseBase(func);
        }

        /// <summary>
        ///     Gets the data response.
        /// </summary>
        /// <typeparam name="TDataWriter">The type of the data writer.</typeparam>
        /// <param name="dataQueryParser">The data query parser.</param>
        /// <returns>The <see cref="IDataResponse{TDataWriter}" />.</returns>
        /// <exception cref="Org.Sdmxsource.Sdmx.Api.Exception.SdmxNoResultsException">
        ///     Could not find Dataflow and/or DSD related
        ///     with this data request
        /// </exception>
        private IDataResponse<TDataWriter> GetDataResponse<TDataWriter>(Func<ISdmxObjectRetrievalManager, IRetrieverFactory, IDataResponse<TDataWriter>> dataQueryParser) where TDataWriter : IDisposable
        {
            return GetDataResponseBase(dataQueryParser);
        }

        /// <summary>
        /// Gets the data response.
        /// </summary>
        /// <typeparam name="TReturnValue">The type of the return value.</typeparam>
        /// <param name="dataQueryParser">The data query parser.</param>
        /// <returns>
        /// The <see cref="IDataResponse{TDataWriter}" />.
        /// </returns>
        /// <exception cref="SdmxNoResultsException">Could not find Dataflow and/or DSD related with this data request</exception>
        private TReturnValue GetDataResponseBase<TReturnValue>(Func<ISdmxObjectRetrievalManager, IRetrieverFactory, TReturnValue> dataQueryParser) where TReturnValue : class
        {
            foreach (var retrieverFactory in this._retrieverFactories)
            {
                MappingStoreIoc.ServiceKey = retrieverFactory.Id;
                var sdmxObjectRetrieval = retrieverFactory.GetSdmxObjectRetrieval(this._dataflowPrincipalManager.GetCurrentPrincipal());

                if (sdmxObjectRetrieval != null)
                {
                    try
                    {
                        var dataResponse = dataQueryParser(sdmxObjectRetrieval, retrieverFactory);
                        if (dataResponse != null)
                        {
                            return dataResponse;
                        }
                    }
                    catch (SdmxNoResultsException e)
                    {
                        _log.Debug(retrieverFactory.Id + " retriever factory returned no results with exception.", e);
                    }

                    _log.Info(retrieverFactory.Id + " retriever factory returned no results");
                }
                else
                {
                    _log.Info(retrieverFactory.Id + " retriever factory didn't return a result");
                }
            }

            throw new SdmxNoResultsException("Could not find Dataflow and/or DSD related with this data request");
        }

        #endregion

        #region Structure requests

        /// <summary>
        ///     Parse structure request.
        /// </summary>
        /// <param name="sdmxSchema">The SDMX schema.</param>
        /// <param name="query">The query.</param>
        /// <returns>The <see cref="ISdmxObjects" />.</returns>
        /// <exception cref="SdmxSemmanticException">Operation not accepted</exception>
        /// <exception cref="SdmxNoResultsException">Could not find requested structures</exception>
        public ISdmxObjects ParseRequest(SdmxSchema sdmxSchema, IRestStructureQuery query)
        {
            throw new InvalidOperationException("Method ParseRequest of RetrieverManager accepting a IRestStructureQuery is not used anymore.");
        }

        /// <summary>
        ///     Parse structure request.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <returns>The <see cref="ISdmxObjects" />.</returns>
        /// <exception cref="SdmxSemmanticException">Operation not accepted</exception>
        /// <exception cref="SdmxNoResultsException">Could not find requested structures</exception>
        public ISdmxObjects ParseRequest(ICommonStructureQuery query)
        {
            if (query == null)
            {
                throw new SdmxSemmanticException(Messages.ErrorOperationNotAccepted);
            }

            using (var scope = authorizationScopeFactory.Create())
            {
                IPrincipal dataflowPrincipal = this._dataflowPrincipalManager.GetCurrentPrincipal();

                var tryRetrieverFactory = BuildRetrieverMethod((factory) => this.GetMutableObjects(factory, query, dataflowPrincipal));

                return this.TryFactories(tryRetrieverFactory);
            }
        }

        /// <summary>
        ///     Gets the structure response for SDMX SOAP.
        /// </summary>
        /// <param name="sdmxSchema">The SDMX schema.</param>
        /// <param name="input">The input.</param>
        /// <returns>The <see cref="IMutableObjects" />.</returns>
        /// <exception cref="ArgumentNullException"><paramref name="input" /> is <see langword="null" />.</exception>
        /// <exception cref="SdmxSemmanticException">Operation not accepted.</exception>
        /// <exception cref="SdmxNoResultsException">Could not find requested structures</exception>
        public ISdmxObjects ParseRequest(SdmxSchemaEnumType sdmxSchema, IReadableDataLocation input)
        {
            if (input == null)
            {
                throw new ArgumentNullException("input");
            }

            using (var scope = authorizationScopeFactory.Create())
            {
                IPrincipal dataflowPrincipal = this._dataflowPrincipalManager.GetCurrentPrincipal();

                return sdmxSchema == SdmxSchemaEnumType.VersionTwoPointOne
                    ? this.GetStructureResponseV21(input, dataflowPrincipal)
                    : this.GetStructureResponseV20(input, dataflowPrincipal);
            }
        }

        /// <summary>
        ///     Gets the <see cref="IMutableObjects" /> from SOAP V21 SOAP structure.
        /// </summary>
        /// <param name="retrieverFactory">The retriever factory.</param>
        /// <param name="query">The query.</param>
        /// <param name="allowedDataflows">The allowed dataflows.</param>
        /// <returns>The <see cref="IMutableObjects" />.</returns>
        private static IMutableObjects GetFromSoapV21Structure(IRetrieverFactory retrieverFactory, ICommonStructureQuery query, IPrincipal dataflowPrincipal)
        {
            var retrieverManager = retrieverFactory.GetCommonSdmxObjectRetrievalManager(dataflowPrincipal);
            if (retrieverManager != null)
            {
                return retrieverManager.GetMaintainables(query).MutableObjects;
            }

            return null;
        }

        /// <summary>
        ///     Gets the mutable objects.
        /// </summary>
        /// <param name="factory">The factory.</param>
        /// <param name="query">The query.</param>
        /// <param name="principal"></param>
        /// <returns>The <see cref="IMutableObjects" />.</returns>
        private IMutableObjects GetMutableObjects(IRetrieverFactory factory, ICommonStructureQuery query, IPrincipal principal)
        {
            var mutableStructureSearch = factory.GetCommonSdmxObjectRetrievalManager(principal);
            var mutableObjects = mutableStructureSearch?.GetMaintainables(query).MutableObjects;
            return this._headerHandler.HandleStructures(mutableObjects, query.MaintainableTarget.EnumType);
        }

        /// <summary>
        ///     Tries the retriever factory.
        /// </summary>
        /// <param name="retrieverFactory">The retriever factory.</param>
        /// <param name="structureRetriever">The structure retriever.</param>
        /// <returns>The <see cref="ISdmxObjects" /> if any results are returned; otherwise null.</returns>
        private ISdmxObjects TryRetrieverFactory(IRetrieverFactory retrieverFactory, Func<IRetrieverFactory, IMutableObjects> structureRetriever)
        {
            try
            {
                IMutableObjects mutable = structureRetriever(retrieverFactory);
                if (mutable != null && mutable.AllMaintainables.Count > 0)
                {
                    return mutable.ImmutableObjects;
                }
            }
            catch (SdmxNoResultsException e)
            {
                _log.Debug(retrieverFactory.Id + " retriever factory returned no results with exception.", e);
            }

            _log.Debug(retrieverFactory.Id + " retriever factory returned no results.");
            return null;
        }

        /// <summary>
        ///     Builds the structure retriever method.
        /// </summary>
        /// <param name="structureRetriever">The structure retriever.</param>
        /// <returns>The <see cref="Func{IRetrieverFactory, ISdmxObjects}" />.</returns>
        private Func<IRetrieverFactory, ISdmxObjects> BuildRetrieverMethod(Func<IRetrieverFactory, IMutableObjects> structureRetriever)
        {
            return factory => TryRetrieverFactory(factory, structureRetriever);
        }

        /// <summary>
        ///     Gets the mutable objects.
        /// </summary>
        /// <param name="retrieverFactory">The retriever factory.</param>
        /// <param name="queryWorkspace">The query workspace.</param>
        /// <returns>The <see cref="IMutableObjects" />.</returns>
        private IMutableObjects GetMutableObjects(IRetrieverFactory retrieverFactory, IQueryWorkspace queryWorkspace, IPrincipal dataflowPrincipal)
        {
            IMutableObjects mutable = null;
            var mutableStructureSearch = retrieverFactory.GetCommonSdmxObjectRetrievalManagerSoap20(dataflowPrincipal);
            if (mutableStructureSearch != null)
            {
                ISoap20CommonStructureQuery commonStructureQuery =
                    new Soap20CommonStructureQueryCore(queryWorkspace.SimpleStructureQueries, queryWorkspace.ResolveReferences, false);
                mutable = mutableStructureSearch.RetrieveStructures(commonStructureQuery, dataflowPrincipal).MutableObjects;
            }

            return mutable;
        }

        /// <summary>
        ///     Gets the structure response for SDMX V20 SOAP.
        /// </summary>
        /// <param name="input">The readable data location.</param>
        /// <param name="dataflowPrincipal">The dataflow principal.</param>
        /// <returns>The <see cref="ISdmxObjects" />.</returns>
        private ISdmxObjects GetStructureResponseV20(IReadableDataLocation input, IPrincipal dataflowPrincipal)
        {
            var queryWorkspace = this._queryParsingManagerCustomRequests.ParseQueries(input);
            if (queryWorkspace == null)
            {
                throw new SdmxSemmanticException(Messages.ErrorOperationNotAccepted);
            }

            var query = queryWorkspace.SimpleStructureQueries;
            if (query == null)
            {
                throw new SdmxSemmanticException(Messages.ErrorOperationNotAccepted);
            }

            var sdmxSchema = SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwo);
            var tryRetrieverFactory = BuildRetrieverMethod((factory) => this.GetMutableObjects(factory, queryWorkspace, dataflowPrincipal));

            return this.TryFactories(tryRetrieverFactory);
        }

        /// <summary>
        ///     Gets the structure response for SDMX SOAP V21.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="dataflowPrincipal">The dataflow principal.</param>
        /// <returns>The <see cref="ISdmxObjects" />.</returns>
        /// <exception cref="SdmxSemmanticException">Operation not accepted.</exception>
        /// <exception cref="SdmxNoResultsException">Could not find requested structures</exception>
        private ISdmxObjects GetStructureResponseV21(IReadableDataLocation input, IPrincipal dataflowPrincipal)
        {
            var queryWorkspace = this._queryParsingManager.ParseQueries(input);
            if (queryWorkspace == null)
            {
                throw new SdmxSemmanticException(Messages.ErrorOperationNotAccepted);
            }

            IComplexStructureQuery query = queryWorkspace.ComplexStructureQuery;
            if (query == null)
            {
                throw new SdmxSemmanticException(Messages.ErrorOperationNotAccepted);
            }

            ICommonStructureQuery commonStructureQuery = 
                BuildCommonStructureQuery(query, CommonStructureQueryType.SOAP, StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument));
            var tryRetrieverFactory = this.BuildRetrieverMethod((factory) => GetFromSoapV21Structure(factory, commonStructureQuery, dataflowPrincipal));

            return this.TryFactories(tryRetrieverFactory);
        }

        #endregion

        #region Availability requests

        /// <summary>
        /// Parses the request.
        /// </summary>
        /// <param name="sdmxSchema">The SDMX schema.</param>
        /// <param name="query">The query.</param>
        /// <returns>The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Objects.ISdmxObjects" />.</returns>
        public ISdmxObjects ParseRequest(SdmxSchema sdmxSchema, IRestAvailableConstraintQuery query)
        {
            if (query == null)
            {
                throw new SdmxSemmanticException(Messages.ErrorOperationNotAccepted);
            }

            IRestAvailabilityQuery queryV2 = query as IRestAvailabilityQuery;
            var principal = this._dataflowPrincipalManager.GetCurrentPrincipal();
            using (var scope = this.authorizationScopeFactory.Create())
            {
                if (scope.IsEnabled)
                {
                    if (queryV2 != null)
                    {
                        bool AuthFunc(IStructureReference flowRef) =>
                            scope.CurrentSdmxAuthorisation.CanReadData(flowRef, false);
                        this.HandleQueryV2Authorisation(queryV2, AuthFunc);
                    }
                    else
                    {
                        // TODO get isPit
                        if (!scope.CurrentSdmxAuthorisation.CanReadData(query.FlowRef, false))
                        {
                            string errorMessage = string.Format(CultureInfo.CurrentCulture, Messages.NoDataForDataflowFormat1, query.FlowRef.MaintainableId);
                            throw new SdmxUnauthorisedException(errorMessage);
                        }
                    }
                }
                else
                {
                    // TODO old authorization mechanism
                    var engine = this._dataflowAuthorizationManager.GetAuthorizationEngine(principal);
                    if (engine != null)
                    {
                        if (queryV2 != null)
                        {
                            bool AuthFunc(IStructureReference flowRef) =>
                                engine.GetAuthorization(flowRef, PermissionType.CanReadData) != AuthorizationStatus.Denied;
                            this.HandleQueryV2Authorisation(queryV2, AuthFunc);
                        }
                        else
                        {
                            var status = engine.GetAuthorization(query.FlowRef, PermissionType.CanReadData);
                            if (status == AuthorizationStatus.Denied)
                            {
                                string errorMessage = string.Format(CultureInfo.CurrentCulture, Messages.NoDataForDataflowFormat1, query.FlowRef.MaintainableId);
                                throw new SdmxUnauthorisedException(errorMessage);
                            }
                        }
                    }
                }
            }

            Func<IRetrieverFactory, ISdmxObjects> tryRetrieverFactory = (factory) => this.GetMutableObjects(factory, sdmxSchema, query, principal);
            return this.TryFactories(tryRetrieverFactory);
        }

        /// <summary>
        /// Handles the authorisation check for the availability query for SDMX REST 2.0
        /// </summary>
        /// <param name="query">The query to authorise.</param>
        /// <param name="authFunc">The authorisation method to use.</param>
        /// <exception cref="SdmxUnauthorisedException">If authorisation fails.</exception>
        private void HandleQueryV2Authorisation(IRestAvailabilityQuery query, Func<IStructureReference, bool> authFunc)
        {
            bool isValid = false;
            var flowRefIterator = query.StartFlowRefIterator();
            while (flowRefIterator.MoveNext())
            {
                HeaderScope.RequestAborted.ThrowIfCancellationRequested();

                // TODO get isPit
                if (authFunc(query.FlowRef))
                {
                    isValid = true;
                }
                else
                {
                    // wait for other structure references to end
                    string errorMessage = string.Format(CultureInfo.CurrentCulture, Messages.NoDataForDataflowFormat1, query.FlowRef.MaintainableId);
                    _log.Info(errorMessage);
                }
            }
            if (!isValid)
            {
                string errorMessage = string.Format(CultureInfo.CurrentCulture, Messages.NoDataForDataflowQueryFormat1, query.ToString());
                throw new SdmxUnauthorisedException(errorMessage);
            }
        }

        /// <summary>
        /// Gets the mutable objects.
        /// </summary>
        /// <param name="factory">The factory.</param>
        /// <param name="sdmxSchema">The SDMX schema.</param>
        /// <param name="query">The query.</param>
        /// <param name="principal">The principal.</param>
        /// <returns>The <see cref="IMutableObjects" />.</returns>
        private ISdmxObjects GetMutableObjects(IRetrieverFactory factory, SdmxSchema sdmxSchema, IRestAvailableConstraintQuery query, IPrincipal principal)
        {
            var mutableStructureSearch = factory.GetAvailableDataManager(sdmxSchema, principal);
            var retrievalManager = factory.GetSdmxObjectRetrieval(principal);
            IMutableObjects mutableObjects;

            if (query is IRestAvailabilityQuery queryV2)
            {
                mutableObjects = new MutableObjectsImpl();

                var flowRefIterator = queryV2.StartFlowRefIterator();
                bool hasMultipleReferences = flowRefIterator.Count > 1;

                while (flowRefIterator.MoveNext() && !HeaderScope.RequestAborted.IsCancellationRequested)
                {
                    try
                    {
                        var dynamiccQuery = new AvailableConstraintQueryV2(queryV2, retrievalManager);
                        var wildcardIterator = dynamiccQuery.StartFlowRefIterator();
                        hasMultipleReferences |= wildcardIterator.Count > 1;
                        while (wildcardIterator.MoveNext())
                        {
                            mutableObjects = mutableStructureSearch.Retrieve(dynamiccQuery, mutableObjects, hasMultipleReferences);
                        }
                    }
                    catch (SdmxNoResultsException)
                    {
                        // wait for other structure references to end
                        _log.Debug($"{factory.Id} returned no results for {queryV2.FlowRef}.");
                    }
                }

                if (mutableObjects.AllMaintainables.Count == 0)
                {
                    throw new SdmxNoResultsException();
                }
            }
            else
            {
                var dynamiccQuery = new AvailableConstraintQuery(query, retrievalManager);
                mutableObjects = mutableStructureSearch.Retrieve(dynamiccQuery);
            }

            return mutableObjects.ImmutableObjects;
        }

        #endregion

        /// <summary>
        ///     Tries the factories.
        /// </summary>
        /// <param name="tryRetrieverFactory">The method that will try all retriever factory.</param>
        /// <returns>The <see cref="ISdmxObjects" />.</returns>
        /// <exception cref="SdmxNoResultsException">Could not find requested structures</exception>
        private ISdmxObjects TryFactories(Func<IRetrieverFactory, ISdmxObjects> tryRetrieverFactory)
        {
            ISdmxObjects sdmxObjects = new SdmxObjectsImpl();

            foreach (var retrieverFactory in this._retrieverFactories)
            {
                HeaderScope.RequestAborted.ThrowIfCancellationRequested();

                MappingStoreIoc.ServiceKey = retrieverFactory.Id;
                ISdmxObjects immutableObjects;
                try
                {
                    immutableObjects = tryRetrieverFactory(retrieverFactory);
                }
                catch (SdmxNoResultsException)
                {
                    continue;
                }
                if (immutableObjects != null)
                {
                    if (this._structureRequestBehavior == RequestBehaviorType.First)
                    {
                        return immutableObjects;
                    }

                    if (this._structureRequestBehavior == RequestBehaviorType.Merge)
                    {
                        sdmxObjects.Merge(immutableObjects);
                    }
                }
            }

            if (sdmxObjects.GetAllMaintainables().Count == 0)
            {
                throw new SdmxNoResultsException("Could not find requested structures");
            }

            return sdmxObjects;
        }

        #region query converters
        //TODO - common structure query - IRestStructureQuery & IComplexStructureQuery should be completely substituted by ICommonStructureQuery

        private ICommonStructureQuery BuildCommonStructureQuery(IComplexStructureQuery structureQuery, CommonStructureQueryType queryType, StructureOutputFormat outputFormat)
        {
            if (structureQuery == null)
            {
                throw new ArgumentNullException(nameof(structureQuery));
            }

            var builder = CommonStructureQueryCore.Builder.NewQuery(queryType, outputFormat)
                .SetMaintainableTarget(structureQuery.StructureReference.ReferencedStructureType.MaintainableStructureType)
                .SetAnnotationReference(structureQuery.StructureReference.AnnotationReference)
                .SetVersionRequests(new VersionRequestCore(structureQuery.StructureReference.VersionReference))
                .SetRequestedDetail(structureQuery.StructureQueryMetadata.StructureQueryDetail)
                .SetItemCascade(GetItemCascadeValue(structureQuery.StructureQueryMetadata.StructureQueryDetail))
                .SetReferencedDetail(structureQuery.StructureQueryMetadata.ReferencesQueryDetail)
                .SetReferences(structureQuery.StructureQueryMetadata.StructureReferenceDetail);

            if (structureQuery.StructureQueryMetadata.ReferenceSpecificStructures != null)
            {
                builder.SetSpecificReferences(structureQuery.StructureQueryMetadata.ReferenceSpecificStructures.ToArray());
            }

            if (structureQuery.StructureReference.ChildReference != null)
            {
                builder.SetChildReferences(structureQuery.StructureReference.ChildReference.ToArray());
            }

            if (structureQuery.StructureReference.Id != null && structureQuery.StructureReference.Id.Operator.EnumType == TextSearchEnumType.Equal)
            {
                builder.SetMaintainableIds(structureQuery.StructureReference.Id.SearchParameter);
            }
            else
            {
                builder.SetComplexMaintainableId(structureQuery.StructureReference.Id);
            }

            if (structureQuery.StructureReference.AgencyId != null && structureQuery.StructureReference.AgencyId.Operator.EnumType == TextSearchEnumType.Equal)
            {
                builder.SetAgencyIds(structureQuery.StructureReference.AgencyId.SearchParameter);
            }
            else
            {
                builder.SetComplexAgencyId(structureQuery.StructureReference.AgencyId);
            }

            return builder.Build();
        }

        private static CascadeSelection GetItemCascadeValue(ComplexStructureQueryDetail structureQueryDetail)
        {
            return structureQueryDetail.EnumType == ComplexStructureQueryDetailEnumType.CascadedMatchedItems ? CascadeSelection.True : CascadeSelection.False;
        }

        #endregion
    }
}