﻿// -----------------------------------------------------------------------
// <copyright file="FaultException.cs" company="EUROSTAT">
//   Date Created : 2019-02-05
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Runtime.Serialization;
using System.Xml;
using Estat.Sri.Ws.Controllers.Model;

namespace Estat.Sri.Ws.Controllers.Exception
{
    public class FaultException<TDetail> : System.Exception
    {
        public FaultException(SdmxFault detail, XmlQualifiedName code)
        {
            this.Detail = detail;
            this.Code = code;
        }

        protected FaultException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public FaultException(string message, SdmxFault detail, XmlQualifiedName code)
            : base(message)
        {
            this.Detail = detail;
            this.Code = code;
        }

        public FaultException(string message, System.Exception innerException, SdmxFault detail, XmlQualifiedName code)
            : base(message, innerException)
        {
            this.Detail = detail;
            this.Code = code;
        }

        public SdmxFault Detail { get; }

        public XmlQualifiedName Code { get; }
    }
}