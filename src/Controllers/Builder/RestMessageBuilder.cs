// -----------------------------------------------------------------------
// <copyright file="RestMessageBuilder.cs" company="EUROSTAT">
//   Date Created : 2015-12-14
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Configuration;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using Controllers.Utils;
using Estat.Nsi.DataRetriever;
using Estat.Sri.Ws.Controllers.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;

namespace Estat.Sri.Ws.Controllers.Builder
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using Estat.Sdmxsource.Extension.Model;
    using Estat.Sri.Ws.Controllers.Controller;

    using log4net;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The REST Message builder.
    /// </summary>
    public class RestMessageBuilder 
    {
        /// <summary>
        ///     The _builder.
        /// </summary>
       // private static readonly WebFaultExceptionRestBuilder _builder;

        /// <summary>
        ///     The _log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(RestMessageBuilder));

        /// <summary>
        ///     The _content type
        /// </summary>
        private readonly string _contentType;

        /// <summary>
        ///     The _context
        /// </summary>
        private readonly IHttpContextAccessor _contextAccessor;

        /// <summary>
        /// The _response content header
        /// </summary>
        private readonly IResponseContentHeader _responseContentHeader;

        private readonly IList<string> _varyHeaders;

        /// <summary>
        ///     Initializes static members of the <see cref="RestMessageBuilder" /> class.
        /// </summary>
        static RestMessageBuilder()
        {
           // _builder = new WebFaultExceptionRestBuilder();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RestMessageBuilder" /> class.
        /// </summary>
        /// <param name="contextAccessor">The context.</param>
        /// <param name="responseContentHeader">The response content header.</param>
        /// <param name="retrieverFactory"></param>
        public RestMessageBuilder(IHttpContextAccessor contextAccessor, IResponseContentHeader responseContentHeader, IList<string> varyByHttpHeaders)
        {
            if (responseContentHeader == null)
            {
                throw new ArgumentNullException("responseContentHeader");
            }

            this._contextAccessor = contextAccessor;
            this._responseContentHeader = responseContentHeader;
            this._contentType = HeaderScope.ReturnDisseminationDbSql? "application/json" : responseContentHeader.MediaType.ToString();

            var acceptHeader = this._contextAccessor.HttpContext.Request.Headers["Accept"];
            if (acceptHeader.Any(x => x.Contains("zip=true", StringComparison.OrdinalIgnoreCase)))
            {
                this._contentType += ";zip=true";
            }

            _varyHeaders = varyByHttpHeaders;          
        }

        /// <summary>
        ///     Builds the response SOAP message.
        /// </summary>
        /// <param name="controller">The controller.</param>
        /// <param name="format"></param>
        /// <returns>The <see cref="Message" />.</returns>
        public void Build(IStreamController<Stream> controller, IMessageFormat format)
        {
            this.BuildPrivate(controller, this._contextAccessor.HttpContext.Response.Body, this._responseContentHeader, format);
        }

        /// <summary>
        ///     Builds the response SOAP message asynchronous.
        /// </summary>
        /// <param name="controller">The controller.</param>
        /// <param name="format"></param>
        /// <returns>The <see cref="Message" />.</returns>
        public async Task BuildAsync(IStreamController<Stream> controller, IMessageFormat format)
        {
            await this.BuildPrivateAsync(controller, this._contextAccessor.HttpContext.Response.Body, this._responseContentHeader, format);
        }

        /// <summary>
        ///  
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public async Task BuildToFileAsync(IStreamController<Stream> controller, IMessageFormat format, string filePath)
        {
            await this.BuildFilePrivateAsync(controller, filePath, this._responseContentHeader, format);
        }


        /// <summary>
        /// Builds the response SOAP message.
        /// </summary>
        /// <param name="controller">The controller.</param>
        /// <param name="outputStream">The output stream.</param>
        /// <param name="responseContentHeader">The response content header.</param>
        /// <param name="context">The context.</param>
        /// <param name="format"></param>
        private void BuildPrivate(IStreamController<Stream> controller, Stream outputStream, IResponseContentHeader responseContentHeader,  IMessageFormat format)
        {
            try
            {
                this._contextAccessor.HttpContext.Response.ContentType = this._contentType;
                var actions = new Queue<Action>();
                if (!string.IsNullOrWhiteSpace(responseContentHeader.Language))
                {
                    actions.Enqueue(() => this._contextAccessor.HttpContext.Response.Headers.Add("Content-Language", responseContentHeader.Language));
                }

                // TODO should those be added to the queue ?
                actions.Enqueue(() => this.SetupResponseHttpHeader(format));
                controller.StreamTo(outputStream, actions);
                outputStream.Flush();
                outputStream.Close();
            }
            catch (SdmxResponseSizeExceedsLimitException e)
            {
                // error is on payload.
                _log.Warn(e.Message, e);
            }
            catch (SdmxResponseTooLargeException e)
            {
                // error is on payload.
                _log.Warn(e.Message, e);
            }
            catch (DataRetrieverException e)
            {
                _log.Error(e.Message, e);
                throw new ServiceException((HttpStatusCode) e.SdmxErrorCode.HttpRestErrorCode, e);
            }
        }

        /// <summary>
        /// Builds the response SOAP message asynchronous.
        /// </summary>
        /// <param name="controller">The controller.</param>
        /// <param name="outputStream">The output stream.</param>
        /// <param name="responseContentHeader">The response content header.</param>
        /// <param name="context">The context.</param>
        /// <param name="format"></param>
        private async Task BuildPrivateAsync(IStreamController<Stream> controller, Stream outputStream, IResponseContentHeader responseContentHeader, IMessageFormat format)
        {
            try
            {
                using (var wrappedStream = new StreamWrapper(outputStream, this._contextAccessor.HttpContext.RequestAborted))
                {
                    this._contextAccessor.HttpContext.Response.ContentType = this._contentType;
                    var actions = new Queue<Action>();
                    if (!string.IsNullOrWhiteSpace(responseContentHeader.Language))
                    {
                        actions.Enqueue(() => this._contextAccessor.HttpContext.Response.Headers.Add("Content-Language", responseContentHeader.Language));
                    }

                    // TODO should those be added to the queue ?
                    actions.Enqueue(() => this.SetupResponseHttpHeader(format));
                    await controller.StreamToAsync(wrappedStream, actions);
                    wrappedStream.Flush();
                }
            }
            catch (SdmxResponseSizeExceedsLimitException e)
            {
                // error is on payload.
                _log.Warn(e.Message, e);
            }
            catch (SdmxResponseTooLargeException e)
            {
                // error is on payload.
                _log.Warn(e.Message, e);
            }
            catch (DataRetrieverException e)
            {
                _log.Error(e.Message, e);
                throw new ServiceException((HttpStatusCode)e.SdmxErrorCode.HttpRestErrorCode, e);
            }
        }

        /// <summary>
        /// Builds the response SOAP message asynchronous.
        /// </summary>
        /// <param name="controller">The controller.</param>
        /// <param name="outputStream">The output stream.</param>
        /// <param name="responseContentHeader">The response content header.</param>
        /// <param name="context">The context.</param>
        /// <param name="format"></param>
        private async Task BuildFilePrivateAsync(IStreamController<Stream> controller, string filePath, IResponseContentHeader responseContentHeader, IMessageFormat format)
        {
            try
            {
                using (var wrappedStream = new StreamWrapper(File.Create(filePath), this._contextAccessor.HttpContext.RequestAborted))
                {
                    this._contextAccessor.HttpContext.Response.ContentType = this._contentType;
                    var actions = new Queue<Action>();
                    if (!string.IsNullOrWhiteSpace(responseContentHeader.Language))
                    {
                        actions.Enqueue(() => this._contextAccessor.HttpContext.Response.Headers.Add("Content-Language", responseContentHeader.Language));
                    }

                    // TODO should those be added to the queue ?
                    actions.Enqueue(() => this.SetupResponseHttpHeader(format));
                    await controller.StreamToAsync(wrappedStream, actions);
                    wrappedStream.Flush();
                    await wrappedStream.DisposeAsync();
                }
            }
            catch (SdmxResponseSizeExceedsLimitException e)
            {
                // error is on payload.
                _log.Warn(e.Message, e);
            }
            catch (SdmxResponseTooLargeException e)
            {
                // error is on payload.
                _log.Warn(e.Message, e);
            }
            catch (DataRetrieverException e)
            {
                _log.Error(e.Message, e);
                throw new ServiceException((HttpStatusCode)e.SdmxErrorCode.HttpRestErrorCode, e);
            }
            catch(IOException e)
            {
                _log.Error(e.Message, e);
            }
        }

        private void SetupResponseHttpHeader(IMessageFormat format)
        {
            this._contextAccessor.HttpContext.Response.Headers.Add("Accept-Ranges", "values");
            if (_varyHeaders != null)
            {
                ISet<string> varyHeader = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
                foreach (var headerToCheck in _varyHeaders)
                {
                    var headerValues = this._contextAccessor.HttpContext.Request.Headers[headerToCheck];
                    if (!StringValues.IsNullOrEmpty(headerValues))
                    {
                        // NOTE we add the header name not its value(s)
                        varyHeader.Add(headerToCheck);
                    }
                }

                if (varyHeader.Count > 0)
                {
                    if (this._contextAccessor.HttpContext.Response.Headers.TryGetValue("Vary", out StringValues vs))
                    {
                        varyHeader.ExceptWith(vs.ToArray());
                    }

                    if (varyHeader.Count > 0)
                    {
                        this._contextAccessor.HttpContext.Response.Headers.Append("Vary", varyHeader.ToArray());
                    }
                }
            }

            var cacheHeader = this._contextAccessor.HttpContext.Request.Headers["Cache-Control"];
            if (string.IsNullOrWhiteSpace(this._contextAccessor.HttpContext.Request.Headers["Authorization"]) && cacheHeader != "no-cache" && cacheHeader != "no-store")
            {
                var maxAge = format.Name == "Data" ? ConfigurationManager.AppSettings["data.cache-control.max-age"] : ConfigurationManager.AppSettings["structure.cache-control.max-age"];
                int maxAgeValue;
                if (maxAge != null && int.TryParse(maxAge, NumberStyles.Any, CultureInfo.InvariantCulture, out maxAgeValue))
                {
                    this._contextAccessor.HttpContext.Response.Headers.Remove("cache-control");
                    this._contextAccessor.HttpContext.Response.Headers.Add("cache-control", new[] { $"public,max-age={TimeSpan.FromSeconds(maxAgeValue)}" });
                }
            }
        }
    }

    internal class StreamWrapper : Stream
    {
        private readonly Stream _stream;
        private readonly CancellationToken _cancellationToken;

        public StreamWrapper(Stream stream, CancellationToken cancellationToken)
        {
            _stream = stream;
            _cancellationToken = cancellationToken;
        }

        public override void Flush()
        {
            
            _cancellationToken.ThrowIfCancellationRequested();
            _stream.Flush();       
        }

        public override ValueTask DisposeAsync()
        {
            try
            {
                _stream.DisposeAsync();
                return ValueTask.CompletedTask;
            }
            catch (Exception exc)
            {
                return ValueTask.FromException(exc);
            }
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            _cancellationToken.ThrowIfCancellationRequested();
            return _stream.Read(buffer, offset, count);
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            _cancellationToken.ThrowIfCancellationRequested();
            return _stream.Seek(offset, origin);
        }

        public override void SetLength(long value)
        {
            _cancellationToken.ThrowIfCancellationRequested();
            _stream.SetLength(value);
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            _cancellationToken.ThrowIfCancellationRequested();
            _stream.Write(buffer, offset, count);
        }

        public override bool CanRead => this._stream.CanRead;

        public override bool CanSeek => this._stream.CanSeek;

        public override bool CanWrite => this._stream.CanWrite;

        public override long Length => this._stream.Length;

        public override long Position
        {
            get => this._stream.Position;
            set => this._stream.Position = value;
        }
    }
}