﻿// -----------------------------------------------------------------------
// <copyright file="StubSdmxObjectBuilder.cs" company="EUROSTAT">
//   Date Created : 2017-03-17
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Controllers.Builder
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using DryIoc;

    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;

    /// <summary>
    /// Builds a stub SDMX Maintainable artefact from a Reference. 
    /// </summary>
    public class StubSdmxObjectBuilder : IBuilder<IMaintainableObject, IStructureReference>, IDisposable
    {
        /// <summary>
        /// The immutable to mutable interface dictionary
        /// </summary>
        private readonly Dictionary<Type, Type> _typeDictionary;

        /// <summary>
        /// The container
        /// </summary>
        private readonly Container _container;

        /// <summary>
        /// Initializes a new instance of the <see cref="StubSdmxObjectBuilder"/> class.
        /// </summary>
        public StubSdmxObjectBuilder()
        {
            var types = typeof(MaintainableMutableCore<>).Assembly.GetTypes();
            var sdmxObjectsAssembly = types.Where(type => type.IsPublic
                && !type.IsAbstract
                && !type.IsInterface
                && !typeof(ICrossSectionalDataStructureMutableObject).IsAssignableFrom(type)
                && typeof(IMaintainableMutableObject).IsAssignableFrom(type));
          
            this._container = new Container(rules => rules.With(FactoryMethod.ConstructorWithResolvableArguments));
            this._container.RegisterMany(sdmxObjectsAssembly, reuse: Reuse.Transient, ifAlreadyRegistered: IfAlreadyRegistered.Keep);

            this._typeDictionary =
                SdmxStructureType.Values.Where(type => type.IsMaintainable)
                    .Select(type => type.MaintainableInterface)
                    .Where(type => type.GetProperty(nameof(IMaintainableObject.MutableInstance)) != null)
                    .Distinct()
                    .ToDictionary(
                        type => type,
                        type => type.GetProperty(nameof(IMaintainableObject.MutableInstance)).PropertyType);
        }

        /// <summary>
        /// Builds an object of type <see cref="IMaintainableObject"/>
        /// </summary>
        /// <param name="buildFrom">An Object to build the output object from
        ///             </param>
        /// <returns>
        /// Object of type <see cref="IMaintainableObject"/>
        /// </returns>
        /// <exception cref="T:Org.Sdmxsource.Sdmx.Api.Exception.SdmxException">- If anything goes wrong during the build process
        /// </exception>
        public IMaintainableObject Build(IStructureReference buildFrom)
        {
            if (buildFrom == null)
            {
                return null;
            }

            Type mutableConcreteType;
            if (this._typeDictionary.TryGetValue(buildFrom.MaintainableStructureEnumType.MaintainableInterface, out mutableConcreteType))
            {
                var mutable = (IMaintainableMutableObject)this._container.Resolve(mutableConcreteType);
                mutable.Id = buildFrom.MaintainableId;
                mutable.AgencyId = buildFrom.AgencyId;
                mutable.Version = buildFrom.Version;
                mutable.Stub = true;
                mutable.StructureURL = new Uri("http://change.me");
                mutable.AddName("en", "Internal stub object");
                return mutable.ImmutableInstance;
            }

            return null;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="managed"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool managed)
        {
            if (managed)
            {
                if (_container != null)
                {
                    _container.Dispose();
                }
            }
        }
    }
}