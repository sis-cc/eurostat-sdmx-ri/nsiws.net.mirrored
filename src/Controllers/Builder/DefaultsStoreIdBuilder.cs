// -----------------------------------------------------------------------
// <copyright file="DefaultsStoreIdBuilder.cs" company="EUROSTAT">
//   Date Created : 2020-09-20
//   Copyright (c) 2009, 2020 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Security.Principal;
using Estat.Sdmxsource.Extension.Builder;
using Estat.Sdmxsource.Extension.Manager;
using estat.sri.ws.auth.api;
using Estat.Sri.Ws.Configuration;
using Estat.Sri.Ws.Controllers.Manager;
using log4net;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;

namespace Estat.Sri.Ws.Controllers.Builder
{
    public class DefaultsStoreIdBuilder : IStoreIdBuilder
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(DefaultsStoreIdBuilder));
        /// <summary>
        /// The dataflow principal manager
        /// </summary>
        private readonly IDataflowPrincipalManager _dataflowPrincipalManager;

        private readonly SettingsFromConfigurationManager _configuration;
        private readonly IHttpContextAccessor _contextAccessor;

        public DefaultsStoreIdBuilder(IDataflowPrincipalManager dataflowPrincipalManager, SettingsFromConfigurationManager configuration, IHttpContextAccessor contextAccessor)
        {
            this._dataflowPrincipalManager = dataflowPrincipalManager;
            this._configuration = configuration;
            this._contextAccessor = contextAccessor;
        }

        public string Build()
        {
            return this.Build(null);
        }

        public string Build(IPrincipal principal)
        {
            if (principal == null)
            {
                principal = this._dataflowPrincipalManager.GetCurrentPrincipal();
            }

            var config = this._configuration.MappingStoreId;

            if (config.FromQueryParameter == QueryParameterOption.Never)
            {
                return config.Default;
            }

            string sid = this.GetStoreIdFromQueryParameter();
            if (string.IsNullOrWhiteSpace(sid))
            {
                return config.Default;
            }
            
            if (config.FromQueryParameter == QueryParameterOption.Always)
            {
                return sid;
            }

            if (principal is SriPrincipalWithRules)
            {
                return sid;
            }

            return config.Default;
        }

        private string GetStoreIdFromQueryParameter()
        {  
            StringValues sid = this._contextAccessor.HttpContext.Request.Query["sid"];
            _log.DebugFormat("Sid parameter is equal to '{0}'", sid);
            if (!StringValues.IsNullOrEmpty(sid))
            {
                return sid;
            }

            return null;
        }
    }
}