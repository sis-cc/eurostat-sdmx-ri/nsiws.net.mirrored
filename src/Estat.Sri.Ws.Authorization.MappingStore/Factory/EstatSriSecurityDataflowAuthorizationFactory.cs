﻿// -----------------------------------------------------------------------
// <copyright file="EstatSriSecurityDataflowAuthorizationFactory.cs" company="EUROSTAT">
//   Date Created : 2017-06-07
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// ----------------------------------------------------------------------
namespace Estat.Sri.DataflowAuthorization.Factory
{
    using estat.sri.ws.auth.api;
    using Estat.Sdmxsource.Extension.Constant;
    using Estat.Sdmxsource.Extension.Engine;
    using Estat.Sdmxsource.Extension.Factory;
    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.Api.Utils;
    using Estat.Sri.MappingStore.Store.Engine;
    using Estat.Sri.MappingStoreRetrieval.Engine;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.Security.Model;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using System;
    using System.Collections.Generic;
    using System.Security.Principal;

    public class EstatSriSecurityDataflowAuthorizationFactory : IDataflowAuthorizationFactory
    {
        /// <summary>
        /// The configuration store manager
        /// </summary>
        private readonly IConnectionStringBuilderManager _configurationStoreManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="EstatSriSecurityDataflowAuthorizationFactory"/> class.
        /// </summary>
        /// <param name="configurationStoreManager">The configuration store manager.</param>
        /// <exception cref="ArgumentNullException">configurationStoreManager</exception>
        public EstatSriSecurityDataflowAuthorizationFactory(IConnectionStringBuilderManager configurationStoreManager)
        {
            if (configurationStoreManager == null)
            {
                throw new ArgumentNullException(nameof(configurationStoreManager));
            }

            _configurationStoreManager = configurationStoreManager;
        }

        /// <summary>
        /// Gets the engine.
        /// </summary>
        /// <param name="principal">The principal.</param>
        /// <returns>The <see cref="T:Estat.Sdmxsource.Extension.Engine.IDataflowAuthorizationEngine" />.</returns>
        public IDataflowAuthorizationEngine GetEngine(IPrincipal principal)
        {
            if ( !(principal is SriPrincipal))
            {
                return null;
            }

            Database database = null;
            var connectionStringSetting = this._configurationStoreManager.Build(principal);
            if (connectionStringSetting != null)
            {
                database = new Database(connectionStringSetting);
            }

            if (database == null)
            {
                throw new SdmxServiceUnavailableException("Authorization store cannot be accessed");
            }

            var retrievalEngineContainer = new RetrievalEngineContainer(database);
            var maintainableRefEngine = new MaintainableRefRetrieverEngine(database, new MappingStoreRetrievalManager(retrievalEngineContainer));

            Func<SdmxStructureType, string, IDictionary<long, string>> urnRetriever;
            if ((principal is SriPrincipal mauser && mauser.IsInRole(nameof(PermissionType.AdminRole)))||
               (principal is SriPrincipalWithRules sriPrincipalWithRules && sriPrincipalWithRules.IsInRole(nameof(PermissionType.AdminRole))))
            {
                // admin can access anything
                urnRetriever = (type, l) => maintainableRefEngine.RetrievesUrnMap(type);
            }
            else
            {
                urnRetriever = maintainableRefEngine.RetrievesUrnMapForUser;
            }

            return new SriAuthorizationEngine(principal, urnRetriever,retrievalEngineContainer.CategorisationRetrievalEngine );
        }

        private Func<SdmxStructureType, string, IDictionary<long, string>> DisableAuthorization(Func<SdmxStructureType, string, IDictionary<long, string>> urnRetriever)
        {
            return (type, l) =>
            {
                ISdmxAuthorization sdmxAuthorization = SdmxAuthorizationScope.Current;
                if (SdmxAuthorizationScope.Enabled)
                {
                    SdmxAuthorizationScope.ClearCurrent();
                }
                try
                {
                    return urnRetriever(type ,l);
                }
                finally
                {
                    SdmxAuthorizationScope.Current = sdmxAuthorization;
                }

            };
        }
    }
}
