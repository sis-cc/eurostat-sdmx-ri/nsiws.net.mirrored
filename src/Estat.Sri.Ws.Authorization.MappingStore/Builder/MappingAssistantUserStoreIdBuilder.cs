﻿// -----------------------------------------------------------------------
// <copyright file="MappingAssistantUserStoreIdBuilder.cs" company="EUROSTAT">
//   Date Created : 2018-6-6
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
//
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Ws.Authorization.MappingStore.Builder
{
    using Estat.Sdmxsource.Extension.Builder;
    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using System;
    using System.Security.Principal;

    /// <summary>
    /// This implementation expects a <see cref="MappingAssistantUser"/>
    /// </summary>
    /// <seealso cref="Estat.Sdmxsource.Extension.Builder.IStoreIdBuilder" />
    public class MappingAssistantUserStoreIdBuilder : IStoreIdBuilder
    {
        /// <summary>
        /// The dataflow principal manager
        /// </summary>
        private readonly IDataflowPrincipalManager _dataflowPrincipalManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="MappingAssistantUserStoreIdBuilder"/> class.
        /// </summary>
        /// <param name="dataflowPrincipalManager">The dataflow principal manager.</param>
        /// <exception cref="ArgumentNullException">dataflowPrincipalManager</exception>
        public MappingAssistantUserStoreIdBuilder(IDataflowPrincipalManager dataflowPrincipalManager)
        {
            if (dataflowPrincipalManager == null)
            {
                throw new ArgumentNullException(nameof(dataflowPrincipalManager));
            }

            _dataflowPrincipalManager = dataflowPrincipalManager;
        }

        /// <summary>
        /// Builds the store id using the default principal or implementation specific method.
        /// </summary>
        /// <returns>The store ID or null</returns>
        public string Build()
        {
            return Build(null);
        }

        /// <summary>
        /// Builds the store id using the specified principal.
        /// </summary>
        /// <param name="principal">The principal.</param>
        /// <returns>The store ID or null</returns>
        public string Build(IPrincipal principal)
        {
            if (principal == null)
            {
                principal = _dataflowPrincipalManager.GetCurrentPrincipal();
            }

            var sriPrincipal = principal as MappingAssistantUser;
            if (sriPrincipal != null)
            {
                return sriPrincipal.StoreId;
            }

            return null;
        }
    }
}