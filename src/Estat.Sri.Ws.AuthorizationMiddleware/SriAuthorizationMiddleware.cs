﻿using estat.sri.ws.auth.api;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using System.Threading;
using System.Security.Principal;
using Estat.Sri.Ws.Configuration;

namespace Estat.Sri.Ws.AuthorizationMiddleware
{
    public class SriAuthorizationMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IUserAuthorizationRulesRetriever _userAuthorizationRulesRetriever;
        private readonly AuthorizationConfiguration _configuration;

        public SriAuthorizationMiddleware(RequestDelegate next, IUserAuthorizationRulesRetriever userAuthorizationRulesRetriever, SettingsFromConfigurationManager configurationManager)
        {
            _next = next;
            this._userAuthorizationRulesRetriever = userAuthorizationRulesRetriever;
            this._configuration = configurationManager.Authorization;
        }

        public async Task Invoke(HttpContext context)
        {
            if (this._configuration != null && this._configuration.Enabled)
            {
                IPrincipal principal;
                if ("context".Equals(this._configuration.PrincipalFrom, System.StringComparison.OrdinalIgnoreCase))
                {
                    // OpenId & standard ASP.NET Core
                    principal = context.User;
                }
                else
                {
                    // NSI auth module
                    principal = Thread.CurrentPrincipal;
                }

                SriPrincipalWithRules sriPrincipalWithRules = this._userAuthorizationRulesRetriever.GetUserWithRules(principal);

                // in NSIWS we are still using ThreadPrincipal
                Thread.CurrentPrincipal = sriPrincipalWithRules;
                // context.User = sriPrincipalWithRules;
            }

            await this._next.Invoke(context);
        }
    }
}
