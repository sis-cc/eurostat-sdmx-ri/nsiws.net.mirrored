﻿// -----------------------------------------------------------------------
// <copyright file="NSIRegistryV21Service.cs" company="EUROSTAT">
//   Date Created : 2016-08-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using Estat.Sdmxsource.Extension.Manager;
using Estat.Sri.Ws.Controllers.Engine;
using Microsoft.AspNetCore.Http;
using Org.Sdmxsource.Sdmx.Api.Util;
using Org.Sdmxsource.Util.Extensions;

namespace Estat.Sri.Ws.Soap
{
    using System;
    using System.Xml;
    using Estat.Sri.Ws.Controllers.Builder;
    using Estat.Sri.Ws.Controllers.Constants;
    using Estat.Sri.Ws.Controllers.Controller;
    using Estat.Sri.Ws.Controllers.Extension;
    using Estat.Sri.Ws.Controllers.Manager;
    using Estat.Sri.Ws.SubmitStructure;

    using log4net;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.Registry.Response;
    using Org.Sdmxsource.Sdmx.Util.Sdmx;
    using Org.Sdmxsource.Util.Io;
    using Estat.Sdmxsource.Extension.Model.Error;
    using System.Threading.Tasks;

    /// <summary>
    /// The NSI SDMX v2.1 Registry service.
    /// </summary>
    /// <seealso cref="Estat.Sri.Ws.Soap.INSIRegistryV21Service" />
    public class NSIRegistryV21Service : INSIRegistryV21Service
    {
        /// <summary>
        /// The schema version.
        /// </summary>
        private const SdmxSchemaEnumType SchemaVersion = SdmxSchemaEnumType.VersionTwoPointOne;

        /// <summary>
        /// The log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(NSIRegistryV21Service));

        /// <summary>
        ///     The _fault builder
        /// </summary>
        private readonly MessageFaultSoapv21Builder _messageFaultBuilder = new MessageFaultSoapv21Builder();

        /// <summary>
        /// The decorated standard SDMX V21 service
        /// </summary>
        private readonly NsiV21ServiceBase _decoratedStdV21Service;

        /// <summary>
        /// The registry interface submit structure response builder
        /// </summary>
        private readonly SubmitStructureResponseBuilder _registryInterfaceSubmitStructureResponseBuilder = new SubmitStructureResponseBuilder();

        /// <summary>
        /// The submit structure controller
        /// </summary>
        private readonly SubmitStructureController _submitStructureController;

        private readonly IStoreIdBuilderManager _storeIdBuilderManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="NSIRegistryV21Service" /> class.
        /// </summary>
        /// <param name="submitController">The structure persistence manager.</param>
        /// <param name="dataRequestController">The data request controller.</param>
        /// <param name="structureRequestController">The structure request controller.</param>
        /// <exception cref="ArgumentNullException">messageBuilderManager is null</exception>
        public NSIRegistryV21Service(SubmitStructureController submitController, IDataRequestController dataRequestController, IStructureRequestController structureRequestController, IStoreIdBuilderManager storeIdBuilderManager)
        {
            this._decoratedStdV21Service = new NsiV21ServiceBase(dataRequestController, structureRequestController, SoapNamespaces.SdmxV21RR);
            this._submitStructureController = submitController;
            this._storeIdBuilderManager = storeIdBuilderManager;
        }


        private ResponseWithSdmxObjects ProcessRegistryInterfaceRequest(IReadableDataLocation dataLocation, SubmitStructureController controller)
        {
            ResponseWithSdmxObjects responseWithSdmxObjects;
            var registryType = SdmxMessageUtil.GetRegistryMessageType(dataLocation);
            if (registryType != RegistryMessageEnumType.SubmitStructureRequest)
            {
                throw this._messageFaultBuilder.BuildException(new SdmxNotImplementedException("Method not implemented"), registryType.ToString());
            }

            var dataSetAction = SdmxMessageUtil.GetDataSetAction(dataLocation);
            SubmitStructureConstant.ActionType istatAction;
            if (!Enum.TryParse(dataSetAction.ToString(), true, out istatAction))
            {
                istatAction = SubmitStructureConstant.ActionType.Replace;
            }

            responseWithSdmxObjects = controller.Submit(dataLocation, istatAction, this.GetStoreId());
            return responseWithSdmxObjects;
        }

        /// <summary>
        /// Gets the store identifier.
        /// </summary>
        /// <param name="ctx">The CTX.</param>
        /// <returns>The store identifier from the URL, if found;otherwise null</returns>
        private string GetStoreId()
        {
            return this._storeIdBuilderManager.Build();
        }

        public async Task HandleRequest(IReadableDataLocation request, SoapOperation soapOperation, HttpContext context)
        {
            if (soapOperation.IsOneOf(SoapOperation.RegistryInterfaceRequest, SoapOperation.SubmitStructure))
            {
                if (!SettingsManager.EnableSubmitStructure || this._submitStructureController == null)
                {
                    return;
                }

                ResponseWithSdmxObjects responseWithSdmxObjects;
                bool registryInterfaceIsRootElement;
                if (soapOperation == SoapOperation.SubmitStructure)
                {
                    responseWithSdmxObjects = this.SubmitStructureRequestWorkaround(request);
                    registryInterfaceIsRootElement = false;
                }
                else
                {
                    responseWithSdmxObjects = this.ProcessRegistryInterfaceRequest(request, this._submitStructureController);
                    registryInterfaceIsRootElement = true;
                }

                var streamController = new StreamController<XmlWriter>(async (writer, queue) =>
                {
                    var response = soapOperation.GetResponse();
                    XmlQualifiedName operationQualifiedName = new XmlQualifiedName(response.ToString(), SoapNamespaces.SdmxV21RR);
                    SoapWriter soapWriter = new SoapWriter(writer);
                    var format = new SubmitStructureResponse21Format(writer, responseWithSdmxObjects.ResponseHeader, registryInterfaceIsRootElement);
                    queue.RunAll(); // This is required to write the soap envelope TODO check if it is still true
                    soapWriter.WriteStartEnvelope();
                    soapWriter.WriteBodyContents(operationQualifiedName, null);
                    this._submitStructureController.Write(format, responseWithSdmxObjects.Responses); // Write the response
                    soapWriter.CloseDocument();
                });

                await XmlHelper.WriteToOutput(context, streamController);
            }
            else
            {
                await this._decoratedStdV21Service.HandleRequest(request, soapOperation, context);
            }
        }

        private ResponseWithSdmxObjects SubmitStructureRequestWorkaround(IReadableDataLocation request)
        {
// TODO workaround till proper support is added in SdmxSource
            // Il documento template che verrà caricato con gli artefatti da importare
            var xDomTemp = new XmlDocument();

            // Il documento sorgente passato in input
            var xDomSource = SubmitStructureUtil.MessageToXDom(request);

            // Creo gli elementi del file template
            xDomTemp.InnerXml = SubmitStructureConstant.xmlTemplate;

            // Valido il documento e ricavo l'action
            var actionValue = SubmitStructureUtil.ValidateDocument(xDomSource);
            var action = SubmitStructureConstant.ActionType.Replace;
            if (!string.IsNullOrWhiteSpace(actionValue))
            {
                action = (SubmitStructureConstant.ActionType)Enum.Parse(typeof(SubmitStructureConstant.ActionType), actionValue);
            }

            // Imposto l'Header
            SubmitStructureUtil.SetHeader(xDomTemp, xDomSource);

            // Il nodo root "Structure" del template
            var xTempStructNode = xDomTemp.SelectSingleNode("//*[local-name()='Structure']");

            // Creo il nodo "Structures" che conterrà gli artefatti
            var xSourceStructNode = xDomTemp.CreateNode(XmlNodeType.Element, "Structures", "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message");

            // Inserisco nel nodo "Structures" gli artefatti presenti nell' sdmx passato in input
            xSourceStructNode.InnerXml = xDomSource.SelectSingleNode("//*[local-name()='Structures']").InnerXml;

            // Aggiungo al template l'elemento "Structures" con gli artefatti da caricare
            xTempStructNode.AppendChild(xSourceStructNode);

            // Converto il documento in un MemoryReadableLocation
            ResponseWithSdmxObjects responseWithSdmxObjects;
            using (var mRL = new MemoryReadableLocation(SubmitStructureUtil.ConvertToBytes(xDomTemp)))
            {
                responseWithSdmxObjects = this._submitStructureController.Submit(mRL, action, this.GetStoreId());
            }

            return responseWithSdmxObjects;
        }
    }
}