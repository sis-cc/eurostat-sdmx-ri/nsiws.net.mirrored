﻿// -----------------------------------------------------------------------
// <copyright file="NSIStdV21Service.cs" company="EUROSTAT">
//   Date Created : 2013-10-22
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Xml;
using Microsoft.AspNetCore.Http;
using Org.Sdmxsource.Sdmx.Api.Util;

namespace Estat.Sri.Ws.Soap
{
    using System;
    using System.Threading.Tasks;
    using Estat.Sri.Ws.Controllers.Constants;
    using Estat.Sri.Ws.Controllers.Controller;
    using Estat.Sri.Ws.Controllers.Manager;

    /// <summary>
    ///     The SDMXv2.1 SOAP service.
    /// </summary>
    public class NSIStdV21Service : INSIStdV21Service
    {
        /// <summary>
        /// The NSI V21 service base
        /// </summary>
        private readonly NsiV21ServiceBase _nsiV21ServiceBase;

        /// <summary>
        /// Initializes a new instance of the <see cref="NSIStdV21Service" /> class.
        /// </summary>
        /// <param name="dataRequestController">The data request controller.</param>
        /// <param name="requestController">The request controller.</param>
        /// <exception cref="ArgumentNullException"><paramref name="dataRequestController"/> is <see langword="null" />.</exception>
        public NSIStdV21Service(IDataRequestController dataRequestController, IStructureRequestController requestController)
        {
            if (dataRequestController == null)
            {
                throw new ArgumentNullException("dataRequestController");
            }

            if (requestController == null)
            {
                throw new ArgumentNullException("requestController");
            }

            //if (messageBuilderManager == null)
            //{
            //    throw new ArgumentNullException("messageBuilderManager");
            //}

            this._nsiV21ServiceBase = new NsiV21ServiceBase(dataRequestController, requestController, SoapNamespaces.SdmxV21);
        }


        public async Task HandleRequest(IReadableDataLocation request, SoapOperation soapOperation, HttpContext context)
        {
            await this._nsiV21ServiceBase.HandleRequest(request, soapOperation, context);
        }
    }
}