// -----------------------------------------------------------------------
// <copyright file="NSIRegistryV21ServiceErrorHandler.cs" company="EUROSTAT">
//   Date Created : 2017-06-09
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Xml;
using Estat.Sri.Ws.Controllers.Constants;
using Microsoft.AspNetCore.Http;
using Org.Sdmxsource.Sdmx.Api.Util;

namespace Estat.Sri.Ws.Soap
{
    using System;
    using System.Threading.Tasks;
    using Estat.Sri.Ws.Controllers.Builder;
    using Estat.Sri.Ws.Controllers.Factory;

    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// The SDMX v2.1 SOAP web service error handler.
    /// </summary>
    public class NSIRegistryV21ServiceErrorHandler : INSIRegistryV21Service
    {
        /// <summary>
        ///     The _log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(NSIRegistryV21ServiceErrorHandler));

        /// <summary>
        /// The _decorated service.
        /// </summary>
        private readonly INSIRegistryV21Service _decoratedService;

        /// <summary>
        ///     The _fault builder
        /// </summary>
        private readonly IMessageFaultSoapBuilder _messageFaultBuilder;

        /// <summary>
        /// Initializes a new instance of the <see cref="NSIRegistryV21ServiceErrorHandler"/> class.
        /// </summary>
        /// <param name="decoratedService">
        /// The decorated service.
        /// </param>
        /// <param name="faultSoapBuilderFactory">
        /// The fault SOAP builder factory.
        /// </param>
        public NSIRegistryV21ServiceErrorHandler(INSIRegistryV21Service decoratedService, IMessageFaultSoapBuilderFactory faultSoapBuilderFactory)
        {
            if (decoratedService == null)
            {
                throw new ArgumentNullException(nameof(decoratedService));
            }

            if (faultSoapBuilderFactory == null)
            {
                throw new ArgumentNullException(nameof(faultSoapBuilderFactory));
            }

            this._decoratedService = decoratedService;
            this._messageFaultBuilder = faultSoapBuilderFactory.GetMessageFaultSoapBuilder(SdmxSchemaEnumType.VersionTwoPointOne);
        }

        public async Task HandleRequest(IReadableDataLocation request, SoapOperation soapOperation, HttpContext context)
        {
            try
            {
                await this._decoratedService.HandleRequest(request, soapOperation, context);
            }
            catch (Exception e)
            {
                _log.Error(e);
                throw this._messageFaultBuilder.BuildException(e, soapOperation.ToString());
            }
        }
    }
}