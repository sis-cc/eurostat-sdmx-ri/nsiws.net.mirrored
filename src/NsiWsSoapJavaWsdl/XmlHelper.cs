﻿// -----------------------------------------------------------------------
// <copyright file="XmlHelper.cs" company="EUROSTAT">
//   Date Created : 2019-02-04
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Estat.Sri.Ws.Controllers.Controller;
using Microsoft.AspNetCore.Http;

namespace Estat.Sri.Ws.Soap
{
    public static class XmlHelper
    {
        public static XmlWriterSettings GetXmlWriterSettings()
        {
            return new XmlWriterSettings() { Encoding = new UTF8Encoding(false), NamespaceHandling = NamespaceHandling.OmitDuplicates, CloseOutput = false, WriteEndDocumentOnClose = false};
        }

        public static XmlWriter CreateSoapResponseWriter(Stream responseStream)
        {
            return XmlWriter.Create(responseStream, GetXmlWriterSettings());
        }

        public static async Task WriteToOutput(HttpContext context, IStreamController<XmlWriter> streamController)
        {
            // Warning the following should not put under using because they mess up with error reporting
            // If they are closed in case of an exception then status code is locked to 200
            var responseStream = context.Response.Body;
            var writer = XmlHelper.CreateSoapResponseWriter(responseStream);
            await streamController.StreamToAsync(writer, new Queue<Action>());

            writer.Flush();
            // It must not close if there is an exception
            writer.Close();
            responseStream.Flush();
            responseStream.Close();
        }
    }
}