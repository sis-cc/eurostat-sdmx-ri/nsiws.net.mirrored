// -----------------------------------------------------------------------
// <copyright file="MappingStoreConfig.cs" company="EUROSTAT">
//   Date Created : 2020-09-24
//   Copyright (c) 2009, 2020 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Configuration
{
    using System;

    public class DisseminationDbConfig
    {
        public string DbType { get; }
        public string ConnectionString { get; }

        public DisseminationDbConfig(string dbType, string connectionString)
        {
            if (string.IsNullOrEmpty(dbType))
            {
                throw new ArgumentException("Value cannot be null or empty.", nameof(dbType));
            }

            if (string.IsNullOrEmpty(dbType))
            {
                throw new ArgumentException("Value cannot be null or empty.", nameof(dbType));
            }

            if (string.IsNullOrEmpty(connectionString))
            {
                throw new ArgumentException("Value cannot be null or empty.", nameof(connectionString));
            }

            this.DbType = dbType;
            this.ConnectionString = connectionString;
        }
    }
}