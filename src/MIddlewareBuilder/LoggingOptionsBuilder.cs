﻿using Estat.Sdmxsource.Extension.Builder;
using Estat.Sri.Ws.LoggingOptions;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace MiddlewareBuilder
{
    public class LoggingOptionsBuilder : IMiddlewareBuilder
    {
        public void Configure(IApplicationBuilder app)
        {
            app.UseMiddleware<LoggingOptionsMiddleware>();
        }

        public void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
        }
    }
}
