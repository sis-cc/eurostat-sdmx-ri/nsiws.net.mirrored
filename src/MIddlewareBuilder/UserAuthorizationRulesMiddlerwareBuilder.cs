﻿// -----------------------------------------------------------------------
// <copyright file="UserAuthorizationRulesMiddlerwareBuilder.cs" company="EUROSTAT">
//   Date Created : 2020-7-17
//   Copyright (c) 2009, 2020 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using DryIoc;
using estat.sri.ws.auth.api;
using Estat.Sdmxsource.Extension.Builder;
using Estat.Sri.Ws.AuthorizationMiddleware;
using log4net;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using Estat.Sri.Ws.Configuration;

namespace MIddlewareBuilder
{
    public class UserAuthorizationRulesMiddlerwareBuilder : IMiddlewareBuilder
    {
        private AuthorizationConfiguration _authorizationConfiguration;
        
        public void Configure(IApplicationBuilder app)
        {
            if (_authorizationConfiguration != null && _authorizationConfiguration.Enabled)
            {
                app.UseMiddleware<SriAuthorizationMiddleware>();
            }
        }

        public void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            _authorizationConfiguration = AuthorizationConfigurationUtils.GetConfiguration(configuration);
        }
    }
}
