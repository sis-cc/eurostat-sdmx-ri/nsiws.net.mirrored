using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;
using Estat.Sdmxsource.Extension.Builder;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;

namespace MiddlewareBuilder
{
    public class AuthConfiguration
    {
        public bool Enabled { get; set; }
        public bool AllowAnonymous { get; set; }
        public string Authority { get; set; }
        public string ClientId { get; set; }
        public bool RequireHttps { get; set; }
        public bool ValidateIssuer { get; set; }
        public bool ShowPii { get; set; }
        public string Audience { get; set; }
    }

    public class OpenIdMiddlewareBuilder : IMiddlewareBuilder
    {
        private AuthConfiguration _config;

        public void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            this._config = configuration.GetSection("auth").Get<AuthConfiguration>();

            if (this._config == null || !this._config.Enabled)
                return;

            // to show more descriptive error message when there is a problem with a token 
            IdentityModelEventSource.ShowPII = this._config.ShowPii;

            // By default, Microsoft has some legacy claim mapping that converts standard JWT claims into proprietary ones. This removes those mappings.
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            var validAudiences = new List<string>(2)
            {
                _config.ClientId
            };

            if (!string.IsNullOrEmpty(_config.Audience))
            {
                validAudiences.Add(_config.Audience);
            }

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

            }).AddJwtBearer(options =>
            {
                options.Authority = this._config.Authority;
                options.RequireHttpsMetadata = this._config.RequireHttps;
                options.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuer = this._config.ValidateIssuer,
                    ValidAudiences = validAudiences
                };
            });

            if (this._config.AllowAnonymous)
            {
                var policy = new AuthorizationPolicyBuilder()
                    .AddRequirements(new AllowAnonymousWithoutToken())
                    .Build();

                services.Configure<MvcOptions>(options => options.Filters.Add(new AuthorizeFilter(policy)));
                services.AddSingleton<IAuthorizationHandler, AllowAnonymousWithoutTokenHandler>();
            }
            else
            {
                services.Configure<MvcOptions>(options => options.Filters.Add(new AuthorizeFilter()));
            }
        }

        public void Configure(IApplicationBuilder app)
        {
            if (this._config == null || !this._config.Enabled)
                return;

            app.UseAuthentication();
        }
    }

    internal class AllowAnonymousWithoutToken : IAuthorizationRequirement
    {}

    internal class AllowAnonymousWithoutTokenHandler : AuthorizationHandler<AllowAnonymousWithoutToken>
    {
        readonly IHttpContextAccessor _http = null;

        public AllowAnonymousWithoutTokenHandler(IHttpContextAccessor http)
        {
            this._http = http;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, AllowAnonymousWithoutToken requirement)
        {
            if(string.IsNullOrEmpty(this._http.HttpContext.Request.Headers["Authorization"]))
                context.Succeed(requirement);

            if (context.User.Identity.IsAuthenticated)
                context.Succeed(requirement);

            return Task.CompletedTask;
        }
    }
}
