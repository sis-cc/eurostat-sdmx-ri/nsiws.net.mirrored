﻿// -----------------------------------------------------------------------
// <copyright file="SettingsHeaderRetriever.cs" company="EUROSTAT">
//   Date Created : 2017-05-12
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
// ReSharper disable once CheckNamespace
namespace Estat.Sri.Ws.Header.Default
{
    using System;
    using System.Collections.Generic;
    using System.Xml;

    using Estat.Nsi.DataRetriever.Builders;
    using Estat.Sri.SdmxXmlConstants;
    using Estat.Sri.Ws.Configuration;
    using Estat.Sri.Ws.Header.Default.Constants;

    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Util.Objects;

    /// <summary>
    /// The Old Header Settings compatibility implementation
    /// </summary>
    /// <example>
    /// <code>
    /// <![CDATA[
    ///  <applicationSettings>
    /// <!-- Old Default header settings-->
    /// <!--  Specify 'SettingsHeaderRetriever' to enable -->
    /// <Estat.Sri.Ws.Controllers.Constants.HeaderSettings>
    /// <setting name = "test" serializeAs="String">
    /// <value>true</value>
    /// </setting>
    /// <setting name = "truncated" serializeAs="String">
    /// <value>false</value>
    /// </setting>
    /// <setting name = "name" serializeAs="String">
    /// <value>Trans46302</value>
    /// </setting>
    /// <setting name = "sendercontactdepartment" serializeAs="String">
    /// <value>Statistics</value>
    /// </setting>
    /// <setting name = "sendercontacttelephone" serializeAs="String">
    /// <value>210 2222222</value>
    /// </setting>
    /// <setting name = "sendercontactfax" serializeAs="String">
    /// <value>210 00010999</value>
    /// </setting>
    /// <setting name = "sendercontactx400" serializeAs="String">
    /// <value/>
    /// </setting>
    /// <setting name = "sendercontacturi" serializeAs="String">
    /// <value>http://www.sdmx.org</value>
    /// </setting>
    /// <setting name = "receivercontacttelephone" serializeAs="String">
    /// <value>210 1234567</value>
    /// </setting>
    /// <setting name = "receivercontactfax" serializeAs="String">
    /// <value>210 3810999</value>
    /// </setting>
    /// <setting name = "receivercontactx400" serializeAs="String">
    /// <value>lalala</value>
    /// </setting>
    /// <setting name = "receivercontacturi" serializeAs="String">
    /// <value>http://www.sdmx.org</value>
    /// </setting>
    /// <setting name = "datasetagency" serializeAs="String">
    /// <value>BIS</value>
    /// </setting>
    /// <setting name = "datasetaction" serializeAs="String">
    /// <value>Append</value>
    /// </setting>
    /// <setting name = "reportingbegin" serializeAs="String">
    /// <value>2000-12-01T00:00:00</value>
    /// </setting>
    /// <setting name = "reportingend" serializeAs="String">
    /// <value>2006-01-01T00:00:00</value>
    /// </setting>
    /// <setting name = "lang" serializeAs="String">
    /// <value>en</value>
    /// </setting>
    /// <setting name = "id" serializeAs="String">
    /// <value>IT1001</value>
    /// </setting>
    /// <setting name = "prepared" serializeAs="String">
    /// <value>2001-03-11T09:30:47-05:00</value>
    /// </setting>
    /// <setting name = "senderid" serializeAs="String">
    /// <value>ISTAT</value>
    /// </setting>
    /// <setting name = "sendername" serializeAs="String">
    /// <value>Italian Statistical Institute</value>
    /// </setting>
    /// <setting name = "sendercontactname" serializeAs="String">
    /// <value>Francesco Rizzo</value>
    /// </setting>
    /// <setting name = "sendercontactrole" serializeAs= "String" >
    /// < value > IT Staff</value>
    /// </setting>
    /// <setting name = "sendercontactemail" serializeAs= "String" >
    /// < value > rizzo@istat.it</value>
    /// </setting>
    /// <setting name = "receiverid" serializeAs= "String" >
    /// < value > ESTAT </ value >
    /// </ setting >
    /// < setting name= "receivername" serializeAs= "String" >
    /// < value > Eurostat </ value >
    /// </ setting >
    /// < setting name= "receivercontactname" serializeAs= "String" >
    /// < value > Bengt - Ake Lindblad</value>
    /// </setting>
    /// <setting name = "receivercontactdepartment" serializeAs= "String" >
    /// < value > Information Technology</value>
    /// </setting>
    /// <setting name = "receivercontactrole" serializeAs= "String" >
    /// < value > IT Staff</value>
    /// </setting>
    /// <setting name = "receivercontactemail" serializeAs= "String" >
    /// < value > bengt-ake.lindblad@ec.europa.eu</value>
    /// </setting>
    /// <setting name = "datasetid" serializeAs= "String" >
    /// < value > ISTAT_JD_237 </ value >
    /// </ setting >
    /// < setting name= "extracted" serializeAs= "String" >
    /// < value > 2001 - 03 - 11T09:30:47-05:00</value>
    /// </setting>
    /// <setting name = "source" serializeAs= "String" >
    /// < value > source </ value >
    /// </ setting >
    /// < setting name= "keyfamilyref" serializeAs= "String" >
    /// < value > kfRef </ value >
    /// </ setting >
    /// < setting name= "keyfamilyagency" serializeAs= "String" >
    /// < value > kfAg </ value >
    /// </ setting >
    /// </ Estat.Sri.Ws.Controllers.Constants.HeaderSettings >
    /// </ applicationSettings >
    /// ]]>
    /// </code>
    /// </example>
    /// <seealso cref="Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.IHeaderRetrievalManager" />
    public class SettingsHeaderRetriever : IHeaderRetrievalManager
    {
        /// <summary>
        /// The header settings
        /// </summary>
        private readonly HeaderSettings _headerSettings;
        /// <summary>The configuration</summary>
        private readonly SettingsFromConfigurationManager _configuration;
        /// <summary>
        /// Initializes a new instance of the <see cref="SettingsHeaderRetriever"/> class.
        /// </summary>
        public SettingsHeaderRetriever(SettingsFromConfigurationManager configuration)
        {
            this._headerSettings = HeaderSettings.Default;
            _configuration = configuration;
        }

        /// <summary>
        /// Gets a header object
        /// </summary>
        /// <value/>
        public IHeader Header
        {
            get
            {
                return InitialiseHeader(this._headerSettings);
            }
        }

        /// <summary>
        /// Gets the receiver list.
        /// </summary>
        /// <param name="lang">
        /// The language.
        /// </param>
        /// <param name="headerSettings">
        /// The header settings.
        /// </param>
        /// <returns>
        /// The Sender <see cref="IParty"/>
        /// </returns>
        private static IList<IParty> GetReceiverList(string lang, HeaderSettings headerSettings)
        {
            // the receiver id is mandatory if we set a Receiver
            if (string.IsNullOrWhiteSpace(headerSettings.receiverid))
            {
                return new IParty[0];
            }

            IList<ITextTypeWrapper> textTypeWrapperReceiver = new List<ITextTypeWrapper>();
            if (!string.IsNullOrWhiteSpace(headerSettings.receivername))
            {
                textTypeWrapperReceiver.Add(new TextTypeWrapperImpl(lang, headerSettings.receivername, null));
            }

            IContactMutableObject receiverContact = new ContactMutableObjectCore();

            if (!string.IsNullOrWhiteSpace(headerSettings.receivercontactname))
            {
                receiverContact.AddName(new TextTypeWrapperMutableCore(lang, headerSettings.receivercontactname));
            }

            if (!string.IsNullOrWhiteSpace(headerSettings.receivercontactdepartment))
            {
                receiverContact.AddDepartment(new TextTypeWrapperMutableCore(lang, headerSettings.receivercontactdepartment));
            }

            if (!string.IsNullOrWhiteSpace(headerSettings.receivercontactrole))
            {
                receiverContact.AddRole(new TextTypeWrapperMutableCore(lang, headerSettings.receivercontactrole));
            }

            if (!string.IsNullOrWhiteSpace(headerSettings.receivercontacttelephone))
            {
                receiverContact.AddTelephone(headerSettings.receivercontacttelephone);
            }

            if (!string.IsNullOrWhiteSpace(headerSettings.receivercontactfax))
            {
                receiverContact.AddFax(headerSettings.receivercontactfax);
            }

            if (!string.IsNullOrWhiteSpace(headerSettings.receivercontactx400))
            {
                receiverContact.AddX400(headerSettings.receivercontactx400);
            }

            if (!string.IsNullOrWhiteSpace(headerSettings.receivercontacturi))
            {
                receiverContact.AddUri(headerSettings.receivercontacturi);
            }

            if (!string.IsNullOrWhiteSpace(headerSettings.receivercontactemail))
            {
                receiverContact.AddEmail(headerSettings.receivercontactemail);
            }

            // RECEIVER
            IList<IContact> contactsReceiver = new List<IContact>();
            if (HasContactInformation(receiverContact))
            {
                IContact contactImmutableReceiver = new ContactCore(receiverContact);
                contactsReceiver.Add(contactImmutableReceiver);
            }

            IList<IParty> receiverList = new List<IParty>();
            IParty receiver = new PartyCore(textTypeWrapperReceiver, ValidatePartyId(headerSettings.receiverid), contactsReceiver, null);
            receiverList.Add(receiver);

            return receiverList;
        }

        /// <summary>
        /// Gets the sender.
        /// </summary>
        /// <param name="headerSettings">
        /// The header settings.
        /// </param>
        /// <param name="lang">
        /// The language.
        /// </param>
        /// <returns>
        /// The Sender <see cref="IParty"/>
        /// </returns>
        private static IParty GetSender(HeaderSettings headerSettings, string lang)
        {
            IList<ITextTypeWrapper> textTypeWrapperSender = new List<ITextTypeWrapper>();
            if (!string.IsNullOrWhiteSpace(headerSettings.sendername))
            {
                textTypeWrapperSender.Add(new TextTypeWrapperImpl(lang, headerSettings.sendername, null));
            }

            IContactMutableObject senderContact = new ContactMutableObjectCore();
            if (!string.IsNullOrWhiteSpace(headerSettings.sendercontactname))
            {
                senderContact.AddName(new TextTypeWrapperMutableCore(lang, headerSettings.sendercontactname));
            }

            if (!string.IsNullOrWhiteSpace(headerSettings.sendercontactdepartment))
            {
                senderContact.AddDepartment(new TextTypeWrapperMutableCore(lang, headerSettings.sendercontactdepartment));
            }

            if (!string.IsNullOrWhiteSpace(headerSettings.sendercontactrole))
            {
                senderContact.AddRole(new TextTypeWrapperMutableCore(lang, headerSettings.sendercontactrole));
            }

            if (!string.IsNullOrWhiteSpace(headerSettings.sendercontacttelephone))
            {
                senderContact.AddTelephone(headerSettings.sendercontacttelephone);
            }

            if (!string.IsNullOrWhiteSpace(headerSettings.sendercontactfax))
            {
                senderContact.AddFax(headerSettings.sendercontactfax);
            }

            if (!string.IsNullOrWhiteSpace(headerSettings.sendercontactx400))
            {
                senderContact.AddX400(headerSettings.sendercontactx400);
            }

            if (!string.IsNullOrWhiteSpace(headerSettings.sendercontacturi))
            {
                senderContact.AddUri(headerSettings.sendercontacturi);
            }

            if (!string.IsNullOrWhiteSpace(headerSettings.sendercontactemail))
            {
                senderContact.AddEmail(headerSettings.sendercontactemail);
            }

            // SENDER
            IList<IContact> contactsSender = new List<IContact>();
            if (HasContactInformation(senderContact))
            {
                IContact contactImmutableSender = new ContactCore(senderContact);
                contactsSender.Add(contactImmutableSender);
            }

            IParty sender = new PartyCore(textTypeWrapperSender, ValidatePartyId(headerSettings.senderid), contactsSender, null);
            return sender;
        }

        /// <summary>
        /// Validates the partyId.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns>The validated id.</returns>
        private static string ValidatePartyId(string id)
        {
            var partyId = ValidationUtil.CleanPartyId(id);

            if (!id.Equals(partyId, StringComparison.OrdinalIgnoreCase))
            {
                _log.Warn($"Invalid partyId: '{id}' replaced with: '{partyId}'");
            }

            return partyId;
        }

        /// <summary>
        /// Determines whether the specified <paramref name="contact"/> has any information
        /// </summary>
        /// <param name="contact">
        /// The contact.
        /// </param>
        /// <returns>
        /// <c>true</c> if it has information; otherwise, <c>false</c>.
        /// </returns>
        private static bool HasContactInformation(IContactMutableObject contact)
        {
            return NotEmpty(contact.Departments) || NotEmpty(contact.Names) || NotEmpty(contact.Email) || NotEmpty(contact.Fax) || NotEmpty(contact.Roles) || NotEmpty(contact.Telephone) || NotEmpty(contact.Uri) || NotEmpty(contact.X400);
        }

        /// <summary>
        /// Check if the specified collection is not empty
        /// </summary>
        /// <typeparam name="T">
        /// The collection item type
        /// </typeparam>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// True if the collection is not empty; otherwise false
        /// </returns>
        private static bool NotEmpty<T>(ICollection<T> collection)
        {
            return collection.Count > 0;
        }

        /// <summary>
        /// Parses the date time.
        /// </summary>
        /// <param name="fromConfig">
        /// From configuration.
        /// </param>
        /// <returns>
        /// The <see cref="DateTime"/> if <paramref name="fromConfig"/> contains a valid DateTime; otherwise <c>null</c>.
        /// </returns>
        private static DateTime? ParseDateTime(string fromConfig)
        {
            if (string.IsNullOrWhiteSpace(fromConfig))
            {
                return null;
            }

            DateTime validDateTime;
            if (DateTime.TryParse(fromConfig, out validDateTime))
            {
                return validDateTime;
            }

            return null;
        }

        /// <summary>
        /// This method initializes the Header Settings
        /// </summary>
        /// <param name="headerSettings">The header settings.</param>
        /// <returns>
        /// The <see cref="IHeader" />.
        /// </returns>
        private IHeader InitialiseHeader(HeaderSettings headerSettings)
        {
            IList<ITextTypeWrapper> name = new List<ITextTypeWrapper>();
            var lang = headerSettings.lang;
            if (!string.IsNullOrWhiteSpace(headerSettings.name))
            {
                name.Add(new TextTypeWrapperImpl(lang, headerSettings.name, null));
            }

            var sender = GetSender(headerSettings, lang);

            var receiverList = GetReceiverList(lang, headerSettings);

            IDictionary<string, string> additionalAttributes = new Dictionary<string, string>();
            if (!string.IsNullOrWhiteSpace(headerSettings.keyfamilyref))
            {
                additionalAttributes.Add(NameTableCache.GetElementName(ElementNameTable.KeyFamilyRef), headerSettings.keyfamilyref);
            }

            if (!string.IsNullOrWhiteSpace(headerSettings.keyfamilyagency))
            {
                additionalAttributes.Add(NameTableCache.GetElementName(ElementNameTable.KeyFamilyAgency), headerSettings.keyfamilyagency);
            }

            if (!string.IsNullOrWhiteSpace(headerSettings.datasetagency))
            {
                additionalAttributes.Add(NameTableCache.GetElementName(ElementNameTable.DataSetAgency), headerSettings.datasetagency);
            }

            var extracted = ParseDateTime(headerSettings.extracted);
            var prepared = ParseDateTime(headerSettings.prepared);
            var reportingBegin = ParseDateTime(headerSettings.reportingbegin);
            var reportingEnd = ParseDateTime(headerSettings.reportingend);

            IList<ITextTypeWrapper> source = new List<ITextTypeWrapper>();
            if (!string.IsNullOrWhiteSpace(headerSettings.source))
            {
                source.Add(new TextTypeWrapperImpl(lang, headerSettings.source, null));
            }

            var datasetAction = string.IsNullOrWhiteSpace(headerSettings.datasetaction) ? string.IsNullOrWhiteSpace(_configuration.DatasetAction)? DatasetAction.GetFromEnum(DatasetActionEnumType.Information): DatasetAction.GetAction(_configuration.DatasetAction) : DatasetAction.GetAction(headerSettings.datasetaction);
            bool test = true;

            if (!string.IsNullOrWhiteSpace(headerSettings.test))
            {
                test = XmlConvert.ToBoolean(headerSettings.test);
            }

            var id = !string.IsNullOrWhiteSpace(headerSettings.id) ? headerSettings.id : HeaderId.Unset;
            return new HeaderImpl(additionalAttributes, null, null, datasetAction, id, headerSettings.datasetid, null, extracted, prepared, reportingBegin, reportingEnd, name, source, receiverList, sender, test);
        }

        /// <summary>
        ///     The _log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(SettingsHeaderRetriever));
       
    }
}