using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Xml;
using Estat.Sdmxsource.Extension.Engine;
using Estat.Sdmxsource.Extension.Factory;
using Estat.Sdmxsource.Extension.Model;
using Estat.Sri.Ws.Configuration;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
using Org.Sdmxsource.Sdmx.Structureparser.Builder.SuperObjects;

namespace Estat.Sri.Ws.Format.Sdmx.Factory
{
    public class SDMXCSVFormatFactory : IDataWriterPluginContract
    {
        /// <summary>
        /// The _content list
        /// </summary>
        //TODO: refactor
        private readonly IList<string> _contentList = new[]
        {
            "text/csv",
            "text/csv;labels=id",
            "text/csv;labels=both",
            "text/csv;timeformat=original",
            "text/csv;timeformat=normalized",
            "text/csv;labels=id;timeformat=original",
            "text/csv;labels=both;timeformat=original",
            "text/csv;labels=id;timeformat=normalized",
            "text/csv;labels=both;timeformat=normalized",

            "text/x-csv",
            "text/x-csv",
            "text/x-csv;labels=id",
            "text/x-csv;labels=both",
            "text/x-csv;timeformat=original",
            "text/x-csv;timeformat=normalized",
            "text/x-csv;labels=id;timeformat=original",
            "text/x-csv;labels=both;timeformat=original",
            "text/x-csv;labels=id;timeformat=normalized",
            "text/x-csv;labels=both;timeformat=normalized",

            "application/vnd.sdmx.data+csv",
            "application/vnd.sdmx.data+csv;labels=id",
            "application/vnd.sdmx.data+csv;labels=both",
            "application/vnd.sdmx.data+csv;timeformat=original",
            "application/vnd.sdmx.data+csv;timeformat=normalized",
            "application/vnd.sdmx.data+csv;labels=id;timeformat=original",
            "application/vnd.sdmx.data+csv;labels=both;timeformat=original",
            "application/vnd.sdmx.data+csv;labels=id;timeformat=normalized",
            "application/vnd.sdmx.data+csv;labels=both;timeformat=normalized",

            "application/vnd.sdmx.data+csv;version=2",
            "application/vnd.sdmx.data+csv;version=2;labels=id",
            "application/vnd.sdmx.data+csv;version=2;labels=name",
            "application/vnd.sdmx.data+csv;version=2;labels=both",
            "application/vnd.sdmx.data+csv;version=2;timeformat=original",
            "application/vnd.sdmx.data+csv;version=2;timeformat=normalized",
            "application/vnd.sdmx.data+csv;version=2;labels=id;timeformat=original",
            "application/vnd.sdmx.data+csv;version=2;labels=name;timeformat=original",
            "application/vnd.sdmx.data+csv;version=2;labels=both;timeformat=original",
            "application/vnd.sdmx.data+csv;version=2;labels=id;timeformat=normalized",
            "application/vnd.sdmx.data+csv;version=2;labels=name;timeformat=normalized",
            "application/vnd.sdmx.data+csv;version=2;labels=both;timeformat=normalized",

            "application/vnd.sdmx.data+csv;version=2.0",
            "application/vnd.sdmx.data+csv;version=2.0;labels=id",
            "application/vnd.sdmx.data+csv;version=2.0;labels=name",
            "application/vnd.sdmx.data+csv;version=2.0;labels=both",
            "application/vnd.sdmx.data+csv;version=2.0;timeformat=original",
            "application/vnd.sdmx.data+csv;version=2.0;timeformat=normalized",
            "application/vnd.sdmx.data+csv;version=2.0;labels=id;timeformat=original",
            "application/vnd.sdmx.data+csv;version=2.0;labels=name;timeformat=original",
            "application/vnd.sdmx.data+csv;version=2.0;labels=both;timeformat=original",
            "application/vnd.sdmx.data+csv;version=2.0;labels=id;timeformat=normalized",
            "application/vnd.sdmx.data+csv;version=2.0;labels=name;timeformat=normalized",
            "application/vnd.sdmx.data+csv;version=2.0;labels=both;timeformat=normalized",

            "application/vnd.sdmx.data+csv;version=2.0.0",
            "application/vnd.sdmx.data+csv;version=2.0.0;labels=id",
            "application/vnd.sdmx.data+csv;version=2.0.0;labels=name",
            "application/vnd.sdmx.data+csv;version=2.0.0;labels=both",
            "application/vnd.sdmx.data+csv;version=2.0.0;timeformat=original",
            "application/vnd.sdmx.data+csv;version=2.0.0;timeformat=normalized",
            "application/vnd.sdmx.data+csv;version=2.0.0;labels=id;timeformat=original",
            "application/vnd.sdmx.data+csv;version=2.0.0;labels=name;timeformat=original",
            "application/vnd.sdmx.data+csv;version=2.0.0;labels=both;timeformat=original",
            "application/vnd.sdmx.data+csv;version=2.0.0;labels=id;timeformat=normalized",
            "application/vnd.sdmx.data+csv;version=2.0.0;labels=name;timeformat=normalized",
            "application/vnd.sdmx.data+csv;version=2.0.0;labels=both;timeformat=normalized",
        };

        /// <summary>
        /// The _super objects builder
        /// </summary>
        private readonly SuperObjectsBuilder _superObjectsBuilder = new SuperObjectsBuilder();

        private readonly ITranslatorFactory _translatorFactory;

        /// <summary>
        /// The configuration
        /// </summary>
        private readonly SettingsFromConfigurationManager _configuration;

        public SDMXCSVFormatFactory(ITranslatorFactory translatorFactory, SettingsFromConfigurationManager configuration)
        {
            this._translatorFactory = translatorFactory;
            this._configuration = configuration;
        }

        /// <summary>
        /// Gets the supported formats and header values.
        /// </summary>
        /// <param name="restDataRequest">The rest data request.</param>
        /// <returns>
        /// The list of supported formats - <see cref="T:Estat.Sdmxsource.Extension.Model.IRestResponse`1" />.
        /// </returns>
        public IEnumerable<IRestResponse<IDataFormat>> GetDataFormats(IRestDataRequest restDataRequest)
        {
            if (restDataRequest == null)
            {
                throw new ArgumentNullException("restDataRequest");
            }

            var charSet = restDataRequest.SortedAcceptHeaders.CharSets.Select(quality => quality.Value).FirstOrDefault()
                          ?? new UTF8Encoding(false);
            
            var superObjectRetriever = new Lazy<SuperObjectRetriever>(() => new SuperObjectRetriever(restDataRequest,this._superObjectsBuilder, this._translatorFactory));

            var requestedComponents = GetRequestedComponents(restDataRequest.DataQuery);

            return
                this._contentList.Select(
                    s => BuildRestResponse(s, charSet, superObjectRetriever, restDataRequest.RetrievalManager, requestedComponents));
        }

        /// <inheritdoc cref="IDataWriterPluginContract.GetAvailableDataFormats"/>
        public IEnumerable<string> GetAvailableDataFormats()
        {
            return this._contentList;
        }

        /// <summary>
        /// Gets the data format.
        /// </summary>
        /// <param name="dataType">Type of the data.</param>
        /// <param name="xmlWriter">The XML writer.</param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Data.IDataFormat" />.
        /// </returns>
        /// <exception cref="System.NotImplementedException">Not supported</exception>
        public IDataFormat GetDataFormat(ISoapRequest<DataType> dataType, XmlWriter xmlWriter)
        {
            // This is used for SOAP. Only XML formats apply here normally 
            return null;
        }

        /// <summary>
        /// Builds the rest response.
        /// </summary>
        /// <param name="format">The format.</param>
        /// <param name="s">The s.</param>
        /// <param name="charSet">The character set.</param>
        /// <param name="language"></param>
        /// <param name="configuration"></param>
        /// <returns>The <see cref="IRestResponse{IDataFormat}"/></returns>
        private IRestResponse<IDataFormat> BuildRestResponse(string s, Encoding charSet, Lazy<SuperObjectRetriever> retriever, ISdmxObjectRetrievalManager retrievalManager, List<string> requestedComponents)
        {
            var contentType = new ContentType(s) { CharSet = charSet.WebName };

            var responseContentHeader = new ResponseContentHeader(
                contentType,
                new Lazy<string>(() => string.Join(",",retriever.Value.Translator.SelectedLanguages)));

          

            return new RestResponse<IDataFormat>(new Lazy<IDataFormat>(() =>
            {
                var dataType = contentType.Parameters.ContainsKey("version") && contentType.Parameters["version"].StartsWith("2")
                    ? DataType.GetFromEnum(DataEnumType.CsvV20):
                    DataType.GetFromEnum(DataEnumType.Csv);

                var isOldTestClientFormat = s.Contains("x-csv", StringComparison.CurrentCulture);
                var format = new SdmxCsvDataFormat(
                   retriever.Value.RetrievalManager,
                   new SdmxCsvOptions(charSet, requestedComponents, this._configuration.UseCultureSpecificColumnAndDecimalSeparators, isOldTestClientFormat),
                   retriever.Value.Translator,
                   dataType,
                   retrievalManager
                );

                if (contentType.Parameters.ContainsKey("labels"))
                {
                    switch (contentType.Parameters["labels"])
                    {
                        case "name":
                            format.Options.CsvLabels = CsvLabels.GetFromEnum(CsvLabelsEnumType.Name);
                            break;
                        case "both":
                            format.Options.CsvLabels = CsvLabels.GetFromEnum(CsvLabelsEnumType.Both);
                            break;
                        default:
                            format.Options.CsvLabels = CsvLabels.GetFromEnum(CsvLabelsEnumType.Id);
                            break;
                    }
                }

                if (contentType.Parameters.ContainsKey("timeformat") && contentType.Parameters["timeformat"] == "normalized")
                {
                    format.Options.UseNormalizedTime = true;
                }

                return format;

            }), responseContentHeader);
        }

        private List<string> GetRequestedComponents(IDataQuery dataQuery)
        {
            var requestedComponens = new List<string>();
            var dataStructure = dataQuery.DataStructure;
            var metadataStructure = dataQuery.MetadataStructure;

            //Dimensions
            if (dataStructure?.DimensionList?.Dimensions != null)
            {
                foreach (var dimension in dataStructure.DimensionList.Dimensions)
                {
                    requestedComponens.Add(dimension.Id);
                }
            }

            //Measure
            if (dataQuery.Measures == MeasuresEnumType.All &&
                dataStructure?.PrimaryMeasure?.Id != null)
            {
                requestedComponens.Add(dataStructure?.PrimaryMeasure?.Id);
            }


            //Multiple measures
            if (dataStructure?.MeasureList?.Measures != null)
            {
                foreach (var measure in dataStructure.MeasureList.Measures)
                {
                    requestedComponens.Add(measure.Id);
                }
            }


            //Attributes
            if (dataQuery.Attributes != AttributesEnumType.None && dataQuery.Attributes != AttributesEnumType.Msd
                && dataStructure?.AttributeList?.Attributes != null)
            {
                foreach (var attribute in dataStructure.AttributeList.Attributes)
                {
                    requestedComponens.Add(attribute.Id);
                }
            }

            //Metadata attributes
            if ((dataQuery.Attributes == AttributesEnumType.All || dataQuery.Attributes == AttributesEnumType.Msd) &&  metadataStructure != null)
            {
                var metadataAttributes = metadataStructure.GetMetadataAttributes();
                foreach (var metadataAttribute in metadataAttributes)
                {
                    requestedComponens.Add(metadataAttribute.GetFullIdPath(false));
                }
            }

            return requestedComponens;
        }
    }

}