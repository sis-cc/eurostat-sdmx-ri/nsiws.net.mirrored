﻿// -----------------------------------------------------------------------
// <copyright file="SriDataFormatFactory.cs" company="EUROSTAT">
//   Date Created : 2016-06-30
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Format.Sdmx.Factory
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Xml;

    using Estat.Sdmxsource.Extension.Factory;
    using Estat.Sdmxsource.Extension.Model;
    using Estat.Sri.Ws.Format.Sdmx.Constants;
    using Estat.Sri.Ws.Format.Sdmx.Model;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;

    /// <summary>
    /// The SDMX RI data format factory.
    /// </summary>
    public class SriDataFormatFactory : AbstractDataWriterPluginContract
    {
        /// <summary>
        /// The REST data types
        /// </summary>
        private readonly Dictionary<string, DataMediaType> _dataTypes;

        /// <summary>
        /// Initializes a new instance of the <see cref="SriDataFormatFactory" /> class.
        /// </summary>
        public SriDataFormatFactory()
            : base(DataMediaType.Values.Select(type => type.MediaType.ToString()).ToArray())
        {
            this._dataTypes = DataMediaType.Values.ToDictionary(type => type.MediaType.ToString());
        }

        /// <summary>
        /// Gets the supported formats and header values.
        /// </summary>
        /// <param name="restDataRequest">The rest data request.</param>
        /// <returns>
        /// The list of supported formats - <see cref="T:Estat.Sdmxsource.Extension.Model.IRestResponse`1"/>.
        /// </returns>
        public override IEnumerable<IRestResponse<IDataFormat>> GetDataFormats(IRestDataRequest restDataRequest)
        {
            if (restDataRequest == null)
            {
                throw new ArgumentNullException("restDataRequest");
            }

            var restResponses = base.GetDataFormats(restDataRequest).ToArray();
            if (string.IsNullOrWhiteSpace(restDataRequest.DataQuery.DimensionAtObservation) || restDataRequest.DataQuery.DimensionAtObservation.Equals(DimensionObject.TimeDimensionFixedId))
            {
                return restResponses;
            }

            return restResponses.Where(response => CanHandleDimensionAtObservation(response.Format.SdmxDataFormat));
        }

        /// <summary>
        /// Gets the data format.
        /// </summary>
        /// <param name="dataType">Type of the data.</param>
        /// <param name="xmlWriter">The XML writer.</param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Data.IDataFormat" />.
        /// </returns>
        public override IDataFormat GetDataFormat(ISoapRequest<DataType> dataType, XmlWriter xmlWriter)
        {
            if (dataType == null)
            {
                throw new ArgumentNullException("dataType");
            }

            if (xmlWriter == null)
            {
                throw new ArgumentNullException("xmlWriter");
            }

            var sdmxDataFormat = dataType.FormatType;
            if (sdmxDataFormat != null && (sdmxDataFormat.SchemaVersion.IsXmlFormat() && DataMediaType.Values.Any(type => type.Format == sdmxDataFormat)))
            {
                return new SriSoapDataFormat(sdmxDataFormat, xmlWriter);
            }

            return null;
        }

        /// <summary>
        /// Gets the data format.
        /// </summary>
        /// <param name="mediaType">Type of the media.</param>
        /// <param name="encoding">The encoding.</param>
        /// <param name="dataRequest">The data request.</param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Data.IDataFormat" />.
        /// </returns>
        protected override IDataFormat BuildFormat(string mediaType, Encoding encoding, IRestDataRequest dataRequest)
        {
            DataMediaType dataMediaType;
            if (this._dataTypes.TryGetValue(mediaType, out dataMediaType))
            {
                return new SriRestDataFormat(dataMediaType.Format, encoding);
            }

            return null;
        }

        /// <summary>
        /// Determines whether the <paramref name="dataType"/> can handle dimension at observation.
        /// </summary>
        /// <param name="dataType">The data type.</param>
        /// <returns>
        ///   <c>true</c> if the <paramref name="dataType"/> can handle dimension at observation; otherwise, <c>false</c>.
        /// </returns>
        private static bool CanHandleDimensionAtObservation(DataType dataType)
        {
            if (dataType.EnumType == DataEnumType.CrossSectional20)
            {
                // DimensionAtObservation is set to a non-null value by DataQueryImpl and in Cross Sectional it may have its measure dimension, time dimension or all dimensions
                return true;
            }

            switch (dataType.SchemaVersion.EnumType)
            {
                case SdmxSchemaEnumType.Null:
                case SdmxSchemaEnumType.VersionOne:
                case SdmxSchemaEnumType.VersionTwo:
                case SdmxSchemaEnumType.Edi:
                    return false;
                default:
                    return true;
            }
        }
    }
}