﻿// -----------------------------------------------------------------------
// <copyright file="SriSoapDataWriterFactory.cs" company="EUROSTAT">
//   Date Created : 2015-12-17
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Format.Sdmx.Factory
{
    using System;
    using System.IO;

    using Estat.Sri.Ws.Format.Sdmx.Model;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.DataParser.Engine;
    using Org.Sdmxsource.Sdmx.Structureparser.Model;

    /// <summary>
    /// Factory for generating the SDMX RI default <see cref="IDataWriterEngine"/>
    /// </summary>
    public class SriSoapDataWriterFactory : IDataWriterFactory
    {
        /// <summary>
        /// The tool indicator
        /// </summary>
        private readonly IToolIndicator _toolIndicator;

        /// <summary>
        /// Initializes a new instance of the <see cref="SriSoapDataWriterFactory"/> class.
        /// </summary>
        /// <param name="toolIndicator">The tool indicator.</param>
        public SriSoapDataWriterFactory(IToolIndicator toolIndicator = null)
        {
            this._toolIndicator = toolIndicator;
        }

        /// <summary>
        /// Gets the data writer engine.
        /// </summary>
        /// <param name="dataFormat">The data format.</param>
        /// <param name="outStream">The out stream.</param>
        /// <returns>The <see cref="IDataWriterEngine"/>.</returns>
        /// <exception cref="ArgumentException">Both outStream and dataFormat,XmlWriter set. Please set only one of them.</exception>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "The Disposable is returned")]
        public IDataWriterEngine GetDataWriterEngine(IDataFormat dataFormat, Stream outStream)
        {
            SriSoapDataFormat sriDataFormat = dataFormat as SriSoapDataFormat;
            if (sriDataFormat != null)
            {
                if (sriDataFormat.Writer == null)
                {
                    throw new ArgumentException("SriSoapDataFormat.Writer not set.");
                }

                DataWriterEngineBase engine = null;
                switch (sriDataFormat.SdmxDataFormat.BaseDataFormat.EnumType)
                {
                    case BaseDataFormatEnumType.Generic:

                        engine = new GenericDataWriterEngine(sriDataFormat.Writer, sriDataFormat.SdmxDataFormat.SchemaVersion);
                        break;
                    case BaseDataFormatEnumType.Compact:
                        engine = new CompactDataWriterEngine(sriDataFormat.Writer, sriDataFormat.SdmxDataFormat.SchemaVersion);
                        break;
                }

                if (engine != null && this._toolIndicator != null)
                {
                    engine.GeneratedFileComment = this._toolIndicator.GetComment();
                }

                return engine;
            }

            return null;
        }
    }
}