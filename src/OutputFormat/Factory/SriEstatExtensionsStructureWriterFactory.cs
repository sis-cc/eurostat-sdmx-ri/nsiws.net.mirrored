﻿// -----------------------------------------------------------------------
// <copyright file="SriEstatExtensionsStructureWriterFactory.cs" company="EUROSTAT">
//   Date Created : 2016-07-12
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Format.Sdmx.Factory
{
    using Estat.Sdmxsource.Extension.Engine;
    using Estat.Sri.SdmxStructureMutableParser.Factory;

    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Structureparser.Model;

    /// <summary>
    /// The SDMX-RI Rest structure writer factory. It just decorates the <see cref="SdmxStructureWriterV2Factory" />.
    /// </summary>
    /// <remarks>Used a decorator to make the <see cref="SdmxStructureWriterV2Factory"/> more discoverable to developers.</remarks>
    public class SriEstatExtensionsStructureWriterFactory : AbstractStructureWriterFactory
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SriEstatExtensionsStructureWriterFactory" /> class.
        /// </summary>
        public SriEstatExtensionsStructureWriterFactory()
            : this(null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SriEstatExtensionsStructureWriterFactory" /> class.
        /// </summary>
        /// <param name="indicator">The indicator.</param>
        public SriEstatExtensionsStructureWriterFactory(IToolIndicator indicator)
            : base(indicator != null ? (IStructureWriterFactory)new WritingEngineDecoratorFactory(indicator, null, new SdmxStructureWriterV2Factory()) : new SdmxStructureWriterV2Factory())
        {
        }
    }
}