// -----------------------------------------------------------------------
// <copyright file="SriStructureFormatFactory.cs" company="EUROSTAT">
//   Date Created : 2016-07-06
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------



namespace Estat.Sri.Ws.Format.Sdmx.Factory
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Net.Mime;
    using System.Text;
    using System.Xml;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Estat.Sdmxsource.Extension.Constant;
    using Estat.Sdmxsource.Extension.Factory;
    using Estat.Sdmxsource.Extension.Model;
    using Estat.Sri.SdmxStructureMutableParser.Model;
    using Estat.Sri.Ws.Format.Sdmx.Constants;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model;

    /// <summary>
    /// The SDMX RI structure format factory.
    /// </summary>
    public class SriStructureFormatFactory : AbstractStructureWriterPluginContract
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SriStructureFormatFactory"/> class.
        /// </summary>
        public SriStructureFormatFactory()
            : base(StructureMediaType.RestValues.Select(type => type.MediaType.ToString()).ToArray())
        {
        }

        /// <summary>
        /// Gets the structure format.
        /// </summary>
        /// <param name="soapRequest">Type of the structure.</param>
        /// <param name="xmlWriter">The XML writer.</param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Format.IStructureFormat" />.
        /// </returns>
        public override IStructureFormat GetStructureFormat(ISoapRequest<StructureOutputFormat> soapRequest, XmlWriter xmlWriter)
        {
            if (soapRequest == null)
            {
                throw new ArgumentNullException("soapRequest");
            }

            if (xmlWriter == null)
            {
                throw new ArgumentNullException("xmlWriter");
            }

            if (soapRequest.FormatType.OutputVersion.IsXmlFormat() && StructureMediaType.SoapValues.Any(type => type.Format == soapRequest.FormatType))
            {
                if (soapRequest.FormatType.OutputVersion.EnumType == SdmxSchemaEnumType.VersionTwo && soapRequest.Extensions == SdmxExtension.EstatExtensions)
                {
                    return new SdmxXmlStructureV2Format(soapRequest.FormatType, xmlWriter);
                }

                return new SdmxXmlStructureFormat(soapRequest.FormatType, xmlWriter);
            }

            return null;
        }

        /// <summary>
        /// Builds the format. This is used only for REST. This needs to be overridden. The <paramref name="mediaType" /> and the <paramref name="encoding" /> are the most important.
        /// </summary>
        /// <param name="mediaType">Type of the media.</param>
        /// <param name="header">The header.</param>
        /// <param name="encoding">The encoding.</param>
        /// <param name="sortedAcceptHeaders">The sorted accept headers.</param>
        /// <param name="structureType"></param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Format.IStructureFormat" />.
        /// </returns>
        protected override IStructureFormat BuildFormat(string mediaType, WebHeaderCollection header, Encoding encoding, ISortedAcceptHeaders sortedAcceptHeaders)
        {
            var format = StructureMediaType.GetFormat(new ContentType(mediaType));
            return new SdmxStructureFormat(format, encoding);
        }
    }
}