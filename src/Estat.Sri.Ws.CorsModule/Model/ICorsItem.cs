﻿namespace Estat.Sri.Ws.CorsModule.Model
{
    public interface ICorsItem
    {
        string Domain { get; }
        string AllowedMethods { get; }
        string AllowedHeaders { get; }
        bool AllowCredentials { get; }
        string ExposedHeaders { get; }
    }
}
