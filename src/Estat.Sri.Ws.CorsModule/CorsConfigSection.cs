﻿using System.Configuration;
using Estat.Sri.Ws.CorsModule.Model;

namespace Estat.Sri.Ws.CorsModule
{
    public class CorsConfigSection : ConfigurationSection
    {
        [ConfigurationProperty("corsCollection", IsRequired = true)]
        [ConfigurationCollection(typeof(CorsItemCollection))]
        public CorsItemCollection CorsCollection => (CorsItemCollection)this["corsCollection"];
    }

    public class CorsItemCollection : ConfigurationElementCollection
    {
        /// <summary>
        /// When overridden in a derived class, creates a new <see cref="T:System.Configuration.ConfigurationElement"/>.
        /// </summary>
        /// <returns>
        /// A newly created <see cref="T:System.Configuration.ConfigurationElement"/>.
        /// </returns>
        protected override ConfigurationElement CreateNewElement()
        {
            return new CorsItem();
        }

        /// <summary>
        /// Gets the element key for a specified configuration element when overridden in a derived class.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Object"/> that acts as the key for the specified <see cref="T:System.Configuration.ConfigurationElement"/>.
        /// </returns>
        /// <param name="element">The <see cref="T:System.Configuration.ConfigurationElement"/> to return the key for. </param>
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((CorsItem)element).Domain;
        }
    }

    public class CorsItem : ConfigurationElement, ICorsItem
    {
        [ConfigurationProperty("domain", IsRequired = true)]
        public string Domain
        {
            get { return (string)this["domain"]; }
            set { this["domain"] = value; }
        }

        [ConfigurationProperty("allowed-methods", IsRequired = true)]
        public string AllowedMethods
        {
            get { return (string)this["allowed-methods"]; }
            set { this["allowed-methods"] = value; }
        }

        [ConfigurationProperty("allowed-headers", IsRequired = true)]
        public string AllowedHeaders
        {
            get { return (string)this["allowed-headers"]; }
            set { this["allowed-headers"] = value; }
        }

        [ConfigurationProperty("exposed-headers", IsRequired = true)]
        public string ExposedHeaders
        {
            get { return (string)this["exposed-headers"]; }
            set { this["exposed-headers"] = value; }
        }

        [ConfigurationProperty("allow-credentials", IsRequired = false)]
        public bool AllowCredentials
        {
            get { return (bool)this["allow-credentials"]; }
            set { this["allow-credentials"] = value; }
        }
    }
}
