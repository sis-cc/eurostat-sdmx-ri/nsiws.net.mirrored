﻿// -----------------------------------------------------------------------
// <copyright file="SwaggerConstants.cs" company="EUROSTAT">
//   Date Created : 2021-09-15
//   Copyright (c) 2009, 2021 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Ws.Rest.Utils.ApiDocumentation
{
    using System.Collections.Generic;
    
    public static class ApiConstants
    {
        public enum SuccessResponseType
        {
            CODE_200,
            CODE_200_SCHEMAS,
            CODE_200_STRUCT,
            CODE_200_META
        }

        public enum SupportedApiVersion
        {
            V1, V2
        }

        public static readonly string[] CONTROLLERS =
        {
            "Structure", "Data", "Available",
            "StructureV2", "DataV2", "Availability"
        };

        /// <summary>
        /// These are the failure response types with description - common for all requests in both api versions.
        /// </summary>
        public static readonly Dictionary<string, string> FAIL_RESPONSE_TYPES = new Dictionary<string, string>()
        {
            { "304", "No changes" },
            { "400", "Bad syntax" },
            { "401", "Unauthorized" },
            { "403", "Forbidden" },
            { "404", "Not found" },
            { "406", "Not acceptable" },
            { "413", "Request entity too large" },
            { "414", "Uri too long" },
            { "500", "Internal server error" },
            { "501", "Not implemented" },
            { "503", "Service unavailable" }
        };

        /// <summary>
        /// Contains the supported mime types for each <see cref="SuccessResponseType"/> and for each <see cref="SupportedApiVersion"/>.
        /// </summary>
        public static readonly Dictionary<SupportedApiVersion, Dictionary<SuccessResponseType, string[]>> CONTENT_TYPES = 
            new Dictionary<SupportedApiVersion, Dictionary<SuccessResponseType, string[]>>();

        static ApiConstants()
        {
            CONTENT_TYPES.Add(SupportedApiVersion.V1, V1ContentTypes());
            CONTENT_TYPES.Add(SupportedApiVersion.V2, V2ContentTypes());
        }

        /// <summary>
        /// Get the supported mime types for each <see cref="SuccessResponseType"/> in <see cref="SupportedApiVersion.V1"/>
        /// as documented in the specifications.
        /// </summary>
        /// <returns></returns>
        private static Dictionary<SuccessResponseType, string[]> V1ContentTypes()
        {
            var v1ContentTypes = new Dictionary<SuccessResponseType, string[]>();
            v1ContentTypes.Add(SuccessResponseType.CODE_200, new []
            {
                "application/vnd.sdmx.genericdata+xml;version=2.1",
                "application/vnd.sdmx.structurespecificdata+xml;version=2.1",
                "application/vnd.sdmx.generictimeseriesdata+xml;version=2.1",
                "application/vnd.sdmx.structurespecifictimeseriesdata+xml;version=2.1",
                "application/vnd.sdmx.data+csv;version=1.0.0",
                "application/vnd.sdmx.data+json;version=1.0.0"
            });
            v1ContentTypes.Add(SuccessResponseType.CODE_200_SCHEMAS, new []
            {
                "application/vnd.sdmx.schema+xml;version=2.1",
                "application/vnd.sdmx.structure+xml;version=2.1",
                "application/vnd.sdmx.structure+json;version=1.0.0"
            });
            v1ContentTypes.Add(SuccessResponseType.CODE_200_STRUCT, new []
            {
                "application/vnd.sdmx.structure+xml;version=2.1",
                "application/vnd.sdmx.structure+json;version=1.0.0"
            });
            return v1ContentTypes;
        }

        /// <summary>
        /// Get the supported mime types for each <see cref="SuccessResponseType"/> in <see cref="SupportedApiVersion.V2"/>
        /// as documented in the specifications.
        /// </summary>
        /// <returns></returns>
        private static Dictionary<SuccessResponseType, string[]> V2ContentTypes()
        {
            var v2ContentTypes = new Dictionary<SuccessResponseType, string[]>();
            v2ContentTypes.Add(SuccessResponseType.CODE_200, new []
            {
                "application/vnd.sdmx.data+json;version=2.0.0",
                "application/vnd.sdmx.data+csv;version=2.0.0",
                "application/vnd.sdmx.data+xml;version=3.0.0",
                "application/vnd.sdmx.genericdata+xml;version=2.1",
                "application/vnd.sdmx.structurespecificdata+xml;version=2.1",
                "application/vnd.sdmx.generictimeseriesdata+xml;version=2.1",
                "application/vnd.sdmx.structurespecifictimeseriesdata+xml;version=2.1",
                "application/vnd.sdmx.data+csv;version=1.0.0",
                "application/vnd.sdmx.data+json;version=1.0.0"
            });
            v2ContentTypes.Add(SuccessResponseType.CODE_200_SCHEMAS, new []
            {
                "application/vnd.sdmx.schema+xml;version=3.0.0",
                "application/vnd.sdmx.structure+xml;version=3.0.0",
                "application/vnd.sdmx.structure+json;version=2.0.0",
                "application/vnd.sdmx.schema+xml;version=2.1",
                "application/vnd.sdmx.structure+xml;version=2.1",
                "application/vnd.sdmx.structure+json;version=1.0.0"
            });
            v2ContentTypes.Add(SuccessResponseType.CODE_200_STRUCT, new []
            {
                "application/vnd.sdmx.structure+xml;version=3.0.0",
                "application/vnd.sdmx.structure+json;version=2.0.0",
                "application/vnd.sdmx.structure+xml;version=2.1",
                "application/vnd.sdmx.structure+json;version=1.0.0"
            });
            v2ContentTypes.Add(SuccessResponseType.CODE_200_META, new []
            {
                "application/vnd.sdmx.metadata+json;version=2.0.0",
                "application/vnd.sdmx.metadata+xml;version=3.0.0",
                "application/vnd.sdmx.metadata+csv;version=1.0.0",
                "application/vnd.sdmx.genericmetadata+xml;version=2.1",
                "application/vnd.sdmx.structurespecificmetadata+xml;version=2.1"
            });
            return v2ContentTypes;
        }
    }
}