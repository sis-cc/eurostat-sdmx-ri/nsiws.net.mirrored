﻿// -----------------------------------------------------------------------
// <copyright file="SwaggerQueryParameterFilter.cs" company="EUROSTAT">
//   Date Created : 2021-09-14
//   Copyright (c) 2009, 2021 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// ----------------------------------------------------------------------

namespace Estat.Sri.Ws.Rest.Utils.Swagger
{
    using System;
    using Microsoft.OpenApi.Models;
    using ApiDocumentation;
    using Swashbuckle.AspNetCore.SwaggerGen;
    using System.Linq;
    using System.Collections.Generic;
    using Microsoft.OpenApi.Any;
    
    // ReSharper disable ClassNeverInstantiated.Global
    
    /// <summary>
    /// Converts the <see cref="ApiParameterAttribute"/> to <see cref="OpenApiParameter"/>
    /// that swagger uses for the documentation.
    /// </summary>
    public class SwaggerApiParameterFilter : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            var attributes = context.MethodInfo.GetCustomAttributes(true)
                    .OfType<ApiParameterAttribute>();
            
            foreach (var attribute in attributes)
            {
                List<IOpenApiAny> enumValues = new List<IOpenApiAny>();
                if (attribute.AvailableValues.Any())
                {
                    enumValues.AddRange(attribute.AvailableValues.Select(v=> new OpenApiString(v)));
                }

                var parameter = operation.Parameters.FirstOrDefault(p => p.In == ParameterLocation.Path && p.Name == attribute.Name)
                                ?? new OpenApiParameter();
                if (string.IsNullOrEmpty(parameter.Name))
                {
                    operation.Parameters.Add(parameter);
                }

                parameter.Name = attribute.Name;
                parameter.Description = attribute.Description;
                parameter.In = attribute.Location;
                parameter.Schema = new OpenApiSchema()
                {
                    Type = attribute.DataType,
                    Format = attribute.Format,
                    Pattern = attribute.Pattern,
                    Example = new OpenApiString(attribute.Example)
                };
                if (!string.IsNullOrEmpty(attribute.DefaultValue))
                {
                    parameter.Schema.Default = new OpenApiString(attribute.DefaultValue);
                }
                if (attribute.DataType.Equals("array", StringComparison.OrdinalIgnoreCase))
                {
                    parameter.Schema.Items = new OpenApiSchema()
                    {
                        Type = "string",
                        Enum = enumValues
                    };
                }
                else if (attribute.Style != null)
                {
                    parameter.Style = attribute.Style;
                }
                else
                {
                    parameter.Schema.Enum = enumValues;
                }
                parameter.Required = attribute.Required;
            }
        }
    }
}