﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Estat.Sri.Ws.Rest.Utils.Swagger
{
    //the swagger package keeps the documentation in a dictionary and the key is generated from the attibute
    //of the controller (i.e. for rest/v{apiVersion:apiVersion}/data/ the key is v{apiVersion}) so we need to replace the the string literal "v{apiVersion}" 
    //with the actual value of the version in order to be matched with the correct version of the swagger document
    //(i.e rest/v1/data/ with doc version v1)
    public class ReplaceVersionWithExactValueInPath : Swashbuckle.AspNetCore.SwaggerGen.IDocumentFilter
    {
        public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
        {
            var paths = new OpenApiPaths();
            foreach (var path in swaggerDoc.Paths)
            {
                if (path.Key.Contains("rest"))
                {
                    var key = path.Key;
                    if (path.Key.Contains("apiVersion"))
                    {
                        key = path.Key.Replace("v{apiVersion}", swaggerDoc.Info.Version.Split('.').First());
                    }

                    paths.Add(key, path.Value);
                }
            }

            swaggerDoc.Paths = paths;
        }
    }

    public static class SwaggerInclusionPredicate
    {
        public static bool IsTrueFor(string docName, ApiDescription apiDesc)
        {
            if (!apiDesc.TryGetMethodInfo(out MethodInfo methodInfo)) return false;

            var versions = methodInfo.DeclaringType
                .GetCustomAttributes(true)
                .OfType<ApiVersionAttribute>()
                .SelectMany(attr => attr.Versions);

            var maps = methodInfo.GetCustomAttributes(true)
                .OfType<MapToApiVersionAttribute>()
                .SelectMany(attr => attr.Versions);
            var versionString = docName.Remove(0, 1);
            if (versionString.Length < 3)
            {
                versionString += ".0";
            }
            var version = new Version(versionString);

            if (versions.Any())
            {
                if (version == new Version("1.0") && !apiDesc.RelativePath.Contains("apiVersion"))
                {
                    if (maps.Any())
                    {
                        return maps.Any(v => $"v{v.ToString()}" == docName);
                    }
                    return true;
                }
                var controllerName = ((ControllerActionDescriptor)apiDesc.ActionDescriptor).ControllerName;
                if (version == new Version("1.0") && apiDesc.RelativePath.Contains("apiVersion") && (controllerName == "Structure" || controllerName == "Data" || controllerName == "Available"))
                {
                    if (maps.Any())
                    {
                        return maps.Any(v => $"v{v.ToString()}" == docName);
                    }
                    return false;
                }

                if (version > new Version("1.0") && apiDesc.RelativePath.Contains("apiVersion"))
                {
                    return versions.Any(v => $"v{v.ToString()}" == docName) &&
                           maps.Any(v => $"v{v.ToString()}" == docName);
                }
            }
            return false;
        }
    }

}
