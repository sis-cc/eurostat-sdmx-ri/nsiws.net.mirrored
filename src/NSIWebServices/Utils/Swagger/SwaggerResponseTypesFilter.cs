﻿// -----------------------------------------------------------------------
// <copyright file="SwaggerResponseFilter.cs" company="EUROSTAT">
//   Date Created : 2021-09-14
//   Copyright (c) 2009, 2021 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Ws.Rest.Utils.Swagger
{
    using System.Linq;
    using ApiDocumentation;
    using Microsoft.OpenApi.Models;
    using Microsoft.AspNetCore.Http;
    using Swashbuckle.AspNetCore.SwaggerGen;
    using System.Collections.Generic;
    using Sdmxsource.Extension.Manager;
    using Microsoft.AspNetCore.Mvc.Controllers;
    
    // ReSharper disable ClassNeverInstantiated.Global
    
    /// <summary>
    /// Builds all possible response types for an endpoint;
    /// all fail responses and success response with possible content types
    /// </summary>
    public class SwaggerResponseTypesFilter : IOperationFilter
    {
        private readonly IFormatResolverManager _formatResolverManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="SwaggerResponseTypesFilter"/>.
        /// </summary>
        /// <param name="formatResolverManager">Used to get the available response formats from plugins.</param>
        public SwaggerResponseTypesFilter(IFormatResolverManager formatResolverManager)
        {
            this._formatResolverManager = formatResolverManager;
        }
        
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            // apply common fail responses
            foreach (var (key, value) in ApiConstants.FAIL_RESPONSE_TYPES)
            {
                if (operation.Responses.ContainsKey(key))
                {
                    continue;
                }

                operation.Responses.Add(key, new OpenApiResponse() { Description = value });
            }
            
            // get/add success response
            string statusCode = StatusCodes.Status200OK.ToString();
            var (_, response) = operation.Responses
                .FirstOrDefault(r => r.Key.Equals(statusCode));
            if (response == null)
            {
                response = new OpenApiResponse() { Description = "OK" };
                operation.Responses.Add(statusCode, response);
            }
            else
            {
                response.Description = "OK";
            }
            
            // get content types from controller ApiResponseTypeAttribute
            var apiResponseAttributes = context.MethodInfo.GetCustomAttributes(true)
                .OfType<ProducesApiResponseTypeAttribute>();
            
            foreach (var attribute in apiResponseAttributes)
            {
                foreach (var contentType in ApiConstants.CONTENT_TYPES[attribute.ApiVersion][attribute.Name])
                {
                    AddContentType(response, contentType); 
                }
            }
            
            // get content types from plugins
            var controllerName = ((ControllerActionDescriptor)context.ApiDescription.ActionDescriptor).ControllerName;
            switch (controllerName)
            {
                // for data response
                case "Data":
                case "DataV2":
                {
                    foreach (var dataFormat in this._formatResolverManager.GetAvailableDataFormats())
                    {
                        AddContentType(response, dataFormat); 
                    }

                    break;
                }
                // for structure response
                case "Available":
                case "Availability":
                case "Structure":
                case "StructureV2":
                {
                    foreach (var structureFormat in this._formatResolverManager.GetAvailableStructureFormats())
                    {
                        AddContentType(response, structureFormat);
                    }

                    break;
                }
            }
        }

        private static void AddContentType(OpenApiResponse response, string contentType)
        {
            if (!response.Content.ContainsKey(contentType))
            {
                response.Content.Add(
                    new KeyValuePair<string, OpenApiMediaType>(contentType, new OpenApiMediaType()));
            }
        }
    }
}