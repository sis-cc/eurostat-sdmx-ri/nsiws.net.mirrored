using System.Net;
using System.Threading.Tasks;
using Controllers.Utils;
using Microsoft.AspNetCore.Http;
using Org.Sdmxsource.Sdmx.Api.Exception;

namespace Estat.Sri.Ws.Rest
{
    using System;

    using log4net;

    /// <summary>
    /// Class DataResourceErrorHandler.
    /// </summary>
    /// <seealso cref="Estat.Sri.Ws.Rest.IDataResource" />
    public class DataResourceErrorHandler : IDataResource
    {
        /// <summary>
        /// The decorated resource
        /// </summary>
        private readonly IDataResource _decoratedResource;

        private readonly IHttpContextAccessor _contextAccessor;

        /// <summary>
        /// The logger
        /// </summary>
        private static readonly ILog _logger = LogManager.GetLogger(typeof(DataResourceErrorHandler));

        /// <summary>
        /// The _fault exception rest builder
        /// </summary>
       // private readonly WebFaultExceptionRestBuilder _faultExceptionRestBuilder = new WebFaultExceptionRestBuilder();

        /// <summary>
        /// Initializes a new instance of the <see cref="DataResourceErrorHandler"/> class.
        /// </summary>
        /// <param name="decoratedResource">The decorated resource.</param>
        public DataResourceErrorHandler(IDataResource decoratedResource,IHttpContextAccessor contextAccessor)
        {
            this._decoratedResource = decoratedResource;
            this._contextAccessor = contextAccessor;
        }

        /// <summary>
        /// Gets the generic data.
        /// </summary>
        /// <param name="flowRef">The flow reference.</param>
        /// <param name="key">The key.</param>
        /// <param name="providerRef">The provider reference.</param>
        /// <returns>
        /// The <see cref="Message"/>.
        /// </returns>
        public void GetGenericData(string flowRef, string key, string providerRef)
        {
            try
            {
                this._decoratedResource.GetGenericData(flowRef, key, providerRef);
            }
            catch (Exception e)
            {
                _logger.ErrorFormat("GetGenericData : {0}/{1}/{2}", flowRef, key, providerRef);
                _logger.Error(e);
                if (e is SdmxException sdmxException)
                {
                    throw new ServiceException((HttpStatusCode)sdmxException.HttpRestErrorCode, sdmxException);
                }

                if (e is ServiceException)
                {
                    throw;
                }

                throw new ServiceException(HttpStatusCode.InternalServerError, e);
            }
        }

        /// <summary>
        /// Gets the generic data asynchronous.
        /// </summary>
        /// <param name="flowRef">The flow reference.</param>
        /// <param name="key">The key.</param>
        /// <param name="providerRef">The provider reference.</param>
        /// <returns>
        /// The <see cref="Message"/>.
        /// </returns>
        public async Task GetGenericDataAsync(string flowRef, string key, string providerRef)
        {
            try
            {
                await this._decoratedResource.GetGenericDataAsync(flowRef, key, providerRef);
            }
            catch (Exception e)
            {
                _logger.ErrorFormat("GetGenericData : {0}/{1}/{2}", flowRef, key, providerRef);
                _logger.Error(e);
                if (e is SdmxException sdmxException)
                {
                    throw new ServiceException((HttpStatusCode)sdmxException.HttpRestErrorCode, sdmxException);
                }

                if (e is ServiceException)
                {
                    throw;
                }

                throw new ServiceException(HttpStatusCode.InternalServerError, e);
            }
        }

        /// <summary>
        /// Gets the generic data asynchronous for REST 2.0.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="agencyID">The agency(ies).</param>
        /// <param name="resourceID">The resource id(s).</param>
        /// <param name="version">The version(s).</param>
        /// <param name="key">The key(s).</param>
        /// <returns>The <see cref="Message"/>.</returns>
        public async Task GetGenericDataV2Async(string context, string agencyID, string resourceID, string version, string key)
        {
            try
            {
                await this._decoratedResource.GetGenericDataV2Async(context, agencyID, resourceID, version, key);
            }
            catch (Exception e)
            {
                _logger.ErrorFormat("GetGenericData : {0}/{1}/{2}/{3}/{4}", context, agencyID, resourceID, version, key);
                _logger.Error(e);
                if (e is SdmxException sdmxException)
                {
                    throw new ServiceException((HttpStatusCode)sdmxException.HttpRestErrorCode, sdmxException);
                }

                if (e is ServiceException)
                {
                    throw;
                }

                throw new ServiceException(HttpStatusCode.InternalServerError, e);
            }
        }
    }
}