// -----------------------------------------------------------------------
// <copyright file="AvailableResource.cs" company="EUROSTAT">
//   Date Created : 2018-04-18
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
//
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Threading.Tasks;
using Controllers.Utils;
using Estat.Sdmxsource.Extension.Builder;
using Estat.Sdmxsource.Extension.Factory;
using Estat.Sdmxsource.Extension.Model;
using Estat.Sri.Ws.Controllers.Builder;
using Estat.Sri.Ws.Controllers.Controller;
using Estat.Sri.Ws.Controllers.Extension;
using Microsoft.AspNetCore.Http;

namespace Estat.Sri.Ws.Rest
{
    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sri.Ws.Controllers.Manager;
    using Estat.Sri.Ws.Controllers.Model;
    using Estat.Sri.Ws.Rest.Utils;
    using Microsoft.Extensions.Configuration;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Output;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Builder.Common;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;

    /// <inheritdoc />
    public class AvailableResource : IAvailableResource
    {
        private readonly IRetrieverManager _retrieverManager;
        private readonly IFormatResolverManager _formatResolverManager;
        private readonly IStructureWriterManager _writerManager;

        private readonly IHttpContextAccessor _httpContextAccessor;
        /// <summary>
        /// The _translator factory
        /// </summary>
        private readonly ITranslatorFactory _translatorFactory;
        private readonly IConfiguration _configuration;

        /// <summary>
        /// The sorted accept headers builder
        /// </summary>
        private readonly SortedAcceptHeadersBuilder _acceptHeaderBuilder = new SortedAcceptHeadersBuilder();

        /// <summary>
        /// Initializes a new instance of the <see cref="AvailableResource"/> class.
        /// </summary>
        /// <param name="retrieverManager">The retriever manager.</param>
        /// <param name="formatResolverManager">The format resolver manager.</param>
        /// <param name="writerManager">The writer manager.</param>
        public AvailableResource(IRetrieverManager retrieverManager, IFormatResolverManager formatResolverManager, IStructureWriterManager writerManager,IHttpContextAccessor httpContextAccessor, ITranslatorFactory translatorFactory, IConfiguration configuration)
        {
            this._retrieverManager = retrieverManager;
            this._formatResolverManager = formatResolverManager;
            _writerManager = writerManager;
            this._httpContextAccessor = httpContextAccessor;
            this._translatorFactory = translatorFactory;
            this._configuration = configuration;
        }

        /// <summary>
        /// Gets the data.
        /// </summary>
        /// <param name="flowRef">The flow reference.</param>
        /// <param name="key">The key.</param>
        /// <param name="providerRef">The provider reference.</param>
        /// <param name="componentIds">The component ids.</param>
        /// <returns>The <see cref="void"/>.</returns>
        /// <exception cref="WebFaultException">Bad requests</exception>
        public void GetData(string flowRef, string key, string providerRef, string componentIds)
        {
            if (string.IsNullOrEmpty(flowRef))
            {
                throw new ServiceException(HttpStatusCode.BadRequest);
            }

            var ctx = this._httpContextAccessor.HttpContext;
            if (ctx == null)
            {
                throw new ServiceException(HttpStatusCode.ServiceUnavailable);
            }

            var path = new List<string>();
            path.Add(RestAvailableConstraintQuery.ResourceName);
            path.Add(flowRef);
            path.Add(key);
            path.Add(providerRef);
            path.Add(componentIds);

            var incomingWebRequestContext = this._httpContextAccessor.HttpContext.Request;
            var restQuery = new RestAvailableConstraintQuery(string.Join("/", path), HeaderUtils.GetQueryStringAsDict(this._httpContextAccessor.HttpContext));
            var headers = new WebHeaderCollection();
            foreach (var keyPairValue in incomingWebRequestContext.Headers)
            {
                if (!keyPairValue.Key.StartsWith(':'))
                {
                    headers.Add(keyPairValue.Key, keyPairValue.Value);
                }
            }

            var response = this._formatResolverManager.GetStructureFormat(headers, RestApiVersion.V1);
         
            var outputVersion = response.Format.SdmxOutputFormat != null ? response.Format.SdmxOutputFormat.OutputVersion : SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwoPointOne);
            var sdmxObjects = _retrieverManager.ParseRequest(outputVersion, restQuery);
            if (response.Format is IFormatWithTranslator formatTranslator)
            {
                var requestedLanguages = _acceptHeaderBuilder.Build(headers).Languages;
                formatTranslator.Translator = this._translatorFactory.GetTranslator(sdmxObjects, requestedLanguages);
            }
            Func<Stream, Queue<Action>, Task> function = async (stream, queue) =>
            {
                queue.RunAll();
                this._writerManager.WriteStructures(sdmxObjects, response.Format, stream);
            };

            var actionWithHeader = new ResponseActionWithHeader(function, response.ResponseContentHeader);
            var messageFormat = new RestFormat(this._httpContextAccessor, actionWithHeader.ResponseContentHeader);
            RestMessageBuilder builder = new RestMessageBuilder(this._httpContextAccessor, actionWithHeader.ResponseContentHeader, HeaderUtils.VaryByHeaders(this._configuration));
            var controller = new StreamController<Stream>(actionWithHeader.Function);
            builder.Build(controller, messageFormat);
        }

        /// <summary>
        /// Gets the data asynchronous.
        /// </summary>
        /// <param name="flowRef">The flow reference.</param>
        /// <param name="key">The key.</param>
        /// <param name="providerRef">The provider reference.</param>
        /// <param name="componentIds">The component ids.</param>
        /// <returns>The <see cref="void"/>.</returns>
        /// <exception cref="WebFaultException">Bad requests</exception>
        public async Task GetDataAsync(string flowRef, string key, string providerRef, string componentIds)
        {
            if (string.IsNullOrEmpty(flowRef))
            {
                throw new ServiceException(HttpStatusCode.BadRequest);
            }

            var ctx = this._httpContextAccessor.HttpContext;
            if (ctx == null)
            {
                throw new ServiceException(HttpStatusCode.ServiceUnavailable);
            }

            var path = new List<string>();
            path.Add(RestAvailableConstraintQuery.ResourceName);
            path.Add(flowRef);
            path.Add(key);
            path.Add(providerRef);
            path.Add(componentIds);

            var incomingWebRequestContext = this._httpContextAccessor.HttpContext.Request;
            var restQuery = new RestAvailableConstraintQuery(string.Join("/", path), HeaderUtils.GetQueryStringAsDict(this._httpContextAccessor.HttpContext));
            var headers = new WebHeaderCollection();
            foreach (var keyPairValue in incomingWebRequestContext.Headers)
            {
                if (!keyPairValue.Key.StartsWith(':'))
                {
                    headers.Add(keyPairValue.Key, keyPairValue.Value);
                }
            }

            var response = this._formatResolverManager.GetStructureFormat(headers, RestApiVersion.V1);

            var outputVersion = response.Format.SdmxOutputFormat != null ? response.Format.SdmxOutputFormat.OutputVersion : SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwoPointOne);
            var sdmxObjects = _retrieverManager.ParseRequest(outputVersion, restQuery);
            if (response.Format is IFormatWithTranslator formatTranslator)
            {
                var requestedLanguages = _acceptHeaderBuilder.Build(headers).Languages;
                formatTranslator.Translator = this._translatorFactory.GetTranslator(sdmxObjects, requestedLanguages);
            }
            Func<Stream, Queue<Action>, Task> function = async (stream, queue) =>
            {
                queue.RunAll();
                this._writerManager.WriteStructures(sdmxObjects, response.Format, stream);
            };

            var actionWithHeader = new ResponseActionWithHeader(function, response.ResponseContentHeader);
            var messageFormat = new RestFormat(this._httpContextAccessor, actionWithHeader.ResponseContentHeader);
            RestMessageBuilder builder = new RestMessageBuilder(this._httpContextAccessor, actionWithHeader.ResponseContentHeader, HeaderUtils.VaryByHeaders(this._configuration));
            var controller = new StreamController<Stream>(actionWithHeader.Function);
            await builder.BuildAsync(controller, messageFormat);
        }

        /// <summary>
        /// Gets the data asynchronous, based on the rest v2 protocol.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="agencyID">The agency(ies).</param>
        /// <param name="resourceID">The resource id(s).</param>
        /// <param name="version">The version(s).</param>
        /// <param name="keys">The key(s).</param>
        /// <param name="componentId">The component id(s).</param>
        /// <returns>The <see cref="void"/>.</returns>
        /// <exception cref="WebFaultException">Bad requests</exception>
        public async Task GetDataV2Async(string context, string agencyID, string resourceID, string version, string keys, string componentId)
        {
            if (string.IsNullOrEmpty(context))
            {
                throw new ServiceException(HttpStatusCode.BadRequest);
            }

            var ctx = this._httpContextAccessor.HttpContext;
            if (ctx == null)
            {
                throw new ServiceException(HttpStatusCode.ServiceUnavailable);
            }

            var path = new List<string>();
            path.Add(RestAvailabilityQuery.ResourceName);
            path.Add(context);
            path.Add(agencyID);
            path.Add(resourceID);
            path.Add(version);
            path.Add(keys);
            path.Add(componentId);

            var incomingWebRequestContext = this._httpContextAccessor.HttpContext.Request;
            var restQuery = new RestAvailabilityQuery(string.Join("/", path), HeaderUtils.GetQueryStringAsDict(this._httpContextAccessor.HttpContext));
            var headers = new WebHeaderCollection();
            foreach (var keyPairValue in incomingWebRequestContext.Headers)
            {
                if (!keyPairValue.Key.StartsWith(':'))
                {
                    headers.Add(keyPairValue.Key, keyPairValue.Value);
                }
            }

            var response = this._formatResolverManager.GetStructureFormat(headers, RestApiVersion.V2);

            var outputVersion = response.Format.SdmxOutputFormat != null ? response.Format.SdmxOutputFormat.OutputVersion : SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwoPointOne);
            var sdmxObjects = _retrieverManager.ParseRequest(outputVersion, restQuery);
            if (response.Format is IFormatWithTranslator formatTranslator)
            {
                var requestedLanguages = _acceptHeaderBuilder.Build(headers).Languages;
                formatTranslator.Translator = this._translatorFactory.GetTranslator(sdmxObjects, requestedLanguages);
            }
            Func<Stream, Queue<Action>, Task> function = async (stream, queue) =>
            {
                queue.RunAll();
                this._writerManager.WriteStructures(sdmxObjects, response.Format, stream);
            };

            var actionWithHeader = new ResponseActionWithHeader(function, response.ResponseContentHeader);
            var messageFormat = new RestFormat(this._httpContextAccessor, actionWithHeader.ResponseContentHeader);
            RestMessageBuilder builder = new RestMessageBuilder(this._httpContextAccessor, actionWithHeader.ResponseContentHeader, HeaderUtils.VaryByHeaders(this._configuration));
            var controller = new StreamController<Stream>(actionWithHeader.Function);
            await builder.BuildAsync(controller, messageFormat);
        }
    }
}