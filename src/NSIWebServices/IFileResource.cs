using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estat.Sri.Ws.Rest
{
    public interface IFileResource 
    {
        /// <summary>
        /// Gets the generic data and write them to a file
        /// </summary>
        /// <param name="flowRef"></param>
        /// <param name="key"></param>
        /// <param name="providerRef"></param>
        /// <returns></returns>
        Task GetGenericDataAndWriteOutPutAsync(string flowRef, string key, string providerRef, string filePath);
    }
}
