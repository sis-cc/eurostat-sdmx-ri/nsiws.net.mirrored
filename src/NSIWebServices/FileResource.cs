using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Controllers.Utils;
using Estat.Sri.Ws.Controllers.Builder;
using Estat.Sri.Ws.Controllers.Controller;
using Estat.Sri.Ws.Controllers.Model;
using Estat.Sri.Ws.Rest.Utils;
using log4net;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Model.Query;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
using Org.Sdmxsource.Util;

namespace Estat.Sri.Ws.Rest
{
    public class FileResource : IFileResource 
    {
        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog _logger = LogManager.GetLogger(typeof(FileResource));

        /// <summary>
        /// The _data request controller
        /// </summary>
        private readonly IDataRequestController _dataRequestController;

        private readonly IHttpContextAccessor _contextAccessor;
        private readonly IConfiguration _configuration;

        /// <summary>
        ///  Initializes a new instance of the <see cref="FileResource" /> class.
        /// </summary>
        /// <param name="dataRequestController"></param>
        /// <param name="contextAccessor"></param>
        /// <param name="configuration"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public FileResource(IDataRequestController dataRequestController, IHttpContextAccessor contextAccessor, IConfiguration configuration)
        {
            if (dataRequestController == null)
            {
                throw new ArgumentNullException("dataRequestController");
            }      

            this._dataRequestController = dataRequestController;
            this._contextAccessor = contextAccessor;
            this._configuration = configuration;
        }

        public async Task GetGenericDataAndWriteOutPutAsync(string flowRef, string key, string providerRef, string filePath)
        {
            HttpContext ctx = this._contextAccessor.HttpContext;

            if (string.IsNullOrEmpty(flowRef))
            {
                throw new ServiceException(HttpStatusCode.NotImplemented); // $$$ Strange 501 ?
            }

            await this.ProcessDataRequestRestAndWriteOutput(flowRef, key, providerRef, filePath, ctx);
        }

        private async Task ProcessDataRequestRestAndWriteOutput(string flowRef, string key, string providerRef, string filePath, HttpContext ctx)
        {
            var acceptHeader = ctx.Request.Headers["Accept"];
            var requestAccept = acceptHeader.FirstOrDefault() ?? "";
            _logger.Info("Got request Content type= " + requestAccept);

            AcceptHeaderFromQueryParameters.Update(ctx.Request.Query, ctx.Request.Headers);
            IRestDataQuery query = BuildQueryBean(flowRef, key, providerRef);

            // TODO stop using WebHeaderCollection, it doesn't support HTTP/2
            var webHeaderCollection = new WebHeaderCollection();
            foreach (var header in ctx.Request.Headers)
            {
                if (!header.Key.StartsWith(':'))
                {
                    webHeaderCollection.Add(header.Key, header.Value);
                }
            }

            ctx.RequestAborted.ThrowIfCancellationRequested();

            using (new HeaderScope(webHeaderCollection, false, ctx.RequestAborted))
            {
                var response = this._dataRequestController.ParseRequest(query, ctx.Request.Headers);
                _logger.Info("Selected representation info for the controller: format =" + response.ResponseContentHeader.MediaType);
                IMessageFormat messageFormat = new RestFormat(this._contextAccessor, response.ResponseContentHeader) { Name = "Data" };
                RestMessageBuilder builder = new RestMessageBuilder(this._contextAccessor, response.ResponseContentHeader, HeaderUtils.VaryByHeaders(this._configuration));

                //Set Content-Disposition header if payload should be in Zip or uncompressed file as attachment
                ctx.SetResponseContentDispositionHeader(response.ResponseContentHeader.MediaType.MediaType, out string fileName, $"{flowRef}+{key}");

                var controller = ctx.IsZipContentDispositionHeader()
                    ? new ZipStreamController(response.Function, fileName)
                    : new StreamController<Stream>(response.Function);

                await builder.BuildToFileAsync(controller, messageFormat, filePath);
            }
        }

        /// <summary>
        /// Builds the query bean.
        /// </summary>
        /// <param name="flowRef">The flow ref.</param>
        /// <param name="key">The key.</param>
        /// <param name="providerRef">The provider ref.</param>
        /// <param name="queryParameters">The query parameters.</param>
        /// <returns>The <see cref="IRestDataQuery" />.</returns>
        /// <exception cref="WebFaultException{String}">An error occurred</exception>
        /// <exception cref="WebFaultException{T}">An error occurred</exception>
        private IRestDataQuery BuildQueryBean(string flowRef, string key, string providerRef)
        {
            var queryString = new string[4];
            queryString[0] = "data";
            queryString[1] = flowRef;
            queryString[2] = key;
            queryString[3] = providerRef;

            IDictionary<string, string> paramsDict = HeaderUtils.GetQueryStringAsDict(this._contextAccessor.HttpContext);
            IRestDataQuery restQuery;

            try
            {
                restQuery = new RESTDataQueryCore(queryString, paramsDict);
            }
            catch (SdmxException e)
            {
                _logger.Error(e.Message, e);
                throw;
            }
            catch (Exception e)
            {
                _logger.Error(e.Message, e);
                throw new ServiceException(HttpStatusCode.BadRequest, e);
            }

            return restQuery;
        }
    }
}
