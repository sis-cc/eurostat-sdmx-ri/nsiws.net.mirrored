﻿// -----------------------------------------------------------------------
// <copyright file="DataResourceOnTheFlyAnnotations.cs" company="EUROSTAT">
//   Date Created : 2018-01-09
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Specialized;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Estat.Sri.Ws.Rest
{
    using Estat.Sdmxsource.Extension.Constant;
    using Estat.Sdmxsource.Extension.Util;
    using Estat.Sri.Ws.Controllers.Manager;
    using Estat.Sri.Ws.Rest.Utils;

    /// <summary>
    /// Apply on the fly annotation to the 
    /// </summary>
    /// <seealso cref="Estat.Sri.Ws.Rest.IDataResource" />
    public class DataResourceOnTheFlyAnnotations : IDataResource
    {
        /// <summary>
        /// The data resource
        /// </summary>
        private readonly IDataResource _dataResource;

        private readonly IHttpContextAccessor _contextAccessor;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataResourceOnTheFlyAnnotations"/> class.
        /// </summary>
        /// <param name="dataResource">The data resource.</param>
        public DataResourceOnTheFlyAnnotations(IDataResource dataResource, IHttpContextAccessor contextAccessor)
        {
            this._dataResource = dataResource;
            this._contextAccessor = contextAccessor;
        }

        /// <summary>
        /// Gets the generic data.
        /// </summary>
        /// <param name="flowRef">The flow reference.</param>
        /// <param name="key">The key.</param>
        /// <param name="providerRef">The provider reference.</param>
        /// <returns>
        /// The <see cref="Message" />.
        /// </returns>
        public void GetGenericData(string flowRef, string key, string providerRef)
        {
            var httpContext = this._contextAccessor.HttpContext;
            var allowedAnnotations = SettingsManager.OnTheFlyAnnotation;
            if (allowedAnnotations == OnTheFlyAnnotationType.None || httpContext == null)
            {
                this._dataResource.GetGenericData(flowRef, key, providerRef);
            }

            var nameValueCollection = new NameValueCollection();
            foreach (var keyPair in httpContext.Request.Query)
            {
                nameValueCollection.Add(keyPair.Key, keyPair.Value);
            }
            var requestedAnnotations = HeaderUtils.GetOnTheFlyAnnotationType(nameValueCollection);
            if (requestedAnnotations == OnTheFlyAnnotationType.None)
            {
                this._dataResource.GetGenericData(flowRef, key, providerRef);
            }

            using (new OnTheFlyAnnotationScope(requestedAnnotations))
            {
                this._dataResource.GetGenericData(flowRef, key, providerRef);
            }
        }

        /// <summary>
        /// Gets the generic data asynchronous.
        /// </summary>
        /// <param name="flowRef">The flow reference.</param>
        /// <param name="key">The key.</param>
        /// <param name="providerRef">The provider reference.</param>
        /// <returns>
        /// The <see cref="Message" />.
        /// </returns>
        public async Task GetGenericDataAsync(string flowRef, string key, string providerRef)
        {
            var httpContext = this._contextAccessor.HttpContext;
            var allowedAnnotations = SettingsManager.OnTheFlyAnnotation;
            if (allowedAnnotations == OnTheFlyAnnotationType.None || httpContext == null)
            {
                await this._dataResource.GetGenericDataAsync(flowRef, key, providerRef);
            }

            var nameValueCollection = new NameValueCollection();
            foreach (var keyPair in httpContext.Request.Query)
            {
                nameValueCollection.Add(keyPair.Key, keyPair.Value);
            }
            var requestedAnnotations = HeaderUtils.GetOnTheFlyAnnotationType(nameValueCollection);
            if (requestedAnnotations == OnTheFlyAnnotationType.None)
            {
                await this._dataResource.GetGenericDataAsync(flowRef, key, providerRef);
            }

            using (new OnTheFlyAnnotationScope(requestedAnnotations))
            {
                await this._dataResource.GetGenericDataAsync(flowRef, key, providerRef);
            }
        }

        /// <summary>
        /// Gets the generic data asynchronous for REST 2.0.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="agencyID">The agency(ies).</param>
        /// <param name="resourceID">The resource id(s).</param>
        /// <param name="version">The version(s).</param>
        /// <param name="key">The key(s).</param>
        /// <returns>The <see cref="Message" />.</returns>
        public Task GetGenericDataV2Async(string context, string agencyID, string resourceID, string version, string key)
        {
            throw new System.NotImplementedException();
        }
    }
}