﻿// -----------------------------------------------------------------------
// <copyright file="NsiWsSubmitFactory.cs" company="EUROSTAT">
//   Date Created : 2017-11-22
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Ws.StructurePersistance.Factory
{
    using Estat.Sdmxsource.Extension.Factory;
    using System;
    using Estat.Sdmxsource.Extension.Engine;
    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStore.Store.Factory;
    using Estat.Sri.MappingStoreRetrieval.Factory;
    using estat.sri.ws.auth.generic;
    using Estat.Sri.Mapping.Api.Utils;
    using System.Threading;

    public class NsiWsSubmitFactory : IStructureSubmitFactory
    {
        /// <summary>
        /// The service key
        /// </summary>
        private const string ServiceKey = "MappingStoreRetrieversFactory";

        /// <summary>
        /// The mapping store based factory
        /// </summary>
        private readonly IStructureSubmitFactory _sriFactory;
        private readonly IConnectionStringBuilderManager _connectionStringBuilderManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="NsiWsSubmitFactory"/> class.
        /// </summary>
        /// <param name="connectionStringBuilderManager">The configuration store manager.</param>
        /// <exception cref="ArgumentNullException">configurationStoreManager</exception>
        public NsiWsSubmitFactory(IConnectionStringBuilderManager connectionStringBuilderManager)
        {
            if (connectionStringBuilderManager == null)
            {
                throw new ArgumentNullException(nameof(connectionStringBuilderManager));
            }

            _sriFactory = new StructureSubmitMappingStoreFactory(connectionStringBuilderManager.Build);
            MappingStoreIoc.Register<RetrievalEngineContainerFactory>(ServiceKey);
            _connectionStringBuilderManager = connectionStringBuilderManager;
        }

        /// <summary>
        /// Gets the engine for the specific store identified by <paramref name="storeId" />.
        /// </summary>
        /// <param name="storeId">The store identifier.</param>
        /// <returns>The <see cref="T:Estat.Sdmxsource.Extension.Engine.IStructureSubmitterEngine" /> if <paramref name="storeId" /> is supported; otherwise null.</returns>
        public IStructureSubmitterEngine GetEngine(string storeId)
        {
            if (storeId == null)
            {
                storeId = _connectionStringBuilderManager.Build()?.Name;
            }

            SetServiceKeyForIoc();
            return _sriFactory.GetEngine(storeId);
        }

        /// <summary>
        /// Sets the <see cref="System.Threading.AsyncLocal"/> <see cref="MappingStoreIoc.ServiceKey"/>
        /// </summary>
        private static void SetServiceKeyForIoc()
        {
            MappingStoreIoc.ServiceKey = ServiceKey;
        }
    }
}
