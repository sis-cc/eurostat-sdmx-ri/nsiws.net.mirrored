#!/bin/bash

set -u 
set -e
#set -x

# modifies the version in Directory.Build.Props and the RI only versions in PackageReference in */*.csproj

if [[ $# < 1 ]]; then
    echo "usage:"
    echo "$0 (major|minor|patch) <local repo containing only SDMXRI packages> "
    echo "Examples"
    echo "$0 minor '/absolute/path/to/nuget/repo/folder/' "
    exit -1
fi

# mandatory parameters
limitUpdate="$1"

# variables
props=Directory.Build.props

function isPreRelease() {
    local props=$1
    if grep -q '<VersionSuffix>[^<]\+<.VersionSuffix>' "$props"; then
      echo -n ' --pre-release Always '
    else
      echo -n ' --pre-release Never '
    fi
}

function getUpperVersionLimitParam() {
  local limit="${1,,}" # to lower case
  case "$limit" in
    minor)
      echo -n ' --version-lock major '
      ;;
    patch)
      echo -n ' --version-lock minor '
      ;;
     *)
       echo -n ''
       ;;
   esac
}

if [ ! -f "${props}" ]; then
    echo "No "${props}" found"
    exit -1
fi

includePreRelease=$(isPreRelease "$props")
highestVersion=$(getUpperVersionLimitParam $limitUpdate)

export PATH="$PATH:$HOME/.dotnet/tools"
if [[ ! -f /tmp/.dotnet-outdated-installed ]]; then
  if dotnet tool list -g | grep -q dotnet-outdated-tool; then 
    echo "OK outdated installed"
  else 
    dotnet tool install --global dotnet-outdated-tool
  fi
  touch /tmp/.dotnet-outdated-installed 
fi

# We might have different solution files e.g. one for dev and for testing
for slnFile in *.sln; do
    # First we restore everything but without looking for dependencies for dependencies
    # (although at least recent documentation doesn't mention package but project..)
    dotnet restore --no-dependencies -p:NoWarn=NU1605 "${slnFile}" 
done

# Filter in only outpackages as we cannot limit dotnet outdated to use specific sources
# TODO fixme, NUGET REPO can be a url
dotnet nuget disable source nuget
ourpkg=" -inc estat.sri -inc estat.sdmx -inc org.sdmx -inc xobjects -inc middleware "
dotnet-outdated $includePreRelease $highestVersion $ourpkg -n -u NSIWebServices.sln
dotnet nuget enable source nuget

if [[ -n $(git status -s -- Directory.Packages.props) ]]; then
  git add Directory.Packages.props
  git commit -m"Updated deps to latest $limitUpdate"
fi
