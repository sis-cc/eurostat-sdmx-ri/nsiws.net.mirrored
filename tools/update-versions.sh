#!/bin/bash

set -u 
set -e
#set -x

# modifies the version in Directory.Build.Props 

if [[ $# < 1 ]]; then
    echo "usage:"
    echo "$0 <version>"
    echo "Examples"
    echo "$0 1.4.5"
    exit -1
fi

# mandatory parameters without any build metadata
version="${1%%+*}"

# variables
props=Directory.Build.props

function updateCurrentProjectVersion() {
    local versionPrefix="$1"
    local props="$2"
    sed -i.bak \
        -e "s@\(<VersionPrefix>\)[0-9.]\+\(</VersionPrefix>\)@\1${versionPrefix}\2@" \
        "${props}"
}

# Update or insert the versionSuffix
# Not using xml tools as Windows GIT bash doesn't have them
function updateSuffix() {
    local versionSuffix="$1"
    local props="$2"
    if grep -q '<VersionSuffix>[^<]*<.VersionSuffix>' "$props"; then
      # VersionSuffix exists in props
      sed -i.bak \
        -e "s@\(<VersionSuffix>\)[^<]*\(</VersionSuffix>\)@\1${versionSuffix}\2@" \
        "${props}"
    else
      #get spaces before <VersionPrefix>
      spaces=$(sed -n -E 's/^(\s*)<VersionPrefix>[0-9.]+<\/VersionPrefix>$/\1/p' "$props") 
      # VersionSuffix doesn't exist
      sed -i.bak \
        -e "/[0-9]<\/VersionPrefix>/a\\${spaces}<VersionSuffix>${versionSuffix}</VersionSuffix>" \
        "${props}"
    fi
}
# Remove the versionSuffix
# Not using xml tools as Windows GIT bash doesn't have them
function removeSuffix() {
    local props="$1"
    if grep -q '<VersionSuffix>[^<]*<.VersionSuffix>' "$props"; then
      # VersionSuffix exists in props
      sed -i.bak \
        -e "/<VersionSuffix>/d" \
        "${props}"
    fi
}
if [ ! -f "${props}" ]; then
    echo "No "${props}" found"
    exit -1
fi

versionPrefix=${version%%-*}

updateCurrentProjectVersion "$versionPrefix" "$props"
if [[ $version == *-* ]]; then
  versionSuffix=${version#*-}
  updateSuffix "$versionSuffix" "$props"
else
  versionSuffix=''
  removeSuffix "$props"
fi

if [[ -n $(git status -s -- "$props") ]]; then
  git add "$props"
  git commit -m"Pump versionPrefix to $versionPrefix and suffix $versionSuffix"
fi
