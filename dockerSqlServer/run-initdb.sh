#wait for the SQL Server to come up
echo "**** will wait to make sure sql server has started before running sql initialization scripts"
sleep 30s
if [ -d /docker-entrypoint/initdb/sql ]; then
	for filename in /docker-entrypoint/initdb/sql/*.sql; do
		#this for loop will also return the folder name itself which we skip
	 [ -e "$filename" ] || continue

		echo "**** will execute sql script: $filename"
		/opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P $SA_PASSWORD -d master -i "$filename" -v userplaceholder="$MAUSER" passwordplaceholder="$MAPASSWORD"
	
		rm "$filename"
	done
fi