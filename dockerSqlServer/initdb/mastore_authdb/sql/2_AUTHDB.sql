--RENAME EXISTING DATABASE AUTHDB_SCRATCH LOADED FROM authdb_scratch.tar.gz
USE master;  
GO  
ALTER DATABASE AUTHDB_SCRATCH SET SINGLE_USER WITH ROLLBACK IMMEDIATE
GO
ALTER DATABASE AUTHDB_SCRATCH MODIFY NAME = AUTHDB ;
GO  
ALTER DATABASE AUTHDB SET MULTI_USER
GO

--CREATE USER
USE AUTHDB;
GO
DROP USER IF EXISTS [$(userplaceholder)];
CREATE USER [$(userplaceholder)] FOR LOGIN [$(userplaceholder)];
ALTER ROLE [db_datareader] ADD MEMBER [$(userplaceholder)];
ALTER ROLE [db_datawriter] ADD MEMBER [$(userplaceholder)];
ALTER ROLE [db_ddladmin] ADD MEMBER [$(userplaceholder)];
ALTER ROLE [db_owner] ADD MEMBER [$(userplaceholder)];
GO