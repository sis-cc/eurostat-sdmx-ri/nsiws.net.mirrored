#!/bin/sh

if [ -d /docker-entrypoint/initdb/data ]; then
  chmod 777 /docker-entrypoint/initdb/data 
  for filename in /docker-entrypoint/initdb/data/*.tar.gz
  do
    if [ ! -f "${filename}" ]; then
      break
    fi
    echo "will extract $filename"
    tar xzvf "$filename" -C /docker-entrypoint/initdb/data/
    rm "$filename"
  done
fi
