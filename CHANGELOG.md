# HISTORY

## BREAKING CHANGE 9.x

- Retriever Factory Plugin API has changed. All Mutable Search Managers where replaced the new `ICommonSdmxObjectRetrievalManager` and `ICommonSdmxObjectRetrievalManagerSoap20`.
- Data queries startPeriod/endPeriod now assume the whole period e.g. 2022-05 will become in annual 2023. Previously it would converted to 2022.

## BREAKING CHANGE 9.9.0+ MariaDB driver

It affects only existing installations that use MariaDB either as Auth DB or Mastore DB. DDB are not affected.
Due to missing support for MariaDB 10.11 during development, the application now uses `MySqlConnector` driver instead of Oracle's `MySql.Data.Client`.
Mapping store and Auth Db configuration that uses the old MariaDB driver needs to be removed and recreated.

## BREAKING CHANGE 9.10.0+ SQL Server

The minimum supported SQL Server version is 2016.
In addition the compatibility level of the database must be at least `130`.
More information can be found at [Microsoft](https://learn.microsoft.com/en-us/sql/relational-databases/databases/view-or-change-the-compatibility-level-of-a-database?view=sql-server-ver16)

For example if the mapping store database is named `MASTORE` then as SQL Server administrator run:

```sql
ALTER DATABASE MASTORE SET COMPATIBILITY_LEVEL = 130
```

## nsiws.net v9.12.0 (2024-06-28) (MSDB 7.1.2)

### Details v9.12.0

1. Updated registries endpoints
1. Added mTLS authentication ( password-less) for MariaDB
1. Added feature that allows the user to download artefacts from SOAP 2.0 registry endpoint with all cross references

### Tickets v9.12.0

The following new features added:

- SDMXRI-2441 MAWEB: Dataflow import from registry
- SDMXRI-2347 mTLS authentication ( password-less) for MariaDB
- SDMXRI-2422 SDMX-CSV .NET improve support for array values for measures

The following bugs have been corrected:

- SDMXRI-2411 minor issues found in v9.11.0-(2024-04-25)
- SDMXRI-2418 MAWEB: REST endpoint not recorded correctly

## nsiws.net v9.11.0 (2024-04-25)  (MSDB 7.1.1)

### Details v9.11.0

1. Compatibility with Oracle 19c when importing item schemes. Fix a store procedure that used the JSON data type that is available from Oracle 21c.
1. Bugfixes related to SDMX-ML 3.0.0
   1. Valid to/from not written in SDMX-ML 3.0.0 files
   1. Organisation schemes, Contact URI not parsed/written correctly

### Tickets v9.11.0

The following new features added:

- SDMXRI-2395: MAWEB NET: Message improvement when selected MS is not initialized

The following bugs have been corrected:

- SDMXRI-2394: MAWEB NET: ORACLE 19c
- SDMXRI-2397: SDMXRI minor issues found in v9.10.0-(2024-01-04)

## nsiws.net v9.10.0 (2024-04-01) (MSDB 7.1)

### Details v9.10.0

1. Improve support for metadata attribute usage
1. Improve support SDMX-CSV 2.0.0 and multiple measures
1. Disable the MSD missing exception when querying data for a data structure that references an MSD via an annotation.
1. Remove unused methods from the Translator interface
1. Use a hybrid more performant way to import codelists and other item schemes.The item names, description and annotations are now stored as JSON. This change allows bulk insert of items.
1. Support storing/retrieving SDMX 3.0.0 metadata attribute usage
1. Many bug fixes
   1. Caching resulted to incorrect references when the imported file contained artefacts with the same id.
   1. NSI CLient couldn't download SDMX 2.0 Cross Sectional data in some cases
   1. Could not created Mapping Sets for Cross Sectional DSD in some cases

### Tickets v9.10.0

The following new features added:

- SDMXRI-2375: RE: Very impacting issue in NSI_WS/MA_WS 8.15.1 and higher (ISTAT)
- SDMXRI-2379: SDMX-CSV 2.0.0 Fix support for Measures (.NET) (SDMX3.0,SDMXCSV2.0.0)
- SDMXRI-2197: SDMXRI support storing/retrieving SDMX 3.0.0 DSD Metadata attribute usage info (.net) (DataStructure,SDMX3.0)
- SDMXRI-1573: sending files via the SDMXRI to Edamis (on-hold)
- SDMXRI-2286: SDMX RI implement import codelists performance improvements
- SDMXRI-2197: SDMXRI support storing/retrieving SDMX 3.0.0 DSD Metadata attribute usage info (.net) (DataStructure,SDMX3.0)

The following bugs have been corrected:

- SDMXRI-2364: MAWEB .NET- Missing dimensions/data structures in mapping set, for CrossSectional dataflow
- SDMXRI-2308: Nsi-Client .NET- CrossSectional file is downloaded as StructureSpecific
- SDMXRI-2365: NSI_WS 8.16.0: Bug on updating the not empty text of the annotations in the dataflows
- SDMXRI-2356: SDMXRI minor issues found in v9.9.0-(2024-02-09)

## nsiws.net v9.9.0 (2024-02-09)

### Details v9.9.0

1. Updated vulnerable 3rd party dependencies to their latest version
1. Support for MariaDB 10.11 with new driver `MySqlConnector`
1. Support for parsing Data Consumer Schemes from SDMX 3.0.0 XML structure
1. Improved performance for retrieving and storing artefacts.
1. Many bugfixes discovered while testing SDMXRI

### Tickets v9.9.0

The following new features added:

- SDMXRI-2355: Update vulnerable 3rd party dependencies
- SDMXRI-2255: Update MariaDB version support to LTS 10.11
- SDMXRI-2288: SDMXRI WS performance improvements (SDMXRI)
- SDMXRI-2330: MAWEB and Windows authentication
- SDMXRI-2099: SdmxSource .NET DataConsumerScheme SDMX-ML 3.0.0 reader (DataConsumerScheme,SDMX-ML-Reader,SDMX3.0)

The following bugs have been corrected:

- SDMXRI-2319: SDMXRI minor issues found in v9.8.0-alpha (2023-12-14)

## nsiws.net v9.8.0 (2023-12-15)(MSDB 7.0.4)(AUTHDB v1.0)

### Details v9.8.0

The following new features added:

- SDMXRI-2098: SdmxSource .NET DataConsumerScheme SDMX-ML 3.0.0 writer

## nsiws.net v9.7.0 (2023-12-05)(MSDB 7.0)

### Details v9.7.0

The following new features added:

- SDMXRI-1890: SDMX RI retriever/persist Data Content Constraint measures
- SDMXRI-1891: SDMX RI retriever/persist Data Content Constraint attributes
- SDMXRI-2078: SdmxSource .NET AgencyScheme SDMX-ML 3.0.0 reader
- SDMXRI-2077: SdmxSource .NET AgencyScheme SDMX-ML 3.0.0 writer
- SDMXRI-2123: SdmxSource .NET OrganisationUnitScheme SDMX-ML 3.0.0 reader
- SDMXRI-2122: SdmxSource .NET OrganisationUnitScheme SDMX-ML 3.0.0 writer
- SDMXRI-2096: SdmxSource .NET DataConstraint SDMX-ML 3.0.0 reader
- SDMXRI-2095: SdmxSource .NET DataConstraint SDMX-ML 3.0.0 writer


## nsiws.net v9.6.0 (2023-11-03) (MSDB 7.0) (AUTHDB v1.0)

The following new features added:

- SDMXRI-2101: SdmxSource .NET DataProviderScheme SDMX-ML 3.0.0 writer
- SDMXRI-2084: SdmxSource .NET CategoryScheme SDMX-ML 3.0.0 reader
- SDMXRI-2083: SdmxSource .NET CategoryScheme SDMX-ML 3.0.0 writer
- SDMXRI-2126: SdmxSource .NET ProvisionAgreement SDMX-ML 3.0.0 reader
- SDMXRI-2125: SdmxSource .NET ProvisionAgreement SDMX-ML 3.0.0 writer
- SDMXRI-2206: Update SDMX-CSV v2 and SDMX-JSON v2 to use 3.0.0 DataWriterEngine methods
- SDMXRI-1902: SdmxSource SDMX 2.0.0 JSON structure format parser (.NET)

## nsiws.net v9.5.0-alpha (2023-07-13) (AUTHTDB 1.0 )

- Included changes from 8.18.1

## nsiws.net v8.18.1 (2023-08-17) (MSDB 6.20)

### Details v8.18.1

1. Reverted request for HTTP 304 to be returned as it doesn't allow a body
1. Upgrade to new version SQL Server driver due to a security issues
1. Fix a regression related to EMBARGO_TIME for DSD that do not have this attribute

### Tickets v8.18.1

The following new features were reverted:

- SDMXRI-1869: Instead of HTTP 422 return HTTP 304 for non-updated artefacts where no differences were found (OECD)

The following new features were amended:

- SDMXRI-2180: Restricted access to confidential or embargoed data (OECD,PULL_REQUEST)

The following new features added:

- SDMXRI-2212: Upgrade System.Data.SqlClient to most recent version (OECD,PULL_REQUEST)

## nsiws.net v8.18.0 (2023-08-01) (MSDB 6.20) (AUTHDB v1.0)

### Details v8.18.0

1. Use [central package management](https://learn.microsoft.com/en-us/nuget/consume-packages/central-package-management), versions of dependencies from all projects are stored in `Directory.Packages.props`.
1. Use HTTP Code 304 in case there were no differences when attempting to update an artefact
1. HCL improvements
1. Optional validation of the `EMBARGO_TIME` DSD attribute when present
1. Optionally restrict access of embargoed or confidential data
1. Available constraint bugfixes

### Tickets v8.18.0

The following new features added:

- SDMXRI-2161: Publish pre-release nuget packages to nuget.org (OECD)
- SDMXRI-2180: Restricted access to confidential or embargoed data (OECD,PULL_REQUEST)
- SDMXRI-2182: Implement SDMX-CSV 2.0 `labels=name` option (OECD,PULL_REQUEST)
- SDMXRI-1869: Instead of HTTP 422 return HTTP 304 for non-updated artefacts where no differences were found (OECD)
- SDMXRI-2187: The codes of a HCL should be returned in the order of their creation (OECD,PULL_REQUEST)

The following bugs have been corrected:

- SDMXRI-1968: "Procedure or function 'INSERT_HCL_CODE' expects parameter '@p_lcd_id', which was not supplied." error when submitting HCL
- SDMXRI-2188: Critical vulnerability in System.Text.Encodings.Web (CVE-2021-26701)
- SDMXRI-2191: "500 Internal Server Error" for availableconstraint query when no data
- SDMXRI-2183: Available CC wrongly restrains time range by time period selection

## nsiws.net v8.17.0 (2023-06-07) (MSDB 6.20) (AUTHDB v1.0)

### Details v8.17.0

1. Generated warning for duplicate concepts or codes in Cube Regions of Content Constraints
1. SDMX-JSON data writer enhancements
   1. allow use of special characters (`~`, `*`) in response written by the writer
   1. Changes in HCL support in data
1. Align with final standard approach agreed in SDMX-TWG.

|Format|Numeric types: float, double|Text types|
|------|----------------------------|----------|
|XML|NaN|#N/A|
|JSON|NaN|#N/A|
|CSV|NaN|#N/A|

- Change the NSIWS data-writers to return these special values accordingly.
  - For Text types there should not be any special treatement because they should be already stored and returned from the data database as {{#N/A}}
  - For float and double, make sure that {{NaN}} can be parsed accordingly to all three formats. Specially to JSON.
- Remove from NSIWS the config. setting [useIntentionallyMissingMeasures|https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/blob/develop/config/Properties.json] which is not used anymore

### Tickets v8.17.0

The following new features added:

- SDMXRI-2163: NaN managed as intentionally missing value (OECD,PULL_REQUEST)
- SDMXRI-2166: Update  Google.Cloud.Logging.Log4Net nuget
- SDMXRI-2151: Configure new default format URL option to generate a csv file with labels (OECD,PULL_REQUEST)
- SDMXRI-2028: Expecting error when uploading content constraint with duplicated codes in a repeated concept
- SDMXRI-2164: Update json 2.0 writer to use special characters (~,*) in metadata response (OECD,PULL_REQUEST)
- SDMXRI-2157: HCL parents output in SDMX-json,  use first localised HCL definition from AnnotationText and then non-localised HCL from AnnotationTitle, followup of SDMXRI-2136 (OECD,PULL_REQUEST)
- SDMXRI-2155: add parameter to add "-" and '*' as wildcard symbols to DataQueryImpl filter  (OECD,PULL_REQUEST)
- SDMXRI-1924: Add a new HTTP X-Level option to get the referential metadata only at the current level (2021.0333-QTM5,OECD,PULL_REQUEST)

The following bugs have been corrected:

- SDMXRI-2143: generated count sql query should include IS NULL check when optional attribute has constraint (OECD,PULL_REQUEST)

## nsiws.net v8.16.0 (2023-05-04)  (MSDB v6.20) (AUTHDB v1.0)

### Details v8.16.0

1. Merged changes from OECD related to:
   1. Faster import Content Constraints
   1. Make `updatedAfter` work with content constrain filtering at attributes
   1. In OECD  for referential metadata the string `-` is used as an intentionally missing value for dimension (normal and time dimension) and has a different treatment to an empty string when imported. But during writing the passed string for a Time dimension was tried to be parsed as SDMX time and was throwing an exception. This change is to avoid an exception and allow the minus symbol the same as an empty string.
   1. SDMX-JSON 2.0.0 better handle datasets with delete action and metadata attribute usage, e.g. in data requests with `updatedAfter`.
   1. Support a special annotation that emulates SDMX 3.0.0 Hierarchical association that allows including hierarchical codelist in SDMX-JSON data
1. Fix a bug where allowed content constraints were returned as actual when requested as stubs

### Tickets v8.16.0

The following new features added:

- SDMXRI-2147: UpdatedAfter data queries should return higher level attributes when filtering is applied  (OECD,PULL_REQUEST)
- SDMXRI-2137: Improve performance of function to save the actual content constraint (OECD,PULL_REQUEST)
- SDMXRI-2146: Treat "-" as an empty string when parsing datetime with DateUtil (OECD,PULL_REQUEST)
- SDMXRI-2141: Support metadata output in json/csv with updatedAfter parameter (multiple datasets) (OECD,PULL_REQUEST)
- SDMXRI-2138: write deleted observations as single datasets when updatedAfter parameter is used (OECD,PULL_REQUEST)
- SDMXRI-2136: SDMX-JSON data: Return definition of parents when child items have data, use hierachical codelist through annotation definition. Addon to SDMXRI-2037 (OECD,PULL_REQUEST)
- SDMXRI-2140: UpdatedAfter query filter for metadata queries (OECD,PULL_REQUEST)
- SDMXRI-2143: generated count sql query should include IS NULL check when optional attribute has constraint
- SDMXRI-2045: Retrieve empty textual intentionally missing measures in xml

The following bugs have been corrected:

- SDMXRI-2046: An allowed content constraint is returned as "Actual" when retrieving as stub
- SDMXRI-2051: Json writer throws an error for DSD with a single non-time dimension (OECD,PULL_REQUEST)

## nsiws.net v9.4.0-alpha (2023-07-13)

### Details v9.4.0-alpha

1. Support SDMX 3.0.0 MeasureRelationship
1. bugfixes
   1. Could not save Mapping Set for final Dataflow on databases where empty value is different than `null` (Mariadb and SQL Server)
   1. fix generating sql for structure version requests; improve handling of multiple versions and wildcards
   1. Queries run outside transaction
1. Support for Categorisation in SDMX-ML 3.0.0 format
   1. It uses an approach ported from sdmx-core

### Tickets v9.4.0-alpha

The following new features added:

- SDMXRI-2173: MeasureRelationship support in SDMXRI (.NET) (DataStructure,SDMX3.0)
- SDMXRI-2081: SdmxSource .NET Categorisation SDMX-ML 3.0.0 reader (Categorisation,SDMX-ML-Reader,SDMX3.0)
- SDMXRI-2080: SdmxSource .NET Categorisation SDMX-ML 3.0.0 writer (Categorisation,SDMX-ML-Writer,SDMX3.0)

The following bugs have been corrected:

- SDMXRI-2176: MAWEB.NET : save a mapping set under SQL SERVER MS
- SDMXRI-2174: SDMXRI minor issues found in v9.3.0-alpha (2023-06-

## nsiws.net v9.3.0-alpha (2023-06-16)

### Details v9.3.0-alpha

1. SDMX JSON 2.0 Structure support
1. Test Client legacy CSV support
1. Return warning when duplicate codes found in CubeRegion
1. Many bug fixes

### Tickets v9.3.0-alpha

The following new features added:

- SDMXRI-1785: SDMX REST 2.0.0 API  support SDMX 2.0.0 JSON structure format (SDMX3.0)
- SDMXRI-1880: Test Client in MAWEB - Old CSV support (2022.0146–QTM3,PUBLIC_2023)
- SDMXRI-1901: SdmxSource support SDMX 2.0.0 JSON structure format writer (.net) (SDMX3.0)

The following bugs have been corrected:

- SDMXRI-2162:  SDMXRI minor issues found in v9.2.0-alpha.2 (2023-05-18)
- SDMXRI-2028: Expecting error when uploading content constraint with duplicated codes in a repeated concept
- SDMXRI-2158: Dataset and DDB Connection issues in MAWEB.NET 9.2.0-alpha

## nsiws.net v9.2.0-alpha (2023-05-19) (MSDB 7.0-alpha.4)

### Details v9.2.0-alpha

1. Specific changes from 8.16.0 merged
1. More work towards SDMX REST 2.0.0 Availability
1. Availability for SDMX 2.1 working again
1. Many bugfixes
1. Bugixes in MSDB 7 schema

### Tickets v9.2.0-alpha

The following new features added:

- SDMXRI-1798: SDMX REST 2.0.0 API  availability support new query parameter "c" (SDMX3.0)
- SDMXRI-1794: SDMX REST 2.0.0 API  support the new URL path for availability (SDMX3.0)
- SDMXRI-1925: Available Constraint (SDMX 2.1) / availability 3.0 / special partial requests (SOAP 2.0) (SDMX3.0)

## nsiws.net v9.1.0-alpha (2023-04-07) (MSDB v7.0-alpha)

### BREAKING CHANGE MSDB7

- Retriever Factory Plugin API has changed. All Mutable Search Managers where replaced the new `ICommonSdmxObjectRetrievalManager` and `ICommonSdmxObjectRetrievalManagerSoap20`.
- Data queries startPeriod/endPeriod now assume the whole period e.g. 2022-05 will become in annual 2023. Previously it would converted to 2022.

### Known issues

### Details v9.1.0-alpha

1. Includes all changes and fixes from the public release versions (8.13.0, 8.14.0, 8.15.0 and 8.15.1)
1. SDMXRI Data Registration work with the new MADB7
1. Implicit wildcards for calls to rest v2 when dimensions are missing fix
1. support for SDMX 3.0.0 XML structure format
1. Implemented SDMX REST 2.0.0 API data support for "attributes", "c" and "measures" parameters
1. Implemented referencepartial support for structure request
1. Increased SDMX 3.0.0 and SDMX REST 2.0.0 compliance
1. More existing features made to work with the wildcard mechanism in MADB7
1. Note the 7.0 MSDB has been updated, so in order to test the MSDB needs to be initialized again or upgrade from a version before 7.0

### Tickets v9.1.0-alpha

- SDMXRI-1840: SDMX 3.0.0 - MADB 7/MAWS migration Retrieve/Persist Entities (2021.0333-QTM4,Package_1_SDMX3.0,SDMX3.0)
- SDMXRI-1839: SDMX 3.0.0 - MADB 7/MAWS migration - Retrieve/Persist SDMX artefacts (2021.0333-QTM4,Package_1_SDMX3.0,SDMX3.0)
- SDMXRI-2044: Merge changes from 8.15.1 to 9.0.0-alpha in SDMXRI (2022.0146–QTM3)
- SDMXRI-2043: Additional testing for Public release Mapping Assistant 8.x
- SDMXRI-1990: Update 3rd party dependencies for the public release (.NET) (2022.0146–QTM1)
- SDMXRI-1790: SDMX REST 2.0.0 API  data support new query parameter measures (2022.0146–QTM1,SDMX3.0)
- SDMXRI-1788: SDMX REST 2.0.0 API  data support new query parameter attributes (2022.0146–QTM2,SDMX3.0)
- SDMXRI-1789: SDMX REST 2.0.0 API  data support new query parameter "c" (2021.0333-QTM5,SDMX3.0)
- SDMXRI-1784: SDMX REST 2.0.0 API  support SDMX 3.0.0 XML structure format (2021.0333-QTM5,SDMX3.0)
- SDMXRI-1910: SDMXRI SDMX 3.0 Support for DSD 3.0 Measures (2022.0146–QTM3,SDMX3.0)
- SDMXRI-1920: SDMXRI SDMX 3.0.0  referencepartial detail in Structure requests (Package_1_SDMX3.0,SDMX3.0)
- SDMXRI-2049: SdmxSource: Conversion between SDMX time periods not always compliant

### Merged nsiws.net v8.15.1 (2023-02-22) (MSDB v6.20) (AUTHDB v1.0)

#### Backwards compatibility v8.15.1

- Codelists referenced with available constraints REST API may contain more codes due to including indirect parents.
- JSON Data v2 output changes, include parent codes with data

#### Details v8.15.1

1. Add a conditional parameter to JsonWriter 2.0 and NSI to include parent codes without data , that have children with data. Use parent-child relation from a hierarchical codelist, that might be attached to DSD/Dataflow through an annotation.
1. Codelists retrieved with Availability REST API as a reference have the full parent hierarchy.

#### Tickets v8.15.1

The following new features added:

- SDMXRI-2037: SDMX-JSON data: Return definition of parents when child items have data, use hierachical codelist through annotation definition. (OECD,PULL_REQUEST)
- SDMXRI-2027: Update MappingSet/Dataset to allow storing different SQL queries for different dataset actions (Updated,Deleted,Inserted,Active)  (OECD,PULL_REQUEST)
- SDMXRI-2047: NSI WS: issue about available content constraints (ISTAT)

The following bugs have been corrected:

- SDMXRI-2041: MAWEB GUI: Oracle connectionstring list

### Merged nsiws.net v8.15.0 (2023-01-26) (MSDB v6.20) (AUTHDB v1.0)

#### Details v8.15.0

1. Support dimensions with missing values in json format
1. Text format data type `Count` should be considered numeric data type.
1. Ignore duplicate key/attribute values entries in Cube Regions
1. For the SDMX REST `updatedAfter` feature; in addition to existing  support different SQL Queries per dataset action. It requires a Dataset with `EditorType` equal to `MultiQuery` and a JSON message containing the SQL Queries. (Experimental)

#### Tickets v8.15.0

The following new features added:

- SDMXRI-2025: Retrieve intentionally missing values (OECD,PULL_REQUEST)
- SDMXRI-2023: Support dimensions with NULL values in JSON writer (OECD needs to output wildcarded observations) (OECD,PULL_REQUEST)
- SDMXRI-2029: Update textType Count as numeric (OECD,PULL_REQUEST)
- SDMXRI-2027: Update MappingSet/Dataset to allow storing different SQL queries for different dataset actions (Updated,Deleted,Inserted,Active)  (OECD,PULL_REQUEST)

The following bugs have been corrected:

- SDMXRI-2028: Expecting error when uploading content constraint with duplicated codes in a repeated concept

### Merged nsiws.net v8.14.0 (2022-12-21) (MSDB v6.20) (AUTHDB v1.0)

Target Mapping Store v6.20.1 which includes the fix for MariaDB 10.6 `OFFSET` keyword.

### Merged nsiws.net v8.13.0 (2022-11-29) (MSDB v6.20) (AUTHDB v1.0)

#### Details v8.13.0

1. Target Mapping Store database 6.20
1. Update 3rd party dependencies, includes vulnerability fixes and updated DB drivers
1. Improve complete stub support via REST
1. Fix issues related to User managements where Mapping Store is on Oracle
1. Data Queries : Generate `IN` clauses instead of `OR` when possible
   1. Equals operator is used, the default in SOAP 2.1 and REST v2 `c` and the only option in SOAP 2.0 and REST key
1. Support multiple multiple actions in SDMX-CSV
1. SDMX-CSV can use now culture specific decimal separator
1. Extension HTTP header `X-Range` for Range requests.
1. Update documentation for:
   1. Supported database versions see [Supported databases](doc/SupportedDatabases.md)

#### Tickets v8.13.0

The following new features added:

- SDMXRI-1970: Culture specific column and decimal separators in sdmx csv (OECD,PULL_REQUEST)
- SDMXRI-1990: Update 3rd party dependencies for the public release (.NET)
- SDMXRI-1982: Add alternative usage of 'X-Range' header for range requests (OECD,PULL_REQUEST)
- SDMXRI-1954: Update openid configuration with conditional audience for token validation
- SDMXRI-1866: MariaDB 10.6.\* not compatible with MAWEB
- SDMXRI-2006: Suuport multiple startDatalow calls and inlude Action column in csv2.0 to support multiple actions (Append, Delete) (OECD)
- SDMXRI-1878: Add errors array in json v1 and v2 (OECD,PULL_REQUEST)
- SDMXRI-1991: XML reader yields the datasets twice when there is only one empty in the data file (OECD)
- SDMXRI-1966: RI WS, performance issue

The following bugs have been corrected:

- SDMXRI-1969: NSIWS: Authentication problem
- SDMXRI-1947: detail=allcompletestubs parameter doesn't return all related artefact's annotations
- SDMXRI-1989: Issue when adding new mapping store
- SDMXRI-1967: MAWEB: User management error
- SDMXRI-1972: Incorrect timezone usage in SDMX-JSON

## nsiws.net v9.0.0-alpha.2 (2022-11-03) (MSDB v7.0-alpha2)

### Details v9.0.0-alpha.2

1. SDMX REST v2.0.0 (SDMX 3.0.0)
   1. Support for query parameter `attributes`
   1. Support for query parameters `c`
   1. Plugin to output SDMX 3.0.0 XML in NSIWS
1. SDMX 3.0.0 various fixes improvements, programming interfaces updates
1. SDMX-CSV use culture info to determine the decimal separator
1. SDMX-JSON didn't use the correct timezone

### Tickets v9.0.0-alpha.2

The following new features added:

- SDMXRI-1789: SDMX REST 2.0.0 API  data support new query parameter "c" (SDMX3.0)
- SDMXRI-1840: SDMX 3.0.0 - MADB 7/MAWS migration Retrieve/Persist Entities (Package_1_SDMX3.0,SDMX3.0)
- SDMXRI-1839: SDMX 3.0.0 - MADB 7/MAWS migration - Retrieve/Persist SDMX artefacts (Package_1_SDMX3.0,SDMX3.0)
- SDMXRI-1784: SDMX REST 2.0.0 API  support SDMX 3.0.0 XML structure format (SDMX3.0)
- SDMXRI-1919: SDMX 3.0.0 Update non-final DSD (Package_1_SDMX3.0,SDMX3.0)
- SDMXRI-1921: SDMX 3.0.0 Update non-final Artefacts (Package_1_SDMX3.0,SDMX3.0)

The following bugs have been corrected:

- SDMXRI-1972: Incorrect timezone usage in SDMX-JSON

## nsiws.net v9.0.0-alpha (2022-09-30) (MSDB v7.0-alpha)

### BREAKING CHANGE MSDB7 v9.0.0-alpha

- Retriever Factory Plugin API has changed. All Mutable Search Managers where replaced the new `ICommonSdmxObjectRetrievalManager` and `ICommonSdmxObjectRetrievalManagerSoap20`.

### Known issues v9.0.0-alpha

1. Updating artefacts doesn't work
1. Importing Content constraints will fail
1. Data availability doesn't always work
1. reference partial is not working
1. Authorization not always working

### Details v9.0.0-alpha

1. Alpha release that targets the MSDB 7.0
1. Major changes in Transcoding entities especially Time Transcoding

### Tickets v9.0.0-alpha

The following new features added:

- SDMXRI-1839: SDMX 3.0.0 - MADB 7/MAWS migration - Retrieve/Persist SDMX artefacts (Package_1_SDMX3.0,SDMX3.0)
- SDMXRI-1840: SDMX 3.0.0 - MADB 7/MAWS migration Retrieve/Persist Entities (Package_1_SDMX3.0,SDMX3.0)

## nsiws.net v8.12.2 (2022-09-27) (MSDB v6.19) (AUTHDB v1.0)

### Details v8.12.2

1. Improvements related to REST calls
   1. for the generating zip files
   1. new format option for CSV files
1. Bug fix when recording actions without an authenticated user
1. Bugfixes

### Tickets v8.12.2

The following new features added:

- SDMXRI-1936: Configure new default format URL option to generate a csv file (OECD,PULL_REQUEST)

The following bugs have been corrected:

- SDMXRI-1937: Correct Content-Disposition & Content-Type headers in data V2 responses and so also sub-folder issue in zipped responses
- SDMXRI-1935: Exception at meta data related SQL queries when content constraint has restriction on an attribute
- SDMXRI-1943: Fix UserAction management when Thread.CurrentPrincipal.Identity.Name is empty

## nsiws.net v8.0.14 (2022-06-02) (MSDB v6.10) (AUTHDB v1.0)

### Details v8.0.14

1. Restrict the characters written to `structureID` and `structureRef` XML attribute in SDMX 2.1 datasets.

### Tickets v8.0.14

The following new features added:

- SDMXRI-1875: Support of character @ and - in the structures' ID and Agency's ID

## nsiws.net v8.0.13 (2022-04-11) (MSDB v6.10) (AUTHDB v1.0)

1. Updated to the latest public release libraries. No change in functionality

## nsiws.net v8.12.1 (2022-08-15) (MSDB v6.19) (AUTHDB v1.0)

### BREAKING CHANGE (8.12.1)

The NSIWS by default will use now the Operating System default locale/Culture. As a result SDMX-JSON requests may need `Accept-Language` HTTP Header to return localised text like annotation text. In the past it defaulted to English (en).
To return to the default behavior please check the [The default culture at the CONFIGURATION](doc/CONFIGURATION.md)

### Details v8.12.1

1. Improve support for `isMultiLingual` in SDMX 2.1 DSD
1. SDMX CSV v2.0.0 enhancements
   1. Read dataset attributes and actions per row
   1. Changes in DataReaderEngine to support action per observations.
1. Support for Merge Action
1. Correct time range constraint in data query.
1. Add new csv option for `format` REST query parameter.
1. Improvements to responses of SDMX-CSV v2.0.0 and zip'ed files
1. Minor fixes
   1. Some SDMX REST v1 structure requests in case of an error would return an empty response with HTTP status code 200.
   1. SOAP 2.1 data requests not working

### Tickets v8.12.1

The following new features added:

- SDMXRI-1936: Configure new default format URL option to generate a csv file (OECD,PULL_REQUEST)
- SDMXRI-1939: Extend CsvDataReaderEngineV2 to read dataset attributes on every row (OECD,PULL_REQUEST)
- SDMXRI-1934: Add new Merge Action, add CurrentAction to dataReaderEngine (OECD,PULL_REQUEST)
- SDMXRI-1941: Improve integration with latest features/PR

The following bugs have been corrected:

- SDMXRI-1937: Correct Content-Disposition & Content-Type headers in data V2 responses and so also sub-folder issue in zipped responses
- SDMXRI-1938: Allowedtimecontraint timerange wrongly applied to a data query
- SDMXRI-1835: Wrong behaviour of 'isMultiLingual' property
- SDMXRI-1935: Exception at meta data related SQL queries when content constraint has restriction on an attribute

## nsiws.net v8.12.0 (2022-07-08) (MSDB 6.19) (AUTHDB 1.0)

### Details v8.12.0

1. Produce an error/warning when importing a StructureSet with ConceptSchemeMap
   1. An error when the StructureSet has no CodelistMap/StructureMap
   1. A warning when the StructureSet has also CodelistMap/StructureMap
   1. Fixed issue in XSD generated classes parsing ConceptSchemeMap
1. Option to send logs to Google cloud platform (OECD)
1. New HTTP Header X-Level to control the metadata attribute levels in data (OECD)

### Tickets v8.12.0

The following new features added:

- SDMXRI-1924: Add a new HTTP X-Level option to get the referential metadata only at the current level (OECD,PULL_REQUEST)
- SDMXRI-1856: Add optional possibility to send logs to Google cloud platform (OECD)
- SDMXRI-1495: Provide clear error message "Not implemented" when creating StructureSet with ConceptSchemeMap (OECD)

## nsiws.net v8.11.1 (2022-06-17)

### Details v8.11.1

1. A way to include errors in json v1 & v2
1. Maintain UTF format in valid/from
1. SDMX 3.0.0 progress
   1. SDMX 3.0.0 StructureSpecificData support
   1. Improve SDMX REST API 2.0.0 API for structure resources
   1. SDMX REST v2.0.0 API support SDMX 3.0.0 structure types
   1. Improvements for SDMX 3.0.0 Structure XML (non-final/final handling)
1. Ignore position provided in SDMX 2.1 XML files for Data Structure Dimensions
1. Fix issue in `MaintainableUtil<T>.FilterCollectionGetLatest:`
1. Restrict the characters written to `structureID` and `structureRef` XML attribute in SDMX 2.1 datasets.
1. SDMX-JSON bugfixes
1. Support multiple plugins for available data retrieval

### Tickets v8.11.1

The following new features added:

- SDMXRI-1793: SDMX REST 2.0.0 API  data support SDMX StructureSpecificData 3.0.0 (SDMX3.0)
- SDMXRI-1781: SDMX REST 2.0.0 API structure URL path change (SDMX3.0)
- SDMXRI-1861: Update DotStat.DataAccess.NuGet reference in Authorization.net v8.11.0 (OECD)
- SDMXRI-1852: NSIWS plugin with default retriever
- SDMXRI-1858: NSI should stop executing aborted/abandoned requests
- SDMXRI-1878: Add errors array in json v1 and v2 (OECD,PULL_REQUEST)
- SDMXRI-1805: SdmxSource SDMX 3.0.0 StructureSpecificData XML writer (SDMX3.0)
- SDMXRI-1867: Actual content constraints' validFrom/validTo artefact properties to be exposed in UTC format (OECD,PULL_REQUEST)
- SDMXRI-1854: [NuGet Gallery] Message for owners of the package 'Estat.SdmxSource.SdmxSourceUtil (3rd-level-support)
- SDMXRI-1801: SdmxSource SDMX 3.0.0 Structure XML parser (SDMX3.0)
- SDMXRI-1802: SdmxSource SDMX 3.0.0 Structure XML writer (SDMX3.0)

The following bugs have been corrected:

- SDMXRI-1853: Swagger issue when the service is hosted under a virtual directory
- SDMXRI-1845: Incorrect SDMX-JSON localised names when language is unmatched
- SDMXRI-1803: In DSD 2.1 submissions, the dimension "position" property must be ignored
- SDMXRI-1875: Support of character @ and - in the structures' ID and Agency's ID
- SDMXRI-1843: "Semantic Error - 404" error log entry added when value of ObservationImpl.ObsTime is not valid
- SDMXRI-1872: "Operations that change non-concurrent collections must have exclusive access" error in NSI WS when getting data

## BREAKING CHANGE

NSIWS now requires .NET 6.

## nsiws.net v8.11.0 (2022-04-14) (MSDB v6.19) (AUTHDB v1.0)

### Details v8.11.0

1. Synchronize interfaces between Java and .NET on StructureSpecificData XML parser for 3.0.0
1. first/last observations were ignored when using Range in NSIWS
1. NSIWS now targets .NET 6
1. Improve performance when deleting an hierarchical codelist
1. Fix issue which prevented the update of content constraints if validity was set
1. Fix regressions related to mapping set auto-deletion
1. Fix issue producing very big SQL queries when the user is administrator and authorization is enabled

### Tickets v8.11.0

The following new features added:

- SDMXRI-1775: SDMX RI .NET migrate to .NET 6
- SDMXRI-1830: Add option to enable HTTP response compression (OECD,PULL_REQUEST)
- SDMXRI-1804: SdmxSource SDMX 3.0.0 StructureSpecificData XML parser (SDMX3.0)
- SDMXRI-1829: Improve performance of hierarchical code list deletion (OECD)
- SDMXRI-1831: Update DotStat.DataAccess.NuGet reference in Authorization.net v8.9.2 (OECD)

The following bugs have been corrected:

- SDMXRI-1836: Incorrect total number of observations when firstNObservations/lastNObservations parameter is provided
- SDMXRI-1846: ContentConstraintImportEngine prevents update of content constraints when StartDate and EndDate attributes are set
- SDMXRI-1842: DryIoc Exception in maapi.net tool v8.9.2
- SDMXRI-1834: Error while trying to delete a dataflow with mappingsets
- SDMXRI-1832: Metadata attributes attached to frequency dim fixed
- SDMXRI-1833: Critical issue in MDM connected to the MA WS

## nsiws.net v8.9.2 (2022-02-25)(MSDB v6.19)

### Details v8.9.2

1. Delete also Datasets when cascade deleting Mapping Sets for deleted Dataflows. *Note* this applies only when deleting Dataflows and cascade delete Mapping Sets is enabled.
1. SDMX-CSV v2 and SDMX-JSON 2.0.0 (SDMX 3.0.0) metadata in data fixes
1. New rest endpoint for retrieving status of DDB (Blocked by default)

### Tickets v8.9.2

The following new features added:

- SDMXRI-1819: Referential metadata fixes (OECD,PULL_REQUEST)
- SDMXRI-1827: Health check of DisseminationDb (OECD,PULL_REQUEST)

The following bugs have been corrected:

- SDMXRI-1771: Leftover mappingset Db related records after DataFlow deletion

## nsiws.net v8.9.1 (2022-02-03) (MSDB v6.19)

### Details v8.9.1

1. Metadata support in Datasets
1. Initial SDMX-CSV v2.0.0 support for data and metadata
1. Fix issue preventing writing hierarchical codelist ID in SDMX-JSON
1. Improve Multi-dataset support in SDMX-JSON
1. Improve performance in firstNObservations data queries

### Tickets v8.9.1

The following new features added:

- SDMXRI-1795: Data retriever metadata support (OECD,PULL_REQUEST)
- SDMXRI-1809: Support for SDMX-JSON (meta)data message v2.0 in NSI WS (OECD,PULL_REQUEST)
- SDMXRI-1806: SDMX-CSV 2.0.0 (meta)data writer
- SDMXRI-1758: Improve the performance of firstNObserverions queries and fix the result set when a range is requested. (OECD,PULL_REQUEST)

The following bugs have been corrected:

- SDMXRI-1808: Error at dataflow retrieval with detail=referencepartial parameter when related category scheme is partial
- SDMXRI-1816: SDMX-JSON: Hierarchy in Hierarchical Codelist (HCL) misses the ID (and links)
- SDMXRI-1812: JSON v1.0 writer improperly writes structure section of multi-dataset data messages

## nsiws.net v8.9.0 (2021-12-17)

### Details v8.9.0

1. Support mapping of metadata attributes
1. Writer for SDMX-JSON Data 2.0.0 with metadata attributes

### Tickets v8.9.0

The following new features added:

- SDMXRI-1772: Support mapping of metadata similar to existing mapping of data (OECD,PULL_REQUEST)
- SDMXRI-1779: SDMX-JSON writer 2.0 with metadata (OECD,PULL_REQUEST)

## nsiws.net v8.8.0 (2021-11-05) (MSDB v6.17.1) (AUTHDB v1.0)

### Details v8.8.0

1. Use of MADB 6.17.1 version with bugfix for MariaDB sequence syntax error
1. SDMX-CSV 2.0 data reader attribute verification
1. SDMX 3.0 REST API, use of keyword in key parameter
1. Fix structure-specific data messages that cannot be validated with the SDMX Converter
1. Fix AfterPeriod and BeforePeriod in CCs that are wrongly converted to StartPeriod and EndPeriod

### Tickets v8.8.0

The following new features added:

- SDMXRI-1745: Implement SDMX-CSV 2.0.0 data reader (OECD,PULL_REQUEST)
- SDMXRI-1565: SDMX 3.0 REST WS implementation (future-QTM)

The following bugs have been corrected:

- SDMXRI-1732: Due to an inconsistent structure ID, structure-specific data messages cannot be validated with the SDMX Converter
- SDMXRI-1687: AfterPeriod and BeforePeriod in CCs must not be converted to StartPeriod and EndPeriod

## nsiws.net v8.7.1 (2021-09-29) (MSDB v6.17) (AUTHDB v1.0)

### Details v8.7.1

1. Fix regression where wrong database type identifier was used
1. Accept ISO 8601 Milliseconds. Note writting them is not supported.
1. SDMX CSV 2.0.0 (SDMX 3.0.0) parsing support

### Tickets v8.7.1

The following new features added:

- SDMXRI-1745: Implement SDMX-CSV 2.0.0 data reader (OECD,PULL_REQUEST)
- SDMXRI-1659: nuget.org GetTimeFormatOfDate reports invalid ISO date time (nuget)

The following bugs have been corrected:

- SDMXRI-1753: regression bug in changes introduces in SDMXRI-1738, SDMXRI-1689

## nsiws.net v8.7.0 (2021-09-23)

### Details v8.7.0

1. Introduce the changes under SDMX 3.0 REST API
1. Show SDMX Open API online documentation in NSIWS

### Tickets v8.7.0

The following new features added:

- SDMXRI-1562: NSIWS on-line SDMX REST Open API  (future-QTM)
- SDMXRI-1565: SDMX 3.0 REST WS implementation (future-QTM)

## nsiws.net v8.6.0 (2021-09-02)

### Details v8.6.0

1. Return Sql queries generated for data
1. Get the DDB type when building data sql queries

### Tickets v8.6.0

The following new features added:

- SDMXRI-1564: integrate the Test Client inside the MA WEB (future-QTM)

The following bugs have been corrected:

- SDMXRI-1738: SDMXRI minor issues found in v8.5.0 (2021-08-05)

## nsiws.net v8.5.0 (2021-08-12)(MSDB v6.16) (AUTHDB v1.0)

### Details v8.5.0

1. Fix NPE in mysql servernames
1. Fixed a bug where Mapping Sets that were created with previous versions of Mapping Assistant were not listed
1. Fixed a bug where getting user actions was throwing an exception
1. Fixed a bug where updating a non-final codelist returned an incomplete status message
1. Implemented structure part for SDMX 3.0 REST WS
    1. Support for multiple values with comma as separator
    2. Support for wildcards

### Tickets v8.5.0

The following new features added:

- SDMXRI-1730: When mapping contains pre-generated period_start and period_end columns for TIME, generated SQL query uses incorrect parameters from passed REST query (startPeriod, endPeriod)
- SDMXRI-1565: SDMX 3.0 REST WS implementation (future-QTM)
- SDMXRI-1683: Allow attributes with AttributeRelationship Group which includes the time dimension
- SDMXRI-1739: Add StructureSet, MetadataStructure, Metadataflow, ProvisionAgreement support to JSON structure message
- SDMXRI-1641: change error code when Range Request Header is provided

The following bugs have been corrected:

- SDMXRI-1717: SDMXRI minor issues found in v8.4.0 (2021-07-08)
- SDMXRI-1737: list Mapping Sets created with old MA
- SDMXRI-1591: fix retrieving user action entities
- SDMXRI-1676: Improve successful message when updating a non-final codelist
- SDMXRI-1496: deleting local codes of dataset column leaves orphans records
- SDMXRI-1691: Unable to upload non-final MSD
- SDMXRI-1689: Dynamic avalability constraint:constructed sql uses unescaped Dimension column

## The following tickets have been included from the latest patch release (8.0.10)

### Details v8.0.9

1. Update dataflow that has categorization instead of using Delete/Insert
1. Change error code when trying to delete a dataset already in use.

### Tickets v8.0.9

The following bugs have been corrected:

- SDMXRI-1679: minor issues found in v8.0.8

### Details v8.0.8

1. Fixed SQL upgrade script preventing upgrade when a Content Constraint was attached to more than one artefact
(note this change affects only `Estat.Sri.Mapping.Tool.exe` when used to upgrade MSDB)

### Tickets v8.0.8

- SDMXRI-1671: MAWEB NET: impossible to update MS 6.5

## nsiws.net v8.4.1 (2021-07-15) (MSDB v6.15) (AUTHDB v1.0)

### Details v8.4.1

1. Updated the msdb.sql package version (6.15.1) to correctly label MSDB 6.15 version.

### Tickets v8.4.1

The following bugs have been corrected:

- SDMXRI-1716: MAWS.NET 8.4.0 initializes msdb to 6.14 (ISTAT)

## nsiws.net v8.4.0 (2021-07-08) (MSDB v6.15) (AUTHDB v1.0)

### Details v8.4.0

1. Add to configuration the currently hardcoded annotation for category scheme/dataflow/category/categorisation order.
1. Successfully handles errors during data retrieving.
1. Introduce SQL pagination for data requests containing the header parameter "Range"
1. Use the CubeRegion's TimeRange instead of ReferencePeriod when returning the range of time periods with data in "available" ContentConstraints.

#### Changes that affect backward compatibility (V8.4.0)

1. NSIWS uses the 422 HTTP Status code for semantic errors instead of the 403.

### Tickets v8.4.0

The following new features added:

- SDMXRI-1543: Incorrect HTTP status code returned for failing data queries (future-QTM)
- SDMXRI-1489: Make order annotation configurable or according the agreed annotation from TWG (.NET) (QTM10-2019.0281)
- SDMXRI-1693: Introduce SQL pagination for range requests. (OECD)

The following bugs have been corrected:

- SDMXRI-1692: SDMXRI minor issues found in v8.0.9 (2021-06-17) and 8.3.0
- SDMXRI-1527: For "available" ContentConstraint replace ReferencePeriod by CubeRegion-TimeRange

## nsiws.net v8.3.1 (2021-07-01) (MSDB v6.14) (AUTHDB v1.0)

### Details v8.3.1

1. Fix issue not being able to get SDMX artefacts as anonymous user when authentication is enabled and nsiws.xml is configured to allow anonymous user

### Tickets v8.3.1

The following bugs have been corrected:

- SDMXRI-1695: Support anonymous access for /sdmx/rest in maws.net

## nsiws.net v8.3.0 (2021-06-22) (MSDB v6.14) (AUTHDB v1.0)

## Breaking change in v8.3.0

Time period information in generated or available Content Constraints is no longer recorded in Reference Period but in Time Range under Cube Region.

### Details v8.3.0

1. Support reading DDB settings from a config file. Only one DDB connection string is supported right now.
1. Support for pre-formatted time dimension mapping
1. Use time range in generated and available content constraints.

### Tickets v8.3.0

The following new features added:

- SDMXRI-1673: NSI WS should take data database connection parameters from configuration (OECD,PULL_REQUEST)
- SDMXRI-1538: Enrich existing data retriever to take advantage of a mapped query with Time range columns (future-QTM)

The following bugs have been corrected:

- SDMXRI-1679: SDMXRI minor issues found in v8.0.8 (2021-05-28) and 8.2.0
- SDMXRI-1527: For "available" ContentConstraint replace ReferencePeriod by CubeRegion-TimeRange

## nsiws.net v8.2.0 (2021-05-31) (MSDB v6.13) (AUTHDB v1.0)

### Details v8.2.0

1. Increase maximum number of concurrent data requests by using asynchronous methods, from DDB data retrieval to REST/SOAP APIs
1. Improve performance by searching a Time Dimension and Frequency only once me DSD immutable object after profiling information shown by OECD
1. Fix TimeRange after/before period mapping to start/end period
1. Added validation for StartDate (i.e. valid from) of in maintainable files
1. Retrieve data for attributes that attach to groups that contains Time Dimension
1. Improve error message when a content constraint is submitted for a non-existent dataflow

### Tickets v8.2.0

The following new features added:

- SDMXRI-1622: Make rest data retreival asynchronous  (OECD,future-QTM)
- SDMXRI-1278: NSIWS support multiple versions of SDMX REST API (.NET) (QTM6-2019.0281)
- SDMXRI-1660: Allow attributes at group-level that includes the time dimension (OECD,PULL_REQUEST)
- SDMXRI-1534: Treatments of afterPeriod and beforePeriod are mixed up in TimeRangeCore implementation (OECD,future-QTM)
- SDMXRI-1643: Improve performance when getting the Frequency dimension from a DSD (OECD,future-QTM)

The following bugs have been corrected:

- SDMXRI-1644: StartDate of a MaintainableObjectCore instance can be changed to invalid value
- SDMXRI-1535: Content constraint structure message validation
- SDMXRI-1623: PIT issues

## nsiws.net v8.1.3 (2021-04-28)(MSDB v6.13) (AUTHDB v1.0)

### Details v8.1.3

1. Added documentation for building nsiws.net.
1. Allow setting the header dataset action in the configuration file.
1. Integrating the changes from the public release branches back to develop and Fixing business cases & tests that broke in develop branch.
1. Support versioning of nsiws rest api.
1. Added Regex validation of Party/Id.
1. Automatic deletion of mapping sets belonging to the dataflow when the dataflow is deleted.
1. Fix a bug where an extra '/' was added to the links generated(e.g. SDMXRegistry URL).
1. Fixed a bug where a content constraint that was not compliant with SDMX standards was accepted when submitted.
1. Improve error/status message for all structure updates

### Tickets v8.1.3

The following new features added:

- SDMXRI-1460: Make DataSetAction (header) configurable (.net) (QTM10-2019.0281)
- SDMXRI-1393: Document NSIWS.NET compilation in VS (QTM10-2019.0281)
- SDMXRI-1616: SRI.NET integrate changes from public / fix issues in develop branch (OECD)
- SDMXRI-1278: NSIWS support multiple versions of SDMX REST API (.NET) (QTM6-2019.0281)
- SDMXRI-1582: Regex validation of Party/Id (OECD,PULL_REQUEST)
- SDMXRI-1536: Automatic deletion of mapping sets belonging to the dataflow when the dataflow is deleted (OECD,PULL_REQUEST)
- SDMXRI-1507: NSIWS Retrieve annotations for artefacts stored as stubs in MSDB (ISTAT,QTM9-2019.0281)
- SDMXRI-1432: Make StructureUsage/DataSetID/DataSetAction configurable per dataflow (.NET) (QTM10-2019.0281)
- SDMXRI-1594: Improve error/status message for all structure updates (OECD,PULL_REQUEST)
- SDMXRI-1553: Values of attributes at group attachment level not written in series node in sdmx-json
- SDMXRI-1460: Make DataSetAction (header) configurable (.net) (QTM10-2019.0281)

The following bugs have been corrected:

- SDMXRI-1559: SdmxRegistryService URL configuration
- SDMXRI-1623: PIT issues
- SDMXRI-1535: Content constraint structure message validation
- SDMXRI-1537: Not possible to update parent relations in a non-final codelist used in a DSD
- SDMXRI-1574: Possible bug related category retrieval
- SDMXRI-1588: Json writer dataflow without time dimension has a bug in a flat format
- SDMXRI-1566: Fix writing of NULLs in JSON data message when primary measure is not string-type
- SDMXRI-1581: NSI Client: unable to retrieve data
- SDMXRI-1441: MAWEB NET : querying registry : fail to import structure
- SDMXRI-1550: MAWEB NET: viwe mapped Data and data validation
- SDMXRI-1116: Metadata Structure - "isPresentational" field no saved
- SDMXRI-1539: MappingSetEntityRetriever retrieves entities of version 1.0.0 when version 1.0 is requested
- SDMXRI-1620: Dataretriever fetches and loops through the complete resultset from SQL server regardless specified range in a Range header
- SDMXRI-1607: Issues in extracting time periods with non-calendar year reporting
- SDMXRI-1580: Fix usage of lastNObservations parameter with reservedKeywordToStringFormat

## nsiws.net v8.1.2 (2021-01-08) (MSDB v6.12) (AUTHDB v1.0)

### Details v8.1.2

1. Merge changes from the public release branch v8.0.3
1. Point in time tweaking
1. Observer interface that is triggered on structure updates
1. Workaround deadlocks in SQL Server for handling multiple data requests

### Tickets v8.1.2

The following new features added:

- SDMXRI-1157: NSI web service: Implement observer interface for structure updates (OECD,PULL_REQUEST)
- SDMXRI-1522: Allowed Constraints filter data query (OECD,PULL_REQUEST)
- SDMXRI-1340: SDMXRI use AuthManagement web service (OECD)
- SDMXRI-1336: Point in Time support (OECD,PULL_REQUEST,QTM10-2019.0281)
- SDMXRI-1494: CsvDataReaderEngine additional exception handling (OECD)

The following bugs have been corrected:

- SDMXRI-1515: Fix select statements having reserved SQL words
- SDMXRI-1400: MAWEB/WS issues with 2020-07-03 release
- SDMXRI-1462: NSIWS.NET submit response has duplicate language entries
- SDMXRI-1441: MAWEB NET : querying registry : fail to import structure
- SDMXRI-1519: SdmxSource data reader doesn't support dataflow without time dimension when format is "generic xml" or "csv"
- SDMXRI-1504: AvailableConstraintQuery to use DateFrom, DateTo
- SDMXRI-1443: Support of observation values with string data type at SDMX-JSON data writers

## nsiws.net v8.1.1 (2020-11-25) (MSDB v6.12) (AUTHDB v1.0)

### Breaking change in v8.1.1

The format of the Submit response has changed slightly.
Instead of having multiple Text entries grouped by under an ErrorMessage with the same code they are not in separate ErrorMessage.

Previous response was generated like the example below:

```xml
<message:ErrorMessage code="150">
   <common:Text xml:lang="en">Not Updated : Codelist ESTAT:CL_ACTIVITY(1.2) Reason .... </common:Text>
   <common:Text xml:lang="en">Not Updated : Codelist ESTAT:CL_FREQ(1.2) Reason .... </common:Text>
</message:ErrorMessage>
```

The current response is now generated like the example below:

```xml
<message:ErrorMessage code="150">
   <common:Text xml:lang="en">Not Updated : Codelist ESTAT:CL_ACTIVITY(1.2) Reason .... </common:Text>
</message:ErrorMessage>
<message:ErrorMessage code="150">
   <common:Text xml:lang="en">Not Updated : Codelist ESTAT:CL_FREQ(1.2) Reason .... </common:Text>
</message:ErrorMessage>
```

This change was made in order to be compatible with the Java version and because it is more correct.

### Details v8.1.1

1. Target MSDB v6.12
1. Support escaping column names with configuration per db provider
1. Experimental feature. To workaround URL size limitations it is possible to `POST` the key in the body in REST `availableconstraint` queries. See [doc/EXTENDED_API](doc/EXTENDED_API.md)
1. Support for updating non-final that is referenced by other artefacts
   1. Item Schemes
   1. Dataflows
   1. Data Structures
      1. Cross Sectional attributes are not fully supported
   1. Items/Components that are referenced by other artefacts are not removed
1. Fix Annotation update with multiple text entries
1. Fix issue that generated errors when querying actualconstraints and allowedconstraints with version.
1. Fix internal server error when trying to delete or replace a codelist that is referenced by a concept scheme.
1. Counting observations in some cases would ignore startPeriod and endPeriod in available content constraint queries
1. MinValue and MaxValue are now saved into the MSDB
1. Fixed issues updating final/non-final MSD
1. The NSIWS submit response when using POST/PUT methods in REST contained duplicated Text entries with the same language code.
1. Bugfixes

### Tickets v8.1.1

The following new features added:

- SDMXRI-1157: NSI web service: Implement observer interface for structure updates (OECD,PULL_REQUEST)
- SDMXRI-1340: SDMXRI use AuthManagement web service (OECD)
- SDMXRI-1336: Point in Time support (OECD,PULL_REQUEST,QTM10-2019.0281)
- SDMXRI-1494: CsvDataReaderEngine additional exception handling (OECD)
- SDMXRI-1474: NSIWS available constraint URL limitation (NET) (ISTAT,QTM10-2019.0281)
- SDMXRI-1277: Support updating referenced non-final item scheme artefacts (.NET) (OECD,PUBLIC_2020,QTM6-2019.0281)
- SDMXRI-1452: Support updating referenced non-final DSD (.NET) (PUBLIC_2020,QTM10-2019.0281)

The following bugs have been corrected:

- SDMXRI-1515: Fix select statements having reserved SQL words
- SDMXRI-1400: MAWEB/WS issues with 2020-07-03 release
- SDMXRI-1504: AvailableConstraintQuery to use DateFrom, DateTo
- SDMXRI-1443: Support of observation values with string data type at SDMX-JSON data writers
- SDMXRI-1462: NSIWS.NET submit response has duplicate language entries
- SDMXRI-1441: MAWEB NET : querying registry : fail to import structure
- SDMXRI-1466: Annotation update issue
- SDMXRI-1391: Querying actualconstraint or allowedconstraint by version fails
- SDMXRI-1418: Foreign Key constraint violation exception when creating SDMX artefacts (OECD, PULL_REQUEST)
- SDMXRI-1445: Allow referencing non-final codelists in a non-final hierarchical codelist (OECD)
- SDMXRI-1516: Extend the length of MAPPING_SET table's ID column
- SDMXRI-1442: Issues related to the management of Metadata Structure Definition
- SDMXRI-1458: In TextFormatTypesQueryEngine class minValue and maxValue facets of TextFormat are not saved to database

## nsiws.net v8.1.0 (2020-10-16) (MSDB v6.11) (AUTHDB v1.0)

### Details v8.1.0

1. Target MSDB v6.11
1. Support for new authorisation mechanism. It supports SDMXRI and DoStat authorization
1. Better integration of Open ID authentication
1. Support of embargo/point in time. Users with a specific authorization rule and HTTP header can request embargoed data.
   1. A Dataflow can now have multiple Mapping Sets.
   1. One Mapping Set can be active.
   1. Mapping sets have validity dates.
1. Fixed updating final MSD
1. Allow a non-final MSD to reference a non-final concept scheme.
1. Fix saving minValue and maxValue facets when saving a DSD
1. Workaround issues with HTTP/2 headers
1. Fix log4net filter configuration

### Tickets v8.1.0

The following new features added:

- SDMXRI-1340: SDMXRI use AuthManagement web service (OECD)
- SDMXRI-1336: Point in Time support (OECD)

The following bugs have been corrected:

- SDMXRI-1442: Issues related to the management of Metadata Structure Definition (OECD)
- SDMXRI-1458: In TextFormatTypesQueryEngine class minValue and maxValue facets of TextFormat are not saved to database (OECD)
- SDMXRI-1443: Support of observation values with string data type at SDMX-JSON data writers

## nsiws.net v8.0.6 (2021-04-09) (MSDB v6.10) (AUTHDB v1.0)

### Details v8.0.6

1. Non-Time Dimension components with format date time mapped to DateTime columns would produce non ISO 8601 date time
1. Fixed issue not being update to update parent code in a non-final codelist in a DSD
1. Mapping Validation would not work on DSD without time dimension

### Tickets v8.0.6

The following bugs have been corrected:

- SDMXRI-1621: SDMXRI minor issues found in v8.0.5 (2021-03-18)
- SDMXRI-1537: Not possible to update parent relations in a non-final codelist used in a DSD

## nsiws.net v8.0.5 (2021-03-12) (MSDB v6.10) (AUTHDB v1.0)

### Details v8.0.5

1. Fix for a bug that retrieves mapping sets of dataflow version 1.0.0 when version 1.0 is requested (OECD)
1. Increase IIS limits for submitting structure files
1. Fix issue with double '/' in WSDL url when running under Kestrel (OECD)

### Tickets v8.0.5

The following new features added:

- SDMXRI-1551: NSIWS: issue in importing a big codelist (ISTAT,QTM9-2019.0281)

The following bugs have been corrected:

- SDMXRI-1559: SdmxRegistryService URL configuration

## nsiws.net v8.0.4 (2021-01-28) (MSDB v6.10) (AUTHDB v1.0)

### Details v8.0.4

1. Fix ServiceURL not showing in SDMX 2.0 structure messages

### Tickets v8.0.4

The following new features added:

- SDMXRI-1295: SDMX v2.0 serviceUrl vs structureUrl in sdmxsource (.net) (QTM6-2019.0281)

## nsiws.net v8.0.3 (2020-12-17) (MSDB v6.10) (AUTHDB v1.0)

### Details v8.0.3

1. Workaround for HTTP/2.0 HTTP headers
1. Fix error shown when starting NSIWS under kestrel
1. Improve SDMX JSON and SDMX CSV data requests by avoiding requesting the same concept scheme for every component in a DSD.
1. Updating dataflow annotations returned a warning
1. ContentConstraints were not correctly generated from transcoding if there was a mis-configuration in Frequency/Time Dimension transcodings.
1. Temporary allow only request to make SDMX artefact changes to workaround Mapping Store schema issues.
1. Many bugfixes

### Tickets v8.0.3

The following new features added:

- SDMXRI-1525: NSIWS.NET don't crash on HTTP/2,0 headers (PUBLIC_2020,future-QTM)
- SDMXRI-1498: Investigate performance regression between 7.10 vs 8.0 (.NET) (PUBLIC_2020,QTM10-2019.0281)

The following bugs have been corrected:

- SDMXRI-1518: MAWEB/WS issues with 2020-11-19 release
- SDMXRI-1466: Annotation update issue
- SDMXRI-1476: wrongly formatted content constraint is generated
- SDMXRI-1510: deadlocks in MS SQL during deletion of artefacts within parallel requests in NSI.WS

## nsiws.net v8.0.2 (2020-11-18) (MSDB v6.10) (AUTHDB v1.0)

### Details v8.0.2

1. Experimental feature. To workaround URL size limitations it is possible to `POST` the key in the body in REST `availableconstraint` queries. See [doc/EXTENDED_API](doc/EXTENDED_API.md)
1. Support for updating non-final that is referenced by other artefacts
   1. Item Schemes
   1. Dataflows
   1. Data Structures
      1. Cross Sectional attributes are not fully supported
   1. Items/Components that are referenced by other artefacts are not removed
1. Fix Annotation update with multiple text entries
1. Fix issue that generated errors when querying actualconstraints and allowedconstraints with version.
1. Fix internal server error when trying to delete or replace a codelist that is referenced by a concept scheme.
1. Counting observations in some cases would ignore startPeriod and endPeriod in available content constraint queries

### Tickets v8.0.2

The following new features added:

- SDMXRI-1474: NSIWS available constraint URL limitation (NET) (ISTAT,QTM10-2019.0281)
- SDMXRI-1277: Support updating referenced non-final item scheme artefacts (.NET) (OECD,PUBLIC_2020,QTM6-2019.0281)
- SDMXRI-1452: Support updating referenced non-final DSD (.NET) (PUBLIC_2020,QTM10-2019.0281)

The following bugs have been corrected:

- SDMXRI-1466: Annotation update issue
- SDMXRI-1391: Querying actualconstraint or allowedconstraint by version fails
- SDMXRI-1418: Foreign Key constraint violation exception when creating SDMX artefacts (OECD, PULL_REQUEST)
- SDMXRI-1504: AvailableConstraintQuery to use DateFrom, DateTo

## nsiws.net v8.0.1 (2020-10-22) (MSDB v6.10) (AUTHDB v1.0)

### Breaking change 8.0.1

The format of the Submit response has changed slightly.
Instead of having multiple Text entries grouped by under an ErrorMessage with the same code they are not in separate ErrorMessage.

Previous response was generated like the example below:

```xml
<message:ErrorMessage code="150">
   <common:Text xml:lang="en">Not Updated : Codelist ESTAT:CL_ACTIVITY(1.2) Reason .... </common:Text>
   <common:Text xml:lang="en">Not Updated : Codelist ESTAT:CL_FREQ(1.2) Reason .... </common:Text>
</message:ErrorMessage>
```

The current response is now generated like the example below:

```xml
<message:ErrorMessage code="150">
   <common:Text xml:lang="en">Not Updated : Codelist ESTAT:CL_ACTIVITY(1.2) Reason .... </common:Text>
</message:ErrorMessage>
<message:ErrorMessage code="150">
   <common:Text xml:lang="en">Not Updated : Codelist ESTAT:CL_FREQ(1.2) Reason .... </common:Text>
</message:ErrorMessage>
```

This change was made in order to be compatible with the Java version and because it is more correct.

### Details v8.0.1

1. Support for updating non-final that is referenced by other artefacts
   1. Final status is updated
   1. Codelist and Concept Scheme. New items are added, removed items are removed (except items that are used).
   1. Transcoding rules are removed
   1. Dataflow
   1. DSD, support for DSD is incomplete.
1. MinValue and MaxValue are now saved into the MSDB
1. Fixed issues updating final/non-final MSD
1. The NSIWS submit response when using POST/PUT methods in REST contained duplicated Text entries with the same language code.

### Tickets v8.0.1

The following new features added:

- SDMXRI-1277: Support updating referenced non-final item scheme artefacts (.NET) (OECD,PUBLIC_2020,QTM6-2019.0281) (PARTIAL)

The following bugs have been corrected:

- SDMXRI-1442: Issues related to the management of Metadata Structure Definition (OECD)
- SDMXRI-1458: In TextFormatTypesQueryEngine class minValue and maxValue facets of TextFormat are not saved to database (OECD, PULL_REQUEST)
- SDMXRI-1462: NSIWS.NET submit response has duplicate language entries
- SDMXRI-1445: Allow referencing non-final codelists in a non-final hierarchical codelist (OECD)

## nsiws.net v8.0.0 (2020-09-24) (MSDB v6.10) (AUTHDB v1.0)

### Details v8.0.0

By default the Accept-Charset is considered when caching
The Header Id is now generated as a 6-digit number from a sequence in order to match the requirements for ECB
The Header Datasetid is now set to the id of the datastructure unless there is a datasetid defined in the default header in the configuration file
It is possible to request a zip file by including zip=true in the Accept header mime type.

### Tickets v8.0.0

The following new features added:

- SDMXRI-1446: Add Accept-Charset header to Vary header list (OECD)
- SDMXRI-1416: NSIWS Header ID align to ECB requirements (.NET) (PUBLIC_2020)
- SDMXRI-1335: NSIWS Zip file download feature (OECD,PULL_REQUEST,QTM6-2019.0281)
- SDMXRI-1428: Make DatasetID (header) configurable per WS instance (.NET)

## Important  changes in v7.x.x

1. Starting from 7.0.0 NSIWS targets .NET Core and require ASP.NET Hosting module to work with IIS
   1. Up to version 7.10.x targets .NET Core 2.2
   1. Since version 7.11.0 targets .NET Core 3.1
1. Each `NSIWS` instance requires a separate Application Pool with target : 'No Managed'
1. `Web.Config` contains only the IIS Configuration needed for running `NSIWS` on IIS
1. The rest of the configuration resides now in [Main configuration file](doc/CONFIGURATION.md#main-configuration-file) and `config/*.json` files

*WARNING* Currently NSI WS will try to copy `config/app.config` if it exists to `NSIWebServiceCore.dll.config` everytime the application pool starts or recycles.
To avoid this behavior remove the `config/app.config` or move it to `NSIWebServiceCore.dll.config`

## nsiws.net v7.13.2 (2020-09-02) (MSDB v6.10) (AUTHDB v1.0)

### Details v7.13.2

1. SDMX v2.1 data message Header can reference now either a dataflow or a DSD. This can change in `config/sdmx-header.json`. The default is `dataflow`
1. It is possible to receive a zip file by setting `zip=true` in the Accept HTTP header (From OECD)
1. Experimental feature. To workaround URL size limitations it is possible to `POST` the key in the body in REST data queries. See [doc/EXTENDED_API](doc/EXTENDED_API.md)

### Tickets v7.13.2

The following new features added:

- SDMXRI-1380: REST Data queries and URL size limitations (.NET) (ISTAT,future-QTM)
- SDMXRI-1414: NSIWS make StructureUsage configurable (.NET) (PUBLIC_2020,future-QTM)
- SDMXRI-1335: NSIWS Zip file download feature (OECD,PULL_REQUEST,QTM6-2019.0281)

## nsiws.net v7.13.1 (2020-07-23) (MSDB v6.10) (AUTHDB v1.0)

### Important changes v7.13.1

#### Non backwards compatible changes/breaking changes v7.13.1

SDMX-JSON message output has changed.

##### meta content languages changes in all messages

`meta.content-languages` renamed to `meta.contentLanguages` in structure and data messages

##### links array changes in structure messages

- empty links array is never written
- `link.type` is never written
- `link.urn` is only written if the request had an accept header: `"urn=true"`
- `link.rel` possible types
  - `self`: only added if `"urn=true"` is defined
  - `external`: only added if `isExternalReference=true`
  - `href` is populated with structureURL
  - `urn` only added if `"urn=true"` is defined
  - `structure`: only added if `"urn=true"` is defined and the artefact has a DSD or MSD cross-reference
  - direct `"urn"` artefact property is removed as it's not part of the standard and the same information can be found now in the links array with `"self"` `link.rel` type

### Details v7.13.1

1. SDMX-CSV and SDMX-JSON improvements from OECD
1. Fix issue ignoring observation values when reading SDMX v2.1 Flat Generic Data from OECD
1. Support for Hierarchical codelists in SDMX-JSON from OECD
1. Various bugfixes, including
   1. Improved Provision agreement import/retrieval support, Metadaflow and better error reporting
   1. Couldn't use/save MariaDB DDB connections when a port was used

### Tickets v7.13.1

The following new features added:

- SDMXRI-1368: Add Hierarchical codelist support to JSON structure message  (OECD,PULL_REQUEST)

The following bugs have been corrected:

- SDMXRI-1402: CsvDataReaderEngine to handle data with ids+labels along with freetext attributes having ": " (OECD,PULL_REQUEST)
- SDMXRI-1397: Optional attributes are NOT required when importing data (not working for CSV import) (OECD,PULL_REQUEST)
- SDMXRI-1367: Fix JSON format: contentLanguages, links (OECD,PULL_REQUEST)
- SDMXRI-1384: GenericDataReaderEngine doesn't read observation value from flat data XML (OECD,PULL_REQUEST)

## nsiws.net v7.13.0 (2020-07-02) (MSDB v6.10) (AUTHDB v1.0)

### Details v7.13.0

1. Included a new document that contains [IIS common issues and solutions](doc/IIS_WORKAROUNDS.md)
1. New health source for checking the MSDB details from OECD.

### Tickets v7.13.0

The following new features added:

- SDMXRI-1330: SDMX_RI Rest issues (3rd-level-support)
- SDMXRI-1379: Add mapping store database version to a healthcheck (OECD, PULL_REQUEST)

## nsiws.net v7.12.2 (2020-06-12) (MSDB v6.10) (AUTHDB v1.0)

### Details v7.12.2

1. Target MSDB v6.10
1. Added SQL Server docker file that starts an empty DB and an AUTHDB
1. Check if a request has a `X-Requested-With` HTTP request header in order not to include the HTTP response `WWW-Authenticate`.
1. Support for `Accept-Encoding: application/zip` to download structure/data in zip file.
1. Category scheme enhancements from OECD:
   1. Manage hierarchies of items in the queries using dots (see <https://github.com/sdmx-twg/sdmx-rest/blob/master/v2_1/ws/rest/docs/4_3_structural_queries.md>)
   1. The same category (with the same id) can appear on multiple nodes in the hierarchy
   1. Manage "references=dataflow" properly
1. Support for Ignore Production Flag for data.
1. Concept core representation support, text format and Codelist

### Tickets v7.12.2

The following new features added:

- SDMXRI-1366: MAWS.NET : Delete the popup that asks to sigin (QTM6-2019.0281)
- SDMXRI-1125: Provide Linux-based Docker image for NSI web service + MSDB/AuthDB (OECD,QTM2-2019.0281) (PARTIAL)
- SDMXRI-1335: NSIWS Zip file download feature (OECD, PULL_REQUEST)
- SDMXRI-1361: Add CSV support to SdmxDataReaderFactory (OECD,PULL_REQUEST)
- SDMXRI-1308: Manage hierarchical categoryschemes (OECD,QTM6-2019.0281, PULL_REQUEST)
- SDMXRI-1219: Restore the ignoreProductionFlagForData parameter (.NET) (ISTAT,QTM6-2019.0281)
- SDMXRI-1265: Support Concept Scheme Core representation store/retrieval/references (.NET) (OECD,QTM6-2019.0281)
- SDMXRI-1255: MAWEB: Registries WS listed (QTM6-2019.0281)
- SDMXRI-1365: MSDB add name unique constraints for DDB, Dataset tables (QTM6-2019.0281)

## nsiws.net v7.12.1 (2020-05-21) (MSDB v6.9) (AUTHDB v1.0)

### Tickets v7.12.1

The following new features added:

- SDMXRI-1281: SDMX v2.1 format, use StructureUsage (.NET) (OECD,QTM6-2019.0281)

## nsiws.net v7.12.0 (2020-05-13) (MSDB v6.9) (AUTHDB v1.0)

### Details v7.12.0

1. In SDMX v2.1 DSD it is now possible for an attribute to attach to the Time Dimension
1. Support for groups in SDMX-CSV
1. Only one content constraint of the same type that is attached to the same artefact can apply to a specific component.
   1. Validity dates are considered
1. Multiple enhancement to the maapi tool (Estat.Sri.Mapping.Tool.exe)
   1. Pass connection string in command line
   1. New option for upgrade command to initialize the MSDB if it is not initialized
   1. Better error reporting
1. Changes in Dockerfile to produce smaller images (and faster)

### Tickets v7.12.0

The following new features added:

- SDMXRI-1125: Provide Linux-based Docker image for NSI web service + MSDB/AuthDB (OECD,QTM2-2019.0281)
- SDMXRI-1172: Apply rules for multiple content constraints (of same type) attached to same artefact (OECD,QTM2-2019.0281)
- SDMXRI-1123: Enhancements for command line MAAPI Tool (OECD,QTM2-2019.0281)
- SDMXRI-1298: NSI WS recognises attributes attached to TIME_PERIOD as observation level attributes (.NET) (OECD,QTM6-2019.0281)
- SDMXRI-1224: SDMX-CSV data writer support for groups (.NET) (OECD,QTM2-2019.0281)
- SDMXRI-1281: SDMX v2.1 format, use StructureUsage (.NET) (OECD,QTM6-2019.0281)

## nsiws.net v7.11.5 (2020-04-09)  (MSDB v6.9) (AUTHDB v1.0)

### Known issues v7.11.5

Reproduced so far only under IIS (in Docker) and SOAP UI.
Some SOAP/REST calls might return an empty response with HTTP header 200 although no error is found in the logs. Repeating the call returns the correct response.
It is not clear yet if it is SOAP UI in fault or NSIWS or IIS.

### Important changes v7.11.5

1. Affects REST calls and POST/PUT methods
   1. The status code for updating successfully non-final SDMX artefacts that have dependencies has changed from `403` to `200`.
   1. The status code for inserting and updating multiple SDMX artefacts has changed from `207` to `201`

### Details v7.11.5

1. Fix issue with nuget package references. Some package reference versions were wildcarded causing inconsistent behavior between builds
1. Delay calls to MSDB from format plugins only when they are needed
1. Automatic Category creation is controlled by configuration. See [CONFIGURATION](config/Properties.json)
1. Do not report as error/warning updates to non-final SDMX artefacts
1. Do not report as multi-status the combination of successful insert/updates to SDMX artefacts

### Tickets v7.11.5

The following new features added:

- SDMXRI-1110: Make automatic Category creations from categorisation configurable (OECD,QTM2-2019.0281)
- SDMXRI-1311:  Data query performance issue with nsiws (.NET) (ILO,OECD)

The following bugs have been corrected:

- SDMXRI-1315: Inconsistent nuget package reference across NSI solution
- SDMXRI-1267: Not possible to update names/annotations in non-final artefacts that are referenced by other artefacts (OECD)

## nsiws.net v7.11.4 (2020-03-19) (MSDB v6.9) (AUTHDB v1.0)

### Important changes v7.11.4

#### Changes that affect backward compatibility (V7.11.4)

The default logging configuration has changed:

1. User vs System activity
1. Different filenames
1. Dataflow activity disabled now by default

Please consult the [CONFIGURATION](doc/CONFIGURATION.md)

### Details v7.11.4

1. Default logging configuration changed. Logging of user and system activity logged in different files
1. Target MSDB v6.9
   1. Annotation store procedure title parameter size increased (OECD)
   1. Dataflow table new nullable field for linking to Data source table
1. Fixes related to data/structure validation functionality in code re-used by MAWS
1. CORS error handling fixes from OECD
1. New language code support `Ladin Dolomitan`.

### Tickets v7.11.4

The following new features added:

- SDMXRI-1118: Modify default log4net configuration. (OECD,QTM2-2019.0281)
- SDMXRI-968: Implementation of Data Registation/monitor: Configure data source per dataflow (SQL) (QTM2-2019.0281)
- SDMXRI-1206: SDMX validation inside the Mapping Assistant (MA_WEB) (QTM2-2019.0281)
- SDMXRI-1310: Support `lld` ISO-639-3 Language code in SdmxSource.NET (ISTAT)

The following bugs have been corrected:

- SDMXRI-1290: Annotation Title is still fixed to 70 characters
- SDMXRI-1302: headers written in Cors middleware are not present in the http response in case of exception and it's treatment with ExceptionHandler

## nsiws.net v7.11.3 (2020-03-02) (MSDB v6.8) (AUTHDB v1.0)

### Details v7.11.3

1. Fix regression that caused the Web Service to return no results when a query was not for a datastructure

### Tickets v7.11.3

The following bugs have been corrected:

- SDMXRI-1297: Cannot query artefacts other than datastructure

## nsiws.net v7.11.2 (2020-02-27) (MSDB v6.8) (AUTHDB v1.0)

### Important changes v7.11.2

#### Changes that affect backward compatibility (V7.11.2)

1. Starting from this version NSIWS will allow querying data and structure of a SDMX v2.0 CrossSectional DSD in SDMX v2.1 formats.
   1. Previous versions of NSIWS would return Data Structure Definitions (DSD) with SDMX v2.0 CrossSectional attachment flags as stubs in SDMX v2.1 XML Structure format and previous versions of NSIWS would not allow to retrieve data in SDMX v2.1 XML Format for such DSD.
   1. It is possible to revert to the old behavior by changing or removing the `config/Properties.json` file, see the [CONFIGURATION](doc/CONFIGURATION.md) for more details.
1. Changes in non-final artefact behavior. Starting from this version it is possible to update existing non-final artefacts that are referenced by other artefacts.
   1. Only backward compatible changes will be applied. Therefore it is not possible to remove items/components or even change the final status flag.
   1. Previous versions of NSIWS would respond with an error in such cases

### Details v7.11.2

1. SDMX-JSON structure bug fix related to stub categorisation from OECD
1. Include SDMX v2.0 Cross Sectional metadata in SDMX v2.1 XML and JSON output as annotations and concept roles
1. Allow SDMX v2.1 data requests for dataflow using SDMX v2.0 Cross Sectional DSD
1. Reference partial support for CategorySchemes from OECD
1. Support updating non-final artefacts that are referenced by other artefacts
1. Health check page show retriever version
1. OpenID middleware new configuration option
1. New log4net property to show logged user in logs

### Tickets v7.11.2

The following new features added:

- SDMXRI-1150: Include v2.0 CrossSectional information in SDMX-JSON (.net) (QTM2-2019.0281)
- SDMXRI-1151: SDMX v2.1 data/structure output for SDMX v2.0 XS DSD (.NET) (QTM2-2019.0281)
- SDMXRI-1117: Add user information to logging thread (OECD,QTM2-2019.0281)
- SDMXRI-1285: Tiny NSI WS improvements
- SDMXRI-1292: Implement partial CategorySchemes through detail=referencepartial parameter
- SDMXRI-1267: Not possible to update names/annotations in non-final artefacts that are referenced by other artefacts (OECD)

The following bugs have been corrected:

- SDMXRI-1286: Json writer bug writing categorisation with  allstubs detail (OECD, PR)

## nsiws.net v7.11.1 (2020-02-07) (MSDB v6.8) (AUTHDB v1.0)

### Details v7.11.1

1. Updated Dockerfile for .NET Core 3.1
1. New configuration file to set the maximum size of the request when using Kestrel (e.g. `dotnet NSIWebServiceCore`)
1. Fix issue related to `referencepartial`, concept schemes and DSD with components sharing the same concept identity
1. Merge fixes to JSON DSD parser from ISTAT

### Tickets v7.11.1

The following new features added:

- SDMXRI-1283: Update linux Dockerfile for .net core 3.1 (OECD)
- SDMXRI-1268: Configure max size of request message. (OECD)
- SDMXRI-1084: SDMX JSON DSD Parser (.NET) (ISTAT,QTM2-2018-SC000941)

The following bugs or regressions have been corrected:

- SDMXRI-1214: "Semantic Error - Duplicate language `it` for TextType" when using referencepartial (OECD)

## nsiws.net v7.11.0 (2020-01-20) (MSDB v6.8) (AUTHDB v1.0)

### Details v7.11.0

1. Uses [MSDB v6.8](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/msdb.sql/browse/HISTORY.md?at=refs%2Fheads%2F6.8)
1. Target .NET Core 3.1
1. Support for including NSIWS URL in the response of full artefacts requested as stubs
1. Support for including NSIWS URL in the response of full artefacts requested as stubs
1. Fix default header loading of configuration

### Tickets v7.11.0

The following new features added:

- SDMXRI-1244: NSI WS & MAWS target .NET Core 3.1
- SDMXRI-1140: Mechanism to set the NSI WS URL in returned stub artefacts (stored in full in MASTORE) (OECD,QTM2-2019.0281)
- SDMXRI-1268: Configure max size of request message.
- SDMXRI-1179: IIS workaround, remove WebDAVModule (QTM2-2019.0281)

The following bugs have been corrected:

- SDMXRI-1259: fix namespace for SettingsHeaderRetriever in the app.config file

#### MSDB v6.8 tickets

The following new features added:

- SDMXRI-1208: SDMX ID increase to 255 characters in MSDB (Oracle/MariaDB) (OECD,sync-needed)
- SDMXRI-1052: SDMX ID increase to 255 characters in MSDB (SQL) (OECD,QTM2-2018-SC000941)

The following bugs have been corrected:

- SDMXRI-1222: Referencepartial parameter for request with references doesn't work anymore
- SDMXRI-1123: Enhancements for command line MAAPI Tool (OECD,QTM2-2019.0281) (Partial only missing DROP statements were fixed)
- SDMXRI-1220: Cannot delete or replace categorisations and/or HCL

## nsiws.net v7.10.10 (2019-12-27) (MSDB v6.7) (AUTHDB v1.0)

### Details v7.10.10

1. Fix a security issue that affects MAWS.NET

### Tickets v7.10.10

The following bugs have been corrected:

- SDMXRI-1264: NSIWS/MAWS .NET thread principal not cleared

## nsiws.net v7.10.9 (2019-12-20) (MSDB v6.7) (AUTHDB v1.0)

### Details v7.10.9

1. OpenID improvements from OECD
1. Improve and correct references resolution (all, parents, children, parents and siblings and specific structures)

### Tickets v7.10.9

The following new features added:

- SDMXRI-1243: change OpenIdConnect anonymous access to be configurable
- SDMXRI-1194: Support for references parent/children for PA, Structure Set, MSD and MDF + Constraints (.NET) (ISTAT,OECD,QTM2-2019.0281)

## nsiws.net v7.10.8 (2019-12-06) (MSDB v6.7) (AUTHDB v1.0)

### Details v7.10.8

1. Allow admin user to access all mapping store, to avoid having the administrator to logout and login everytime there is a new Mapping Store.
   OAuth2 caches the user with the mapping store.
1. The keyword `latest` at version is not allowed when deleting artefacts
1. Fix SOAP 2.1 structure requests with CodeWhere and Stubs

### Tickets v7.10.8

The following bugs have been corrected:

- SDMXRI-1237: MAWS.NET cannot get SDMX artefacts after login with OAuth
- SDMXRI-1183: Deleting artefact using version of latest
- SDMXRI-1233: SOAP structure query doesn't return correct results on NSIWS.NET

## nsiws.net v7.10.6 (2019-11-19) (MSDB v6.7) (AUTHDB v1.0)

### Details v7.10.6

1. Another attempt to remove the Value element under TimeRange in Content Constraint
1. Replace isEqualVersion UDF with plain SQL
1. Fix packaging issue with missing Estat.Sri.Msdb.Sql.DLL

### Tickets v7.10.6

The following new features added:

- SDMXRI-1127: Replace UDF dbo.isEqualVersion by native SQL command (OECD)

The following bugs have been corrected:

- SDMXRI-899: Support for Content Constraint Reference Period and Time Range (.NET) (COLLAB,OECD,QTM2-2018-SC000941,SDMXTOOLSTF)

## nsiws.net v7.10.5 (2019-11-15) (MSDB v6.7) (AUTHDB v1.0)

### Details v7.10.5

1. Fix duplicate Vary header issue when it is enabled either in CORS and/or Server side caching.
   1. Now NSI WS Vary header added in SDMX REST calls is controlled by configuration. This does not affect Vary header added by CORS or Server side caching.
1. Fix SDMX v2.1 Content Constraint output issue, when there is a TimeRange inside a CubeRegion
1. Multidataset support in SDMX-JSON v1

### Tickets v7.10.5

The following new features added:

- SDMXRI-1218: Improve Vary header generation in NSIWS.NET (ILO,OECD)
- SDMXRI-1213: Invalid child element 'Value' in TimeRange of Actual Content Constraint
- SDMXRI-1091: SDMX-JSON data multi-dataset support (.NET)

The following bugs have been corrected:

- SDMXRI-1171: Re-submitting some existing artefact is reported as "Created"

## nsiws.net v7.10.3 (2019-10-29) (MSDB v6.7) (AUTHDB v1.0)

### Details v7.10.3

1. Open ID enhancements from OECD

### Tickets v7.10.3

The following new features added:

- SDMXRI-1207: Allow anonymous access with OpenIdConnect middleware, when no token in request + other minor fixes

## nsiws.net v7.10.2 (2019-10-25) (MSDB v6.7) (AUTHDB v1.0)

### Details v7.10.2

1. Improve performance when importing HCL
1. A lot of fixes to bugs and regressions related to stubs
   1. Could not categorise stub dataflows
   1. Structure/Service URL not retrieved from msdb
   1. Could not delete content constraint
1. Support parsing SDMX-JSON DataStructures (from ISTAT)

### Tickets v7.10.2

The following new features added:

- SDMXRI-1198: Improve performance when importing HCL (.net) (ISTAT,OECD)
- SDMXRI-1084: SDMX JSON DSD Parser (.NET) (ISTAT,QTM2-2018-SC000941)

The following bugs have been corrected:

- SDMXRI-1147: Cannot categorise Dataflows that are defined as external references
- SDMXRI-1137: Rest stub parameter results in isExternalReference="true" and `structureURL="http://need/to/changeit"`
- SDMXRI-1167: Not possible anymore to delete or update Allowed Content Constraint

## nsiws.net v7.10.1 (2019-10-11) (MSDB v6.7) (AUTHDB v1.0)

### Details v7.10.1

1. Allow to connect to OpenID service without SSL (OECD)
1. Bugfixes related to the VaryHeader not showing in Structure requests
1. Move all static files under wwwroot
1. Show URN parameter in case of a 406 Not accepted error
1. SDMX-JSON structure include external link in case for each artifact that is a stub/external (OECD)

### Tickets v7.10.1

The following new features added:

- SDMXRI-1164: Add configuration to authentication to disable SSL and token issuer (OECD)
- SDMXRI-1145: Add external link to a json structure message when artifact has isExternalReference=true (OECD)

The following bugs have been corrected:

- SDMXRI-1163: NSIWS Vary Header issue (ILO)
- SDMXRI-1138: nsiws static files (.NET) (OECD)
- SDMXRI-1165: Accept header parameter urn doesn't work (OECD)

## nsiws.net v7.10.0 (2019-09-27) (MSDB v6.7) (AUTHDB v1.0)

### Tickets v7.10.0

The following new features added:

- SDMXRI-1131: Create .net core healtcheck endpoint with json output (OECD)
- SDMXRI-720: Do not include URN in structural metadata output (COLLAB,OECD)

## nsiws.net v7.9.0 (2019-09-06) (MSDB v6.7) (AUTHDB v1.0)

### Details v7.9.0

1. Added OpenId [documentation](openid-middleware.md)
1. NSI WS will not output URN by default. To include URN then the `urn` parameter must be added in the `Accept` HTTP header

### Tickets v7.9.0

The following new features added:

- SDMXRI-720: Do not include URN in structural metadata output (COLLAB,OECD)
- SDMXRI-1094: Add Open-Id connect authentication pluggable middleware (OECD)

## nsiws.net v7.8.0 (2019-09-02) (MSDB v6.7) (AUTHDB v1.0)

### Details v7.8.0

1. Update 3rd party dependencies (from ILO)
1. Support for server side cachning from ILO
1. Further improvements in error messages when importing structual metadata

### Tickets v7.8.0

The following new features added:

- SDMXRI-1111: Server side caching from ILO for NSIWS.NET (ILO)
- SDMXRI-1108: SDMXRI.NET update DryIoc and Oracle dependencies (ILO)
- SDMXRI-854: NSI ws - enhanced status and error messages (.net)

## nsiws.net v7.7.0 (2019-08-23) (MSDB v6.7) (AUTHDB v1.0)

### Tickets v7.7.0

The following new features added:

- SDMXRI-899: Support for Content Constraint Reference Period and Time Range (.NET)
- SDMXRI-854: NSI ws - enhanced status and error messages (.net)
- SDMXRI-871: ContentConstraint retriever puts excluded key values as included
- SDMXRI-1013: SDMX REST support detail=allcompletestubs (.NET)
- SDMXRI-899: Support for Content Constraint Reference Period and Time Range (.NET)
- SDMXRI-861: Implement ''include history'' in SDMX REST (.net)
- SDMXRI-1095: Introduce AdoNetAppender to Log4Net to allow the configuration of the NSIWS to log into a database

## nsiws.net v7.6.0 (2019-07-22) (MSDB v6.7) (AUTHDB v1.0)

### Details v7.6.0

1. Uses [MSDB v6.7](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/msdb.sql/browse/HISTORY.md?at=refs%2Fheads%2F6.7)
1. Support for storing and retrieving SDMX stubs including the URL for structure or service
1. Support for updating the parent item of existing items and the parent item of new items
1. Support for Content Constraints that attach to:
   1. Dataset
   1. Metadataset
   1. Data Provider
   1. Simple datasource
   1. Complementing existing maintainable artefact attachment support,  queryable data source support
1. Support for Include History in REST data queries
1. Include a log4net plugin to allow logging on a database. Note no configuration is provided
   1. [Documentation can be found here](https://github.com/microknights/Log4NetAdoNetAppender)
1. Open ID authentication plugin (middleware) from OECD.

### Tickets v7.6.0

The following new features added:

- SDMXRI-1094: Add Open-Id connect authentication pluggable middleware (OECD)
- SDMXRI-861: Implement ''include history'' in SDMX REST (.net) (QTM6-2018.SC000641)
- SDMXRI-1098: SdmxSource produces ContentConstrain in 2.1 xml in some cases (.NET) (future-QTM)
- SDMXRI-1078: SDMX RI support Stubs (.NET) (ISTAT,OECD)
- SDMXRI-860: Content Constraint support attaching to Item and Data Sources (.net)
- SDMXRI-1021: NSI WS support updating the parent codes of existing final codelist(.net) (ISTAT,QTM2-2018-SC000941)
- SDMXRI-1095: Introduce AdoNetAppender to Log4Net to allow the configuration of the NSIWS to log into a database

## nsiws.net v7.5.0 (2019-06-25) (MSDB v6.6) (AUTHDB v1.0)

## Details v7.5.0

1. Fixes related to authentication via OAuth2 and .NET core migration issues
1. Improve error logging and exception handing (From OECD)

## Tickets v7.5.0

The following new features added:

- SDMXRI-1004: MAWEB login/authentication integrate with MAWS.NET (QTM2-2018-SC000941)

The following bugs have been corrected:

- SDMXRI-1066: Unhandled exception caught in JsonExceptionMiddleware is not logged and doesn't catch error from previous middlewares in the pipeline

## nsiws.net v7.4.0 (2019-05-24) (MSDB v6.5) (AUTHDB v1.0)

## Details v7.4.0

1. Uses MSDB v6.6. Code updated to work updated store procedures
1. Support for queyring content constraints by type in REST. In other words support the new SDMX REST resources /allowedconstraint and /actualconstraint.

## Tickets v7.4.0

The following new features added:

- SDMXRI-711: Get feature for (Valid and/or Actual) ContentContraints (.net) (COLLAB,OECD,QTM6-2018.SC000641)

## nsiws.net v7.3.1 (2019-04-30) (MSDB v6.5) (AUTHDB v1.0)

*WARNING* This version targets .NET Core 2.2 and require ASP.NET Hosting module to work with IIS

## Details v7.3.1

1. Documentation updates, configuration files is now `config/app.config`
1. Document CORS configuration

## Tickets v7.3.1

The following new features added:

- SDMXRI-849: Documentation for the CORS module in NSI WS (.net) (QTM6-2018.SC000641)
- SDMXRI-1015: Make NSI.WS work in Linux container (OECD)

## nsiws.net v7.3.0 (2019-04-25) (MSDB v6.5) (AUTHDB v1.0)

*WARNING* This version targets .NET Core 2.2 and require ASP.NET Hosting module to work with IIS

## Details v7.3.0

1. Support Content Constraints that attach to Metadataflows, Metadatastructure definitions and Provision Agreements
1. Support querying for item scheme items
1. Modify SdmxSource structure query classes to support item queries in both REST and SOAP v2.1.
1. SDMX-JSON hardcoded to use UTF-8 without the BOM. (Changes from OECD)

## Tickets v7.3.0

The following new features added:

- SDMXRI-713: Get references for specific items (in ItemSchemes) only .net (COLLAB,OECD,QTM6-2018.SC000641)
- SDMXRI-859: Support for content constraints attaching to other maintainable artefacts (.net) (QTM6-2018.SC000641)
- SDMXRI-1015: Make NSI.WS work in Linux container (OECD)

The following bugs have been corrected:

- SDMXRI-983: Use hardcoded UTF8 without BOM in json writers (OECD)

## nsiws.net v7.1.2 (2019-03-22) (MSDB v6.5) (AUTHDB v1.0)

*WARNING* This version targets .NET Core 2.2 and require ASP.NET Hosting module to work with IIS

## Details v7.1.2

1. Plugable Middleware, see [PLUGINS documentation](PLUGINS.md)
1. XML Authentication/Authorization store support. The authentication and authorization information is stored in a XML file.  The goal is to use it in tests. See [AUTH docuemntation](AUTH.md)
1. OECD SDMX Json format enhancements:
   1. Changes of data and structure message formats in JSON standard applied on JSON v1.0 components
   1. JSON data message. Set codelist code's parent ID in case of hierarchical codelist

## Tickets v7.1.2

The following new features added:

- SDMXRI-906: Change in localised text in SDMX-JSON data and structure messages
- SDMXRI-962: Updating big codelists
- SDMXRI-740: XML backend for new Authentication mechanism (QTM1-2018.SC000641)

Regressions related to the following tickets were fixed:

- SDMXRI-839: Migrate NSI WS and MAWS to .NET Core (QTM1-2018.SC000641)

## nsiws.net v7.1.1 (2019-03-08) (MSDB v6.5) (AUTHDB v1.0)

*WARNING* This version targets .NET Core 2.2 and require ASP.NET Hosting module to work with IIS

## Details v7.1.1

1. CORS bugfixes from OECD and fix regression with Expose headers
1. Policy module fixes
1. Include a XML based authentication/authorization module, disabled by default

## Tickets v7.1.1

The following new features added:

- SDMXRI-740: XML backend for new Authentication mechanism (QTM1-2018.SC000641)
- SDMXRI-935: Update CorsModule to work in .net core

The following bugs have been corrected:

- SDMXRI-957: NSI WS .NET regressions in policy module
- SDMXRI-839: (Regressions) Migrate NSI WS and MAWS to .NET Core (QTM1-2018.SC000641)
- SDMXRI-870: (Regression) CORS Support for Access-Control-Expose-Headers (.net)

## nsiws.net v7.1.0 (2019-02-15)

## Details v7.1.0

1. Support retrieving data and available constraint when frequencies with multipliers are used.
1. SDMX-JSON writer improvements from OECD
   1. Annotations added at all levels to json data & structure messages according to spec [https://github.com/sdmx-twg/sdmx-json/blob/master/data-message/docs/1-sdmx-json-field-guide.md](https://github.com/sdmx-twg/sdmx-json/blob/master/data-message/docs/1-sdmx-json-field-guide.md)
   1. Dataflow & DSD annotations are merged in json data message, if the annotation with the same type present then lower level takes priority.

## Tickets v7.1.0

The following new features added:

- SDMXRI-842: Support A10 frequency in SDMX RI (QTM1-2018.SC000641)
- SDMXRI-907: Add order property and annotations to component value object in json writer.

## nsiws.net v7.0.0 (2019-02-07)  (MSDB v6.5) (AUTHDB v1.0)

*WARNING* This version targets .NET Core 2.2 and require ASP.NET Hosting module to work with IIS

## Details v7.0.0

1. Fix Provision agreement output
1. SDMX-CSV improvements from OECD, labels and timeFormat

### Breaking changes with v7.0.0

1. Migrate to .NET Core 2.2
1. ConnectionStrings are expected to be in `c:\ProgramData\Eurostat\nsiws.config`
1. IIS Configuration is still in `web.config`
1. other configuration are expected to be in `NSIWebServiceCore.config`
1. Default log file directory has changed to `c:\ProgramData\Eurostat\logs\`

## Tickets v7.0.0

The following new features added:

- SDMXRI-839: Migrate NSI WS and MAWS to .NET Core (QTM1-2018.SC000641)
- SDMXRI-903: Implement Accept header additional parameters (labels & timeFormat) to control CSV output (QTM6-2018.SC000641)
- SDMXRI-902: Apply timeFormat in csv writer to normalize date outputs (QTM6-2018.SC000641)
- SDMXRI-873: SdmxSource support HTTP 406 status code

The following bugs have been corrected:

- SDMXRI-872: Cannot query data with time period criteria without FREQ and TIME_PERIOD DB column an int on sqlserver
- SDMXRI-874: NSI WS .NET produces incorrect Provision Agreement structure

## nsiws.net v6.16.0 (2019-01-15) (MSDB v6.5) (AUTHDB v1.0)

This version targets .NET Framework 4.6.2

## Details v6.16.0

1. Switched to package references. As a result Visual Studio versions earlier than 2017 are no longer supported.
1. Renamed the nuget packages of Estat SdmxSource Extension, DataRetriever, StructureRetriever and SdmxSource
1. This is the first version that targets .NET Framework 4.6.2
1. JSON fixes from the latest SdmxSource
1. It is possible to configure NSI WS to cache results. See [Caching documentation](CACHING.md)
1. Disabled client side caching by default
1. Support for CORS `Access-Control-Expose-Headers` in the CORS module

## Tickets v6.16.0

The following new features added:

- SDMXRI-868: Rename nuget packages
- SDMXRI-838: SDMX RI libraries to target .NET Standard 2.0 (QTM1-2018.SC000641)
- SDMXRI-863: Output caching for rest GET data and structure services (OECD)
- SDMXRI-870: CORS Support for Access-Control-Expose-Headers

## nsiws.net v6.15.1 (2018-12-18)  (MSDB v6.5) (AUTHDB v1.0)

This version targets .NET Framework 4.5

## Details v6.15.1

1. Server-side caching implemented by OECD for SDMX REST requests. See [Documentation](CACHING.md)
1. Disable by default browser side caching.
1. Update dependencies

## Tickets v6.15.1

The following new features added:

- SDMXRI-863: Output caching for rest GET data and structure services (OECD)

## nsiws.net v6.15.0 (2018-10-31) (MSDB v6.5) (AUTHDB v1.0)

Minimum Mapping Assistant DB (MADB/MSDB) version v6.5 (or partially v5.3)
If the provided authentication module is used, then minimum AUTH DB version v1.0

## Details v6.15.0

1. Allow adding new items to final item scheme. This is disabled by default. It can be enabled in configuration.
1. Fixes related to data availability, specifically an error occurred when a key was present.
1. Fix query time criteria handling on .NET when no time transcoding is used
1. Synchronize .NET SDMX v2.0 Dataset element ouput to Java
1. Fix Frequency code for reporting period
1. Support JSON Submit Structure
1. JSON fixes by OECD:
   1. Fix for the missing series level attributes when the component id is not identical to the concept id in json data messages
   1. Parent codes no longer presented at code lists with hierarchy in structure section of `data+json;version=1.0.0`

## Tickets v6.15.0

The following new features added:

- SDMXRI-824: Adding items into finalized item schemes
- SDMXRI-747: CORS support in NSI.WS (QTM1-2018.SC000641)
- SDMXRI-825: MAWS / MA_WEB integration (QTM-ma-frontend,QTM5-2018.SC000641)
- SDMXRI-721: Adjust REST data availability according the SDMX TWG proposal (COLLAB,OECD,QTM1-2018.SC000641)
- SDMXRI-835: Include the dataflowId and the dataflowAgencyId in Compact Data Dataset
- SDMXRI-712: SDMX-JSON structure submit via REST in NSI WS (COLLAB,OECD,QTM1-2018.SC000641)

The following bugs have been corrected:

- SDMXRI-739: No data returned when querying 2010 data from 2010-01-01 to 2010-12-31
- SDMXRI-760: Attribute management fix in csv,  json, xml (abstract, generic readers) (OECD)
- SDMXRI-667: SDMX v2.1 Time Range in .NET (QTM6-2017.SC000281)

## nsiws.net v6.14.0 (2018-09-26) (MSDB v6.5) (AUTHDB v1.0)

Minimum Mapping Assistant DB (MADB/MSDB) version v6.5 (or partially v5.3)
If the provided authentication module is used, then minimum AUTH DB version v1.0

## Details v6.14.0

1. Updated to the latest SDMX RI dependencies
   1. DR v6.5.0
   1. SR v4.4.0
   1. MAAPI v1.13.0
   1. `SdmxSource` v1.12.1
1. Changes in the SDMX CSV writer error messages (OECD)
1. Align SDMX v2.1 Generic Data output to the Java version
1. Fix available constraint query handling
1. Fix text format issue in SDMX JSON Structure parser (OECD)
1. Fix time period output in SDMX-JSON data (OECD)
1. Fix SDMX-JSON DSD dimension position, it starts now from 0. (OECD)
1. Fix SDMX-JSON DSD concept role output, it now has the full URN instead of the id. (OECD)
1. More work towards data availability. Available mode is not yet implemented
1. Log the execution of SQL statements in a CSV file. Disabled by default. Can be enabled in log4net configuration.
1. Fix issue with Content Constraints importing

## Tickets v6.14.0

The following new features added:

- SDMXRI-589: Log duration of execution of SQL Queries  (COLLAB,QTM3-2017.SC000281,QTM6-2017.SC000281,QTM7-2017.SC000281,QTM8-2017.SC000281)
- SDMXRI-747: CORS support in NSI.WS (QTM1-2018.SC000641)
- SDMXRI-686: Generic 21 Format and `ObsDimension` TAG (QTM1-2018.SC000641)
- SDMXRI-642: SDMX-csv writer .NET (OECD)
- SDMXRI-756: Valid/actual content constraints not returned when requested as references to dataflow/dsd (QTM1-2018.SC000641)

The following bugs have been corrected:

- SDMXRI-783: WS doesn't include `SdmxSource` validation errors the response
- SDMXRI-778: NSI WS import Constraints with ReferencePeriod results to error
- SDMXRI-781: SDMX Structure JSON parser (V10) text format issue (OECD)
- SDMXRI-789: "id" and "name" missing for the TIME_PERIOD in JSON (3rd-level-support) (OECD)
- SDMXRI-669: Support for partial/special codelists in REST in .NET (QTM6-2017.SC000281)
- SDMXRI-785: SDMX JSON v1.0 structural metadata writer possibly issues (OECD)

## nsiws.net v6.13.1 (2018-08-07) (MSDB v6.5) (AUTHDB v1.0)

Minimum Mapping Assistant DB (MADB/MSDB) version v6.5 (or partially v5.3)
If the provided authentication module is used, then minimum AUTH DB version v1.0

## Details v6.13.1

1. Bugfixes related to transcoding and the new formatVersion parameter
1. Handle gracefully missing configuration related to mapping
1. Fix Data Retriever version

## Tickets v6.13.1

The following bugs have been corrected:

- SDMXRI-772: NSI WS .NET Cannot retrieve data when Time Transcoding is used
- SDMXRI-673: `formatVersion` is optional

## nsiws.net v6.13.0 (2018-08-03) (MSDB v6.5) (AUTHDB v1.0)

Minimum Mapping Assistant DB (MADB/MSDB) version v6.5 (or partially v5.3)
If the provided authentication module is used, then minimum AUTH DB version v1.0

## Details v6.13.0

1. NSI WS uses now MSDB v6.5. An upgrade is needed. See [Tool.md]
   1. The [Release notes of MSBD v6.5](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/msdb.sql/browse/HISTORY.md?at=6.5)
1. Several bugfixes/changes related Paging using HTTP Range
   1. *Important* it uses a 0 based index now. This means for example get the first 10 items/observations the Range header requires a value like `Range: values=0-9`
   1. Item schemes were not ordered which could lead to different results
   1. An additional item was returned
   1. Follow the `RFC 7233` closer
1. Support for several SDMX REST v1.2.0 features:
   1. If-Modified HTTP Header
   1. Vary HTTP Header
   1. `SDMX` error `Semantic error` now maps to HTTP status code `403 Forbidden`
   1. Format and version can be specified in the URL. Additional formats can be added in the `Web.Config` under `<FormatMapping>/<Mappings>`. e.g.

```xml
   <FormatMapping>
    <Mappings>
      <Mapping Format="genericdata" AcceptHeader="application/vnd.sdmx.genericdata+xml"/>
      <Mapping Format="jsondata" AcceptHeader="application/vnd.sdmx.data+json"/>
      <Mapping Format="structure" AcceptHeader="application/vnd.sdmx.structure+xml"/>
      <Mapping Format="structurespecificdata" AcceptHeader="application/vnd.sdmx.structurespecificdata+xml"/>
      <Mapping Format="csv" AcceptHeader="application/vnd.sdmx.data+csv"/>
    </Mappings>
  </FormatMapping>
```

1. Bugfixes from OECD in JSON, CSV and SDMX-ML formats.
1. Use default mapping store `MappingStoreServer` when Submitting structural metadata and no authentication or old authentication is used.
1. New features from OECD
   1. Support for retrieving content constraints as parents of DSDs and dataflows.
   1. Support for new SDMX-REST feature, [reference partial dependencies](https://github.com/sdmx-twg/sdmx-rest/blob/develop/v2_1/ws/rest/docs/4_3_structural_queries.md#parameters-used-to-further-describe-the-desired-results)

## Tickets v6.13.0

The following new features added:

- SDMXRI-673: SDMX RI  support for SDMX REST v1.2.0  (QTM6-2017.SC000281,QTM8-2017.SC000281)
- SDMXRI-675: POST & Update support in .NET (QTM6-2017.SC000281)
- SDMXRI-605: Extend SDMX RI permission roles (COLLAB,QTM6-2017.SC000281,QTM8-2017.SC000281)
- SDMXRI-596: Support for applying content constrains to codelists in REST (COLLAB,DevelopedByOECD,QTM6-2017.SC000281,QTM8-2017.SC000281)
- SDMXRI-760: Attribute management fix in csv,  json, xml (abstract, generic readers)
- SDMXRI-667: SDMX v2.1 Time Range in .NET (QTM6-2017.SC000281)
- SDMXRI-755: Partial codelists  review of .NET implementation (QTM1-2018.SC000641)
- SDMXRI-637: Split Mapping Store DDL scripts (QTM6-2017.SC000281)
- SDMXRI-605: Extend SDMX RI permission roles (COLLAB,QTM6-2017.SC000281,QTM8-2017.SC000281)

The following bugs have been corrected:

- SDMXRI-766: Range header returning 416 when it overlaps
- SDMXRI-601: SDMX REST paging (QTM6-2017.SC000281,QTM8-2017.SC000281)
- SDMXRI-751: SRI .NET 6.12.0
- SDMXRI-749: Problem with turkich language

## nsiws.net v6.12.1 (2018-07-11) (MSDB v6.4) (AUTHDB v1.0)

Minimum Mapping Assistant DB (MADB/MSDB) version v6.4 (or partially v5.3)
If the provided authentication module is used, then minimum AUTH DB version v1.0

## Details v6.12.1

1. Bugfixes for logging and the blocking issue with data queries

## Tickets v6.12.1

The following new features added:

- SDMXRI-747: CORS support in NSI.WS (From OECD)

The following bugfixes added:

- SDMXRI-750: Test Client and the latest NSI WS
- SDMXRI-754: NSIWS .NET not logging (randomly)

## nsiws.net v6.12.0 (2018-07-02) (MSDB v6.4) (AUTHDB v1.0)

Minimum Mapping Assistant DB (MADB/MSDB) version v6.4 (or partially v5.3)
If the provided authentication module is used, then minimum AUTH DB version v1.0

## Details v6.12.0

1. Bugfixes related to provision agreement retrieval, SDMX RI authentication and SDMX REST paging.
1. Data availability related changes. Only exact mode implemented. See [latest changes](https://github.com/sdmx-twg/sdmx-rest/blob/develop/v2_1/ws/rest/docs/4_6_1_other_queries.md).
   1. Available mode and data provider references are not supported.
1. SDMX JSON & CSV improvements from OECD
1. The `Content-Disposition` is included in the response when `;file` is added in the `Accept` HTTP Header.
1. When writing SDMX v2.0 datasets with DSD specific schema, use the same namespace as in Converter and Registry.
1. Allow plugging 3rd party DDB connection builders. The plugin to use is set in the `Web.Config` configuration option:

```xml
<add key="dbConnectionBuilderDll" value="Estat.Sri.Mapping.MappingStore"/>
```

## Tickets v6.12.0

The following new features added:

- SDMXRI-721: Adjust REST data availability according the SDMX TWG proposal
- SDMXRI-708: Change behavior of SDMX RR Append/Replace and REST POST/PUT
- SDMXRI-727: New SDMX-JSON data message format
- SDMXRI-702: SDMX JSON Structure Reader & Writer
- SDMXRI-707: Add possibility to return sdmx structure/data messages as file attachment in REST service
- SDMXRI-715: Allow using non-final codelists for attributes
- SDMXRI-693: SDMX RI vs SDMX Converter SDMX v2.0 data namespaces
- SDMXRI-590: Allow to plug DDB Connection builders

The following bugfixes added:

- SDMXRI-568: SDMX RI Authentication, outside Mapping Store
- SDMXRI-601: SDMX REST paging

## nsiws.net v6.11.0 (2018-05-01) (2018-03-15) (MSDB v6.4) (AUTHDB v1.0)

Minimum Mapping Assistant DB (MADB/MSDB) version v6.4 (or partially v5.3)
If the provided authentication module is used, then minimum AUTH DB version v1.0

## Details v6.11.0

1. Revert to packages.config in the source code
1. *Draft* support for data availability using the*temporary* resoruce `dynamiconstraints`. The resource will change as soon as it is decided. Also missing are the query parameters for the metric and mode.

  Example request:

```http
  GET http://localhost/ws/rest/dynamiconstraints/ESTAT,SSTSCONS_PROD_M,2.0/T..2../ALL/FREQ?reference=codelist HTTP/1.1
```

1. Support for SDMX v2.1 Time Range (not transcoding yet) and timezone offset.
1. SDMX REST paging for item schemes and data using HTTP Range header. Example [Test SOAP UI project](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/sdmxritests/browse/REST-Paging-soapui-project.xml)
1. It is possible to retrieve provision agreements
1. Fixed issue in ContentConstraint retrieval which ignored the field value of Actual
1. Plugin for SDMX-CSV that uses OECD SDMX CSV data writer.

## Tickets v6.11.0

The following new features added:

- SDMXRI-669: Support for partial/special codelists in REST in .NET (DRAFT)
- SDMXRI-708: Change behavior of SDMX RR Append/Replace and REST POST/PUT
- SDMXRI-596: Retrieve Provision Agreement and fix Actual flag in Content Constraints retrieval
- SDMXRI-678: SDMXRI-642 NSI WS to use the SDMX-CSV
- SDMXRI-601: SDMX REST paging
- SDMXRI-667: SDMX v2.1 Time Range in .NET (No SDMX RI transcoding support yet)

The following bugs have been corrected:

- SDMXRI-716: SDMX REST structure query parameter detail=referencestubs not supported

## NSI Web Service v6.10.0 (2018-03-15) (MSDB v6.4) (AUTHDB v1.0)

Minimum Mapping Assistant DB (MADB/MSDB) version v6.4 (or partially v5.3)
If the provided authentication module is used, then minimum AUTH DB version v1.0

## Details v6.10.0

### New Authentication/Authorization mechanism

Starting from this version the NSI WS uses an new Authentication and Access Rules mechanism.
The schema can be found at [authdb](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/authdb.sql/browse?at=refs%2Fheads%2F1.0)

In addition the access rules/roles are now checked outside the NSI WS in a HTTP module, [Estat.Sri.Ws.PolicyModule](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/nsiws.net/browse/src/Estat.Sri.Ws.PolicyModule?at=refs%2Fheads%2F6.10.0)

For more information see [Authentication/Authorization documentation](AUTH.md)

### Reporting period

*Note 1* Reporting period is supported only in SDMX v2.1 standard.
*Note 2* special attribute `REPORTING_YEAR_START_DAY` is supported when attached to DataSet and Series level.

Reporting period and the special attribute `REPORTING_YEAR_START_DAY` are better supported.
It is possible now to output Reporting Period values such as `2001-M01` even when using Time Transcoding.

### Known issues v6.10.0

1. PC Axis as a DDB is not supported
1. DDB Plugins are needed instead of just configuration option

## Tickets v6.10.0

The following new features added:

- SDMXRI-568: SDMX RI Authentication, outside Mapping Store
- SDMXRI-537: Support transcoding for non-calendar years

The following bugs have been corrected:

- SDMXRI-692: When observation value is null, generate "null" in JSON

## NSI Web Service_v6.9.0 (2018-01-31) (MSDB v6.4)

Minimum Mapping Assistant DB (MADB/MSDB) version v6.4 (or partially v5.3)

### Detailed description v6.9.0

This version depends on MSDB v6.4 which contains bug fixes and performance improvements.

For more information on what has changed on MSDB side please visit the [MSDB HISTORY](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/msdb.sql/browse/HISTORY.md?at=refs%2Fheads%2F6.4)

This release includes:

1. This release re-enables disabled features from v6.8.0
1. New configuration option and query parameter to include local codes in the output as annotations, see [Extended API](EXTENDED_API.md) and [Configuration](CONFIGURATION.md)
1. New data output format that contains a validation report see [Extended API](EXTENDED_API.md)
1. Merged improvement from OECD related to REST and having http and https endpoints.
1. Decrease size of structural metadata in SDMX-ML formats by having better namespace management.

*Note* Building Data Retriever requires Visual Studio 2017 Community or better or [Build Tools for Visual Studio 2017](https://www.visualstudio.com/downloads/#build-tools-for-visual-studio-2017)

### Known issues v6.9.0

1. PC Axis as a DDB is not supported
1. New Authentication system will be replaced
1. DDB Plugins are needed instead of just configuration option

### Tickets v6.9.0

The following new features added:

- SDMXRI-597 - MA WS/API Mapped data validation & errors
- SDMXRI-599 - MA WS/API get mapped data with local codes
- SDMXRI-536 - For WCF/Rest support of both http & https binding
- SDMXRI-593: Inefficient namespace management in generated SDMX structure messages

## NSI Web Service_v6.8.0 (2018-01-31) (MSDB v6.4)

Minimum Mapping Assistant DB (MADB/MSDB) version v6.4 (or partially v5.3)
*NOTE* This release has experimental features from 6.7.0 disabled related to Submit Structural metadata.
For more information please see the full [6.8.0 release notes](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/nsiws.net/browse/CHANGELOG.md?at=refs%2Fheads%2F6.8.0)

### Detailed description v6.8.0

This version depends on MSDB v6.4 which contains bugfixes and performance improvements.

For more information on what has changed on MSDB side please visit the [MSDB HISTORY](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/msdb.sql/browse/HISTORY.md?at=refs%2Fheads%2F6.4)

This release includes:

1. .NET synchronized with Java version
1. The REST url for submitting structural metadata has changed to `/structure` from `/`
1. Use the same WSD target namespace for Submit Structure as in Java
1. Legacy authentication/authorization users, `TestAuthConfig`, are now able again to submit structural metadata.
1. REST requests no longer require a slash `/` at the end of the URL. In the past not ending the REST URL with a `/` would make `WCF` respond with a redirection request.
1. Corrected Cross Sectional dataset writing when there is no measure dimension and the primary measure concept reference id is not OBS_VALUE
1. A command line tool, DataMonitorJob.exe modified to work with non standard Global Registry REST API for submitting data registrations. See [Tool](Tool.md).

### Known issues v6.8.0

1. PC Axis as a DDB is not supported
1. New Authentication system will be replaced
1. DDB Plugins are needed instead of just configuration option

### Tickets v6.8.0

The following new features added:

- SDMXRI-648 - Sync .NET WS to JAVA REST POST URL
- SDMXRI-649 - .NET sync to Java. SOAP Registry Endpoint use Java namespace WSDL
- SDMXRI-650 - .NET Sync to Java TestAuthConfig users should be allowed to submit data
- SDMXRI-595 - Strange behaviour of redirection when not using trailing slash at end of Rest URLs

## NSI Web Service_v6.7.0 (2017-12-08) (MSDB v6.4)

Minimum Mapping Assistant DB (MADB/MSDB) version v6.4 (or partially v5.3)

### Detailed description v6.7.0

This version depends on MSDB v6.4 which contains bugfixes and performance improvements.

For more information on what has changed on MSDB side please visit the [MSDB HISTORY](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/msdb.sql/browse/HISTORY.md?at=refs%2Fheads%2F6.4)

This release includes:

1. A new command line tool, `DataMonitorJob.exe`. When run it will check if there are data updates for each provision agreement and send data registrations to the configured SDMX Registry. See [Tool](Tool.md).
1. On the fly DSD support at Mapping Assistant API/WS. It is now possible to create a DSD/Dataflow and a Mapping Set from a DataSet.
1. Updating the non-final attributes names, descriptions and annotations of *final* Artefacts is now possible. See [Submitting Structural Metadata](SUBMIT_STRUCTURE.md)
1. *Important* New Plugin API for submitting structural metadata that tries to follow the Java API, see [Plugins](PLUGINS.md) for more information.
1. A new command line option `-f` to the Estat.Sri.Mapping.Tool.exe from OECD to force initialization/upgrade.
1. Better error reporting in case of appending, replacing or deleting structural metadata. See [Submitting Structural Metadata](SUBMIT_STRUCTURE.md)
1. Support for configuring a default value for Observation value, when there is no value in the database. Possible options are `NaN` or nothing.

### Known issues v6.7.0

1. PC Axis as a DDB is not supported
1. New Authentication system will be replaced
1. DDB Plugins are needed instead of just configuration option

### Tickets v6.7.0

The following new features added:

- SDMXRI-550: Allow updating of non-final properties of a final artefact
- SDMXRI-456: "Internal Server Error" message instead of the specific message returned previously
- SDMXRI-626: When OBS_VALUE is null in the DDB the output should contains NaN or nothing.

## NSI Web Service_v6.6.1 (2017-11-08)

Minimum Mapping Assistant DB (MADB/MSDB) version v6.3 (or partially v5.3)

### Detailed description v6.6.1

This release includes bugfixes. In one case is documentation for the new configuration option to work with Mapping Store v5.3 and in the other the new RDF plugin was blocking all data SOAP requests.

### Known issues v6.6.1

1. PC Axis as a DDB is not supported
1. New Authentication system will be replaced
1. DDB Plugins are needed instead of just configuration option
1. Submitting structural metadata error reporting is minimal

### Tickets v6.6.1

The following bugs have been corrected:

- SDMXRI-583: Add missing documentation
- SDMXRI-621: .NET WS V6.6.0 error with soap endpoint with all SDMX format except Cross Sectional

## NSI Web Service_v6.6.0 (2017-10-31)

Minimum Mapping Assistant DB (MADB/MSDB) version v6.3 (or partially v5.3)

### Detailed description v6.6.0

As a result of on going work on On The Fly DSD a new version of MSDB has been released and MAAPI depends on it.
*Note* On The Fly DSD support is not included in this release.

For more information on what has changed on MSDB side please visit the [MSDB HISTORY](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/msdb.sql/browse/HISTORY.md?at=refs%2Fheads%2F6.3)

1. A micro-data enhancement, the SDMX writers have been modified so it doesn't write a `NaN` when no observation value is given
1. Add an `Other` data type, so new data format plugins can use it
1. New RDF data and structure format plugin from ISTAT
1. Support for importing non-final structural metadata that depends on non-final structural  metadata. Please note support is limited to Dataflow to DataStructure, DataStructure to Codelist and Concept Scheme.
1. It is now possible to retrieve Mapping Sets, Mappings and Transcodings from MSDB v5.3. It needs the following configuration setting in order to be enabled:

```xml
<appSettings>
  <add key="MappingStoreVersion53" value="5.3"/>
```

### Known issues v6.6.0

1. PC Axis as a DDB is not supported
1. New Authentication system will be replaced
1. DDB Plugins are needed instead of just configuration option
1. Submitting structural metadata error reporting is minimal

### Tickets v6.6.0

The following new features added:

- SDMXRI-564: EGR Obs value NaN vs no value output
- SDMXRI-563: ISTAT RDF Writer
- SDMXRI-540: Allow to import a dsd in which there is a reference to a not final codelist
- SDMXRI-583: NSI WS should also support latest public release Mapping Store v5.3

## NSI Web Service_v6.5.1 (2017-10-18)

Minimum Mapping Assistant DB (MADB/MSDB) version v6.2

### Detailed description v6.5.1

This is a bug fix release. It fixes issues related to Mapping Assistant DB id and IIS.

## NSI Web Service_v6.5.0 (2017-10-06)

Minimum Mapping Assistant DB (MADB/MSDB) version v6.2

### Detailed description v6.5.0

*WARNING* .NET 4.5 is now required by NSI WS
*WARNING* Authentication must be enabled to submit structural metadata, please refer to the installation guide, *Section 4.12 Registry Services – SubmitStructure*
*Note* Authentication/Authorization the Mapping Store database is used

Most changes are internal and affect mostly developers.

The following notes are for developers only:

- The plugin system will load *all* public implementations from a plugin dll. In case this creates issues, please change the visibility of the class. In practice the [DryIoC RegisterMany](https://bitbucket.org/dadhi/dryioc/wiki/RegisterResolve#markdown-header-registermany) is used with each assembly.
- SDMX RI DataRetriever and Structure Retriever use the MAAPI (`Estat.Sri.Mapping.Api`) to retrieve the mapping sec configuration from the Mapping Assistant DB
- The DLL names and NuGet packages for mapping store retrieval/store have been modified to reflect better their functionality
  - `MappingStoreRetrieval.NuGet` changed to `Estat.Sri.Sdmx.MappingStore.Retrieve`
  - `Estat.Sri.MappingStore.Store` changed to `Estat.Sri.Sdmx.MappingStore.Store`
  - In addition versions of the above packages have synchronized with MAAPI so the latest versions are 1.5.0
  - Namespaces remain the same but some classes/interface have moved to `Estat.SdmxSource.Extension.NuGet` (v1.6.1 or higher), `Estat.Sri.Mapping.Api` (v1.5.0 or higher) or `Estat.Sri.Mapping.Store` (v1.5.0 or higher)
- Changes inside the SRI Retriever plugin allow now:
  - To switch between different Mapping Assistant DB, but this is enabled only when used within MA WS
  - To inject a 3rd party method for generating a .NET `System.Data.DbConnection` from a MA API `DbbConnectionEntity` which contains the information from the table `DB_CONNECTION`.
- New plugins which are used by MA API.
  - `Estat.Sri.Mapping.MappingStore` it uses the Mapping Assistant DB.
  - `Estat.Sri.Plugin.MySql` a plugin for accessing databases on MariaDB/MySQL
  - `Estat.Sri.Plugin.Oracle` a plugin for accessing databases on Oracle
  - `Estat.Sri.Plugin.SqlServer` a plugin for accessing databases on SqlServer
  - `Estat.Sri.Plugin.Odbc` a plugin for accessing databases via ODBC (limited support/unsupported)

### Known issues v6.5.0

1. PC Axis as a DDB is not supported
1. New Authentication system will be replaced
1. DDB Plugins are needed instead of just configuration option
1. Submitting structural metadata error reporting is minimal

### Tickets v6.5.0

The following new features added:

- SDMXRI-536: OECD follow up meeting on some NSI WS related questions
- SDMXRI-552: Migrate DR and SR to use MA API
- SDMXRI-558: support multiple mapping stores (NSI & MA WS)

## NSI Web Service_v6.4.1 (2017-09-01)

The following new features have been added:

- Updated dependencies

## NSI Web Service_v6.4.0 (2017-08-05)

### Detailed description v6.4.0

*WARNING* .NET 4.5 is now required by NSI WS

Support for a default value for transcoding has been added. This is the value that will be used when there is no transcoding for a local value or when the value is null or empty.
Support for transcoding uncoded components.
For more information on what has changed on MSDB side please visit the [MSDB HISTORY](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/msdb.sql/browse/HISTORY.md?at=refs%2Fheads%2F6.1)

### Tickets v6.4.0

The following new features added:

- SDMXRI-468: Support of transcoding/mapping of emtpy values
- SDMXRI-469: Transcoding for uncoded DSD components
- SDMXRI-114: Import of Categorisation that uses category from a non-final Category Scheme is now supported because it was allowed in Mapping Assistant and it blocked the import (with SDMX) feature

## NSI Web Service_v6.3.0 (2017-07-31)

### Detailed description 6.3.0

It is now possible to alter the maximum size of the received message via configuration

### Tickets 6.3.0

The following new features added:

- SDMXRI-507: NsiWs: Maximum length of the returned message

## NSI Web Service_v6.2.1 (2017-07-03)

The following bugs have been corrected:

- SDMXRI-530: Single quote in local code used in transcoding generates an error
- SDMXRI-466: MSD/MDF persist/retrieve

## NSI Web Service_v6.2.0 (2017-06-17)

The following new features added:

- SDMXRI-499: MA Web Service (Java/.NET) second release

## NSI Web Service_v6.1.0 (2017-05-22)

The following new features added:

- SDMXRI-444: Improve default header settings in NSI WS

## NSI Web Service_v6.0.1 (2017-04-26)

The following bugs have been corrected:

- SDMXRI-456: "Internal Server Error" message instead of the specific message returned previously

## NSI Web Service_v6.0.0 (2017-04-25)

The following new features added:

- SDMXRI-455: Submit via REST
- SDMXRI-466: MSD/MDF persist/retrieve
- SDMXRI-82: Maria BD / Linux / misc. enhancement
- SDMXRI-471: Implementation of Data Registration functionality
- SDMXRI-487: Modify ISTAT retriever code to be compatible future SDMX RI proof
- SDMXRI-465: ISTAT Review & Merge retrievers from ISTAT

The following bugs have been corrected:

- SDMXRI-258: GenericData flat dataset generation issue
